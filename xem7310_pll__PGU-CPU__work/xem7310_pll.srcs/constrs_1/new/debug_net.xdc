
##== pulse loopback ==##
#set_property MARK_DEBUG true [get_nets r_cnt__pulse_loopback*]
#set_property MARK_DEBUG true [get_nets w_enable__pulse_loopback*]
#set_property MARK_DEBUG true [get_nets w_pulse_loopback_in]
#set_property MARK_DEBUG true [get_nets w_pulse_out]
#set_property MARK_DEBUG true [get_nets w_reset__pulse_loopback_cnt*]
#set_property MARK_DEBUG true [get_nets w_trig__pulse_shot]

##== adc ==##
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/state[*]}]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_clk_adc]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_cnv_adc]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_clk_reset]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_io_reset]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_pin_dlln_frc_low]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_pttn_cnt_up_en]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_delay_locked]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_io_reset]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_valid_serdes]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_cnv_adc_en]
#
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/w_adc_done_init]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_init_trig]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_init_busy]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/r_init_done]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init__adc1]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init__adc2]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init__adc3]
#set_property MARK_DEBUG true [get_nets control_hsadc_quad_inst/check_cnt_cnv__init__adc0]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_wr_fifo[0]__0[*]}]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_wr_fifo[1]__0[*]}]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_wr_fifo[2]__0[*]}]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_wr_fifo[3]__0[*]}]
#set_property MARK_DEBUG true [get_nets {control_hsadc_quad_inst/cnt_cnv[*]}]


##== MCS ==##
##set_property MARK_DEBUG true [get_nets mcs_inst/Reset]
##set_property MARK_DEBUG true [get_nets mcs_inst/Clk]
#set_property MARK_DEBUG true [get_nets {mcs_inst/IO_address[*]}]
#set_property MARK_DEBUG true [get_nets mcs_inst/IO_addr_strobe]
#set_property MARK_DEBUG true [get_nets {mcs_inst/IO_write_data[*]}]
#set_property MARK_DEBUG true [get_nets mcs_inst/IO_write_strobe]
#set_property MARK_DEBUG true [get_nets {mcs_inst/IO_byte_enable[*]}]
#set_property MARK_DEBUG true [get_nets {mcs_inst/IO_read_data[*]}]
#set_property MARK_DEBUG true [get_nets mcs_inst/IO_read_strobe]
#set_property MARK_DEBUG true [get_nets mcs_inst/IO_ready]
##set_property MARK_DEBUG true [get_nets mcs_inst/UART_rxd]
##set_property MARK_DEBUG true [get_nets mcs_inst/UART_txd]
##set_property MARK_DEBUG true [get_nets mcs_inst/UART_Interrupt]
##set_property MARK_DEBUG true [get_nets mcs_inst/INTC_IRQ]
##set_property MARK_DEBUG true [get_nets {mcs_inst/INTC_Interrupt[*]}]
##set_property MARK_DEBUG true [get_nets {mcs_inst/GPIO1_tri_i[*]}]
##set_property MARK_DEBUG true [get_nets {mcs_inst/GPIO1_tri_o[*]}]
#set_property MARK_DEBUG true [get_nets mcs_inst/FIT1_Toggle]
#set_property MARK_DEBUG true [get_nets mcs_inst/PIT1_Toggle]
##set_property MARK_DEBUG true [get_nets mcs_inst/PIT1_Interrupt]

##== LAN ==##
##set_property MARK_DEBUG true [get_nets w_trig_LAN_reset]
##set_property MARK_DEBUG true [get_nets w_trig_SPI_frame]
##set_property MARK_DEBUG true [get_nets w_LAN_INTn]
##
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/i_trig_LAN_reset]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_done_LAN_reset]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/i_trig_SPI_frame]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_done_SPI_frame]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_LAN_RSTn]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_LAN_SCSn]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/i_LAN_MISO]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_LAN_MOSI]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_LAN_SCLK]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/state[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/r_sh_buf_data[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/r_sh_buf_data_read[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/r_sh_buf_adct[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/i_frame_num_byte_data[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/i_frame_data_wr[*]}]
#set_property MARK_DEBUG true [get_nets {master_spi_wz850_inst/o_frame_data_rd[*]}]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/i_frame_ctrl_rdwr_sel]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_frame_done_wr]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/o_frame_done_rd]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_frame_adct]
#set_property MARK_DEBUG true [get_nets master_spi_wz850_inst/r_frame_data]


##== LAN-FIFO ==##
#set_property MARK_DEBUG true [get_nets {LAN_fifo_rd_inst/din[*]}]
#set_property MARK_DEBUG true [get_nets {LAN_fifo_rd_inst/dout[*]}]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/rd_en]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/valid]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/wr_en]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/wr_ack]
#set_property MARK_DEBUG true [get_nets LAN_fifo_rd_inst/empty]
##
#set_property MARK_DEBUG true [get_nets {LAN_fifo_wr_inst/din[*]}]
#set_property MARK_DEBUG true [get_nets {LAN_fifo_wr_inst/dout[*]}]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/rd_en]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/valid]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/wr_en]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/wr_ack]
#set_property MARK_DEBUG true [get_nets LAN_fifo_wr_inst/empty]


##== DWAVE ==##
#
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[0].dwave_core_inst/en}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[1].dwave_core_inst/en}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[2].dwave_core_inst/en}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[3].dwave_core_inst/en}]
#
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[0].dwave_core_inst/w_pulse_en}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[1].dwave_core_inst/w_pulse_en}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[2].dwave_core_inst/w_pulse_en}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[3].dwave_core_inst/w_pulse_en}]
#
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[0].dwave_core_inst/r_pulse_out_f}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[1].dwave_core_inst/r_pulse_out_f}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[2].dwave_core_inst/r_pulse_out_f}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[3].dwave_core_inst/r_pulse_out_f}]
#
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[0].dwave_core_inst/r_pulse_out_f_ref}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[1].dwave_core_inst/r_pulse_out_f_ref}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[2].dwave_core_inst/r_pulse_out_f_ref}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[3].dwave_core_inst/r_pulse_out_f_ref}]
#
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[0].dwave_core_inst/r_subpulse_idx[*]}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[1].dwave_core_inst/r_subpulse_idx[*]}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[2].dwave_core_inst/r_subpulse_idx[*]}]
#set_property MARK_DEBUG true [get_nets {dwave_control_inst/dwave_core[3].dwave_core_inst/r_subpulse_idx[*]}]


##== TEMP SENSOR ==##
#
#set_property MARK_DEBUG true [get_nets r_temp_sig*]
#set_property MARK_DEBUG true [get_nets r_toggle_temp_sig*]


##------------------------------------------------------------------------##




