// # this        : xem7310__pgu_cpu__top.v
// # top xdc     : xem7310__pgu_cpu__top.xdc
// # board       : PGU-CPU-F5500
// # sch         : PGU_CPU_F5500__R190929__4M.pdf

// unused
//`default_nettype none

/* top module integration */
module xem7310__pgu_cpu__top ( 
	// OK interface
	input  wire [4:0]   okUH,
	output wire [2:0]   okHU,
	inout  wire [31:0]  okUHU,
	inout  wire         okAA,
	// external clock ports 
	input  wire         sys_clkp, 
	input  wire         sys_clkn,
	// external ADC ports 
	input  wire			i_XADC_VP,
	input  wire			i_XADC_VN,
	//
	// BANK 13 34 35 signals in connectors
	// MC1 - odd
	output wire			o_B34D_L24P,       // DAC0_DAT_P10
	output wire			o_B34D_L24N,       // DAC0_DAT_N10
	output wire			o_B34D_L17P,       // DAC0_DAT_P9
	output wire			o_B34D_L17N,       // DAC0_DAT_N9
	output wire			o_B34D_L16P,       // DAC0_DAT_P8
	output wire			o_B34D_L16N,       // DAC0_DAT_N8
	input  wire			c_B34D_L14P_SRCC,  // DAC0_DCO_P
	input  wire			c_B34D_L14N_SRCC,  // DAC0_DCO_N
	output wire			o_B34D_L10P,       // DAC0_DCI_P
	output wire			o_B34D_L10N,       // DAC0_DCI_N
	output wire			o_B34D_L20P,       // DAC0_DAT_P7
	output wire			o_B34D_L20N,       // DAC0_DAT_N7
	output wire			o_B34D_L3P,        // DAC0_DAT_P6
	output wire			o_B34D_L3N,        // DAC0_DAT_N6
	output wire			o_B34D_L9P,        // DAC0_DAT_P5
	output wire			o_B34D_L9N,        // DAC0_DAT_N5
	output wire			o_B34D_L2P,        // DAC0_DAT_P4
	output wire			o_B34D_L2N,        // DAC0_DAT_N4
	output wire			o_B34D_L4P,        // DAC0_DAT_P3
	output wire			o_B34D_L4N,        // DAC0_DAT_N3
	output wire			o_B34D_L1P,        // DAC0_DAT_P2
	output wire			o_B34D_L1N,        // DAC0_DAT_N2
	output wire			o_B34D_L7P,        // DAC0_DAT_P1
	output wire			o_B34D_L7N,        // DAC0_DAT_N1
	output wire			o_B34D_L12P_MRCC,  // DAC0_DAT_P0
	output wire			o_B34D_L12N_MRCC,  // DAC0_DAT_N0
    //                                      
	output wire			o_B13_L2P,         // SPIO0_CS
	output wire			o_B13_L2N,         // SPIOx_SCLK
	output wire			o_B13_L4P,         // SPIOx_MOSI
	input  wire			i_B13_L4N,         // SPIOx_MISO
	output wire			o_B13_L1P,         // SPIO1_CS
	//
	// MC1 - even
	output wire			o_B34D_L21P,       // DAC0_DAT_P15
	output wire			o_B34D_L21N,       // DAC0_DAT_N15
	output wire			o_B34D_L19P,       // DAC0_DAT_P14
	output wire			o_B34D_L19N,       // DAC0_DAT_N14
	output wire			o_B34D_L23P,       // DAC0_DAT_P13
	output wire			o_B34D_L23N,       // DAC0_DAT_N13
	output wire			o_B34D_L15P,       // DAC0_DAT_P12
	output wire			o_B34D_L15N,       // DAC0_DAT_N12
	output wire			o_B34D_L13P_MRCC,  // DAC0_DAT_P11
	output wire			o_B34D_L13N_MRCC,  // DAC0_DAT_N11
	input  wire			c_B34D_L11P_SRCC,  // ADC0_DCO_P
	input  wire			c_B34D_L11N_SRCC,  // ADC0_DCO_N
	input  wire			i_B34D_L18P,       // ADC0_DA_P
	input  wire			i_B34D_L18N,       // ADC0_DA_N
	input  wire			i_B34D_L22P,       // ADC0_DB_P
	input  wire			i_B34D_L22N,       // ADC0_DB_N
	output wire			o_B34D_L6P,        // ADCx_CNV_P
	output wire			o_B34D_L6N,        // ADCx_CNV_N
	output wire			o_B34_L5P,         // ADCx_TPT_B
	inout  wire			io_B34_L5N,        // S_IO_0
	output wire			o_B34D_L8P,        // ADCx_CLK_P
	output wire			o_B34D_L8N,        // ADCx_CLK_N
	//
	output wire			o_B13_SYS_CLK_MC1, // DACx_RST_B
	output wire			o_B13_L5P,         // DAC0_CS
	output wire			o_B13_L5N,         // DACx_SCLK
	output wire			o_B13_L3P,         // DACx_SDIO
	input  wire			i_B13_L3N,         // DACx_SDO
	output wire			o_B13_L16P,        // DAC1_CS
	inout  wire			io_B13_L16N,       // S_IO_1
	inout  wire			io_B13_L1N,        // S_IO_2
	//
	// MC2 - odd
	output wire			o_B35D_L21P,       // DAC1_DAT_P10
	output wire			o_B35D_L21N,       // DAC1_DAT_N10
	output wire			o_B35D_L19P,       // DAC1_DAT_P9
	output wire			o_B35D_L19N,       // DAC1_DAT_N9
	output wire			o_B35D_L18P,       // DAC1_DAT_P8
	output wire			o_B35D_L18N,       // DAC1_DAT_N8
	output wire			o_B35D_L23P,       // DAC1_DCI_P
	output wire			o_B35D_L23N,       // DAC1_DCI_N
	output wire			o_B35D_L15P,       // DAC1_DAT_P7
	output wire			o_B35D_L15N,       // DAC1_DAT_N7
	output wire			o_B35D_L9P,        // DAC1_DAT_P6
	output wire			o_B35D_L9N,        // DAC1_DAT_N6
	output wire			o_B35D_L7P,        // DAC1_DAT_P5
	output wire			o_B35D_L7N,        // DAC1_DAT_N5
	input  wire			c_B35D_L11P_SRCC,  // DAC1_DCO_P
	input  wire			c_B35D_L11N_SRCC,  // DAC1_DCO_N
	output wire			o_B35D_L4P,        // DAC1_DAT_P4
	output wire			o_B35D_L4N,        // DAC1_DAT_N4
	output wire			o_B35D_L6P,        // DAC1_DAT_P3
	output wire			o_B35D_L6N,        // DAC1_DAT_N3
	output wire			o_B35D_L1P,        // DAC1_DAT_P2
	output wire			o_B35D_L1N,        // DAC1_DAT_N2
	output wire			o_B35D_L13P_MRCC,  // DAC1_DAT_P1
	output wire			o_B35D_L13N_MRCC,  // DAC1_DAT_N1
	output wire			o_B35D_L12P_MRCC,  // DAC1_DAT_P0
	output wire			o_B35D_L12N_MRCC,  // DAC1_DAT_N0
	//
	input  wire			i_B13_SYS_CLK_MC2, // CLK_REFM
	input  wire			i_B13_L17P,        // LAN_MISO
	output wire			o_B13_L17N,        // LAN_RSTn
	input  wire			c_B13D_L13P_MRCC,  // CLK_COUT_P
	input  wire			c_B13D_L13N_MRCC,  // CLK_COUT_N
	input  wire			i_B13_L11P_SRCC,   // LAN_INTn
	
	// MC2 - even
	output wire			o_B35_IO0,         // CLK_RST_B
	input  wire			i_B35_IO25,        // CLK_LD
	//                                     
	output wire			o_B35D_L24P,       // DAC1_DAT_P15
	output wire			o_B35D_L24N,       // DAC1_DAT_N15
	output wire			o_B35D_L22P,       // DAC1_DAT_P14
	output wire			o_B35D_L22N,       // DAC1_DAT_N14
	output wire			o_B35D_L20P,       // DAC1_DAT_P13
	output wire			o_B35D_L20N,       // DAC1_DAT_N13
	output wire			o_B35D_L16P,       // DAC1_DAT_P12
	output wire			o_B35D_L16N,       // DAC1_DAT_N12
	output wire			o_B35D_L17P,       // DAC1_DAT_P11
	output wire			o_B35D_L17N,       // DAC1_DAT_N11
	input  wire			c_B35D_L14P_SRCC,  // ADC1_DCO_P
	input  wire			c_B35D_L14N_SRCC,  // ADC1_DCO_N
	input  wire			i_B35D_L10P,       // ADC1_DA_P
	input  wire			i_B35D_L10N,       // ADC1_DA_N
	input  wire			i_B35D_L8P,        // ADC1_DB_P
	input  wire			i_B35D_L8N,        // ADC1_DB_N
	output wire			o_B35_L5P,         // CLK_SDIO
	input  wire			i_B35_L5N,         // CLK_SDO
	output wire			o_B35_L3P,         // CLK_CS_B
	output wire			o_B35_L3N,         // CLK_SCLK
	input  wire			i_B35_L2P,         // CLK_STAT
	output wire			o_B35_L2N,         // CLK_SYNC
	//                                     
	input  wire			i_B13D_L14P_SRCC,  // TRIG_IN_P
	input  wire			i_B13D_L14N_SRCC,  // TRIG_IN_N
	output wire			o_B13D_L15P,       // TRIG_OUT_P
	output wire			o_B13D_L15N,       // TRIG_OUT_N
	output wire			o_B13_L6P,         // LAN_SSNn
	output wire			o_B13_L6N,         // LAN_SCLK
	output wire			o_B13_L11N_SRCC,   // LAN_MOSI

	// BANK 15 16 for DDR3
	inout  wire [31:0]  ddr3_dq,           // reserved  
	inout  wire [14:0]  ddr3_addr,         // reserved output
	inout  wire [2 :0]  ddr3_ba,           // reserved output
	inout  wire [0 :0]  ddr3_ck_p,         // reserved output
	inout  wire [0 :0]  ddr3_ck_n,         // reserved output
	inout  wire [0 :0]  ddr3_cke,          // reserved output
	inout  wire         ddr3_cas_n,        // reserved output
	inout  wire         ddr3_ras_n,        // reserved output
	inout  wire         ddr3_we_n,         // reserved output
	inout  wire [0 :0]  ddr3_odt,          // reserved output
	inout  wire [3 :0]  ddr3_dm,           // reserved output
	inout  wire [3 :0]  ddr3_dqs_p,        // reserved 
	inout  wire [3 :0]  ddr3_dqs_n,        // reserved 
	inout  wire         ddr3_reset_n,      // reserved output

	// LED on XEM7310
	output wire [7:0]   led
	);


/* Clock PLL */

// clock pll0
wire clk_out1_200M ; // REFCLK 200MHz for IDELAYCTRL // for pll1
wire clk_out2_140M ; // for pll2 ... 140M
wire clk_out3_10M  ; // for slow logic / I2C //
wire clk_out4_125M ; // unused
wire clk_out5_62p5M; // unused
//
wire clk_locked_pre;
clk_wiz_0  clk_wiz_0_inst (
	// Clock out ports  
	.clk_out1_200M (clk_out1_200M ), 
	.clk_out2_140M (clk_out2_140M ),
	.clk_out3_10M  (clk_out3_10M  ), // not exact due to 140MHz
	.clk_out4_125M (clk_out4_125M ), 
	.clk_out5_62p5M(clk_out5_62p5M), 
	// Status and control signals               
	.locked(clk_locked_pre),
	// Clock in ports
	.clk_in1_p(sys_clkp),
	.clk_in1_n(sys_clkn)
);
// clock pll1
wire clk1_out1_160M; // for DWAVE 
wire clk1_out2_120M; // unused
wire clk1_out3_80M ; // unused
wire clk1_out4_60M ; // unused // 10M exact...
wire clk1_locked;
// clk_wiz_0_1_135M  clk_wiz_0_1_inst ( // 135MHz test
clk_wiz_0_1  clk_wiz_0_1_inst (
	// Clock out ports  
	.clk_out1_160M(clk1_out1_160M),  
	.clk_out2_120M(clk1_out2_120M), 
	.clk_out3_80M (clk1_out3_80M ), 
	.clk_out4_60M (clk1_out4_60M ), 
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk1_locked),
	// Clock in ports
	.clk_in1(clk_out1_200M)
);
// clock pll2
wire clk2_out1_210M; // for HR-ADC and test_clk
wire clk2_out2_105M; // unused
wire clk2_out3_60M ; // for ADC fifo/serdes
wire clk2_out4_30M ; // unused             
wire clk2_locked;
clk_wiz_0_2  clk_wiz_0_2_inst (
	// Clock out ports  
	.clk_out1_210M(clk2_out1_210M),  
	.clk_out2_105M(clk2_out2_105M), 
	.clk_out3_60M (clk2_out3_60M ), 
	.clk_out4_30M (clk2_out4_30M ), 
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk2_locked),
	// Clock in ports
	.clk_in1(clk_out2_140M) // 200M --> 140M for better jitter.
);
// clock pll3 
wire clk3_out1_72M ; // MCS core & UART ref 
wire clk3_out2_144M; // LAN-SPI control 144MHz 
wire clk3_out3_12M ; // IO bridge 12MHz 
// ... DDR3-CON 400MHz (pending)
wire clk3_locked;
clk_wiz_0_3  clk_wiz_0_3_inst (
	// Clock out ports  
	.clk_out1_72M (clk3_out1_72M ),  
	.clk_out2_144M(clk3_out2_144M), 
	.clk_out3_12M (clk3_out3_12M ), 
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk3_locked),
	// Clock in ports
	.clk_in1(clk_out1_200M)
);

// clock pll for DAC 400MHz 
wire clk_dac_out1_400M; // DAC update rate 
wire clk_dac_out2_200M; // DAC DMA rate 
wire clk_dac_out3_100M; // DAC soft CPU rate 
wire clk_dac_out4_50M ; // DAC low speed clk 
//
wire clk_dac_locked;
wire clk_dac_clk_in;
//
clk_wiz_1  clk_wiz_1_inst (
	// Clock out ports  
	.clk_out1_400M (clk_dac_out1_400M),  
	.clk_out2_200M (clk_dac_out2_200M), 
	.clk_out3_100M (clk_dac_out3_100M), 
	.clk_out4_50M  (clk_dac_out4_50M ), // BUFGCE
	// Status and control signals     
	.locked(clk_dac_locked),
	// Clock in ports
	.clk_in1(clk_dac_clk_in)
);
//
wire dac0_dco_clk_out1_400M; // DAC0 update rate 
wire dac0_dco_clk_out2_200M; // DAC0 DMA rate 
wire dac1_dco_clk_out1_400M; // DAC1 update rate 
wire dac1_dco_clk_out2_200M; // DAC1 DMA rate 
//
wire dac0_dco_clk_locked;
wire dac0_dco_clk_in;
wire dac1_dco_clk_locked;
wire dac1_dco_clk_in;
//
clk_wiz_1  clk_wiz_1_0_inst (
	// Clock out ports  
	.clk_out1_400M (dac0_dco_clk_out1_400M), 
	.clk_out2_200M (dac0_dco_clk_out2_200M), 
	.clk_out3_100M (), 
	.clk_out4_50M  (), 
	// Status and control signals     
	.locked(dac0_dco_clk_locked),
	// Clock in ports
	.clk_in1(dac0_dco_clk_in)
);
//
clk_wiz_1  clk_wiz_1_1_inst (
	// Clock out ports  
	.clk_out1_400M (dac1_dco_clk_out1_400M), 
	.clk_out2_200M (dac1_dco_clk_out2_200M), 
	.clk_out3_100M (), 
	.clk_out4_50M  (), 
	// Status and control signals     
	.locked(dac1_dco_clk_locked),
	// Clock in ports
	.clk_in1(dac1_dco_clk_in)
);



// test clock out 
wire w_oddr_out;
//wire w_oddr_in = clk_out1_200M; // OK ...
//wire w_oddr_in = clk_dac_out4_50M; // OK ... xdc proper setup on clock pad 
//wire w_oddr_in = clk_dac_out3_100M; // OK
wire w_oddr_in = clk_dac_out2_200M; // OK
//wire w_oddr_in = clk_dac_out1_400M; // OK ... xdc // with set_max_delay -from [get_pins {clk_wiz_1_inst/inst/seq_reg1_reg[7]/C}] -
//
ODDR #(
	.DDR_CLK_EDGE("SAME_EDGE"), // "OPPOSITE_EDGE" or "SAME_EDGE" 
	.INIT(1'b0),    // Initial value of Q: 1'b0 or 1'b1
	.SRTYPE("SYNC") // Set/Reset type: "SYNC" or "ASYNC" 
) ODDR_inst (
	.Q(w_oddr_out),   // 1-bit DDR output
	.C(w_oddr_in),   // 1-bit clock input
	.CE(1'b1), // 1-bit clock enable input
	.D1(1'b1), // 1-bit data input (positive edge)
	.D2(1'b0), // 1-bit data input (negative edge)
	.R(1'b0),   // 1-bit reset
	.S(1'b0)    // 1-bit set
);

// test pattern 16 bit
// ...


// clock locked 
wire clk_locked = clk1_locked & clk2_locked & clk3_locked;

// system clock
wire sys_clk	= clk_out3_10M;

// system reset 
wire reset_n	= clk_locked;
wire reset		= ~reset_n;
////
	
// check SW_BUILD_ID
parameter REQ_SW_BUILD_ID = 32'h_ACAC_C8C8; // 0 for bypass 
////
	
// FPGA_IMAGE_ID //TODO:update_revision_code
parameter FPGA_IMAGE_ID = 32'h_B0_19_10DD; // PGU-CPU-F5500 // pin map setup
////

// ADC_BASE_FREQ
//parameter ADC_BASE_FREQ = 32'd210_000_000; //
////

// DWAVE_BASE_FREQ
//parameter DWAVE_BASE_FREQ = 32'd160_000_000;
////

/* USB-FPGA OK interface */
// OK Target interface clk:
(* keep = "true" *) 
wire okClk;
// USB Endpoint connections: // TODO: USB endpoint
// Wire In 		0x00 - 0x1F
wire [31:0] ep00wire; //$$ [TEST] SW_BUILD_ID 
wire [31:0] ep01wire; //$$ [TEST] TEST_CON 
wire [31:0] ep02wire; //$$ [TEST] TEST_CC_DIN 
wire [31:0] ep03wire; //
wire [31:0] ep04wire; //
wire [31:0] ep05wire; //
wire [31:0] ep06wire; //
wire [31:0] ep07wire; //
wire [31:0] ep08wire; //
wire [31:0] ep09wire; //
wire [31:0] ep0Awire; //
wire [31:0] ep0Bwire; //
wire [31:0] ep0Cwire; //
wire [31:0] ep0Dwire; //
wire [31:0] ep0Ewire; //
wire [31:0] ep0Fwire; //
wire [31:0] ep10wire; //
wire [31:0] ep11wire; //
wire [31:0] ep12wire; //
wire [31:0] ep13wire; //
wire [31:0] ep14wire; //
wire [31:0] ep15wire; //
wire [31:0] ep16wire; //
wire [31:0] ep17wire; //
wire [31:0] ep18wire; //
wire [31:0] ep19wire; //
wire [31:0] ep1Awire; //
wire [31:0] ep1Bwire; //
wire [31:0] ep1Cwire; //
wire [31:0] ep1Dwire; //$$ [ADC_HS] ADC_HS_UPD_SMP    
wire [31:0] ep1Ewire; //$$ [ADC_HS] ADC_HS_SMP_PRD    
wire [31:0] ep1Fwire; //$$ [ADC_HS] ADC_HS_DLY_TAP_OPT
// Wire Out 	0x20 - 0x3F
wire [31:0] ep20wire; //$$ [TEST] FPGA_IMAGE_ID
wire [31:0] ep21wire; //$$ [TEST] TEST_OUT
wire [31:0] ep22wire = 32'b0; //
wire [31:0] ep23wire = 32'b0; //
wire [31:0] ep24wire = 32'b0; //
wire [31:0] ep25wire = 32'b0; //
wire [31:0] ep26wire = 32'b0; //
wire [31:0] ep27wire = 32'b0; //
wire [31:0] ep28wire = 32'b0; //
wire [31:0] ep29wire = 32'b0; //
wire [31:0] ep2Awire = 32'b0; //
wire [31:0] ep2Bwire = 32'b0; //
wire [31:0] ep2Cwire = 32'b0; //
wire [31:0] ep2Dwire = 32'b0; //
wire [31:0] ep2Ewire = 32'b0; //
wire [31:0] ep2Fwire = 32'b0; //
wire [31:0] ep30wire = 32'b0; //
wire [31:0] ep31wire = 32'b0; //
wire [31:0] ep32wire = 32'b0; //
wire [31:0] ep33wire = 32'b0; //
wire [31:0] ep34wire = 32'b0; //
wire [31:0] ep35wire = 32'b0; //
wire [31:0] ep36wire = 32'b0; //
wire [31:0] ep37wire = 32'b0; //
wire [31:0] ep38wire = 32'b0; //$$ [ADC_HS] ADC_HS_WO
wire [31:0] ep39wire = 32'b0; //$$ [ADC] ADC_BASE_FREQ
wire [31:0] ep3Awire; //$$ [XADC] XADC_TEMP
wire [31:0] ep3Bwire; //$$ [XADC] XADC_VOLT  
wire [31:0] ep3Cwire = 32'b0; //$$ [ADC_HS] ADC_HS_DOUT0
wire [31:0] ep3Dwire = 32'b0; //$$ [ADC_HS] ADC_HS_DOUT1  
wire [31:0] ep3Ewire = 32'b0; //
wire [31:0] ep3Fwire = 32'b0; //
// Trigger In 	0x40 - 0x5F
wire ep40ck = sys_clk       ; wire [31:0] ep40trig; //$$ [TEST] TEST_TI
wire ep41ck = 1'b0; wire [31:0] ep41trig;
wire ep42ck = 1'b0; wire [31:0] ep42trig;
wire ep43ck = 1'b0; wire [31:0] ep43trig;
wire ep44ck = 1'b0; wire [31:0] ep44trig;
wire ep45ck = 1'b0; wire [31:0] ep45trig;
wire ep46ck = 1'b0; wire [31:0] ep46trig;
wire ep47ck = 1'b0; wire [31:0] ep47trig;
wire ep48ck = 1'b0; wire [31:0] ep48trig;
wire ep49ck = 1'b0; wire [31:0] ep49trig;
wire ep4Ack = 1'b0; wire [31:0] ep4Atrig;
wire ep4Bck = 1'b0; wire [31:0] ep4Btrig;
wire ep4Cck = 1'b0; wire [31:0] ep4Ctrig;
wire ep4Dck = 1'b0; wire [31:0] ep4Dtrig;
wire ep4Eck = 1'b0; wire [31:0] ep4Etrig;
wire ep4Fck = 1'b0; wire [31:0] ep4Ftrig;
wire ep50ck = 1'b0; wire [31:0] ep50trig;
wire ep51ck = 1'b0; wire [31:0] ep51trig;
wire ep52ck = 1'b0; wire [31:0] ep52trig;
wire ep53ck = 1'b0; wire [31:0] ep53trig;
wire ep54ck = 1'b0; wire [31:0] ep54trig;
wire ep55ck = 1'b0; wire [31:0] ep55trig;
wire ep56ck = 1'b0; wire [31:0] ep56trig;
wire ep57ck = 1'b0; wire [31:0] ep57trig;
wire ep58ck = 1'b0; wire [31:0] ep58trig;
wire ep59ck = 1'b0; wire [31:0] ep59trig;
wire ep5Ack = 1'b0; wire [31:0] ep5Atrig;
wire ep5Bck = 1'b0; wire [31:0] ep5Btrig;
wire ep5Cck = 1'b0; wire [31:0] ep5Ctrig;
wire ep5Dck = 1'b0; wire [31:0] ep5Dtrig;
wire ep5Eck = 1'b0; wire [31:0] ep5Etrig;
wire ep5Fck = 1'b0; wire [31:0] ep5Ftrig;
// Trigger Out 	0x60 - 0x7F
wire ep60ck = sys_clk     ; wire [31:0] ep60trig; //$$ [TEST] TEST_TO
wire ep61ck = 1'b0; wire [31:0] ep61trig = 32'b0;
wire ep62ck = 1'b0; wire [31:0] ep62trig = 32'b0;
wire ep63ck = 1'b0; wire [31:0] ep63trig = 32'b0;
wire ep64ck = 1'b0; wire [31:0] ep64trig = 32'b0;
wire ep65ck = 1'b0; wire [31:0] ep65trig = 32'b0;
wire ep66ck = 1'b0; wire [31:0] ep66trig = 32'b0;
wire ep67ck = 1'b0; wire [31:0] ep67trig = 32'b0;
wire ep68ck = 1'b0; wire [31:0] ep68trig = 32'b0;
wire ep69ck = 1'b0; wire [31:0] ep69trig = 32'b0;
wire ep6Ack = 1'b0; wire [31:0] ep6Atrig = 32'b0;
wire ep6Bck = 1'b0; wire [31:0] ep6Btrig = 32'b0;
wire ep6Cck = 1'b0; wire [31:0] ep6Ctrig = 32'b0;
wire ep6Dck = 1'b0; wire [31:0] ep6Dtrig = 32'b0;
wire ep6Eck = 1'b0; wire [31:0] ep6Etrig = 32'b0;
wire ep6Fck = 1'b0; wire [31:0] ep6Ftrig = 32'b0;
wire ep70ck = 1'b0; wire [31:0] ep70trig = 32'b0;
wire ep71ck = 1'b0; wire [31:0] ep71trig = 32'b0;
wire ep72ck = 1'b0; wire [31:0] ep72trig = 32'b0;
wire ep73ck = 1'b0; wire [31:0] ep73trig = 32'b0;
wire ep74ck = 1'b0; wire [31:0] ep74trig = 32'b0;
wire ep75ck = 1'b0; wire [31:0] ep75trig = 32'b0;
wire ep76ck = 1'b0; wire [31:0] ep76trig = 32'b0;
wire ep77ck = 1'b0; wire [31:0] ep77trig = 32'b0;
wire ep78ck = 1'b0; wire [31:0] ep78trig = 32'b0;
wire ep79ck = 1'b0; wire [31:0] ep79trig = 32'b0;
wire ep7Ack = 1'b0; wire [31:0] ep7Atrig = 32'b0;
wire ep7Bck = 1'b0; wire [31:0] ep7Btrig = 32'b0;
wire ep7Cck = 1'b0; wire [31:0] ep7Ctrig = 32'b0;
wire ep7Dck = 1'b0; wire [31:0] ep7Dtrig = 32'b0;
wire ep7Eck = 1'b0; wire [31:0] ep7Etrig = 32'b0;
wire ep7Fck = 1'b0; wire [31:0] ep7Ftrig = 32'b0; 
// Pipe In 		0x80 - 0x9F // clock is assumed to use okClk
wire ep80wr; wire [31:0] ep80pipe;
wire ep81wr; wire [31:0] ep81pipe;
wire ep82wr; wire [31:0] ep82pipe;
wire ep83wr; wire [31:0] ep83pipe;
wire ep84wr; wire [31:0] ep84pipe;
wire ep85wr; wire [31:0] ep85pipe;
wire ep86wr; wire [31:0] ep86pipe;
wire ep87wr; wire [31:0] ep87pipe;
wire ep88wr; wire [31:0] ep88pipe;
wire ep89wr; wire [31:0] ep89pipe;
wire ep8Awr; wire [31:0] ep8Apipe;
wire ep8Bwr; wire [31:0] ep8Bpipe;
wire ep8Cwr; wire [31:0] ep8Cpipe;
wire ep8Dwr; wire [31:0] ep8Dpipe;
wire ep8Ewr; wire [31:0] ep8Epipe;
wire ep8Fwr; wire [31:0] ep8Fpipe;
wire ep90wr; wire [31:0] ep90pipe;
wire ep91wr; wire [31:0] ep91pipe;
wire ep92wr; wire [31:0] ep92pipe;
wire ep93wr; wire [31:0] ep93pipe;
wire ep94wr; wire [31:0] ep94pipe;
wire ep95wr; wire [31:0] ep95pipe;
wire ep96wr; wire [31:0] ep96pipe;
wire ep97wr; wire [31:0] ep97pipe;
wire ep98wr; wire [31:0] ep98pipe;
wire ep99wr; wire [31:0] ep99pipe;
wire ep9Awr; wire [31:0] ep9Apipe;
wire ep9Bwr; wire [31:0] ep9Bpipe;
wire ep9Cwr; wire [31:0] ep9Cpipe;
wire ep9Dwr; wire [31:0] ep9Dpipe;
wire ep9Ewr; wire [31:0] ep9Epipe;
wire ep9Fwr; wire [31:0] ep9Fpipe;
// Pipe Out 	0xA0 - 0xBF
wire epA0rd; wire [31:0] epA0pipe = 32'b0;
wire epA1rd; wire [31:0] epA1pipe = 32'b0;
wire epA2rd; wire [31:0] epA2pipe = 32'b0;
wire epA3rd; wire [31:0] epA3pipe = 32'b0;
wire epA4rd; wire [31:0] epA4pipe = 32'b0;
wire epA5rd; wire [31:0] epA5pipe = 32'b0;
wire epA6rd; wire [31:0] epA6pipe = 32'b0;
wire epA7rd; wire [31:0] epA7pipe = 32'b0;
wire epA8rd; wire [31:0] epA8pipe = 32'b0;
wire epA9rd; wire [31:0] epA9pipe = 32'b0;
wire epAArd; wire [31:0] epAApipe = 32'b0;
wire epABrd; wire [31:0] epABpipe = 32'b0;
wire epACrd; wire [31:0] epACpipe = 32'b0;
wire epADrd; wire [31:0] epADpipe = 32'b0;
wire epAErd; wire [31:0] epAEpipe = 32'b0;
wire epAFrd; wire [31:0] epAFpipe = 32'b0;
wire epB0rd; wire [31:0] epB0pipe = 32'b0;
wire epB1rd; wire [31:0] epB1pipe = 32'b0;
wire epB2rd; wire [31:0] epB2pipe = 32'b0;
wire epB3rd; wire [31:0] epB3pipe = 32'b0;
wire epB4rd; wire [31:0] epB4pipe = 32'b0;
wire epB5rd; wire [31:0] epB5pipe = 32'b0;
wire epB6rd; wire [31:0] epB6pipe = 32'b0;
wire epB7rd; wire [31:0] epB7pipe = 32'b0;
wire epB8rd; wire [31:0] epB8pipe = 32'b0;
wire epB9rd; wire [31:0] epB9pipe = 32'b0;
wire epBArd; wire [31:0] epBApipe = 32'b0;
wire epBBrd; wire [31:0] epBBpipe = 32'b0;
wire epBCrd; wire [31:0] epBCpipe = 32'b0;
wire epBDrd; wire [31:0] epBDpipe = 32'b0;
wire epBErd; wire [31:0] epBEpipe = 32'b0;
wire epBFrd; wire [31:0] epBFpipe = 32'b0;
////




/* check IDs */
wire [31:0] w_SW_BUILD_ID = ep00wire; // switch with w_port_wi_00_1
//
wire [31:0] w_FPGA_IMAGE_ID = 
				(w_SW_BUILD_ID==REQ_SW_BUILD_ID)? FPGA_IMAGE_ID : 
				(w_SW_BUILD_ID==32'b0          )? FPGA_IMAGE_ID : 
				32'b0 ;
//
assign ep20wire = w_FPGA_IMAGE_ID;
//
////

/* TEST LED */
// test drive
reg  [7:0] r_test;
// Counter 1:
reg  [31:0] div1;
reg         clk1div;
(* keep = "true" *) reg  [7:0]  count1;
reg         count1eq00;
reg         count1eq80;
wire        reset1;
wire        disable1;
// Counter 2:
reg  [23:0] div2;
reg         clk2div;
(* keep = "true" *) reg  [7:0]  count2;
reg         count2eqFF;
wire        reset2;
wire        up2;
wire        down2;
wire        autocount2;
////


/* XADC */
//
wire [31:0] w_XADC_TEMP;
assign ep3Awire = w_XADC_TEMP;
wire [31:0] w_XADC_VOLT;
assign ep3Bwire = w_XADC_VOLT;
// XADC_DRP
wire [31:0] MEASURED_TEMP_MC;
wire [31:0] MEASURED_VCCINT_MV;
wire [31:0] MEASURED_VCCAUX_MV;
wire [31:0] MEASURED_VCCBRAM_MV;
//
(* keep = "true" *) wire [7:0] dbg_drp;
//
master_drp_ug480 master_drp_ug480_inst(
	.DCLK				(clk_out3_10M), // input DCLK, // Clock input for DRP
	.RESET				(~reset_n), // input RESET,
	.VP					(i_XADC_VP), // input VP, VN,// Dedicated and Hardwired Analog Input Pair
	.VN					(i_XADC_VN),
	.MEASURED_TEMP		(), // output reg [15:0] MEASURED_TEMP, MEASURED_VCCINT,
	.MEASURED_VCCINT	(),
	.MEASURED_VCCAUX	(), // output reg [15:0] MEASURED_VCCAUX, MEASURED_VCCBRAM,
	.MEASURED_VCCBRAM	(),
	// converted to decimal
	.MEASURED_TEMP_MC		(MEASURED_TEMP_MC), 
	.MEASURED_VCCINT_MV		(MEASURED_VCCINT_MV),
	.MEASURED_VCCAUX_MV		(MEASURED_VCCAUX_MV), 
	.MEASURED_VCCBRAM_MV	(MEASURED_VCCBRAM_MV),
	//
	.ALM_OUT	(), // output wire ALM_OUT,
	.CHANNEL	(), // output wire [4:0] CHANNEL,
	.OT			(), // output wire OT,
	.XADC_EOC	(), // output wire XADC_EOC,
	.XADC_EOS	(), // output wire XADC_EOS
	.debug_out	(dbg_drp)
);
//
assign w_XADC_TEMP	= MEASURED_TEMP_MC;
//
assign w_XADC_VOLT = 
	(count2[7:6]==2'b00)? MEASURED_VCCINT_MV :
	(count2[7:6]==2'b01)? MEASURED_VCCAUX_MV :
	(count2[7:6]==2'b10)? MEASURED_VCCBRAM_MV :
		32'b0;
////



// TEST end-point
wire [31:0] w_TEST_CON = ep01wire;
//
wire [31:0] w_TEST_OUT;
assign ep21wire = w_TEST_OUT; // TEST_OUT 
//
wire [31:0] w_TEST_TI = ep40trig;
//
wire [31:0] w_TEST_TO;
assign ep60trig = w_TEST_TO; 
// Counter 1:
assign reset1     = w_TEST_CON[0]; 
assign disable1   = w_TEST_CON[1]; 
assign autocount2 = w_TEST_CON[2]; 
//
assign w_TEST_OUT[15:0] = {count2[7:0], count1[7:0]}; 
assign w_TEST_OUT[31:16] = 16'b0; 
// Counter 2:
assign reset2     = w_TEST_TI[0];
assign up2        = w_TEST_TI[1];
assign down2      = w_TEST_TI[2];
//
assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
////

// LED drive
function [7:0] xem7310_led;
input [7:0] a;
integer i;
begin
	for(i=0; i<8; i=i+1) begin: u
		xem7310_led[i] = (a[i]==1'b1) ? (1'b0) : (1'bz);
	end
end
endfunction
// 
assign led = xem7310_led(r_test ^ count1);
////

// Counter #1
// + Counting using a divided sysclk.
// + Reset sets the counter to 0.
// + Disable turns off the counter.
always @(posedge sys_clk, negedge reset_n) begin
	if (!reset_n) begin
		div1 <= 32'd0; 
		clk1div <= 1'b0;
		count1 <= 8'h00;
		count1eq00 <= 1'b0;
		count1eq80 <= 1'b0;
	end
	else begin
		//
		if (div1 == 32'h000000) begin
			// div1 <= 24'h400000;
			//div1 <= 32'h0BEBC200-1; // 0x0BEBC200 for 1sec @ 200MHz
			div1 <= 32'd010_000_000-1; // 1sec @ 10MHz
			clk1div <= 1'b1;
		end else begin
			div1 <= div1 - 1;
			clk1div <= 1'b0;
		end
		//
		if (clk1div == 1'b1) begin
			if (reset1 == 1'b1) begin
				count1 <= 8'h00;
			end else if (disable1 == 1'b0) begin
				count1 <= count1 + 8'd1;
			end
		end
		//
		if (count1 == 8'h00)
			count1eq00 <= 1'b1;
		else
			count1eq00 <= 1'b0;
		//
		if (count1 == 8'h80)
			count1eq80 <= 1'b1;
		else
			count1eq80 <= 1'b0;
		//
		end
end
////

// Counter #2
// + Reset, up, and down control counter.
// + If autocount is enabled, a divided sys_clk can also
//   upcount.
always @(posedge sys_clk, negedge reset_n) begin
	if (!reset_n) begin
		div2 <= 24'd0; 
		clk2div <= 1'b0;
		count2 <= 8'h00;
		r_test <= 8'h80;
		count2eqFF <= 1'b0;
		end
	else begin
		//
		if (div2 == 24'h000000) begin
			div2 <= 24'h100000;
			clk2div <= 1'b1;
		end else begin
			div2 <= div2 - 1;
			clk2div <= 1'b0;
		end
		//
		if (reset2 == 1'b1) begin
			count2 <= 8'h00;
			r_test <= 8'h80;
			end
		else if (up2 == 1'b1)
			count2 <= count2 + 8'd1;
		else if (down2 == 1'b1)
			count2 <= count2 - 1;
		else if ((autocount2 == 1'b1) && (clk2div == 1'b1)) begin 
			count2 <= count2 + 8'd1;
			//r_test <= {r_test[6:0], r_test[7]}; // circular left
			r_test <= {r_test[0], r_test[7:1]}; // circular right
			end
		//
		if (count2 == 8'hff)
			count2eqFF <= 1'b1;
		else
			count2eqFF <= 1'b0;
		//
		end
end
////




/* okHost wrapper */
//$$ Endpoints
// Wire In 		0x00 - 0x1F
// Wire Out 	0x20 - 0x3F
// Trigger In 	0x40 - 0x5F
// Trigger Out 	0x60 - 0x7F
// Pipe In 		0x80 - 0x9F
// Pipe Out 	0xA0 - 0xBF
ok_endpoint_wrapper  ok_endpoint_wrapper_inst (
	.okUH (okUH ), //input  wire [4:0]   okUH, // external pins
	.okHU (okHU ), //output wire [2:0]   okHU, // external pins
	.okUHU(okUHU), //inout  wire [31:0]  okUHU, // external pins
	.okAA (okAA ), //inout  wire         okAA, // external pin
	// Wire In 		0x00 - 0x1F
	.ep00wire(ep00wire), // output wire [31:0]
	.ep01wire(ep01wire), // output wire [31:0]
	.ep02wire(ep02wire), // output wire [31:0]
	.ep03wire(ep03wire), // output wire [31:0]
	.ep04wire(ep04wire), // output wire [31:0]
	.ep05wire(ep05wire), // output wire [31:0]
	.ep06wire(ep06wire), // output wire [31:0]
	.ep07wire(ep07wire), // output wire [31:0]
	.ep08wire(ep08wire), // output wire [31:0]
	.ep09wire(ep09wire), // output wire [31:0]
	.ep0Awire(ep0Awire), // output wire [31:0]
	.ep0Bwire(ep0Bwire), // output wire [31:0]
	.ep0Cwire(ep0Cwire), // output wire [31:0]
	.ep0Dwire(ep0Dwire), // output wire [31:0]
	.ep0Ewire(ep0Ewire), // output wire [31:0]
	.ep0Fwire(ep0Fwire), // output wire [31:0]
	.ep10wire(ep10wire), // output wire [31:0]
	.ep11wire(ep11wire), // output wire [31:0]
	.ep12wire(ep12wire), // output wire [31:0]
	.ep13wire(ep13wire), // output wire [31:0]
	.ep14wire(ep14wire), // output wire [31:0]
	.ep15wire(ep15wire), // output wire [31:0]
	.ep16wire(ep16wire), // output wire [31:0]
	.ep17wire(ep17wire), // output wire [31:0]
	.ep18wire(ep18wire), // output wire [31:0]
	.ep19wire(ep19wire), // output wire [31:0]
	.ep1Awire(ep1Awire), // output wire [31:0]
	.ep1Bwire(ep1Bwire), // output wire [31:0]
	.ep1Cwire(ep1Cwire), // output wire [31:0]
	.ep1Dwire(ep1Dwire), // output wire [31:0]
	.ep1Ewire(ep1Ewire), // output wire [31:0]
	.ep1Fwire(ep1Fwire), // output wire [31:0]
	// Wire Out 	0x20 - 0x3F
	.ep20wire(ep20wire), // input wire [31:0]
	.ep21wire(ep21wire), // input wire [31:0]
	.ep22wire(ep22wire), // input wire [31:0]
	.ep23wire(ep23wire), // input wire [31:0]
	.ep24wire(ep24wire), // input wire [31:0]
	.ep25wire(ep25wire), // input wire [31:0]
	.ep26wire(ep26wire), // input wire [31:0]
	.ep27wire(ep27wire), // input wire [31:0]
	.ep28wire(ep28wire), // input wire [31:0]
	.ep29wire(ep29wire), // input wire [31:0]
	.ep2Awire(ep2Awire), // input wire [31:0]
	.ep2Bwire(ep2Bwire), // input wire [31:0]
	.ep2Cwire(ep2Cwire), // input wire [31:0]
	.ep2Dwire(ep2Dwire), // input wire [31:0]
	.ep2Ewire(ep2Ewire), // input wire [31:0]
	.ep2Fwire(ep2Fwire), // input wire [31:0]
	.ep30wire(ep30wire), // input wire [31:0]
	.ep31wire(ep31wire), // input wire [31:0]
	.ep32wire(ep32wire), // input wire [31:0]
	.ep33wire(ep33wire), // input wire [31:0]
	.ep34wire(ep34wire), // input wire [31:0]
	.ep35wire(ep35wire), // input wire [31:0]
	.ep36wire(ep36wire), // input wire [31:0]
	.ep37wire(ep37wire), // input wire [31:0]
	.ep38wire(ep38wire), // input wire [31:0]
	.ep39wire(ep39wire), // input wire [31:0]
	.ep3Awire(ep3Awire), // input wire [31:0]
	.ep3Bwire(ep3Bwire), // input wire [31:0]
	.ep3Cwire(ep3Cwire), // input wire [31:0]
	.ep3Dwire(ep3Dwire), // input wire [31:0]
	.ep3Ewire(ep3Ewire), // input wire [31:0]
	.ep3Fwire(ep3Fwire), // input wire [31:0]
	// Trigger In 	0x40 - 0x5F
	.ep40ck(ep40ck), .ep40trig(ep40trig), // input wire, output wire [31:0],
	.ep41ck(ep41ck), .ep41trig(ep41trig), // input wire, output wire [31:0],
	.ep42ck(ep42ck), .ep42trig(ep42trig), // input wire, output wire [31:0],
	.ep43ck(ep43ck), .ep43trig(ep43trig), // input wire, output wire [31:0],
	.ep44ck(ep44ck), .ep44trig(ep44trig), // input wire, output wire [31:0],
	.ep45ck(ep45ck), .ep45trig(ep45trig), // input wire, output wire [31:0],
	.ep46ck(ep46ck), .ep46trig(ep46trig), // input wire, output wire [31:0],
	.ep47ck(ep47ck), .ep47trig(ep47trig), // input wire, output wire [31:0],
	.ep48ck(ep48ck), .ep48trig(ep48trig), // input wire, output wire [31:0],
	.ep49ck(ep49ck), .ep49trig(ep49trig), // input wire, output wire [31:0],
	.ep4Ack(ep4Ack), .ep4Atrig(ep4Atrig), // input wire, output wire [31:0],
	.ep4Bck(ep4Bck), .ep4Btrig(ep4Btrig), // input wire, output wire [31:0],
	.ep4Cck(ep4Cck), .ep4Ctrig(ep4Ctrig), // input wire, output wire [31:0],
	.ep4Dck(ep4Dck), .ep4Dtrig(ep4Dtrig), // input wire, output wire [31:0],
	.ep4Eck(ep4Eck), .ep4Etrig(ep4Etrig), // input wire, output wire [31:0],
	.ep4Fck(ep4Fck), .ep4Ftrig(ep4Ftrig), // input wire, output wire [31:0],
	.ep50ck(ep50ck), .ep50trig(ep50trig), // input wire, output wire [31:0],
	.ep51ck(ep51ck), .ep51trig(ep51trig), // input wire, output wire [31:0],
	.ep52ck(ep52ck), .ep52trig(ep52trig), // input wire, output wire [31:0],
	.ep53ck(ep53ck), .ep53trig(ep53trig), // input wire, output wire [31:0],
	.ep54ck(ep54ck), .ep54trig(ep54trig), // input wire, output wire [31:0],
	.ep55ck(ep55ck), .ep55trig(ep55trig), // input wire, output wire [31:0],
	.ep56ck(ep56ck), .ep56trig(ep56trig), // input wire, output wire [31:0],
	.ep57ck(ep57ck), .ep57trig(ep57trig), // input wire, output wire [31:0],
	.ep58ck(ep58ck), .ep58trig(ep58trig), // input wire, output wire [31:0],
	.ep59ck(ep59ck), .ep59trig(ep59trig), // input wire, output wire [31:0],
	.ep5Ack(ep5Ack), .ep5Atrig(ep5Atrig), // input wire, output wire [31:0],
	.ep5Bck(ep5Bck), .ep5Btrig(ep5Btrig), // input wire, output wire [31:0],
	.ep5Cck(ep5Cck), .ep5Ctrig(ep5Ctrig), // input wire, output wire [31:0],
	.ep5Dck(ep5Dck), .ep5Dtrig(ep5Dtrig), // input wire, output wire [31:0],
	.ep5Eck(ep5Eck), .ep5Etrig(ep5Etrig), // input wire, output wire [31:0],
	.ep5Fck(ep5Fck), .ep5Ftrig(ep5Ftrig), // input wire, output wire [31:0],
	// Trigger Out 	0x60 - 0x7F
	.ep60ck(ep60ck), .ep60trig(ep60trig), // input wire, input wire [31:0],
	.ep61ck(ep61ck), .ep61trig(ep61trig), // input wire, input wire [31:0],
	.ep62ck(ep62ck), .ep62trig(ep62trig), // input wire, input wire [31:0],
	.ep63ck(ep63ck), .ep63trig(ep63trig), // input wire, input wire [31:0],
	.ep64ck(ep64ck), .ep64trig(ep64trig), // input wire, input wire [31:0],
	.ep65ck(ep65ck), .ep65trig(ep65trig), // input wire, input wire [31:0],
	.ep66ck(ep66ck), .ep66trig(ep66trig), // input wire, input wire [31:0],
	.ep67ck(ep67ck), .ep67trig(ep67trig), // input wire, input wire [31:0],
	.ep68ck(ep68ck), .ep68trig(ep68trig), // input wire, input wire [31:0],
	.ep69ck(ep69ck), .ep69trig(ep69trig), // input wire, input wire [31:0],
	.ep6Ack(ep6Ack), .ep6Atrig(ep6Atrig), // input wire, input wire [31:0],
	.ep6Bck(ep6Bck), .ep6Btrig(ep6Btrig), // input wire, input wire [31:0],
	.ep6Cck(ep6Cck), .ep6Ctrig(ep6Ctrig), // input wire, input wire [31:0],
	.ep6Dck(ep6Dck), .ep6Dtrig(ep6Dtrig), // input wire, input wire [31:0],
	.ep6Eck(ep6Eck), .ep6Etrig(ep6Etrig), // input wire, input wire [31:0],
	.ep6Fck(ep6Fck), .ep6Ftrig(ep6Ftrig), // input wire, input wire [31:0],
	.ep70ck(ep70ck), .ep70trig(ep70trig), // input wire, input wire [31:0],
	.ep71ck(ep71ck), .ep71trig(ep71trig), // input wire, input wire [31:0],
	.ep72ck(ep72ck), .ep72trig(ep72trig), // input wire, input wire [31:0],
	.ep73ck(ep73ck), .ep73trig(ep73trig), // input wire, input wire [31:0],
	.ep74ck(ep74ck), .ep74trig(ep74trig), // input wire, input wire [31:0],
	.ep75ck(ep75ck), .ep75trig(ep75trig), // input wire, input wire [31:0],
	.ep76ck(ep76ck), .ep76trig(ep76trig), // input wire, input wire [31:0],
	.ep77ck(ep77ck), .ep77trig(ep77trig), // input wire, input wire [31:0],
	.ep78ck(ep78ck), .ep78trig(ep78trig), // input wire, input wire [31:0],
	.ep79ck(ep79ck), .ep79trig(ep79trig), // input wire, input wire [31:0],
	.ep7Ack(ep7Ack), .ep7Atrig(ep7Atrig), // input wire, input wire [31:0],
	.ep7Bck(ep7Bck), .ep7Btrig(ep7Btrig), // input wire, input wire [31:0],
	.ep7Cck(ep7Cck), .ep7Ctrig(ep7Ctrig), // input wire, input wire [31:0],
	.ep7Dck(ep7Dck), .ep7Dtrig(ep7Dtrig), // input wire, input wire [31:0],
	.ep7Eck(ep7Eck), .ep7Etrig(ep7Etrig), // input wire, input wire [31:0],
	.ep7Fck(ep7Fck), .ep7Ftrig(ep7Ftrig), // input wire, input wire [31:0],
	// Pipe In 		0x80 - 0x9F
	.ep80wr(ep80wr), .ep80pipe(ep80pipe), // output wire, output wire [31:0],
	.ep81wr(ep81wr), .ep81pipe(ep81pipe), // output wire, output wire [31:0],
	.ep82wr(ep82wr), .ep82pipe(ep82pipe), // output wire, output wire [31:0],
	.ep83wr(ep83wr), .ep83pipe(ep83pipe), // output wire, output wire [31:0],
	.ep84wr(ep84wr), .ep84pipe(ep84pipe), // output wire, output wire [31:0],
	.ep85wr(ep85wr), .ep85pipe(ep85pipe), // output wire, output wire [31:0],
	.ep86wr(ep86wr), .ep86pipe(ep86pipe), // output wire, output wire [31:0],
	.ep87wr(ep87wr), .ep87pipe(ep87pipe), // output wire, output wire [31:0],
	.ep88wr(ep88wr), .ep88pipe(ep88pipe), // output wire, output wire [31:0],
	.ep89wr(ep89wr), .ep89pipe(ep89pipe), // output wire, output wire [31:0],
	.ep8Awr(ep8Awr), .ep8Apipe(ep8Apipe), // output wire, output wire [31:0],
	.ep8Bwr(ep8Bwr), .ep8Bpipe(ep8Bpipe), // output wire, output wire [31:0],
	.ep8Cwr(ep8Cwr), .ep8Cpipe(ep8Cpipe), // output wire, output wire [31:0],
	.ep8Dwr(ep8Dwr), .ep8Dpipe(ep8Dpipe), // output wire, output wire [31:0],
	.ep8Ewr(ep8Ewr), .ep8Epipe(ep8Epipe), // output wire, output wire [31:0],
	.ep8Fwr(ep8Fwr), .ep8Fpipe(ep8Fpipe), // output wire, output wire [31:0],
	.ep90wr(ep90wr), .ep90pipe(ep90pipe), // output wire, output wire [31:0],
	.ep91wr(ep91wr), .ep91pipe(ep91pipe), // output wire, output wire [31:0],
	.ep92wr(ep92wr), .ep92pipe(ep92pipe), // output wire, output wire [31:0],
	.ep93wr(ep93wr), .ep93pipe(ep93pipe), // output wire, output wire [31:0],
	.ep94wr(ep94wr), .ep94pipe(ep94pipe), // output wire, output wire [31:0],
	.ep95wr(ep95wr), .ep95pipe(ep95pipe), // output wire, output wire [31:0],
	.ep96wr(ep96wr), .ep96pipe(ep96pipe), // output wire, output wire [31:0],
	.ep97wr(ep97wr), .ep97pipe(ep97pipe), // output wire, output wire [31:0],
	.ep98wr(ep98wr), .ep98pipe(ep98pipe), // output wire, output wire [31:0],
	.ep99wr(ep99wr), .ep99pipe(ep99pipe), // output wire, output wire [31:0],
	.ep9Awr(ep9Awr), .ep9Apipe(ep9Apipe), // output wire, output wire [31:0],
	.ep9Bwr(ep9Bwr), .ep9Bpipe(ep9Bpipe), // output wire, output wire [31:0],
	.ep9Cwr(ep9Cwr), .ep9Cpipe(ep9Cpipe), // output wire, output wire [31:0],
	.ep9Dwr(ep9Dwr), .ep9Dpipe(ep9Dpipe), // output wire, output wire [31:0],
	.ep9Ewr(ep9Ewr), .ep9Epipe(ep9Epipe), // output wire, output wire [31:0],
	.ep9Fwr(ep9Fwr), .ep9Fpipe(ep9Fpipe), // output wire, output wire [31:0],
	// Pipe Out 	0xA0 - 0xBF
	.epA0rd(epA0rd), .epA0pipe(epA0pipe), // output wire, input wire [31:0],
	.epA1rd(epA1rd), .epA1pipe(epA1pipe), // output wire, input wire [31:0],
	.epA2rd(epA2rd), .epA2pipe(epA2pipe), // output wire, input wire [31:0],
	.epA3rd(epA3rd), .epA3pipe(epA3pipe), // output wire, input wire [31:0],
	.epA4rd(epA4rd), .epA4pipe(epA4pipe), // output wire, input wire [31:0],
	.epA5rd(epA5rd), .epA5pipe(epA5pipe), // output wire, input wire [31:0],
	.epA6rd(epA6rd), .epA6pipe(epA6pipe), // output wire, input wire [31:0],
	.epA7rd(epA7rd), .epA7pipe(epA7pipe), // output wire, input wire [31:0],
	.epA8rd(epA8rd), .epA8pipe(epA8pipe), // output wire, input wire [31:0],
	.epA9rd(epA9rd), .epA9pipe(epA9pipe), // output wire, input wire [31:0],
	.epAArd(epAArd), .epAApipe(epAApipe), // output wire, input wire [31:0],
	.epABrd(epABrd), .epABpipe(epABpipe), // output wire, input wire [31:0],
	.epACrd(epACrd), .epACpipe(epACpipe), // output wire, input wire [31:0],
	.epADrd(epADrd), .epADpipe(epADpipe), // output wire, input wire [31:0],
	.epAErd(epAErd), .epAEpipe(epAEpipe), // output wire, input wire [31:0],
	.epAFrd(epAFrd), .epAFpipe(epAFpipe), // output wire, input wire [31:0],
	.epB0rd(epB0rd), .epB0pipe(epB0pipe), // output wire, input wire [31:0],
	.epB1rd(epB1rd), .epB1pipe(epB1pipe), // output wire, input wire [31:0],
	.epB2rd(epB2rd), .epB2pipe(epB2pipe), // output wire, input wire [31:0],
	.epB3rd(epB3rd), .epB3pipe(epB3pipe), // output wire, input wire [31:0],
	.epB4rd(epB4rd), .epB4pipe(epB4pipe), // output wire, input wire [31:0],
	.epB5rd(epB5rd), .epB5pipe(epB5pipe), // output wire, input wire [31:0],
	.epB6rd(epB6rd), .epB6pipe(epB6pipe), // output wire, input wire [31:0],
	.epB7rd(epB7rd), .epB7pipe(epB7pipe), // output wire, input wire [31:0],
	.epB8rd(epB8rd), .epB8pipe(epB8pipe), // output wire, input wire [31:0],
	.epB9rd(epB9rd), .epB9pipe(epB9pipe), // output wire, input wire [31:0],
	.epBArd(epBArd), .epBApipe(epBApipe), // output wire, input wire [31:0],
	.epBBrd(epBBrd), .epBBpipe(epBBpipe), // output wire, input wire [31:0],
	.epBCrd(epBCrd), .epBCpipe(epBCpipe), // output wire, input wire [31:0],
	.epBDrd(epBDrd), .epBDpipe(epBDpipe), // output wire, input wire [31:0],
	.epBErd(epBErd), .epBEpipe(epBEpipe), // output wire, input wire [31:0],
	.epBFrd(epBFrd), .epBFpipe(epBFpipe), // output wire, input wire [31:0],
	// 
	.okClk(okClk)//output wire okClk // sync with write/read of pipe
	);
////


/* BANK signals */
// test assignment 

/* DAC */
//
wire [15:0] DAC0_DAT = 16'b0;
wire        DAC0_DCI = 1'b0;
//
OBUFDS obufds_DAC0_DAT15_inst 	(.O(o_B34D_L21P),      .OB(o_B34D_L21N),      .I(DAC0_DAT[15])	); //
OBUFDS obufds_DAC0_DAT14_inst 	(.O(o_B34D_L19P),      .OB(o_B34D_L19N),      .I(DAC0_DAT[14])	); //
OBUFDS obufds_DAC0_DAT13_inst 	(.O(o_B34D_L23P),      .OB(o_B34D_L23N),      .I(DAC0_DAT[13])	); //
OBUFDS obufds_DAC0_DAT12_inst 	(.O(o_B34D_L15P),      .OB(o_B34D_L15N),      .I(DAC0_DAT[12])	); //
OBUFDS obufds_DAC0_DAT11_inst 	(.O(o_B34D_L13P_MRCC), .OB(o_B34D_L13N_MRCC), .I(DAC0_DAT[11])	); //
//
OBUFDS obufds_DAC0_DAT10_inst 	(.O(o_B34D_L24P),      .OB(o_B34D_L24N),      .I(DAC0_DAT[10])	); //
OBUFDS obufds_DAC0_DAT9_inst 	(.O(o_B34D_L17P),      .OB(o_B34D_L17N),      .I(DAC0_DAT[9])	); //
OBUFDS obufds_DAC0_DAT8_inst 	(.O(o_B34D_L16P),      .OB(o_B34D_L16N),      .I(DAC0_DAT[8])	); //
//
OBUFDS obufds_DAC0_DCI_inst 	(.O(o_B34D_L10P),      .OB(o_B34D_L10N),      .I(DAC0_DCI)	); //
//
OBUFDS obufds_DAC0_DAT7_inst 	(.O(o_B34D_L20P),      .OB(o_B34D_L20N),      .I(DAC0_DAT[7])	); //
OBUFDS obufds_DAC0_DAT6_inst 	(.O(o_B34D_L3P),       .OB(o_B34D_L3N),       .I(DAC0_DAT[6])	); //
OBUFDS obufds_DAC0_DAT5_inst 	(.O(o_B34D_L9P),       .OB(o_B34D_L9N),       .I(DAC0_DAT[5])	); //
OBUFDS obufds_DAC0_DAT4_inst 	(.O(o_B34D_L2P),       .OB(o_B34D_L2N),       .I(DAC0_DAT[4])	); //
OBUFDS obufds_DAC0_DAT3_inst 	(.O(o_B34D_L4P),       .OB(o_B34D_L4N),       .I(DAC0_DAT[3])	); //
OBUFDS obufds_DAC0_DAT2_inst 	(.O(o_B34D_L1P),       .OB(o_B34D_L1N),       .I(DAC0_DAT[2])	); //
OBUFDS obufds_DAC0_DAT1_inst 	(.O(o_B34D_L7P),       .OB(o_B34D_L7N),       .I(DAC0_DAT[1])	); //
OBUFDS obufds_DAC0_DAT0_inst 	(.O(o_B34D_L12P_MRCC), .OB(o_B34D_L12N_MRCC), .I(DAC0_DAT[0])	); //
//
wire DAC0_DCO;
wire c_DAC0_DCO;
IBUFDS ibufds_DAC0_DCO_inst (.I(c_B34D_L14P_SRCC), .IB(c_B34D_L14N_SRCC), .O(c_DAC0_DCO) );
BUFG     bufg_DAC0_DCO_inst (.I(c_DAC0_DCO), .O(DAC0_DCO) ); 
//
assign dac0_dco_clk_in = DAC0_DCO; // for DAC1 400MHz pll
//
wire [15:0] DAC1_DAT = 16'b0;
wire        DAC1_DCI = 1'b0;
//
OBUFDS obufds_DAC1_DAT15_inst 	(.O(o_B35D_L24P),      .OB(o_B35D_L24N),      .I(DAC1_DAT[15])	); //
OBUFDS obufds_DAC1_DAT14_inst 	(.O(o_B35D_L22P),      .OB(o_B35D_L22N),      .I(DAC1_DAT[14])	); //
OBUFDS obufds_DAC1_DAT13_inst 	(.O(o_B35D_L20P),      .OB(o_B35D_L20N),      .I(DAC1_DAT[13])	); //
OBUFDS obufds_DAC1_DAT12_inst 	(.O(o_B35D_L16P),      .OB(o_B35D_L16N),      .I(DAC1_DAT[12])	); //
OBUFDS obufds_DAC1_DAT11_inst 	(.O(o_B35D_L17P),      .OB(o_B35D_L17N),      .I(DAC1_DAT[11])	); //
//
OBUFDS obufds_DAC1_DAT10_inst 	(.O(o_B35D_L21P),      .OB(o_B35D_L21N),      .I(DAC1_DAT[10])	); //
OBUFDS obufds_DAC1_DAT9_inst 	(.O(o_B35D_L19P),      .OB(o_B35D_L19N),      .I(DAC1_DAT[9])	); //
OBUFDS obufds_DAC1_DAT8_inst 	(.O(o_B35D_L18P),      .OB(o_B35D_L18N),      .I(DAC1_DAT[8])	); //
//
OBUFDS obufds_DAC1_DCI_inst 	(.O(o_B35D_L23P),      .OB(o_B35D_L23N),      .I(DAC1_DCI)	); //
//
OBUFDS obufds_DAC1_DAT7_inst 	(.O(o_B35D_L15P),      .OB(o_B35D_L15N),      .I(DAC1_DAT[7])	); //
OBUFDS obufds_DAC1_DAT6_inst 	(.O(o_B35D_L9P),       .OB(o_B35D_L9N),       .I(DAC1_DAT[6])	); //
OBUFDS obufds_DAC1_DAT5_inst 	(.O(o_B35D_L7P),       .OB(o_B35D_L7N),       .I(DAC1_DAT[5])	); //
OBUFDS obufds_DAC1_DAT4_inst 	(.O(o_B35D_L4P),       .OB(o_B35D_L4N),       .I(DAC1_DAT[4])	); //
OBUFDS obufds_DAC1_DAT3_inst 	(.O(o_B35D_L6P),       .OB(o_B35D_L6N),       .I(DAC1_DAT[3])	); //
OBUFDS obufds_DAC1_DAT2_inst 	(.O(o_B35D_L1P),       .OB(o_B35D_L1N),       .I(DAC1_DAT[2])	); //
OBUFDS obufds_DAC1_DAT1_inst 	(.O(o_B35D_L13P_MRCC), .OB(o_B35D_L13N_MRCC), .I(DAC1_DAT[1])	); //
OBUFDS obufds_DAC1_DAT0_inst 	(.O(o_B35D_L12P_MRCC), .OB(o_B35D_L12N_MRCC), .I(DAC1_DAT[0])	); //
//
wire DAC1_DCO;
wire c_DAC1_DCO;
IBUFDS ibufds_DAC1_DCO_inst (.I(c_B35D_L11P_SRCC), .IB(c_B35D_L11N_SRCC), .O(c_DAC1_DCO) );
BUFG     bufg_DAC1_DCO_inst (.I(c_DAC1_DCO), .O(DAC1_DCO) ); 
//
assign dac1_dco_clk_in = DAC1_DCO; // for DAC1 400MHz pll
//
wire DACx_RST_B = 1'b0;
OBUF obuf_DACx_RST_B_inst (.O(o_B13_SYS_CLK_MC1 ), .I(DACx_RST_B ) ); // 
//
wire DAC0_CS    = 1'b0;
wire DACx_SCLK  = 1'b0;
wire DACx_SDIO  = 1'b0;
wire DACx_SDO   ;
wire DAC1_CS    = 1'b0;
//
OBUF obuf_DAC0_CS_inst   (.O(o_B13_L5P         ), .I(DAC0_CS    ) ); // 
OBUF obuf_DACx_SCLK_inst (.O(o_B13_L5N         ), .I(DACx_SCLK  ) ); // 
OBUF obuf_DACx_SDIO_inst (.O(o_B13_L3P         ), .I(DACx_SDIO  ) ); // 
IBUF ibuf_DACx_SDO_inst  (.I(i_B13_L3N         ), .O(DACx_SDO   ) ); //
OBUF obuf_DAC1_CS_inst   (.O(o_B13_L16P        ), .I(DAC1_CS    ) ); // 
//


/* SPIO */
//
wire SPIO0_CS   = 1'b0;
wire SPIOx_SCLK = 1'b0;
wire SPIOx_MOSI = 1'b0;
wire SPIOx_MISO ;
wire SPIO1_CS   = 1'b0;
//
OBUF obuf_SPIO0_CS_inst   (.O(o_B13_L2P         ), .I(SPIO0_CS   ) ); // 
OBUF obuf_SPIOx_SCLK_inst (.O(o_B13_L2N         ), .I(SPIOx_SCLK ) ); // 
OBUF obuf_SPIOx_MOSI_inst (.O(o_B13_L4P         ), .I(SPIOx_MOSI ) ); // 
IBUF ibuf_SPIOx_MISO_inst (.I(i_B13_L4N         ), .O(SPIOx_MISO ) ); //
OBUF obuf_SPIO1_CS_inst   (.O(o_B13_L1P         ), .I(SPIO1_CS   ) ); // 
//


/* ADC */
//
wire   ADC0_DCO;
wire c_ADC0_DCO;
IBUFDS ibufds_ADC0_DCO_inst (.I(c_B34D_L11P_SRCC), .IB(c_B34D_L11N_SRCC), .O(c_ADC0_DCO) );
BUFG     bufg_ADC0_DCO_inst (.I(c_ADC0_DCO), .O(ADC0_DCO) ); 
//
wire ADC0_DA;
wire ADC0_DB;
IBUFDS ibufds_ADC0_DA_inst (.I(i_B34D_L18P), .IB(i_B34D_L18N), .O(ADC0_DA) );
IBUFDS ibufds_ADC0_DB_inst (.I(i_B34D_L22P), .IB(i_B34D_L22N), .O(ADC0_DB) );
//
wire ADCx_CNV   = 1'b0;
wire ADCx_CLK   = 1'b0;
OBUFDS obufds_ADCx_CNV_inst 	(.O(o_B34D_L6P), .OB(o_B34D_L6N), .I(ADCx_CNV)	); //
OBUFDS obufds_ADCx_CLK_inst 	(.O(o_B34D_L8P), .OB(o_B34D_L8N), .I(ADCx_CLK)	); //
wire ADCx_TPT_B = 1'b0;
OBUF obuf_ADCx_TPT_B_inst   (.O(o_B34_L5P         ), .I(ADCx_TPT_B   ) ); // 
//
wire   ADC1_DCO;
wire c_ADC1_DCO;
IBUFDS ibufds_ADC1_DCO_inst (.I(c_B35D_L14P_SRCC), .IB(c_B35D_L14N_SRCC), .O(c_ADC1_DCO) );
BUFG     bufg_ADC1_DCO_inst (.I(c_ADC1_DCO), .O(ADC1_DCO) ); 
//
wire ADC1_DA;
wire ADC1_DB;
IBUFDS ibufds_ADC1_DA_inst (.I(i_B35D_L10P), .IB(i_B35D_L10N), .O(ADC1_DA) );
IBUFDS ibufds_ADC1_DB_inst (.I(i_B35D_L8P ), .IB(i_B35D_L8N ), .O(ADC1_DB) );
//


/* S_IO */
//
wire   S_IO_0;
wire w_S_IO_0_wr = 1'b1;
wire   S_IO_1;
wire w_S_IO_1_wr = 1'b1;
wire   S_IO_2;
wire w_S_IO_2_wr = 1'b1;
//IBUF ibuf_S_IO_0_inst  (.I(io_B34_L5N  ), .O(S_IO_0 ) ); //
//IBUF ibuf_S_IO_1_inst  (.I(io_B13_L16N ), .O(S_IO_1 ) ); //
//IBUF ibuf_S_IO_2_inst  (.I(io_B13_L1N  ), .O(S_IO_2 ) ); //
IOBUF iobuf_S_IO_0_inst  (.IO(io_B34_L5N  ), .T(w_S_IO_0_wr), .I(w_S_IO_0_wr ), .O(S_IO_0 ) ); //
IOBUF iobuf_S_IO_1_inst  (.IO(io_B13_L16N ), .T(w_S_IO_1_wr), .I(w_S_IO_1_wr ), .O(S_IO_1 ) ); //
IOBUF iobuf_S_IO_2_inst  (.IO(io_B13_L1N  ), .T(w_S_IO_2_wr), .I(w_S_IO_2_wr ), .O(S_IO_2 ) ); //
//


/* CLK */
//
wire CLK_REFM;
IBUF ibuf_CLK_REFM_inst (.I(i_B13_SYS_CLK_MC2         ), .O(CLK_REFM ) ); //
//
wire   CLK_COUT;
wire c_CLK_COUT;
IBUFDS ibufds_CLK_COUT_inst (.I(c_B13D_L13P_MRCC), .IB(c_B13D_L13N_MRCC), .O(c_CLK_COUT) );
BUFG     bufg_CLK_COUT_inst (.I(c_CLK_COUT), .O(CLK_COUT) ); 
//
assign clk_dac_clk_in = CLK_COUT; // for DAC/CLK 400MHz pll
//
wire CLK_RST_B = 1'b0;
wire CLK_LD;
wire CLK_STAT;
wire CLK_SYNC = 1'b0;
OBUF obuf_CLK_RST_B_inst (.O(o_B35_IO0         ), .I(CLK_RST_B  ) ); // 
IBUF ibuf_CLK_LD_inst    (.I(i_B35_IO25        ), .O(CLK_LD     ) ); //
IBUF ibuf_CLK_STAT_inst  (.I(i_B35_L2P         ), .O(CLK_STAT   ) ); //
OBUF obuf_CLK_SYNC_inst  (.O(o_B35_L2N         ), .I(CLK_SYNC   ) ); // 
//
wire CLK_SDIO = 1'b0;
wire CLK_SDO  ;
wire CLK_CS_B = 1'b0;
wire CLK_SCLK = 1'b0;
//
OBUF obuf_CLK_SDIO_inst (.O(o_B35_L5P ), .I(CLK_SDIO ) ); // 
IBUF ibuf_CLK_SDO__inst (.I(i_B35_L5N ), .O(CLK_SDO  ) ); //
OBUF obuf_CLK_CS_B_inst (.O(o_B35_L3P ), .I(CLK_CS_B ) ); // 
OBUF obuf_CLK_SCLK_inst (.O(o_B35_L3N ), .I(CLK_SCLK ) ); // 

/* LAN */
//
wire LAN_MISO ;
wire LAN_RSTn = 1'b0;
wire LAN_INTn ;
wire LAN_SSNn = 1'b0;
wire LAN_SCLK = 1'b0;
wire LAN_MOSI = 1'b0;
//
IBUF ibuf_LAN_MISO_inst (.I(i_B13_L17P      ), .O(LAN_MISO ) ); //
OBUF obuf_LAN_RSTn_inst (.O(o_B13_L17N      ), .I(LAN_RSTn ) ); // 
IBUF ibuf_LAN_INTn_inst (.I(i_B13_L11P_SRCC ), .O(LAN_INTn ) ); //
OBUF obuf_LAN_SSNn_inst (.O(o_B13_L6P       ), .I(LAN_SSNn ) ); // 
OBUF obuf_LAN_SCLK_inst (.O(o_B13_L6N       ), .I(LAN_SCLK ) ); // 
OBUF obuf_LAN_MOSI_inst (.O(o_B13_L11N_SRCC ), .I(LAN_MOSI ) ); // 
//


/* TRIG */
//
wire   TRIG_IN;
IBUFDS ibufds_TRIG_IN_inst  (.I(i_B13D_L14P_SRCC), .IB(i_B13D_L14N_SRCC), .O(TRIG_IN) );
//
wire   TRIG_OUT = w_oddr_out; // test assignment
//wire   TRIG_OUT = 1'b0;
OBUFDS obufds_TRIG_OUT_inst (.O(o_B13D_L15P), .OB(o_B13D_L15N), .I(TRIG_OUT)	); //


//// nets for BUFG clocks
//wire c_dco_adc_0;
//wire c_dco_adc_1;
//wire c_dco_adc_2;
//wire c_dco_adc_3;
//// adc0
//IBUFDS ibufds_ADC_00_DB_inst (.I(i_B34D_L22P), .IB(i_B34D_L22N), .O(w_dat1_adc_0) );
//// adc1
//IBUFDS ibufds_ADC_01_DCO_inst (.I(c_B34D_L14P_SRCC), .IB(c_B34D_L14N_SRCC), .O(c_dco_adc_1) ); // check polarity
//BUFG     bufg_ADC_01_DCO_inst (.I(c_dco_adc_1), .O(w_dco_adc_1) ); 
//IBUFDS ibufds_ADC_01_DA_inst (.I(i_B34D_L16P), .IB(i_B34D_L16N), .O(w_dat2_adc_1) );
//IBUFDS ibufds_ADC_01_DB_inst (.I(i_B34D_L17P), .IB(i_B34D_L17N), .O(w_dat1_adc_1) );
//// adc2 ... not used
//IBUFDS ibufds_ADC_10_DCO_inst (.I(c_B34D_L12P_MRCC), .IB(c_B34D_L12N_MRCC), .O(c_dco_adc_2) );
//BUFG     bufg_ADC_10_DCO_inst (.I(c_dco_adc_2), .O(w_dco_adc_2) ); 
//IBUFDS ibufds_ADC_10_DA_inst (.I(i_B34D_L7P), .IB(i_B34D_L7N), .O(w_dat2_adc_2) );
//IBUFDS ibufds_ADC_10_DB_inst (.I(i_B34D_L1P), .IB(i_B34D_L1N), .O(w_dat1_adc_2) );
//// adc3 ... not used
//IBUFDS ibufds_ADC_11_DCO_inst (.I(c_B34D_L13P_MRCC), .IB(c_B34D_L13N_MRCC), .O(c_dco_adc_3) ); 
//BUFG     bufg_ADC_11_DCO_inst (.I(c_dco_adc_3), .O(w_dco_adc_3) ); 
//IBUFDS ibufds_ADC_11_DA_inst (.I(i_B34D_L15P), .IB(i_B34D_L15N), .O(w_dat2_adc_3) );
//IBUFDS ibufds_ADC_11_DB_inst (.I(i_B34D_L23P), .IB(i_B34D_L23N), .O(w_dat1_adc_3) );
////
//wire w_ADC_XX_CLK = w_clk_adc;
//OBUFDS obufds_ADC_XX_CLK_inst 		(.O(o_B34D_L19P), .OB(o_B34D_L19N), .I(w_ADC_XX_CLK)		); // ADC_XX_CLK
////
//wire w_ADC_XX_CNV;
//	assign w_ADC_XX_CNV = (w_enable__pulse_loopback)? w_pulse_out : w_cnv_adc;
//OBUFDS obufds_ADC_XX_CNV_inst 		(.O(o_B34D_L21P), .OB(o_B34D_L21N), .I(w_ADC_XX_CNV)		); // ADC_XX_CNV
////
//OBUF obuf_ADC_XX_TEST_B_inst		(.O(o_B34_L24P), .I(~w_pin_test_adc)); // ADC_XX_TEST_B
//OBUF obuf_ADC_XX_DUAL_LANE_B_inst 	(.O(o_B34_L24N), .I(~w_pin_dln_adc)	); // ADC_XX_DUAL_LANE_B
//// loopback test
//wire w_OSC_IN;
//	assign w_pulse_loopback_in = w_OSC_IN;
//IBUFDS ibufds_OSC_IN_inst (.I(i_B35D_L12P_MRCC), .IB(i_B35D_L12N_MRCC), .O(w_OSC_IN) ); 
//////

/* LAN */
////wire w_LAN_PWDN; // unused 
//// input  wire			i_B13_SYS_CLK_MC2, //$$ LAN_PWDN // unused in WIZ850io // used in WIZ820io
////   ... LAN_PWDN --> input for no use.
//OBUF obuf_o_B35_L21N_inst  (.O(o_B35_L21N ), .I(w_LAN_RSTn) ); // w_LAN_RSTn
//OBUF obuf_o_B35_IO0_inst   (.O(o_B35_IO0  ), .I(w_LAN_SCSn) ); // w_LAN_SCSn
//OBUF obuf_o_B35_IO25_inst  (.O(o_B35_IO25 ), .I(w_LAN_SCLK) ); // w_LAN_SCLK
//OBUF obuf_o_B35_L24P_inst  (.O(o_B35_L24P ), .I(w_LAN_MOSI) ); // w_LAN_MOSI
//OBUF obuf_o_B35_L24N_inst  (.O(o_B35_L24N ), .I(w_LAN_INTn) ); // w_LAN_INTn // input!! to be 
////   ... LAN_INTn --> ouput with 1 for idle // <-- wrong comment
//IBUF ibuf_i_B35_L21P_inst  (.I(i_B35_L21P ), .O(w_LAN_MISO) ); // w_LAN_MISO

/* TEMP SENSOR */
//// support MAX6576ZUT+T
//// temp signal count by 12MHz
//// net in sch: FPGA_IO_B
//// pin: i_B35_L6P
//// check signal on debugger 
//// test
//(* keep = "true" *) wire w_temp_sig;
//(* keep = "true" *) reg r_temp_sig;
//(* keep = "true" *) reg r_toggle_temp_sig;
//(* keep = "true" *) wire w_rise_temp_sig = ~r_temp_sig & w_temp_sig;
////reg [15:0] r_subcnt_temp_sig_period;
////reg [15:0] r_period_temp_sig_period;
////
//IBUF ibuf_i_B35_L6P_inst  (.I(i_B35_L6P ), .O(w_temp_sig) ); // w_temp_sig
////
//wire tmps_clk = clk3_out3_12M;
//// 
////
//always @(posedge tmps_clk, negedge reset_n) begin
//	if (!reset_n) begin
//		r_temp_sig     <= 1'b0;
//		r_toggle_temp_sig  <= 1'b0;
//		end
//	else begin
//		//
//		r_temp_sig     <= w_temp_sig;
//		//
//		if (w_rise_temp_sig) begin 
//			r_toggle_temp_sig <= ~r_toggle_temp_sig;
//			end
//		end
//end



// reserved or tempory output 
//OBUF obuf_TMP_____________inst (.O(o_Bxx_LxxP         ), .I(1'b0 ) ); // TMP


/* ddr3 io */
// ddr3 test assignment 
//IBUFDS IBUFDS_ddr3_dqs0_inst (.I(ddr3_dqs_p[0]), .IB(ddr3_dqs_n[0]), .O() ); // unused
//IBUFDS IBUFDS_ddr3_dqs1_inst (.I(ddr3_dqs_p[1]), .IB(ddr3_dqs_n[1]), .O() ); // unused
//IBUFDS IBUFDS_ddr3_dqs2_inst (.I(ddr3_dqs_p[2]), .IB(ddr3_dqs_n[2]), .O() ); // unused
//IBUFDS IBUFDS_ddr3_dqs3_inst (.I(ddr3_dqs_p[3]), .IB(ddr3_dqs_n[3]), .O() ); // unused
//OBUFDS OBUFDS_ddr3_ck___inst (.O(ddr3_ck_p[0] ), .OB(ddr3_ck_n[0] ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_00_inst (.O(ddr3_addr[ 0]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_01_inst (.O(ddr3_addr[ 1]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_02_inst (.O(ddr3_addr[ 2]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_03_inst (.O(ddr3_addr[ 3]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_04_inst (.O(ddr3_addr[ 4]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_05_inst (.O(ddr3_addr[ 5]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_06_inst (.O(ddr3_addr[ 6]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_07_inst (.O(ddr3_addr[ 7]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_08_inst (.O(ddr3_addr[ 8]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_09_inst (.O(ddr3_addr[ 9]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_10_inst (.O(ddr3_addr[10]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_11_inst (.O(ddr3_addr[11]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_12_inst (.O(ddr3_addr[12]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_13_inst (.O(ddr3_addr[13]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_14_inst (.O(ddr3_addr[14]), .I(1'b0) ); // unused
//
//OBUF OBUF_ddr3_ba_0____inst (.O(ddr3_ba[0]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_ba_1____inst (.O(ddr3_ba[1]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_ba_2____inst (.O(ddr3_ba[2]   ), .I(1'b0) ); // unused
//
//OBUF OBUF_ddr3_cke_0___inst (.O(ddr3_cke[0]  ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_cas_n___inst (.O(ddr3_cas_n   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_ras_n___inst (.O(ddr3_ras_n   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_we_n____inst (.O(ddr3_we_n    ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_odt_0___inst (.O(ddr3_odt[0]  ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_dm_0____inst (.O(ddr3_dm[0]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_dm_1____inst (.O(ddr3_dm[1]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_dm_2____inst (.O(ddr3_dm[2]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_dm_3____inst (.O(ddr3_dm[3]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_reset_n_inst (.O(ddr3_reset_n ), .I(1'b0) ); // unused
////

endmodule
