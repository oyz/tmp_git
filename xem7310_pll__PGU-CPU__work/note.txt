== PGU-CPU-F5500 ==

=== info ===
* fpga image id: __B0_19_10DD
* purpose: pinmap test for PGU-CPU-F5500
* git branch: pgu_cpu_work__B0_pinmap
* git tag: pgu_cpu_work__B0_19_10DD (to-be)

=== new features ===
* pin map for PGU-CPU-F5500
* PLL setup 
* DAC test count up pattern generation @400MHz

=== main features ===
...

##* USB interface
##* 210MHz ADC base freq to support sampling rate 15Msps
##* 160MHz DWAVE base freq to support test waveform 20MHz
##* LAN interface to support SCPI commands

=== design top files ===

* FPGA project file 
	- for PC 
		D:/temp/local_rep_git/xem7310_pll__PGU-CPU__work/xem7310_pll.xpr
	- for note PC
		C:/temp/tmp_local_git/xem7310_pll__PGU-CPU__work/xem7310_pll.xpr
	
* location of main directory
	\xem7310_pll__PGU-CPU__work

...



##* Xilinx vivado project file 	
##	\xem7310_pll__CMU-CPU__work\xem7310_pll.xpr
##* RTL verilog top file 
##	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\sources_1\imports\src_ref\xem7310__cmu_cpu__top.v
##
##* XSDK project top location 
##	\xem7310_pll__CMU-CPU__work\xem7310_pll.sdk
##* XSDK CMU-CPU-TEST-F5500 configuration
##	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\_sdk\src\CMU_CPU_TOP\cmu_cpu_config.h
##* SCPI server design top file 
##	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\_sdk\src\test_spi_wz850\test_spi_wz850.c
##* FPGA image + application mem file 
##	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\_bit\download__mcs_ep_SBT20190322_C8190320.bit
##
##* Python test batch file 
##	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\ok_cmu_cpu__batch.py
##* Python ADC test top file 
##	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\LAN_cmu_cpu__10_self_test.py
##* Python LAN test top file 
##	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\tcp_echo\scpi-client3.py
##* Python library for LAN interface 
##	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\LAN_cmu_cpu__lib.py
##* Python CMU-CPU-TEST-F5500 configuration
##	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\ok_cmu_cpu__lib_conf.py
##
##* Python test batch file for adc mismatch 
##	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\ok_cmu_cpu__batch_a__adc_mismatch.py
##* Python ADC test top file for adc mismatch
##	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\ok_cmu_cpu__10_self_test_a__adc_mismatch.py
##* Python post-process file for adc mismatch
##	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\postproc_csv_files__cmu_cpu__rev.py
##* Python report file for adc mismatch
##	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\read_postproc_files__cmu_cpu.py
	
	
=== FPGA image (bit) files ===
...
