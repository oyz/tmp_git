# postproc_csv_files__cmu_cpu__freq_plot.py
#  
#  read DUMP_* files 
#  output RPT_* file
#
#  list csv files in the directory
#  read temperature value and dual adc data from each file: TEMP, x[n], y[n]
#  calculate DFTs: X[f] = DFT(x[n]), Y[f] = DFT(y[n])
#  collect responses at a given test freq f_t : X[f_t], Y[f_t]
#  calculate ratio: R[F_t] = Y[f_t] / X[f_t]
#  collect magnitude and phase: Mag(X[F_t]), Mag(Y[F_t]), Mag(R[F_t]), Arg(R[F_t])
#  report them into a csv file
#
#  rev0: draft
#
#  https://www.quora.com/How-does-one-extract-arrays-from-CSV-files-with-columns-in-Python
#
#  https://www.devdungeon.com/content/working-binary-data-python
#  https://pyformat.info/
#  https://matplotlib.org/api/_as_gen/matplotlib.pyplot.psd.html#matplotlib.pyplot.psd
#    https://matplotlib.org/gallery/lines_bars_and_markers/psd_demo.html#sphx-glr-gallery-lines-bars-and-markers-psd-demo-py
#
# for import numpy: https://scipy.org/install.html
#   python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
# for import multiarray:
#   https://stackoverflow.com/questions/21324426/numpy-build-fails-with-cannot-import-multiarray
#   pip install numpy
#   pip install matplotlib
#
# https://matplotlib.org/users/tight_layout_guide.html
#
# https://pythonspot.com/tk-file-dialogs/
#
#
# glob - get only txt files
# >>> import glob
# >>> x = glob.glob("*.txt")
# >>> x
# ['ale.txt', 'alunni2015.txt', 'assenze.text.txt', 'text2.txt', 'untitled.txt']
# Using glob to get the full path of the files
# If I should need the absolute path of the files:
# 
# >>> from path import path
# >>> from glob import glob
# >>> x = [path(f).abspath() for f in glob("F:\\*.txt")]
# >>> for f in x:
# ...  print(f)
# ...
# F:\acquistionline.txt
# F:\acquisti_2018.txt
# F:\bootstrap_jquery_ecc.txt



## load library/module
import csv
import numpy as np
import matplotlib.pyplot as plt
plt.ion() # matplotlib interactive mode 
#plt.close('all')
from sys import exit
import tkinter as tk
from tkinter import filedialog
import os
import datetime
#
import glob # for filenames

## controls
#

## parameters
#  


## functions

def is_int(str):
	try:
		#tmp = int(str)
		int(str)
		return True
	except ValueError:
		return False

def is_float(str):
	try:
		#tmp = float(str)
		float(str)
		return True
	except ValueError:
		return False

def is_complex(str):
	try:
		#tmp = complex(str)
		complex(str)
		return True
	except ValueError:
		return False


## start


## list csv files
#LOC_CSV = '*.csv'
#
# directory prefix ...  ########
DIR_PRE = '' # current dir 
#
#DIR_PRE = 'DUMP_BD12345__TS_20190720T120503__NP/'
#DIR_PRE = 'DUMP_BD12345__TS_20190719T085350__NP/'
#DIR_PRE = 'DUMP_BD12345__TS_20190714T000348__NP/'
#DIR_PRE = 'DUMP_BD12345__TS_20190712T071626__NP/'
#DIR_PRE = 'DUMP_BD12345__TS_20190711T072400__NP/'
#DIR_PRE = 'DUMP_BD12345__TS_20190710T052645__NP/'
#DIR_PRE = 'DUMP_BD2345__TS_20190707T063325__NP/'
#DIR_PRE = 'DUMP_BD45__TS_20190705T022359__NP/'
#DIR_PRE = 'DUMP_BD45__TS_20190705T022525__XP/'
#DIR_PRE = 'DUMP_BD1234__TS_20190703T021532__NP/'
#DIR_PRE = 'DUMP_BD1234__TS_20190704T032416__XP/'
#
#DIR_PRE = 'DUMP_BD2345___TS_20190630T124119/'
#DIR_PRE = 'DUMP_BD1___TS_20190629T090046__N_ports/'
#DIR_PRE = 'DUMP_BD1___TS_20190629T144157__X_ports/'
#DIR_PRE = 'DUMP_BD1___TS_20190627T214245__normal_ports/'
#DIR_PRE = 'DUMP_BD1___TS_20190627T152528/'
#DIR_PRE = 'DUMP_BD1___TS_20190624T090908__undersample/'
#DIR_PRE = 'DUMP___TS_20190610T172222__BD135/'

# report file prefix 
#RRT_FILE_PRE  = 'RPT_'
RRT_FILE_PRE  = 'RPT_NP_' ##
#RRT_FILE_PRE  = 'RPT_XP_' ##


# file prefix 
#FILE_PRE = 'DUMP_'
#
FILE_PRE_list = [
#	'DUMP_BD1__', #
#	'DUMP_BD2__', ##
#	'DUMP_BD3__', ##
#	'DUMP_BD4__', #
#	'DUMP_BD5__', #
	'DUMP_BD1_731__', #
	'DUMP_BD5_531__', #
	]


# report timestamp
#TS_FIXED = '' # for now() ##
#
TS_FIXED = '20190725T151515'
#TS_FIXED = '20190720T0121212' ## DUMP_BD12345__TS_20190720T120503__NP
#TS_FIXED = '20190719T090909' ## DUMP_BD12345__TS_20190719T085350__NP
#TS_FIXED = '20190715T101010' ## DUMP_BD12345__TS_20190714T000348__NP
#TS_FIXED = '20190712T161616' ## DUMP_BD12345__TS_20190712T071626__NP
#TS_FIXED = '20190711T111111' ## DUMP_BD12345__TS_20190711T072400__NP
#TS_FIXED = '20190710T191919' ## DUMP_BD12345__TS_20190710T052645__NP
#TS_FIXED = '20190708T090909' ## DUMP_BD2345__TS_20190707T063325__NP
#
#TS_FIXED = '20190706T121212' ## DUMP_BD45__TS_20190705T022359__NP
#TS_FIXED = '20190705T131313' ## DUMP_BD45__TS_20190705T022525__XP
#
#TS_FIXED = '20190704T161616' ## DUMP_BD1234__TS_20190703T021532__NP  DUMP_BD1234__TS_20190704T032416__XP

## case info 
case_info__freq_plot = [
	{'case_code' : 'N_001K_210_15_'},
	{'case_code' : 'N_002K_210_15_'},
	{'case_code' : 'N_005K_210_14_'},
	{'case_code' : 'U_010K_210_21010_'},
	{'case_code' : 'U_020K_210_10505_'},
	{'case_code' : 'U_050K_210_4202_' },
	{'case_code' : 'U_100K_210_2101_' },
	{'case_code' : 'U_200K_210_1051_' }, 
	{'case_code' : 'U_500K_210_421_'  },
	{'case_code' : 'U_001M_210_211_'  },
	{'case_code' : 'U_002M_210_106_'  },
	{'case_code' : 'U_005M_210_43_'   },
	{'case_code' : 'U_010M_210_43_'   },
	]

case_info__freq_plot__old = [
	#{'case_code' : 'N_001K_210_14_'},
	{'case_code' : 'N_001K_210_15_'},
	#{'case_code' : 'N_002K_210_14_'},
	{'case_code' : 'N_002K_210_15_'},
	{'case_code' : 'N_005K_210_14_'},
	#{'case_code' : 'N_005K_210_15_'},
	#{'case_code' : 'U_001K_210_210100_'}, #
	#{'case_code' : 'U_002K_210_105050_'}, #
	#{'case_code' : 'U_005K_210_42020_' }, #
	{'case_code' : 'U_010K_210_21010_'},
	#{'case_code' : 'U_010K_210_21014_'},
	#{'case_code' : 'U_010K_210_21007_'},
	#{'case_code' : 'U_020K_210_10507_'},
	{'case_code' : 'U_020K_210_10505_'},
	#{'case_code' : 'U_020K_210_10501_'},
	#{'case_code' : 'U_050K_210_4203_' },
	{'case_code' : 'U_050K_210_4202_' },
	{'case_code' : 'U_100K_210_2101_' },
	#{'case_code' : 'U_100K_210_2102_' },
	{'case_code' : 'U_200K_210_1051_' },
	{'case_code' : 'U_500K_210_421_'  },
	{'case_code' : 'U_001M_210_211_'  },
	{'case_code' : 'U_002M_210_106_'  },
	{'case_code' : 'U_005M_210_43_'   },
	{'case_code' : 'U_010M_210_43_'   },
	]

#
print(len(case_info__freq_plot))
print(case_info__freq_plot[0]['case_code'])


## class for DFT setting
class c_DFT_info:
	""" variables """
	
	# setup dict for test freq and DFT sample number
	TEST_FREQ_HZ_dict = {}
	SAMPLES_DFT_dict  = {}

	####
	# // TODO: coherent condition setting for normal sampling
	#
	
	#TEST_CASE__N_001K
	######################################
	#TEST_CASE__N_001K_210_2100__EN = 1  # FS_100K = 210e6/2100 # // 210	2100	100	1	0.100	131072	1310.72	1307	130700.00
	TEST_FREQ_HZ_dict ['N_001K_210_2100_'] = 1e3
	#
	SAMPLES_DFT_dict ['N_001K_210_2100_'+'128K_'] = 130700 # 1307 #
	SAMPLES_DFT_dict ['N_001K_210_2100_'+'64K_']  = 65300  # 653  #
	SAMPLES_DFT_dict ['N_001K_210_2100_'+'32K_']  = 31700  # 317  # 200ppm // outlier 0.2%
	SAMPLES_DFT_dict ['N_001K_210_2100_'+'16K_']  = 16300  # 163  # 200ppm // outlier 0.3%
	SAMPLES_DFT_dict ['N_001K_210_2100_'+'8K_']   = 7900   # 79   #
	SAMPLES_DFT_dict ['N_001K_210_2100_'+'4K_']   = 3700   # 37   #
	SAMPLES_DFT_dict ['N_001K_210_2100_'+'2K_']   = 1900   # 19   #
	SAMPLES_DFT_dict ['N_001K_210_2100_'+'1K_']   = 700    # 7    #
	######################################
	#
	######################################
	#TEST_CASE__N_001K_210_1400__EN = 1  # FS_150K = 210e6/1400 # //  210	1400	150	1	0.150	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['N_001K_210_1400_'] = 1e3
	#
	SAMPLES_DFT_dict ['N_001K_210_1400_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['N_001K_210_1400_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['N_001K_210_1400_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['N_001K_210_1400_'+'16K_']  = 16350  # 109  # 4%
	SAMPLES_DFT_dict ['N_001K_210_1400_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['N_001K_210_1400_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['N_001K_210_1400_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['N_001K_210_1400_'+'1K_']   = 750    # 5    #
	######################################
	#
	######################################
	#TEST_CASE__N_001K_210_14__EN   = 1  # FS_15M  = 210e6/14   # //  210	14	15000	1	15.000	131072	8.738133333	7	105000.00
	TEST_FREQ_HZ_dict ['N_001K_210_14_'] = 1e3
	#
	SAMPLES_DFT_dict ['N_001K_210_14_'+'128K_'] = 105000 # 7  #
	SAMPLES_DFT_dict ['N_001K_210_14_'+'64K_']  = 45000  # 3  #
	SAMPLES_DFT_dict ['N_001K_210_14_'+'32K_']  = 30000  # 2  #
	SAMPLES_DFT_dict ['N_001K_210_14_'+'16K_']  = 15000  # 1  #
	SAMPLES_DFT_dict ['N_001K_210_14_'+'8K_']   = 15000  # 1  #
	SAMPLES_DFT_dict ['N_001K_210_14_'+'4K_']   = 15000  # 1  # 
	SAMPLES_DFT_dict ['N_001K_210_14_'+'2K_']   = 15000  # 1  #
	SAMPLES_DFT_dict ['N_001K_210_14_'+'1K_']   = 15000  # 1  #
	######################################
	#
	######################################
	#TEST_CASE__N_001K_210_700__EN = 0x1  # FS_300K0 = 210e6/700 # // 210	700	300	1	NA	0.300	131072	436.907	433	129900.00
	TEST_FREQ_HZ_dict ['N_001K_210_700_'] = 1e3
	#
	SAMPLES_DFT_dict ['N_001K_210_700_'+'128K_'] = 129900 # 433  #
	SAMPLES_DFT_dict ['N_001K_210_700_'+'64K_']  = 63300  # 211  #
	SAMPLES_DFT_dict ['N_001K_210_700_'+'32K_']  = 32700  # 109  #
	SAMPLES_DFT_dict ['N_001K_210_700_'+'16K_']  = 15900  # 53   # 
	SAMPLES_DFT_dict ['N_001K_210_700_'+'8K_']   = 6900   # 23   #
	SAMPLES_DFT_dict ['N_001K_210_700_'+'4K_']   = 3900   # 13   #
	SAMPLES_DFT_dict ['N_001K_210_700_'+'2K_']   = 1500   # 5    #
	SAMPLES_DFT_dict ['N_001K_210_700_'+'1K_']   = 900    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_001K_210_1000__EN = 0x1  # FS_210K0 = 210e6/1000 # // 210	1000	210	1	NA	0.210	131072	624.152	619	129990.00
	TEST_FREQ_HZ_dict ['N_001K_210_1000_'] = 1e3
	#
	SAMPLES_DFT_dict ['N_001K_210_1000_'+'128K_'] = 129990 # 619  #
	SAMPLES_DFT_dict ['N_001K_210_1000_'+'64K_']  = 65310  # 311  #
	SAMPLES_DFT_dict ['N_001K_210_1000_'+'32K_']  = 31710  # 151  #
	SAMPLES_DFT_dict ['N_001K_210_1000_'+'16K_']  = 18690  # 89   #
	SAMPLES_DFT_dict ['N_001K_210_1000_'+'8K_']   = 15330  # 73   #
	SAMPLES_DFT_dict ['N_001K_210_1000_'+'4K_']   = 3990   # 19   #
	SAMPLES_DFT_dict ['N_001K_210_1000_'+'2K_']   = 1470   # 7    #
	SAMPLES_DFT_dict ['N_001K_210_1000_'+'1K_']   = 630    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_001K_210_840__EN = 0x1  # FS_250K0 = 210e6/840 # // 210	840	250	1	NA	0.250	131072	524.288	523	130750.00
	TEST_FREQ_HZ_dict ['N_001K_210_840_'] = 1e3
	#
	SAMPLES_DFT_dict ['N_001K_210_840_'+'128K_'] = 130750 # 523  #
	SAMPLES_DFT_dict ['N_001K_210_840_'+'64K_']  = 64250  # 257  #
	SAMPLES_DFT_dict ['N_001K_210_840_'+'32K_']  = 32750  # 131  #
	SAMPLES_DFT_dict ['N_001K_210_840_'+'16K_']  = 15250  # 61   #
	SAMPLES_DFT_dict ['N_001K_210_840_'+'8K_']   = 7750   # 31   #
	SAMPLES_DFT_dict ['N_001K_210_840_'+'4K_']   = 3250   # 13   #
	SAMPLES_DFT_dict ['N_001K_210_840_'+'2K_']   = 1750   # 7    #
	SAMPLES_DFT_dict ['N_001K_210_840_'+'1K_']   = 750    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_001K_210_750__EN = 0x1  # FS_280K0 = 210e6/750 # // 210	750	280	1	NA	0.280	131072	468.114	467	130760.00
	TEST_FREQ_HZ_dict ['N_001K_210_750_'] = 1e3
	#
	SAMPLES_DFT_dict ['N_001K_210_750_'+'128K_'] = 130760 # 467  #
	SAMPLES_DFT_dict ['N_001K_210_750_'+'64K_']  = 65240  # 233  #
	SAMPLES_DFT_dict ['N_001K_210_750_'+'32K_']  = 31640  # 113  #
	SAMPLES_DFT_dict ['N_001K_210_750_'+'16K_']  = 14840  # 53   #
	SAMPLES_DFT_dict ['N_001K_210_750_'+'8K_']   = 8120   # 29   #
	SAMPLES_DFT_dict ['N_001K_210_750_'+'4K_']   = 3640   # 13   #
	SAMPLES_DFT_dict ['N_001K_210_750_'+'2K_']   = 1960   # 7    #
	SAMPLES_DFT_dict ['N_001K_210_750_'+'1K_']   = 840    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_001K_210_150__EN = 0x001  # FS_1M400 = 210e6/150 # // 210	150	1400	1	NA	1.400	131072	93.623	89	124600.00
	TEST_FREQ_HZ_dict ['N_001K_210_150_'] = 1e3
	#
	SAMPLES_DFT_dict ['N_001K_210_150_'+'128K_'] = 124600 # 89  #
	SAMPLES_DFT_dict ['N_001K_210_150_'+'64K_']  = 60200  # 43  #
	SAMPLES_DFT_dict ['N_001K_210_150_'+'32K_']  = 32200  # 23  #
	SAMPLES_DFT_dict ['N_001K_210_150_'+'16K_']  = 15400  # 11  #
	SAMPLES_DFT_dict ['N_001K_210_150_'+'8K_']   = 7000   # 5   #
	SAMPLES_DFT_dict ['N_001K_210_150_'+'4K_']   = 4200   # 3   #
	SAMPLES_DFT_dict ['N_001K_210_150_'+'2K_']   = 2800   # 2   #
	SAMPLES_DFT_dict ['N_001K_210_150_'+'1K_']   = 1400   # 1   #
	######################################
	#
	######################################
	#TEST_CASE__N_001K_210_15__EN  = 0x0001  # FS_14M00 = 210e6/15 # // 210	15	14000	1	NA	14.000	131072	9.362	7	98000.00
	TEST_FREQ_HZ_dict ['N_001K_210_15_'] = 1e3
	#
	SAMPLES_DFT_dict ['N_001K_210_15_'+'128K_'] = 98000 # 7  ## 98000 # 7  #
	SAMPLES_DFT_dict ['N_001K_210_15_'+'64K_']  = 42000 # 3  ## 70000 # 5  #
	SAMPLES_DFT_dict ['N_001K_210_15_'+'32K_']  = 28000 # 2  ## 42000 # 3  #
	SAMPLES_DFT_dict ['N_001K_210_15_'+'16K_']  = 14000 # 1  ## 28000 # 2  #
	SAMPLES_DFT_dict ['N_001K_210_15_'+'8K_']   = 14000 # 1  #
	SAMPLES_DFT_dict ['N_001K_210_15_'+'4K_']   = 14000 # 1  #
	SAMPLES_DFT_dict ['N_001K_210_15_'+'2K_']   = 14000 # 1  #
	SAMPLES_DFT_dict ['N_001K_210_15_'+'1K_']   = 14000 # 1  #
	######################################
	
	
	#TEST_CASE__N_002K
	######################################
	#TEST_CASE__N_002K_210_1050__EN = 1  # FS_200K = 210e6/1050 # // 210	1050	100	2	0.200	131072	1310.72	1307	130700.00
	TEST_FREQ_HZ_dict ['N_002K_210_1050_'] = 2e3
	#
	SAMPLES_DFT_dict ['N_002K_210_1050_'+'128K_'] = 130700 # 1307 #
	SAMPLES_DFT_dict ['N_002K_210_1050_'+'64K_']  = 65300  # 653  #
	SAMPLES_DFT_dict ['N_002K_210_1050_'+'32K_']  = 31700  # 317  # 100ppm // outlier 700ppm
	SAMPLES_DFT_dict ['N_002K_210_1050_'+'16K_']  = 16300  # 163  # 100ppm // outlier 700ppm
	SAMPLES_DFT_dict ['N_002K_210_1050_'+'8K_']   = 7900   # 79   #
	SAMPLES_DFT_dict ['N_002K_210_1050_'+'4K_']   = 3700   # 37   #
	SAMPLES_DFT_dict ['N_002K_210_1050_'+'2K_']   = 1900   # 19   #
	SAMPLES_DFT_dict ['N_002K_210_1050_'+'1K_']   = 700    # 7    #
	######################################
	#
	######################################
	#TEST_CASE__N_002K_210_700__EN  = 1  # FS_300K = 210e6/700  # //  210	700	150	2	0.300	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['N_002K_210_700_'] = 2e3
	#
	SAMPLES_DFT_dict ['N_002K_210_700_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['N_002K_210_700_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['N_002K_210_700_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['N_002K_210_700_'+'16K_']  = 16350  # 109  # 3%
	SAMPLES_DFT_dict ['N_002K_210_700_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['N_002K_210_700_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['N_002K_210_700_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['N_002K_210_700_'+'1K_']   = 750    # 5    #
	######################################
	#
	######################################
	# TEST_CASE__N_002K_210_14__EN   = 1  # FS_15M  = 210e6/14   # //  210	14	7500	2	15.000	131072	17.47626667	17	127500.00
	TEST_FREQ_HZ_dict ['N_002K_210_14_'] = 2e3
	#
	SAMPLES_DFT_dict ['N_002K_210_14_'+'128K_'] = 127500 # 17  #
	SAMPLES_DFT_dict ['N_002K_210_14_'+'64K_']  = 52500	 # 7   #
	SAMPLES_DFT_dict ['N_002K_210_14_'+'32K_']  = 22500	 # 3   #
	SAMPLES_DFT_dict ['N_002K_210_14_'+'16K_']  = 15000	 # 2   #
	SAMPLES_DFT_dict ['N_002K_210_14_'+'8K_']   = 7500   # 1   #
	SAMPLES_DFT_dict ['N_002K_210_14_'+'4K_']   = 7500   # 1   #
	SAMPLES_DFT_dict ['N_002K_210_14_'+'2K_']   = 7500   # 1   #
	SAMPLES_DFT_dict ['N_002K_210_14_'+'1K_']   = 7500   # 1   #
	######################################
	#
	######################################
	#TEST_CASE__N_002K_210_350__EN = 0x1  # FS_600K0 = 210e6/350 # // 210	350	300	2	NA	0.600	131072	436.907	433	129900.00
	TEST_FREQ_HZ_dict ['N_002K_210_350_'] = 2e3
	#
	SAMPLES_DFT_dict ['N_002K_210_350_'+'128K_'] = 129900 # 433  #
	SAMPLES_DFT_dict ['N_002K_210_350_'+'64K_']  = 63300  # 211  #
	SAMPLES_DFT_dict ['N_002K_210_350_'+'32K_']  = 32700  # 109  #
	SAMPLES_DFT_dict ['N_002K_210_350_'+'16K_']  = 15900  # 53   # 
	SAMPLES_DFT_dict ['N_002K_210_350_'+'8K_']   = 6900   # 23   #
	SAMPLES_DFT_dict ['N_002K_210_350_'+'4K_']   = 3900   # 13   #
	SAMPLES_DFT_dict ['N_002K_210_350_'+'2K_']   = 1500   # 5    #
	SAMPLES_DFT_dict ['N_002K_210_350_'+'1K_']   = 900    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_002K_210_500__EN  = 0x1  # FS_420K0 = 210e6/ 500 # // 210	500		210	2	NA	0.420	131072	624.152	619	129990.00
	TEST_FREQ_HZ_dict ['N_002K_210_500_'] = 2e3
	#
	SAMPLES_DFT_dict ['N_002K_210_500_'+'128K_'] = 129990 # 619  #
	SAMPLES_DFT_dict ['N_002K_210_500_'+'64K_']  = 65310  # 311  #
	SAMPLES_DFT_dict ['N_002K_210_500_'+'32K_']  = 31710  # 151  #
	SAMPLES_DFT_dict ['N_002K_210_500_'+'16K_']  = 18690  # 89   #
	SAMPLES_DFT_dict ['N_002K_210_500_'+'8K_']   = 15330  # 73   #
	SAMPLES_DFT_dict ['N_002K_210_500_'+'4K_']   = 3990   # 19   #
	SAMPLES_DFT_dict ['N_002K_210_500_'+'2K_']   = 1470   # 7    #
	SAMPLES_DFT_dict ['N_002K_210_500_'+'1K_']   = 630    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_002K_210_420__EN = 0x1  # FS_500K0 = 210e6/420 # // 210	420	250	2	NA	0.500	131072	524.288	523	130750.00
	TEST_FREQ_HZ_dict ['N_002K_210_420_'] = 2e3
	#
	SAMPLES_DFT_dict ['N_002K_210_420_'+'128K_'] = 130750 # 523  #
	SAMPLES_DFT_dict ['N_002K_210_420_'+'64K_']  = 64250  # 257  #
	SAMPLES_DFT_dict ['N_002K_210_420_'+'32K_']  = 32750  # 131  #
	SAMPLES_DFT_dict ['N_002K_210_420_'+'16K_']  = 15250  # 61   #
	SAMPLES_DFT_dict ['N_002K_210_420_'+'8K_']   = 7750   # 31   #
	SAMPLES_DFT_dict ['N_002K_210_420_'+'4K_']   = 3250   # 13   #
	SAMPLES_DFT_dict ['N_002K_210_420_'+'2K_']   = 1750   # 7    #
	SAMPLES_DFT_dict ['N_002K_210_420_'+'1K_']   = 750    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_002K_210_300__EN = 0x1  # FS_700K0 = 210e6/300 # // 210	300	350	2	NA	0.700	131072	374.491	373	130550.00
	TEST_FREQ_HZ_dict ['N_002K_210_300_'] = 2e3
	#
	SAMPLES_DFT_dict ['N_002K_210_300_'+'128K_'] = 130550 # 373 #
	SAMPLES_DFT_dict ['N_002K_210_300_'+'64K_']  = 63350  # 181 #
	SAMPLES_DFT_dict ['N_002K_210_300_'+'32K_']  = 31150  # 89  #
	SAMPLES_DFT_dict ['N_002K_210_300_'+'16K_']  = 15050  # 43  #
	SAMPLES_DFT_dict ['N_002K_210_300_'+'8K_']   = 8050   # 23  #
	SAMPLES_DFT_dict ['N_002K_210_300_'+'4K_']   = 3850   # 11  #
	SAMPLES_DFT_dict ['N_002K_210_300_'+'2K_']   = 1750   # 5   #
	SAMPLES_DFT_dict ['N_002K_210_300_'+'1K_']   = 700    # 2   #
	######################################
	#
	######################################
	#TEST_CASE__N_002K_210_75__EN  = 0x001  # FS_2M800 = 210e6/ 75 # // 210	75	1400	2	NA	2.800	131072	93.623	89	124600.00
	TEST_FREQ_HZ_dict ['N_002K_210_75_'] = 2e3
	#
	SAMPLES_DFT_dict ['N_002K_210_75_'+'128K_'] = 124600 # 89  #
	SAMPLES_DFT_dict ['N_002K_210_75_'+'64K_']  = 60200  # 43  #
	SAMPLES_DFT_dict ['N_002K_210_75_'+'32K_']  = 32200  # 23  #
	SAMPLES_DFT_dict ['N_002K_210_75_'+'16K_']  = 15400  # 11  #
	SAMPLES_DFT_dict ['N_002K_210_75_'+'8K_']   = 7000   # 5   #
	SAMPLES_DFT_dict ['N_002K_210_75_'+'4K_']   = 4200   # 3   #
	SAMPLES_DFT_dict ['N_002K_210_75_'+'2K_']   = 2800   # 2   #
	SAMPLES_DFT_dict ['N_002K_210_75_'+'1K_']   = 1400   # 1   #
	######################################
	#
	######################################
	#TEST_CASE__N_002K_210_15__EN  = 0x0001  # FS_14M00 = 210e6/15 # // 210	15	7000	2	NA	14.000	131072	18.725	17	119000.00
	TEST_FREQ_HZ_dict ['N_002K_210_15_'] = 2e3
	#
	SAMPLES_DFT_dict ['N_002K_210_15_'+'128K_'] = 119000 # 17  ## 119000 # 17  #
	SAMPLES_DFT_dict ['N_002K_210_15_'+'64K_']  = 49000  # 7   ## 49000  # 7   #
	SAMPLES_DFT_dict ['N_002K_210_15_'+'32K_']  = 21000  # 3   ## 35000  # 5   #
	SAMPLES_DFT_dict ['N_002K_210_15_'+'16K_']  = 14000  # 2   ## 21000  # 3   #
	SAMPLES_DFT_dict ['N_002K_210_15_'+'8K_']   = 7000   # 1   ## 14000  # 2   #
	SAMPLES_DFT_dict ['N_002K_210_15_'+'4K_']   = 7000   # 1   #
	SAMPLES_DFT_dict ['N_002K_210_15_'+'2K_']   = 7000   # 1   #
	SAMPLES_DFT_dict ['N_002K_210_15_'+'1K_']   = 7000   # 1   #
	######################################
	
	
	#TEST_CASE__N_005K
	######################################
	#TEST_CASE__N_005K_210_420__EN  = 1  # FS_500K = 210e6/420  # // 210	420	100	5	0.500	131072	1310.72	1307	130700.00
	TEST_FREQ_HZ_dict ['N_005K_210_420_'] = 5e3
	#
	SAMPLES_DFT_dict ['N_005K_210_420_'+'128K_'] = 130700 # 1307 #
	SAMPLES_DFT_dict ['N_005K_210_420_'+'64K_']  = 65300  # 653  #
	SAMPLES_DFT_dict ['N_005K_210_420_'+'32K_']  = 31700  # 317  # 50ppm // outlier 0.1%
	SAMPLES_DFT_dict ['N_005K_210_420_'+'16K_']  = 16300  # 163  # 50ppm // outlier 0.1%
	SAMPLES_DFT_dict ['N_005K_210_420_'+'8K_']   = 7900   # 79   #
	SAMPLES_DFT_dict ['N_005K_210_420_'+'4K_']   = 3700   # 37   #
	SAMPLES_DFT_dict ['N_005K_210_420_'+'2K_']   = 1900   # 19   #
	SAMPLES_DFT_dict ['N_005K_210_420_'+'1K_']   = 700    # 7    #
	######################################
	#
	######################################
	#TEST_CASE__N_005K_210_280__EN  = 1  # FS_750K = 210e6/280  # //  210	280	150	5	0.750	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['N_005K_210_280_'] = 5e3
	#
	SAMPLES_DFT_dict ['N_005K_210_280_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['N_005K_210_280_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['N_005K_210_280_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['N_005K_210_280_'+'16K_']  = 16350  # 109  # 2.5%
	SAMPLES_DFT_dict ['N_005K_210_280_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['N_005K_210_280_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['N_005K_210_280_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['N_005K_210_280_'+'1K_']   = 750    # 5    #
	######################################
	#
	######################################
	# TEST_CASE__N_005K_210_14__EN   = 1  # FS_15M  = 210e6/14   # //  210	14	3000	5	15.000	131072	43.69066667	43	129000.00
	TEST_FREQ_HZ_dict ['N_005K_210_14_'] = 5e3
	#
	SAMPLES_DFT_dict ['N_005K_210_14_'+'128K_'] = 129000 # 43 #
	SAMPLES_DFT_dict ['N_005K_210_14_'+'64K_']  = 57000  # 19 #
	SAMPLES_DFT_dict ['N_005K_210_14_'+'32K_']  = 21000  # 7  #
	SAMPLES_DFT_dict ['N_005K_210_14_'+'16K_']  = 15000  # 5  # OK 150ppm
	SAMPLES_DFT_dict ['N_005K_210_14_'+'8K_']   = 6000   # 2  #
	SAMPLES_DFT_dict ['N_005K_210_14_'+'4K_']   = 3000   # 1  #
	SAMPLES_DFT_dict ['N_005K_210_14_'+'2K_']   = 3000   # 1  #
	SAMPLES_DFT_dict ['N_005K_210_14_'+'1K_']   = 3000   # 1  # OK 200ppm
	######################################
	#
	######################################
	#TEST_CASE__N_005K_210_140__EN = 0x1  # FS_1M500 = 210e6/140 # // 210	140	300	5	NA	1.500	131072	436.907	433	129900.00
	TEST_FREQ_HZ_dict ['N_005K_210_140_'] = 5e3
	#
	SAMPLES_DFT_dict ['N_005K_210_140_'+'128K_'] = 129900 # 433  #
	SAMPLES_DFT_dict ['N_005K_210_140_'+'64K_']  = 63300  # 211  #
	SAMPLES_DFT_dict ['N_005K_210_140_'+'32K_']  = 32700  # 109  #
	SAMPLES_DFT_dict ['N_005K_210_140_'+'16K_']  = 15900  # 53   # 
	SAMPLES_DFT_dict ['N_005K_210_140_'+'8K_']   = 6900   # 23   #
	SAMPLES_DFT_dict ['N_005K_210_140_'+'4K_']   = 3900   # 13   #
	SAMPLES_DFT_dict ['N_005K_210_140_'+'2K_']   = 1500   # 5    #
	SAMPLES_DFT_dict ['N_005K_210_140_'+'1K_']   = 900    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_005K_210_200__EN  = 0x1  # FS_1M050 = 210e6/ 200 # // 210	200		210	5	NA	1.050	131072	624.152	619	129990.00
	TEST_FREQ_HZ_dict ['N_005K_210_200_'] = 5e3
	#
	SAMPLES_DFT_dict ['N_005K_210_200_'+'128K_'] = 129990 # 619  #
	SAMPLES_DFT_dict ['N_005K_210_200_'+'64K_']  = 65310  # 311  #
	SAMPLES_DFT_dict ['N_005K_210_200_'+'32K_']  = 31710  # 151  #
	SAMPLES_DFT_dict ['N_005K_210_200_'+'16K_']  = 18690  # 89   #
	SAMPLES_DFT_dict ['N_005K_210_200_'+'8K_']   = 15330  # 73   #
	SAMPLES_DFT_dict ['N_005K_210_200_'+'4K_']   = 3990   # 19   #
	SAMPLES_DFT_dict ['N_005K_210_200_'+'2K_']   = 1470   # 7    #
	SAMPLES_DFT_dict ['N_005K_210_200_'+'1K_']   = 630    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_005K_210_210__EN = 0x1  # FS_1M000 = 210e6/210 # // 210	210	200	5	NA	1.000	131072	655.360	619	123800.00
	TEST_FREQ_HZ_dict ['N_005K_210_210_'] = 5e3
	#
	SAMPLES_DFT_dict ['N_005K_210_210_'+'128K_'] = 123800 # 619 #
	SAMPLES_DFT_dict ['N_005K_210_210_'+'64K_']  = 63400  # 317 #
	SAMPLES_DFT_dict ['N_005K_210_210_'+'32K_']  = 32600  # 163 #
	SAMPLES_DFT_dict ['N_005K_210_210_'+'16K_']  = 15800  # 79  #
	SAMPLES_DFT_dict ['N_005K_210_210_'+'8K_']   = 7400   # 37  #
	SAMPLES_DFT_dict ['N_005K_210_210_'+'4K_']   = 3800   # 19  #
	SAMPLES_DFT_dict ['N_005K_210_210_'+'2K_']   = 1400   # 7   #
	SAMPLES_DFT_dict ['N_005K_210_210_'+'1K_']   = 1000   # 5   #
	######################################
	#
	######################################
	#TEST_CASE__N_005K_210_150__EN = 0x1  # FS_1M400 = 210e6/150 # // 210	150	280	5	NA	1.400	131072	468.114	467	130760.00
	TEST_FREQ_HZ_dict ['N_005K_210_150_'] = 5e3
	#
	SAMPLES_DFT_dict ['N_005K_210_150_'+'128K_'] = 130760 # 467  #
	SAMPLES_DFT_dict ['N_005K_210_150_'+'64K_']  = 65240  # 233  #
	SAMPLES_DFT_dict ['N_005K_210_150_'+'32K_']  = 31640  # 113  #
	SAMPLES_DFT_dict ['N_005K_210_150_'+'16K_']  = 14840  # 53   #
	SAMPLES_DFT_dict ['N_005K_210_150_'+'8K_']   = 8120   # 29   #
	SAMPLES_DFT_dict ['N_005K_210_150_'+'4K_']   = 3640   # 13   #
	SAMPLES_DFT_dict ['N_005K_210_150_'+'2K_']   = 1960   # 7    #
	SAMPLES_DFT_dict ['N_005K_210_150_'+'1K_']   = 840    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_005K_210_30__EN  = 0x001  # FS_7M000 = 210e6/ 30 # // 210	30	1400	5	NA	7.000	131072	93.623	89	124600.00
	TEST_FREQ_HZ_dict ['N_005K_210_30_'] = 5e3
	#
	SAMPLES_DFT_dict ['N_005K_210_30_'+'128K_'] = 124600 # 89  #
	SAMPLES_DFT_dict ['N_005K_210_30_'+'64K_']  = 60200  # 43  #
	SAMPLES_DFT_dict ['N_005K_210_30_'+'32K_']  = 32200  # 23  #
	SAMPLES_DFT_dict ['N_005K_210_30_'+'16K_']  = 15400  # 11  #
	SAMPLES_DFT_dict ['N_005K_210_30_'+'8K_']   = 7000   # 5   #
	SAMPLES_DFT_dict ['N_005K_210_30_'+'4K_']   = 4200   # 3   #
	SAMPLES_DFT_dict ['N_005K_210_30_'+'2K_']   = 2800   # 2   #
	SAMPLES_DFT_dict ['N_005K_210_30_'+'1K_']   = 1400   # 1   #
	######################################
	#
	######################################
	#TEST_CASE__N_005K_210_15__EN  = 0x0001  # FS_14M00 = 210e6/15 # // 210	15	2800	5	NA	14.000	131072	46.811	43	120400.00
	TEST_FREQ_HZ_dict ['N_005K_210_15_'] = 5e3
	#
	SAMPLES_DFT_dict ['N_005K_210_15_'+'128K_'] = 120400 # 43  #
	SAMPLES_DFT_dict ['N_005K_210_15_'+'64K_']  = 64400  # 23  #
	SAMPLES_DFT_dict ['N_005K_210_15_'+'32K_']  = 30800  # 11  #
	SAMPLES_DFT_dict ['N_005K_210_15_'+'16K_']  = 14000  # 5   #
	SAMPLES_DFT_dict ['N_005K_210_15_'+'8K_']   = 5600   # 2   #
	SAMPLES_DFT_dict ['N_005K_210_15_'+'4K_']   = 2800   # 1   #
	SAMPLES_DFT_dict ['N_005K_210_15_'+'2K_']   = 2800   # 1   #
	SAMPLES_DFT_dict ['N_005K_210_15_'+'1K_']   = 2800   # 1   #
	######################################
	
	
	#TEST_CASE__N_010K
	######################################
	#TEST_CASE__N_010K_210_210__EN  = 1  # FS_1M = 210e6/210  # // 210	210	100	10	1.000	131072	1310.72	1307	130700.00
	TEST_FREQ_HZ_dict ['N_010K_210_210_'] = 10e3
	#
	SAMPLES_DFT_dict ['N_010K_210_210_'+'128K_'] = 130700 # 1307 #
	SAMPLES_DFT_dict ['N_010K_210_210_'+'64K_']  = 65300  # 653  #
	SAMPLES_DFT_dict ['N_010K_210_210_'+'32K_']  = 31700  # 317  # 50ppm // outlier 0.1%
	SAMPLES_DFT_dict ['N_010K_210_210_'+'16K_']  = 16300  # 163  # 50ppm // outlier 0.1%
	SAMPLES_DFT_dict ['N_010K_210_210_'+'8K_']   = 7900   # 79   #
	SAMPLES_DFT_dict ['N_010K_210_210_'+'4K_']   = 3700   # 37   #
	SAMPLES_DFT_dict ['N_010K_210_210_'+'2K_']   = 1900   # 19   #
	SAMPLES_DFT_dict ['N_010K_210_210_'+'1K_']   = 700    # 7    #
	######################################
	#
	######################################
	#TEST_CASE__N_010K_210_140__EN  = 1  # FS_1M5  = 210e6/140  # //  210	140	150	10	1.500	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['N_010K_210_140_'] = 10e3
	#
	SAMPLES_DFT_dict ['N_010K_210_140_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['N_010K_210_140_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['N_010K_210_140_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['N_010K_210_140_'+'16K_']  = 16350  # 109  # 2.5%
	SAMPLES_DFT_dict ['N_010K_210_140_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['N_010K_210_140_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['N_010K_210_140_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['N_010K_210_140_'+'1K_']   = 750    # 5    #
	######################################
	#
	######################################
	# TEST_CASE__N_010K_210_14__EN   = 1  # FS_15M  = 210e6/14   # //  210	14	1500	10	15.000	131072	87.38133333	83	124500.00
	TEST_FREQ_HZ_dict ['N_010K_210_14_'] = 10e3
	#
	SAMPLES_DFT_dict ['N_010K_210_14_'+'128K_'] = 124500 # 83 #
	SAMPLES_DFT_dict ['N_010K_210_14_'+'64K_']  = 64500  # 43 #
	SAMPLES_DFT_dict ['N_010K_210_14_'+'32K_']  = 28500  # 19 #
	SAMPLES_DFT_dict ['N_010K_210_14_'+'16K_']  = 10500  # 7  #
	SAMPLES_DFT_dict ['N_010K_210_14_'+'8K_']   = 7500   # 5  # OK 100ppm
	SAMPLES_DFT_dict ['N_010K_210_14_'+'4K_']   = 3000   # 2  #
	SAMPLES_DFT_dict ['N_010K_210_14_'+'2K_']   = 1500   # 1  #
	SAMPLES_DFT_dict ['N_010K_210_14_'+'1K_']   = 1500   # 1  #
	######################################
	#
	######################################
	#TEST_CASE__N_010K_210_70__EN  = 0x1  # FS_3M000 = 210e6/ 70 # // 210	70	300	10	NA	3.000	131072	436.907	433	129900.00
	TEST_FREQ_HZ_dict ['N_010K_210_70_'] = 10e3
	#
	SAMPLES_DFT_dict ['N_010K_210_70_'+'128K_'] = 129900 # 433  #
	SAMPLES_DFT_dict ['N_010K_210_70_'+'64K_']  = 63300  # 211  #
	SAMPLES_DFT_dict ['N_010K_210_70_'+'32K_']  = 32700  # 109  #
	SAMPLES_DFT_dict ['N_010K_210_70_'+'16K_']  = 15900  # 53   # 
	SAMPLES_DFT_dict ['N_010K_210_70_'+'8K_']   = 6900   # 23   #
	SAMPLES_DFT_dict ['N_010K_210_70_'+'4K_']   = 3900   # 13   #
	SAMPLES_DFT_dict ['N_010K_210_70_'+'2K_']   = 1500   # 5    #
	SAMPLES_DFT_dict ['N_010K_210_70_'+'1K_']   = 900    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_010K_210_100__EN  = 0x1  # FS_2M100 = 210e6/ 100 # // 210	100		210	10	NA	2.100	131072	624.152	619	129990.00
	TEST_FREQ_HZ_dict ['N_010K_210_100_'] = 10e3
	#
	SAMPLES_DFT_dict ['N_010K_210_100_'+'128K_'] = 129990 # 619  #
	SAMPLES_DFT_dict ['N_010K_210_100_'+'64K_']  = 65310  # 311  #
	SAMPLES_DFT_dict ['N_010K_210_100_'+'32K_']  = 31710  # 151  #
	SAMPLES_DFT_dict ['N_010K_210_100_'+'16K_']  = 18690  # 89   #
	SAMPLES_DFT_dict ['N_010K_210_100_'+'8K_']   = 15330  # 73   #
	SAMPLES_DFT_dict ['N_010K_210_100_'+'4K_']   = 3990   # 19   #
	SAMPLES_DFT_dict ['N_010K_210_100_'+'2K_']   = 1470   # 7    #
	SAMPLES_DFT_dict ['N_010K_210_100_'+'1K_']   = 630    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_010K_210_84__EN  = 0x1  # FS_2M500 = 210e6/ 84 # // 210	84	250	10	NA	2.500	131072	524.288	523	130750.00
	TEST_FREQ_HZ_dict ['N_010K_210_84_'] = 10e3
	#
	SAMPLES_DFT_dict ['N_010K_210_84_'+'128K_'] = 130750 # 523  #
	SAMPLES_DFT_dict ['N_010K_210_84_'+'64K_']  = 64250  # 257  #
	SAMPLES_DFT_dict ['N_010K_210_84_'+'32K_']  = 32750  # 131  #
	SAMPLES_DFT_dict ['N_010K_210_84_'+'16K_']  = 15250  # 61   #
	SAMPLES_DFT_dict ['N_010K_210_84_'+'8K_']   = 7750   # 31   #
	SAMPLES_DFT_dict ['N_010K_210_84_'+'4K_']   = 3250   # 13   #
	SAMPLES_DFT_dict ['N_010K_210_84_'+'2K_']   = 1750   # 7    #
	SAMPLES_DFT_dict ['N_010K_210_84_'+'1K_']   = 750    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_010K_210_75__EN  = 0x1  # FS_2M800 = 210e6/ 75 # // 210	75	280	10	NA	2.800	131072	468.114	467	130760.00
	TEST_FREQ_HZ_dict ['N_010K_210_75_'] = 10e3
	#
	SAMPLES_DFT_dict ['N_010K_210_75_'+'128K_'] = 130760 # 467  #
	SAMPLES_DFT_dict ['N_010K_210_75_'+'64K_']  = 65240  # 233  #
	SAMPLES_DFT_dict ['N_010K_210_75_'+'32K_']  = 31640  # 113  #
	SAMPLES_DFT_dict ['N_010K_210_75_'+'16K_']  = 14840  # 53   #
	SAMPLES_DFT_dict ['N_010K_210_75_'+'8K_']   = 8120   # 29   #
	SAMPLES_DFT_dict ['N_010K_210_75_'+'4K_']   = 3640   # 13   #
	SAMPLES_DFT_dict ['N_010K_210_75_'+'2K_']   = 1960   # 7    #
	SAMPLES_DFT_dict ['N_010K_210_75_'+'1K_']   = 840    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_010K_210_15__EN  = 0x001  # FS_14M00 = 210e6/ 15 # // 210	15	1400	10	NA	14.000	131072	93.623	89	124600.00
	TEST_FREQ_HZ_dict ['N_010K_210_15_'] = 10e3
	#
	SAMPLES_DFT_dict ['N_010K_210_15_'+'128K_'] = 124600 # 89  #
	SAMPLES_DFT_dict ['N_010K_210_15_'+'64K_']  = 60200  # 43  #
	SAMPLES_DFT_dict ['N_010K_210_15_'+'32K_']  = 32200  # 23  #
	SAMPLES_DFT_dict ['N_010K_210_15_'+'16K_']  = 15400  # 11  #
	SAMPLES_DFT_dict ['N_010K_210_15_'+'8K_']   = 7000   # 5   #
	SAMPLES_DFT_dict ['N_010K_210_15_'+'4K_']   = 4200   # 3   #
	SAMPLES_DFT_dict ['N_010K_210_15_'+'2K_']   = 2800   # 2   #
	SAMPLES_DFT_dict ['N_010K_210_15_'+'1K_']   = 1400   # 1   #
	######################################
	
	
	#TEST_CASE__N_020K
	######################################
	#TEST_CASE__N_020K_210_105__EN  = 1  # FS_2M = 210e6/105  # // 210	105	100	20	2.000	131072	1310.72	1307	130700.00
	TEST_FREQ_HZ_dict ['N_020K_210_105_'] = 20e3
	#
	SAMPLES_DFT_dict ['N_020K_210_105_'+'128K_'] = 130700 # 1307 #
	SAMPLES_DFT_dict ['N_020K_210_105_'+'64K_']  = 65300  # 653  #
	SAMPLES_DFT_dict ['N_020K_210_105_'+'32K_']  = 31700  # 317  # 100ppm // outlier 0.1%
	SAMPLES_DFT_dict ['N_020K_210_105_'+'16K_']  = 16300  # 163  # 100ppm // outlier 0.1%
	SAMPLES_DFT_dict ['N_020K_210_105_'+'8K_']   = 7900   # 79   #
	SAMPLES_DFT_dict ['N_020K_210_105_'+'4K_']   = 3700   # 37   #
	SAMPLES_DFT_dict ['N_020K_210_105_'+'2K_']   = 1900   # 19   #
	SAMPLES_DFT_dict ['N_020K_210_105_'+'1K_']   = 700    # 7    #
	######################################
	#
	######################################
	#TEST_CASE__N_020K_210_70__EN   = 1  # FS_3M   = 210e6/70   # //  210	70	150	20	3.000	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['N_020K_210_70_'] = 20e3
	#
	SAMPLES_DFT_dict ['N_020K_210_70_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['N_020K_210_70_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['N_020K_210_70_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['N_020K_210_70_'+'16K_']  = 16350  # 109  # NG 2%
	SAMPLES_DFT_dict ['N_020K_210_70_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['N_020K_210_70_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['N_020K_210_70_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['N_020K_210_70_'+'1K_']   = 750    # 5    #
	######################################
	#
	######################################
	# TEST_CASE__N_020K_210_14__EN   = 1  # FS_15M  = 210e6/14   # //  210	14	750		20	15.000	131072	174.7626667	173	129750.00
	TEST_FREQ_HZ_dict ['N_020K_210_14_'] = 20e3
	#
	SAMPLES_DFT_dict ['N_020K_210_14_'+'128K_'] = 129750 # 173  #
	SAMPLES_DFT_dict ['N_020K_210_14_'+'64K_']  = 62250  # 83   #
	SAMPLES_DFT_dict ['N_020K_210_14_'+'32K_']  = 32250  # 43   #
	SAMPLES_DFT_dict ['N_020K_210_14_'+'16K_']  = 14250  # 19   # NG 0.4%
	SAMPLES_DFT_dict ['N_020K_210_14_'+'8K_']   = 5250   # 7    #
	SAMPLES_DFT_dict ['N_020K_210_14_'+'4K_']   = 3750   # 5    #
	SAMPLES_DFT_dict ['N_020K_210_14_'+'2K_']   = 1500   # 2    #
	SAMPLES_DFT_dict ['N_020K_210_14_'+'1K_']   = 750    # 1    #
	######################################
	#
	######################################
	#TEST_CASE__N_020K_210_35__EN  = 0x1  # FS_6M000 = 210e6/ 35 # // 210	35	300	20	NA	6.000	131072	436.907	433	129900.00
	TEST_FREQ_HZ_dict ['N_020K_210_35_'] = 20e3
	#
	SAMPLES_DFT_dict ['N_020K_210_35_'+'128K_'] = 129900 # 433  #
	SAMPLES_DFT_dict ['N_020K_210_35_'+'64K_']  = 63300  # 211  #
	SAMPLES_DFT_dict ['N_020K_210_35_'+'32K_']  = 32700  # 109  #
	SAMPLES_DFT_dict ['N_020K_210_35_'+'16K_']  = 15900  # 53   # 
	SAMPLES_DFT_dict ['N_020K_210_35_'+'8K_']   = 6900   # 23   #
	SAMPLES_DFT_dict ['N_020K_210_35_'+'4K_']   = 3900   # 13   #
	SAMPLES_DFT_dict ['N_020K_210_35_'+'2K_']   = 1500   # 5    #
	SAMPLES_DFT_dict ['N_020K_210_35_'+'1K_']   = 900    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_020K_210_50__EN   = 0x1  # FS_4M200 = 210e6/  50 # // 210	50		210	20	NA	4.200	131072	624.152	619	129990.00
	TEST_FREQ_HZ_dict ['N_020K_210_50_'] = 20e3
	#
	SAMPLES_DFT_dict ['N_020K_210_50_'+'128K_'] = 129990 # 619  #
	SAMPLES_DFT_dict ['N_020K_210_50_'+'64K_']  = 65310  # 311  #
	SAMPLES_DFT_dict ['N_020K_210_50_'+'32K_']  = 31710  # 151  #
	SAMPLES_DFT_dict ['N_020K_210_50_'+'16K_']  = 18690  # 89   #
	SAMPLES_DFT_dict ['N_020K_210_50_'+'8K_']   = 15330  # 73   #
	SAMPLES_DFT_dict ['N_020K_210_50_'+'4K_']   = 3990   # 19   #
	SAMPLES_DFT_dict ['N_020K_210_50_'+'2K_']   = 1470   # 7    #
	SAMPLES_DFT_dict ['N_020K_210_50_'+'1K_']   = 630    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_020K_210_42__EN  = 0x1  # FS_5M000 = 210e6/ 42 # // 210	42	250	20	NA	5.000	131072	524.288	523	130750.00
	TEST_FREQ_HZ_dict ['N_020K_210_42_'] = 20e3
	#
	SAMPLES_DFT_dict ['N_020K_210_42_'+'128K_'] = 130750 # 523  #
	SAMPLES_DFT_dict ['N_020K_210_42_'+'64K_']  = 64250  # 257  #
	SAMPLES_DFT_dict ['N_020K_210_42_'+'32K_']  = 32750  # 131  #
	SAMPLES_DFT_dict ['N_020K_210_42_'+'16K_']  = 15250  # 61   #
	SAMPLES_DFT_dict ['N_020K_210_42_'+'8K_']   = 7750   # 31   #
	SAMPLES_DFT_dict ['N_020K_210_42_'+'4K_']   = 3250   # 13   #
	SAMPLES_DFT_dict ['N_020K_210_42_'+'2K_']   = 1750   # 7    #
	SAMPLES_DFT_dict ['N_020K_210_42_'+'1K_']   = 750    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_020K_210_30__EN  = 0x1  # FS_7M000 = 210e6/ 30 # // 210	30	350	20	NA	7.000	131072	374.491	373	130550.00
	TEST_FREQ_HZ_dict ['N_020K_210_30_'] = 20e3
	#
	SAMPLES_DFT_dict ['N_020K_210_30_'+'128K_'] = 130550 # 373 #
	SAMPLES_DFT_dict ['N_020K_210_30_'+'64K_']  = 63350  # 181 #
	SAMPLES_DFT_dict ['N_020K_210_30_'+'32K_']  = 31150  # 89  #
	SAMPLES_DFT_dict ['N_020K_210_30_'+'16K_']  = 15050  # 43  #
	SAMPLES_DFT_dict ['N_020K_210_30_'+'8K_']   = 8050   # 23  #
	SAMPLES_DFT_dict ['N_020K_210_30_'+'4K_']   = 3850   # 11  #
	SAMPLES_DFT_dict ['N_020K_210_30_'+'2K_']   = 1750   # 5   #
	SAMPLES_DFT_dict ['N_020K_210_30_'+'1K_']   = 700    # 2   #
	######################################
	#
	######################################
	#TEST_CASE__N_020K_210_21__EN  = 0x0001  # FS_10M00 = 210e6/21 # // 210	21	500		20	NA	10.000	131072	262.144	257	128500.00
	TEST_FREQ_HZ_dict ['N_020K_210_21_'] = 20e3
	#
	SAMPLES_DFT_dict ['N_020K_210_21_'+'128K_'] = 128500 # 257 #
	SAMPLES_DFT_dict ['N_020K_210_21_'+'64K_']  = 65500  # 131 #
	SAMPLES_DFT_dict ['N_020K_210_21_'+'32K_']  = 30500  # 61  #
	SAMPLES_DFT_dict ['N_020K_210_21_'+'16K_']  = 15500  # 31  #
	SAMPLES_DFT_dict ['N_020K_210_21_'+'8K_']   = 6500   # 13  #
	SAMPLES_DFT_dict ['N_020K_210_21_'+'4K_']   = 3500   # 7   #
	SAMPLES_DFT_dict ['N_020K_210_21_'+'2K_']   = 1500   # 3   #
	SAMPLES_DFT_dict ['N_020K_210_21_'+'1K_']   = 1000   # 2   #
	######################################
	
	
	#TEST_CASE__N_050K
	######################################
	#TEST_CASE__N_050K_210_42__EN   = 1  # FS_5M = 210e6/42   # // 210	42	100	50	5.000	131072	1310.72	1307	130700.00
	TEST_FREQ_HZ_dict ['N_050K_210_42_'] = 50e3
	#
	SAMPLES_DFT_dict ['N_050K_210_42_'+'128K_'] = 130700 # 1307 # 700ppm
	SAMPLES_DFT_dict ['N_050K_210_42_'+'64K_']  = 65300  # 653  # 700ppm
	SAMPLES_DFT_dict ['N_050K_210_42_'+'32K_']  = 31700  # 317  # 700ppm
	SAMPLES_DFT_dict ['N_050K_210_42_'+'16K_']  = 16300  # 163  # 700ppm
	SAMPLES_DFT_dict ['N_050K_210_42_'+'8K_']   = 7900   # 79   # 700ppm
	SAMPLES_DFT_dict ['N_050K_210_42_'+'4K_']   = 3700   # 37   # 700ppm
	SAMPLES_DFT_dict ['N_050K_210_42_'+'2K_']   = 1900   # 19   # 700ppm
	SAMPLES_DFT_dict ['N_050K_210_42_'+'1K_']   = 700    # 7    # 700ppm
	######################################
	#
	######################################
	#TEST_CASE__N_050K_210_28__EN   = 1  # FS_7M5  = 210e6/28   # //  210	28	150	50	7.500	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['N_050K_210_28_'] = 50e3
	#
	SAMPLES_DFT_dict ['N_050K_210_28_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['N_050K_210_28_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['N_050K_210_28_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['N_050K_210_28_'+'16K_']  = 16350  # 109  # 2%
	SAMPLES_DFT_dict ['N_050K_210_28_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['N_050K_210_28_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['N_050K_210_28_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['N_050K_210_28_'+'1K_']   = 750    # 5    #
	######################################
	#
	######################################
	#TEST_CASE__N_050K_210_14__EN   = 0  # FS_15M  = 210e6/14   # //  210	14	300	50	15.000	131072	436.9066667	433	129900.00
	TEST_FREQ_HZ_dict ['N_050K_210_14_'] = 50e3
	#
	SAMPLES_DFT_dict ['N_050K_210_14_'+'128K_'] = 129900 # 433  #
	SAMPLES_DFT_dict ['N_050K_210_14_'+'64K_']  = 63300  # 211  #
	SAMPLES_DFT_dict ['N_050K_210_14_'+'32K_']  = 32700  # 109  #
	SAMPLES_DFT_dict ['N_050K_210_14_'+'16K_']  = 15900  # 53   # 
	SAMPLES_DFT_dict ['N_050K_210_14_'+'8K_']   = 6900   # 23   #
	SAMPLES_DFT_dict ['N_050K_210_14_'+'4K_']   = 3900   # 13   #
	SAMPLES_DFT_dict ['N_050K_210_14_'+'2K_']   = 1500   # 5    #
	SAMPLES_DFT_dict ['N_050K_210_14_'+'1K_']   = 900    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_050K_210_20__EN   = 0x1  # FS_10M50 = 210e6/  20 # // 210	20		210	50	NA	10.500	131072	624.152	619	129990.00
	TEST_FREQ_HZ_dict ['N_050K_210_20_'] = 50e3
	#
	SAMPLES_DFT_dict ['N_050K_210_20_'+'128K_'] = 129990 # 619  #
	SAMPLES_DFT_dict ['N_050K_210_20_'+'64K_']  = 65310  # 311  #
	SAMPLES_DFT_dict ['N_050K_210_20_'+'32K_']  = 31710  # 151  #
	SAMPLES_DFT_dict ['N_050K_210_20_'+'16K_']  = 18690  # 89   #
	SAMPLES_DFT_dict ['N_050K_210_20_'+'8K_']   = 15330  # 73   #
	SAMPLES_DFT_dict ['N_050K_210_20_'+'4K_']   = 3990   # 19   #
	SAMPLES_DFT_dict ['N_050K_210_20_'+'2K_']   = 1470   # 7    #
	SAMPLES_DFT_dict ['N_050K_210_20_'+'1K_']   = 630    # 3    #
	######################################
	#
	######################################
	#TEST_CASE__N_050K_210_21__EN  = 0x1  # FS_10M00 = 210e6/ 21 # // 210	21	200	50	NA	10.000	131072	655.360	619	123800.00
	TEST_FREQ_HZ_dict ['N_050K_210_21_'] = 50e3
	#
	SAMPLES_DFT_dict ['N_050K_210_21_'+'128K_'] = 123800 # 619 #
	SAMPLES_DFT_dict ['N_050K_210_21_'+'64K_']  = 63400  # 317 #
	SAMPLES_DFT_dict ['N_050K_210_21_'+'32K_']  = 32600  # 163 #
	SAMPLES_DFT_dict ['N_050K_210_21_'+'16K_']  = 15800  # 79  #
	SAMPLES_DFT_dict ['N_050K_210_21_'+'8K_']   = 7400   # 37  #
	SAMPLES_DFT_dict ['N_050K_210_21_'+'4K_']   = 3800   # 19  #
	SAMPLES_DFT_dict ['N_050K_210_21_'+'2K_']   = 1400   # 7   #
	SAMPLES_DFT_dict ['N_050K_210_21_'+'1K_']   = 1000   # 5   #
	######################################
	#
	######################################
	#TEST_CASE__N_050K_210_15__EN  = 0x1  # FS_14M00 = 210e6/ 15 # // 210	15	280	50	NA	14.000	131072	468.114	467	130760.00
	TEST_FREQ_HZ_dict ['N_050K_210_15_'] = 50e3
	#
	SAMPLES_DFT_dict ['N_050K_210_15_'+'128K_'] = 130760 # 467  #
	SAMPLES_DFT_dict ['N_050K_210_15_'+'64K_']  = 65240  # 233  #
	SAMPLES_DFT_dict ['N_050K_210_15_'+'32K_']  = 31640  # 113  #
	SAMPLES_DFT_dict ['N_050K_210_15_'+'16K_']  = 14840  # 53   #
	SAMPLES_DFT_dict ['N_050K_210_15_'+'8K_']   = 8120   # 29   #
	SAMPLES_DFT_dict ['N_050K_210_15_'+'4K_']   = 3640   # 13   #
	SAMPLES_DFT_dict ['N_050K_210_15_'+'2K_']   = 1960   # 7    #
	SAMPLES_DFT_dict ['N_050K_210_15_'+'1K_']   = 840    # 3    #
	######################################
	
	
	#TEST_CASE__N_100K
	######################################
	#TEST_CASE__N_100K_210_14__EN   = 1  # FS_15M  = 210e6/14   # // 210	14	150	100	15.000	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['N_100K_210_14_'] = 100e3 
	#
	SAMPLES_DFT_dict ['N_100K_210_14_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['N_100K_210_14_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['N_100K_210_14_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['N_100K_210_14_'+'16K_']  = 16350  # 109  # 2%
	SAMPLES_DFT_dict ['N_100K_210_14_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['N_100K_210_14_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['N_100K_210_14_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['N_100K_210_14_'+'1K_']   = 750    # 5    #
	######################################
	#
	######################################
	#TEST_CASE__N_100K_210_15__EN = 0x1  # FS_14M00 = 210e6/15 # // 210	15	140	100		NA	14.000	131072	936.229		929		130060.00
	TEST_FREQ_HZ_dict ['N_100K_210_15_'] = 100e3 
	#
	SAMPLES_DFT_dict ['N_100K_210_15_'+'128K_'] = 130060 # 929  #
	SAMPLES_DFT_dict ['N_100K_210_15_'+'64K_']  = 65380  # 467  #
	SAMPLES_DFT_dict ['N_100K_210_15_'+'32K_']  = 32620  # 233  #
	SAMPLES_DFT_dict ['N_100K_210_15_'+'16K_']  = 15820  # 113  #
	SAMPLES_DFT_dict ['N_100K_210_15_'+'8K_']   = 7420   # 53   #
	SAMPLES_DFT_dict ['N_100K_210_15_'+'4K_']   = 4060   # 29   #
	SAMPLES_DFT_dict ['N_100K_210_15_'+'2K_']   = 1820   # 13   #
	SAMPLES_DFT_dict ['N_100K_210_15_'+'1K_']   = 980    # 7    #
	######################################
	
	
	#TEST_CASE__N_200K
	######################################
	#TEST_CASE__N_200K_210_14__EN   = 1  # FS_15M  = 210e6/14   # // 210	14	75	200	15.000	131072	1747.626667	1747	131025.00
	TEST_FREQ_HZ_dict ['N_200K_210_14_'] = 200e3
	#
	SAMPLES_DFT_dict ['N_200K_210_14_'+'128K_'] = 131025 # 1747 #
	SAMPLES_DFT_dict ['N_200K_210_14_'+'64K_']  = 64725  # 863  #
	SAMPLES_DFT_dict ['N_200K_210_14_'+'32K_']  = 32475  # 433  #
	SAMPLES_DFT_dict ['N_200K_210_14_'+'16K_']  = 15825  # 211  # 1.5%
	SAMPLES_DFT_dict ['N_200K_210_14_'+'8K_']   = 8175   # 109  #
	SAMPLES_DFT_dict ['N_200K_210_14_'+'4K_']   = 3975   # 53   #
	SAMPLES_DFT_dict ['N_200K_210_14_'+'2K_']   = 1725   # 23   #
	SAMPLES_DFT_dict ['N_200K_210_14_'+'1K_']   = 975    # 13   #
	######################################
	#
	######################################
	#TEST_CASE__N_200K_210_15__EN = 0x1  # FS_14M00 = 210e6/15 # // 210	15	70	200		NA	14.000	131072	1872.457	1871	130970.00
	TEST_FREQ_HZ_dict ['N_200K_210_15_'] = 200e3 
	#
	SAMPLES_DFT_dict ['N_200K_210_15_'+'128K_'] = 130970 # 1871  #
	SAMPLES_DFT_dict ['N_200K_210_15_'+'64K_']  = 65030  # 929   #
	SAMPLES_DFT_dict ['N_200K_210_15_'+'32K_']  = 32690  # 467   #
	SAMPLES_DFT_dict ['N_200K_210_15_'+'16K_']  = 16310  # 233   #
	SAMPLES_DFT_dict ['N_200K_210_15_'+'8K_']   = 7910   # 113   #
	SAMPLES_DFT_dict ['N_200K_210_15_'+'4K_']   = 3710   # 53    #
	SAMPLES_DFT_dict ['N_200K_210_15_'+'2K_']   = 2030   # 29    #
	SAMPLES_DFT_dict ['N_200K_210_15_'+'1K_']   = 910    # 13    #
	######################################
	
	
	#TEST_CASE__N_500K
	######################################
	#TEST_CASE__N_500K_210_14__EN   = 1  # FS_15M  = 210e6/14   # // 210	14	30	500	15.000	131072	4369.066667	4363	130890.00
	TEST_FREQ_HZ_dict ['N_500K_210_14_'] = 500e3
	#
	SAMPLES_DFT_dict ['N_500K_210_14_'+'128K_'] = 130890 # 4363 #
	SAMPLES_DFT_dict ['N_500K_210_14_'+'64K_']  = 65370  # 2179 #
	SAMPLES_DFT_dict ['N_500K_210_14_'+'32K_']  = 32730  # 1091 #
	SAMPLES_DFT_dict ['N_500K_210_14_'+'16K_']  = 16230  # 541  # 10%
	SAMPLES_DFT_dict ['N_500K_210_14_'+'8K_']   = 8130   # 271  #
	SAMPLES_DFT_dict ['N_500K_210_14_'+'4K_']   = 3930   # 131  #
	SAMPLES_DFT_dict ['N_500K_210_14_'+'2K_']   = 2010   # 67   #
	SAMPLES_DFT_dict ['N_500K_210_14_'+'1K_']   = 930    # 31   #
	######################################
	#
	######################################
	#TEST_CASE__N_500K_210_15__EN = 0x1  # FS_14M00 = 210e6/15 # // 210	15	28	500		NA	14.000	131072	4681.143	4679	131012.00
	TEST_FREQ_HZ_dict ['N_500K_210_15_'] = 500e3
	#
	SAMPLES_DFT_dict ['N_500K_210_15_'+'128K_'] = 131012 # 4679 #
	SAMPLES_DFT_dict ['N_500K_210_15_'+'64K_']  = 65492  # 2339 #
	SAMPLES_DFT_dict ['N_500K_210_15_'+'32K_']  = 32564  # 1163 #
	SAMPLES_DFT_dict ['N_500K_210_15_'+'16K_']  = 16156  # 577  #
	SAMPLES_DFT_dict ['N_500K_210_15_'+'8K_']   = 7924   # 283  #
	SAMPLES_DFT_dict ['N_500K_210_15_'+'4K_']   = 3892   # 139  #
	SAMPLES_DFT_dict ['N_500K_210_15_'+'2K_']   = 2044   # 73   #
	SAMPLES_DFT_dict ['N_500K_210_15_'+'1K_']   = 868    # 31   #
	######################################
	
	
	#TEST_CASE__N_001M
	######################################
	#TEST_CASE__N_001M_210_14__EN   = 1  # FS_15M  = 210e6/14   # // 210	14	15	1000	15.000	131072	8738.133333	8737	131055.00
	TEST_FREQ_HZ_dict ['N_001M_210_14_'] = 1000e3 
	#
	SAMPLES_DFT_dict ['N_001M_210_14_'+'128K_'] = 131055 # 8737 #
	SAMPLES_DFT_dict ['N_001M_210_14_'+'64K_']  = 65445  # 4363 #
	SAMPLES_DFT_dict ['N_001M_210_14_'+'32K_']  = 32685  # 2179 #
	SAMPLES_DFT_dict ['N_001M_210_14_'+'16K_']  = 16365  # 1091 # 7.5%
	SAMPLES_DFT_dict ['N_001M_210_14_'+'8K_']   = 8115   # 541  #
	SAMPLES_DFT_dict ['N_001M_210_14_'+'4K_']   = 4065   # 271  #
	SAMPLES_DFT_dict ['N_001M_210_14_'+'2K_']   = 1965   # 131  #
	SAMPLES_DFT_dict ['N_001M_210_14_'+'1K_']   = 1005   # 67   #
	######################################
	#
	######################################
	#TEST_CASE__N_001M_210_15__EN = 0x1  # FS_14M00 = 210e6/15 # // 210	15	14	1000	NA	14.000	131072	9362.286	9349	130886.00
	TEST_FREQ_HZ_dict ['N_001M_210_15_'] = 1000e3 
	#
	SAMPLES_DFT_dict ['N_001M_210_15_'+'128K_'] = 130886 # 9349 #
	SAMPLES_DFT_dict ['N_001M_210_15_'+'64K_']  = 65506  # 4679 #
	SAMPLES_DFT_dict ['N_001M_210_15_'+'32K_']  = 32746  # 2339 #
	SAMPLES_DFT_dict ['N_001M_210_15_'+'16K_']  = 16282  # 1163 #
	SAMPLES_DFT_dict ['N_001M_210_15_'+'8K_']   = 8078   # 577  #
	SAMPLES_DFT_dict ['N_001M_210_15_'+'4K_']   = 3962   # 283  #
	SAMPLES_DFT_dict ['N_001M_210_15_'+'2K_']   = 1946   # 139  #
	SAMPLES_DFT_dict ['N_001M_210_15_'+'1K_']   = 1022   # 73   #
	######################################
	
	
	#TEST_CASE__N_002M
	######################################
	#TEST_CASE__N_002M_210_15__EN   = 1  # FS_14M  = 210e6/15   # // 210	15	7	2000	14.000	131072	18724.57143	18719	131033.00
	TEST_FREQ_HZ_dict ['N_002M_210_15_'] = 2000e3
	#
	SAMPLES_DFT_dict ['N_002M_210_15_'+'128K_'] = 131033 # 18719 #
	SAMPLES_DFT_dict ['N_002M_210_15_'+'64K_']  = 65443  # 9349  #
	SAMPLES_DFT_dict ['N_002M_210_15_'+'32K_']  = 32753  # 4679  #
	SAMPLES_DFT_dict ['N_002M_210_15_'+'16K_']  = 16373  # 2339  # 15%
	SAMPLES_DFT_dict ['N_002M_210_15_'+'8K_']   = 8141   # 1163  #
	SAMPLES_DFT_dict ['N_002M_210_15_'+'4K_']   = 4039   # 577   #
	SAMPLES_DFT_dict ['N_002M_210_15_'+'2K_']   = 1981   # 283   #
	SAMPLES_DFT_dict ['N_002M_210_15_'+'1K_']   = 973    # 139   #
	######################################
	# 
	######################################
	#TEST_CASE__N_002M_210_14__EN = 0x1  # FS_15M00 = 210e6/14 # // 210	14	7.5	2000	NA	15.000	131072	17476.267	17472	131040.00	0.01	0.52	non-prime
	TEST_FREQ_HZ_dict ['N_002M_210_14_'] = 2000e3
	# non-prime
	SAMPLES_DFT_dict ['N_002M_210_14_'+'128K_'] = 131040 # 17472 #
	SAMPLES_DFT_dict ['N_002M_210_14_'+'64K_']  = 65520  # 8736  #
	SAMPLES_DFT_dict ['N_002M_210_14_'+'32K_']  = 32760  # 4368  #
	SAMPLES_DFT_dict ['N_002M_210_14_'+'16K_']  = 16380  # 2184  #
	SAMPLES_DFT_dict ['N_002M_210_14_'+'8K_']   = 8190   # 1092  #
	SAMPLES_DFT_dict ['N_002M_210_14_'+'4K_']   = 4095   # 546   #
	SAMPLES_DFT_dict ['N_002M_210_14_'+'2K_']   = 2040   # 272   #
	SAMPLES_DFT_dict ['N_002M_210_14_'+'1K_']   = 1020   # 136   #
	######################################
	
	
	#TEST_CASE__N_005M
	######################################
	#TEST_CASE__N_005M_210_14__EN   = 1  # FS_15M  = 210e6/14   # // 210	14	3	5000	15.000	131072	43690.66667	43669	131007.00
	TEST_FREQ_HZ_dict ['N_005M_210_14_'] = 5000e3 
	#
	SAMPLES_DFT_dict ['N_005M_210_14_'+'128K_'] = 131007 # 43669 #
	SAMPLES_DFT_dict ['N_005M_210_14_'+'64K_']  = 65463  # 21821 #
	SAMPLES_DFT_dict ['N_005M_210_14_'+'32K_']  = 32727  # 10909 #
	SAMPLES_DFT_dict ['N_005M_210_14_'+'16K_']  = 16347  # 5449  # 30%
	SAMPLES_DFT_dict ['N_005M_210_14_'+'8K_']   = 8157   # 2719  #
	SAMPLES_DFT_dict ['N_005M_210_14_'+'4K_']   = 3981   # 1327  #
	SAMPLES_DFT_dict ['N_005M_210_14_'+'2K_']   = 1983   # 661   #
	SAMPLES_DFT_dict ['N_005M_210_14_'+'1K_']   = 951    # 317   #
	######################################
	#
	######################################
	#TEST_CASE__N_005M_210_15__EN = 0x1  # FS_14M00 = 210e6/15 # // 210	15	2.8	5000	NA	14.000	131072	46811.429	46810	131068.00	0.01	0.56	non-prime
	TEST_FREQ_HZ_dict ['N_005M_210_15_'] = 5000e3 
	# non-prime number 
	SAMPLES_DFT_dict ['N_005M_210_15_'+'128K_'] = 131068 # 46810 #
	SAMPLES_DFT_dict ['N_005M_210_15_'+'64K_']  = 65534  # 23405 #
	SAMPLES_DFT_dict ['N_005M_210_15_'+'32K_']  = 32760  # 11700 #
	SAMPLES_DFT_dict ['N_005M_210_15_'+'16K_']  = 16380  # 5850  #  
	SAMPLES_DFT_dict ['N_005M_210_15_'+'8K_']   = 8190   # 2925  #
	SAMPLES_DFT_dict ['N_005M_210_15_'+'4K_']   = 4088   # 1460  #
	SAMPLES_DFT_dict ['N_005M_210_15_'+'2K_']   = 2044   # 730   #
	SAMPLES_DFT_dict ['N_005M_210_15_'+'1K_']   = 1022   # 365   #
	######################################
	
	
	#TEST_CASE__N_010M
	######################################
	#TEST_CASE__N_010M_210_14__EN   = 1  # FS_15M  = 210e6/14   # // 210	14	1.5	10000	15.000	131072	87381.33333	87380	131070.00
	TEST_FREQ_HZ_dict ['N_010M_210_14_'] = 10e6
	# non-prime number 
	SAMPLES_DFT_dict ['N_010M_210_14_'+'128K_'] = 131070 # 87380 #
	SAMPLES_DFT_dict ['N_010M_210_14_'+'64K_']  = 65535  # 43690 #
	SAMPLES_DFT_dict ['N_010M_210_14_'+'32K_']  = 32766  # 21844 #
	SAMPLES_DFT_dict ['N_010M_210_14_'+'16K_']  = 16383  # 10922 # 15%
	SAMPLES_DFT_dict ['N_010M_210_14_'+'8K_']   = 8190   # 5460  #
	SAMPLES_DFT_dict ['N_010M_210_14_'+'4K_']   = 4095   # 2730  #
	SAMPLES_DFT_dict ['N_010M_210_14_'+'2K_']   = 2046   # 1364  #
	SAMPLES_DFT_dict ['N_010M_210_14_'+'1K_']   = 1023   # 682   #
	######################################
	#
	######################################
	#TEST_CASE__N_010M_210_15__EN = 0x1  # FS_14M00 = 210e6/15 # // 210	15	1.4	10000	NA	14.000	131072	93622.857	93615	131061.00	0.01	0.56	non-prime
	TEST_FREQ_HZ_dict ['N_010M_210_15_'] = 10e6
	# non-prime number 
	SAMPLES_DFT_dict ['N_010M_210_15_'+'128K_'] = 131061 # 93615 #
	SAMPLES_DFT_dict ['N_010M_210_15_'+'64K_']  = 65534  # 46810 #
	SAMPLES_DFT_dict ['N_010M_210_15_'+'32K_']  = 32767  # 23405 #
	SAMPLES_DFT_dict ['N_010M_210_15_'+'16K_']  = 16380  # 11700 #
	SAMPLES_DFT_dict ['N_010M_210_15_'+'8K_']   = 8190   # 5850  #
	SAMPLES_DFT_dict ['N_010M_210_15_'+'4K_']   = 4095   # 2925  #
	SAMPLES_DFT_dict ['N_010M_210_15_'+'2K_']   = 2044   # 1460  #
	SAMPLES_DFT_dict ['N_010M_210_15_'+'1K_']   = 1022   # 730   #
	######################################
	
	
	####
	# // TODO: coherent condition setting for undersampling
	#
	
	# test case U_001K_
	######################################
	#TEST_CASE__U_001K_210_212000__EN = 1  # FS_0K991 # // 210	105	0.001	212000	0.009433962264	0.000991	131072	1248.304762	1237	129885.00
	TEST_FREQ_HZ_dict ['U_001K_210_212000_'] = 0.009433962264e3
	#
	SAMPLES_DFT_dict ['U_001K_210_212000_'+'128K_'] = 129885 # 1237 #
	SAMPLES_DFT_dict ['U_001K_210_212000_'+'64K_']  = 64785  # 617  # 
	SAMPLES_DFT_dict ['U_001K_210_212000_'+'32K_']  = 32235  # 307  #
	SAMPLES_DFT_dict ['U_001K_210_212000_'+'16K_']  = 15855  # 151  #
	SAMPLES_DFT_dict ['U_001K_210_212000_'+'8K_']   = 7665   # 73   #
	SAMPLES_DFT_dict ['U_001K_210_212000_'+'4K_']   = 3885   # 37   # 2%
	SAMPLES_DFT_dict ['U_001K_210_212000_'+'2K_']   = 1995   # 19   #
	SAMPLES_DFT_dict ['U_001K_210_212000_'+'1K_']   = 735    # 7    #
	######################################
	#
	######################################
	#TEST_CASE__U_001K_210_210100__EN = 1  # FS_1K000 = 210e6/210100 # // 210	2100	0.001	210100	0.0004759638267	0.001000	131072	62.4152381	61	128100.00
	TEST_FREQ_HZ_dict ['U_001K_210_210100_'] = 0.0004759638267e3
	#
	SAMPLES_DFT_dict ['U_001K_210_210100_'+'128K_'] = 2100   # 128100 # 61 # NG        #__ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_001K_210_210100_'+'64K_']  = 2100   # 65100  # 31 # NG        #__ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_001K_210_210100_'+'32K_']  = 2100   # 27300  # 13 # OK 500ppm #__ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_001K_210_210100_'+'16K_']  = 2100   # 14700  # 7  # OK 500ppm #__ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_001K_210_210100_'+'8K_']   = 2100   # 6300   # 3  # OK 500ppm #__ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_001K_210_210100_'+'4K_']   = 2100   # 2  # OK 500ppm
	SAMPLES_DFT_dict ['U_001K_210_210100_'+'2K_']   = 2100   # 1  # OK 500ppm
	SAMPLES_DFT_dict ['U_001K_210_210100_'+'1K_']   = 2100   # 1  # OK 500ppm
	######################################
	#
	######################################
	#TEST_CASE__U_001K_210_210125__EN = 0x0000001  # FS_0K99941 = 210e6/210125 =  999.41 # // 1		210	210125	1680	0.001	0.000595	0.000999	16384	9.752	7	11760.00
	TEST_FREQ_HZ_dict ['U_001K_210_210125_'] = 0.000595e3
	#
	SAMPLES_DFT_dict ['U_001K_210_210125_'+'128K_'] = 11760 # 7 #
	SAMPLES_DFT_dict ['U_001K_210_210125_'+'64K_']  = 11760 # 7 #
	SAMPLES_DFT_dict ['U_001K_210_210125_'+'32K_']  = 11760 # 7 #
	SAMPLES_DFT_dict ['U_001K_210_210125_'+'16K_']  = 11760 # 7 #
	SAMPLES_DFT_dict ['U_001K_210_210125_'+'8K_']   = 5040  # 3 #
	SAMPLES_DFT_dict ['U_001K_210_210125_'+'4K_']   = 3360  # 2 #
	SAMPLES_DFT_dict ['U_001K_210_210125_'+'2K_']   = 1680  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210125_'+'1K_']   = 1680  # 1 #
	######################################
	#
	######################################
	#TEST_CASE__U_001K_210_210105__EN = 0x0000001  # FS_0K99950 = 210e6/210105 =  999.50 # // 1		210	210105	2000	0.001	0.000500	0.001000	16384	8.192	7	14000.00
	TEST_FREQ_HZ_dict ['U_001K_210_210105_'] = 0.000500e3
	#
	SAMPLES_DFT_dict ['U_001K_210_210105_'+'128K_'] = 14000 # 7 #
	SAMPLES_DFT_dict ['U_001K_210_210105_'+'64K_']  = 14000 # 7 #
	SAMPLES_DFT_dict ['U_001K_210_210105_'+'32K_']  = 14000 # 7 #
	SAMPLES_DFT_dict ['U_001K_210_210105_'+'16K_']  = 14000 # 7 #
	SAMPLES_DFT_dict ['U_001K_210_210105_'+'8K_']   = 6000  # 3 #
	SAMPLES_DFT_dict ['U_001K_210_210105_'+'4K_']   = 4000  # 2 #
	SAMPLES_DFT_dict ['U_001K_210_210105_'+'2K_']   = 2000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210105_'+'1K_']   = 2000  # 1 #
	######################################
	#
	######################################
	#TEST_CASE__U_001K_210_210075__EN = 0x0000001  # FS_0K99964 = 210e6/210075 =  999.64 # // 1		210	210075	2800	0.001	0.000357	0.001000	16384	5.851	5	14000.00
	TEST_FREQ_HZ_dict ['U_001K_210_210075_'] = 0.000357e3
	#
	SAMPLES_DFT_dict ['U_001K_210_210075_'+'128K_'] = 14000 # 5 #
	SAMPLES_DFT_dict ['U_001K_210_210075_'+'64K_']  = 14000 # 5 #
	SAMPLES_DFT_dict ['U_001K_210_210075_'+'32K_']  = 14000 # 5 #
	SAMPLES_DFT_dict ['U_001K_210_210075_'+'16K_']  = 14000 # 5 #
	SAMPLES_DFT_dict ['U_001K_210_210075_'+'8K_']   = 5600  # 2 #
	SAMPLES_DFT_dict ['U_001K_210_210075_'+'4K_']   = 2800  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210075_'+'2K_']   = 2800  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210075_'+'1K_']   = 2800  # 1 #
	######################################
	#
	######################################
	#TEST_CASE__U_001K_210_210035__EN = 0x0000001  # FS_0K99983 = 210e6/210035 =  999.83 # // 1		210	210035	6000	0.001	0.000167	0.001000	16384	2.731	2	12000.00
	TEST_FREQ_HZ_dict ['U_001K_210_210035_'] = 0.000167e3
	#
	SAMPLES_DFT_dict ['U_001K_210_210035_'+'128K_'] = 12000 # 2 #
	SAMPLES_DFT_dict ['U_001K_210_210035_'+'64K_']  = 12000 # 2 #
	SAMPLES_DFT_dict ['U_001K_210_210035_'+'32K_']  = 12000 # 2 #
	SAMPLES_DFT_dict ['U_001K_210_210035_'+'16K_']  = 12000 # 2 #
	SAMPLES_DFT_dict ['U_001K_210_210035_'+'8K_']   = 6000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210035_'+'4K_']   = 6000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210035_'+'2K_']   = 6000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210035_'+'1K_']   = 6000  # 1 #
	######################################
	#
	######################################
	#TEST_CASE__U_001K_210_210025__EN = 0x0000001  # FS_0K99988 = 210e6/210025 =  999.88 # // 1		210	210025	8400	0.001	0.000119	0.001000	16384	1.950	1	8400.00
	TEST_FREQ_HZ_dict ['U_001K_210_210025_'] = 0.000119e3
	#
	SAMPLES_DFT_dict ['U_001K_210_210025_'+'128K_'] = 8400  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210025_'+'64K_']  = 8400  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210025_'+'32K_']  = 8400  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210025_'+'16K_']  = 8400  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210025_'+'8K_']   = 8400  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210025_'+'4K_']   = 8400  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210025_'+'2K_']   = 8400  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210025_'+'1K_']   = 8400  # 1 #
	######################################
	#
	######################################
	#TEST_CASE__U_001K_210_210015__EN = 0x0000001  # FS_0K99993 = 210e6/210015 =  999.93 # // 1		210	210015	14000	0.001	0.000071	0.001000	16384	1.170	1	14000.00
	TEST_FREQ_HZ_dict ['U_001K_210_210015_'] = 0.000071e3
	#
	SAMPLES_DFT_dict ['U_001K_210_210015_'+'128K_'] = 14000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210015_'+'64K_']  = 14000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210015_'+'32K_']  = 14000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210015_'+'16K_']  = 14000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210015_'+'8K_']   = 14000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210015_'+'4K_']   = 14000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210015_'+'2K_']   = 14000  # 1 #
	SAMPLES_DFT_dict ['U_001K_210_210015_'+'1K_']   = 14000  # 1 #
	######################################
	
	
	
	# test case U_002K_
	######################################
	#TEST_CASE__U_002K_210_105050__EN = 1  # FS_1K999 = 210e6/105050 # // 210	2100	0.002	105050	0.0009519276535	0.001999	131072	62.4152381	61	128100.00
	TEST_FREQ_HZ_dict ['U_002K_210_105050_'] = 0.0009519276535e3
	#
	SAMPLES_DFT_dict ['U_002K_210_105050_'+'128K_'] = 2100   # 128100 # 61 # NG        # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_002K_210_105050_'+'64K_']  = 2100   # 65100  # 31 # NG        # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_002K_210_105050_'+'32K_']  = 2100   # 27300  # 13 # OK 200ppm # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_002K_210_105050_'+'16K_']  = 2100   # 14700  # 7  # OK 200ppm # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_002K_210_105050_'+'8K_']   = 2100   # 6300   # 3  # OK 200ppm # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_002K_210_105050_'+'4K_']   = 2100   # 1  # OK 200ppm 
	SAMPLES_DFT_dict ['U_002K_210_105050_'+'2K_']   = 2100   # 1  # OK 200ppm
	SAMPLES_DFT_dict ['U_002K_210_105050_'+'1K_']   = 2100   # 1  # OK 200ppm
	######################################
	#
	######################################
	#TEST_CASE__U_002K_210_105075__EN = 0x0000001  # FS_1K99857 = 210e6/105075 = 1998.57 # // 2		210	105075	1400	0.002	0.001428	0.001999	16384	11.703	11	15400.00
	TEST_FREQ_HZ_dict ['U_002K_210_105075_'] = 0.001428e3
	#
	SAMPLES_DFT_dict ['U_002K_210_105075_'+'128K_'] = 15400 # 11 #
	SAMPLES_DFT_dict ['U_002K_210_105075_'+'64K_']  = 15400 # 11 #
	SAMPLES_DFT_dict ['U_002K_210_105075_'+'32K_']  = 15400 # 11 #
	SAMPLES_DFT_dict ['U_002K_210_105075_'+'16K_']  = 15400 # 11 #
	SAMPLES_DFT_dict ['U_002K_210_105075_'+'8K_']   = 7000  # 5  #
	SAMPLES_DFT_dict ['U_002K_210_105075_'+'4K_']   = 2800  # 2  #
	SAMPLES_DFT_dict ['U_002K_210_105075_'+'2K_']   = 1400  # 1  #
	SAMPLES_DFT_dict ['U_002K_210_105075_'+'1K_']   = 1400  # 1  #
	######################################
	#
	######################################
	#TEST_CASE__U_002K_210_105070__EN = 0x0000001  # FS_1K99867 = 210e6/105070 = 1998.67 # // 2		210	105070	1500	0.002	0.001332	0.001999	16384	10.923	7	10500.0
	TEST_FREQ_HZ_dict ['U_002K_210_105070_'] = 0.001332e3
	#
	SAMPLES_DFT_dict ['U_002K_210_105070_'+'128K_'] = 10500  # 7 #
	SAMPLES_DFT_dict ['U_002K_210_105070_'+'64K_']  = 10500  # 7 #
	SAMPLES_DFT_dict ['U_002K_210_105070_'+'32K_']  = 10500  # 7 #
	SAMPLES_DFT_dict ['U_002K_210_105070_'+'16K_']  = 10500  # 7 #
	SAMPLES_DFT_dict ['U_002K_210_105070_'+'8K_']   = 7500   # 5 #
	SAMPLES_DFT_dict ['U_002K_210_105070_'+'4K_']   = 3000   # 2 #
	SAMPLES_DFT_dict ['U_002K_210_105070_'+'2K_']   = 1500   # 1 #
	SAMPLES_DFT_dict ['U_002K_210_105070_'+'1K_']   = 1500   # 1 #
	######################################
	#
	######################################
	#TEST_CASE__U_002K_210_105035__EN = 0x0000001  # FS_1K99933 = 210e6/105035 = 1999.33 # // 2		210	105035	3000	0.002	0.000666	0.001999	16384	5.461	5	15000.0
	TEST_FREQ_HZ_dict ['U_002K_210_105035_'] = 0.000666e3
	#
	SAMPLES_DFT_dict ['U_002K_210_105035_'+'128K_'] = 15000  # 5 #
	SAMPLES_DFT_dict ['U_002K_210_105035_'+'64K_']  = 15000  # 5 #
	SAMPLES_DFT_dict ['U_002K_210_105035_'+'32K_']  = 15000  # 5 #
	SAMPLES_DFT_dict ['U_002K_210_105035_'+'16K_']  = 15000  # 5 #
	SAMPLES_DFT_dict ['U_002K_210_105035_'+'8K_']   = 6000   # 2 #
	SAMPLES_DFT_dict ['U_002K_210_105035_'+'4K_']   = 3000   # 1 #
	SAMPLES_DFT_dict ['U_002K_210_105035_'+'2K_']   = 3000   # 1 #
	SAMPLES_DFT_dict ['U_002K_210_105035_'+'1K_']   = 3000   # 1 #
	######################################
	
	
	
	# test case U_005K_
	######################################
	# //210	5250	0.005	42008	0.000952199581	0.004999	131072	24.96609524	23	120750.00
	TEST_FREQ_HZ_dict ['U_005K_'] = 000.952199581 
	#
	SAMPLES_DFT_dict ['U_005K_'+'128K_'] = 120750 # 23 # poor 10%
	SAMPLES_DFT_dict ['U_005K_'+'64K_']  = 57750  # 11 # poor 10%
	SAMPLES_DFT_dict ['U_005K_'+'32K_']  = 26250  # 5  # 400ppm
	SAMPLES_DFT_dict ['U_005K_'+'16K_']  = 15750  # 3  # 400ppm
	SAMPLES_DFT_dict ['U_005K_'+'8K_']   = 10500  # 2  # 400ppm
	SAMPLES_DFT_dict ['U_005K_'+'4K_']   = 5250   # 1  # 400ppm
	SAMPLES_DFT_dict ['U_005K_'+'2K_']   = 5250   # 1  # 400ppm
	SAMPLES_DFT_dict ['U_005K_'+'1K_']   = 5250   # 1  # 400ppm
	######################################
	#
	######################################
	#TEST_CASE__U_005K_210_42020__EN  = 1  # FS_4K998 = 210e6/ 42020 # // 210	2100	0.005	42020	0.002379819134	0.004998	131072	62.4152381	61	128100.00
	TEST_FREQ_HZ_dict ['U_005K_210_42020_'] = 0.002379819134e3
	#
	SAMPLES_DFT_dict ['U_005K_210_42020_'+'128K_'] = 2100   # 128100 # 61 # NG       # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_005K_210_42020_'+'64K_']  = 2100   # 65100  # 31 # NG       # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_005K_210_42020_'+'32K_']  = 2100   # 27300  # 13 # OK 40ppm # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_005K_210_42020_'+'16K_']  = 2100   # 14700  # 7  # OK 40ppm # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_005K_210_42020_'+'8K_']   = 2100   # 6300   # 3  # OK 40ppm # __ADC_NUM_SAMPLES_SHORT2 = 2**12
	SAMPLES_DFT_dict ['U_005K_210_42020_'+'4K_']   = 2100   # 1  # OK 40ppm
	SAMPLES_DFT_dict ['U_005K_210_42020_'+'2K_']   = 2100   # 1  # OK 40ppm
	SAMPLES_DFT_dict ['U_005K_210_42020_'+'1K_']   = 2100   # 1  # OK 40ppm
	######################################
	#
	######################################
	#TEST_CASE__U_005K_210_42025__EN  = 0x0000001  # FS_4K9970  = 210e6/ 42025 =  4997.0 # // 5		210	42025	1680	0.005	0.002974	0.004997	16384	9.752	7	11760.00
	TEST_FREQ_HZ_dict ['U_005K_210_42025_'] = 0.002974e3
	#
	SAMPLES_DFT_dict ['U_005K_210_42025_'+'128K_'] = 11760  # 7  #
	SAMPLES_DFT_dict ['U_005K_210_42025_'+'64K_']  = 11760  # 7  #
	SAMPLES_DFT_dict ['U_005K_210_42025_'+'32K_']  = 11760  # 7  #
	SAMPLES_DFT_dict ['U_005K_210_42025_'+'16K_']  = 11760  # 7  #
	SAMPLES_DFT_dict ['U_005K_210_42025_'+'8K_']   = 5040   # 3  #
	SAMPLES_DFT_dict ['U_005K_210_42025_'+'4K_']   = 3360   # 2  #
	SAMPLES_DFT_dict ['U_005K_210_42025_'+'2K_']   = 1680   # 1  #
	SAMPLES_DFT_dict ['U_005K_210_42025_'+'1K_']   = 1680   # 1  #
	######################################
	#
	######################################
	#TEST_CASE__U_005K_210_42021__EN  = 0x0000001  # FS_4K9975  = 210e6/ 42021 =  4997.5 # // 5		210	42021	2000	0.005	0.002499	0.004998	16384	8.192	7	14000.00
	TEST_FREQ_HZ_dict ['U_005K_210_42021_'] = 0.002499e3
	#
	SAMPLES_DFT_dict ['U_005K_210_42021_'+'128K_'] = 14000  # 7  #
	SAMPLES_DFT_dict ['U_005K_210_42021_'+'64K_']  = 14000  # 7  #
	SAMPLES_DFT_dict ['U_005K_210_42021_'+'32K_']  = 14000  # 7  #
	SAMPLES_DFT_dict ['U_005K_210_42021_'+'16K_']  = 14000  # 7  #
	SAMPLES_DFT_dict ['U_005K_210_42021_'+'8K_']   = 6000   # 3  #
	SAMPLES_DFT_dict ['U_005K_210_42021_'+'4K_']   = 4000   # 2  #
	SAMPLES_DFT_dict ['U_005K_210_42021_'+'2K_']   = 2000   # 1  #
	SAMPLES_DFT_dict ['U_005K_210_42021_'+'1K_']   = 2000   # 1  #
	######################################
	#
	######################################
	#TEST_CASE__U_005K_210_42015__EN  = 0x0000001  # FS_4K9982  = 210e6/ 42015 =  4998.2 # // 5		210	42015	2800	0.005	0.001785	0.004998	16384	5.851	5	14000.00
	TEST_FREQ_HZ_dict ['U_005K_210_42015_'] = 0.002499e3
	#
	SAMPLES_DFT_dict ['U_005K_210_42015_'+'128K_'] = 14000  # 5  #
	SAMPLES_DFT_dict ['U_005K_210_42015_'+'64K_']  = 14000  # 5  #
	SAMPLES_DFT_dict ['U_005K_210_42015_'+'32K_']  = 14000  # 5  #
	SAMPLES_DFT_dict ['U_005K_210_42015_'+'16K_']  = 14000  # 5  #
	SAMPLES_DFT_dict ['U_005K_210_42015_'+'8K_']   = 5600   # 2  #
	SAMPLES_DFT_dict ['U_005K_210_42015_'+'4K_']   = 2800   # 1  #
	SAMPLES_DFT_dict ['U_005K_210_42015_'+'2K_']   = 2800   # 1  #
	SAMPLES_DFT_dict ['U_005K_210_42015_'+'1K_']   = 2800   # 1  #
	######################################
	
	
	# test case U_010K_
	######################################
	# //210	5250	0.01	21004	0.001904399162	0.009998	131072	24.96609524	23	120750.00
	TEST_FREQ_HZ_dict ['U_010K_'] = 001.904399162 
	#
	SAMPLES_DFT_dict ['U_010K_'+'128K_'] = 120750 # 23 # poor 10%
	#SAMPLES_DFT_dict ['U_010K_'+'64K_']  = 89250  # 17 # poor 10%
	#SAMPLES_DFT_dict ['U_010K_'+'64K_']  = 68250  # 13 # poor 10%
	SAMPLES_DFT_dict ['U_010K_'+'64K_']  = 57750  # 11 # poor 10%
	#SAMPLES_DFT_dict ['U_010K_'+'32K_']  = 36750  # 7  # poor 10%
	#SAMPLES_DFT_dict ['U_010K_'+'32K_']  = 31500  # 6  # 200ppm // non-prime 
	SAMPLES_DFT_dict ['U_010K_'+'32K_']  = 26250  # 5  # 200ppm
	SAMPLES_DFT_dict ['U_010K_'+'16K_']  = 15750  # 3  # 200ppm
	SAMPLES_DFT_dict ['U_010K_'+'8K_']   = 10500  # 2  # 200ppm
	SAMPLES_DFT_dict ['U_010K_'+'4K_']   = 5250   # 1  # 200ppm
	SAMPLES_DFT_dict ['U_010K_'+'2K_']   = 5250   # 1  # 200ppm
	SAMPLES_DFT_dict ['U_010K_'+'1K_']   = 5250   # 1  # 200ppm
	######################################
	#
	######################################
	#TEST_CASE__U_010K_210_21010__EN  = 1  # FS_9K995 = 210e6/ 21010 # // 210	2100	0.01	21010	0.004759638267	0.009995	131072	62.4152381	61	128100.00
	TEST_FREQ_HZ_dict ['U_010K_210_21010_'] = 0.004759638267e3
	#
	SAMPLES_DFT_dict ['U_010K_210_21010_'+'128K_'] = 27300  #128100 # 61 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_010K_210_21010_'+'64K_']  = 27300  #65100  # 31 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_010K_210_21010_'+'32K_']  = 27300  # 13 #
	SAMPLES_DFT_dict ['U_010K_210_21010_'+'16K_']  = 14700  # 7  #
	SAMPLES_DFT_dict ['U_010K_210_21010_'+'8K_']   = 6300   # 3  #
	SAMPLES_DFT_dict ['U_010K_210_21010_'+'4K_']   = 2100   # 1  #
	SAMPLES_DFT_dict ['U_010K_210_21010_'+'2K_']   = 2100   # 1  #
	SAMPLES_DFT_dict ['U_010K_210_21010_'+'1K_']   = 2100   # 1  #
	######################################
	#
	######################################
	#TEST_CASE__U_010K_210_21015__EN  = 0x0000001  # FS_9K9929  = 210e6/ 21015 =  9992.9 # // 10		210	21015	1400	0.01	0.007138	0.009993	131072	93.623	89	124600.00
	TEST_FREQ_HZ_dict ['U_010K_210_21015_'] = 0.007138e3
	#
	SAMPLES_DFT_dict ['U_010K_210_21015_'+'128K_'] = 124600 # 89  #
	SAMPLES_DFT_dict ['U_010K_210_21015_'+'64K_']  = 60200  # 43  #
	SAMPLES_DFT_dict ['U_010K_210_21015_'+'32K_']  = 32200  # 23  #
	SAMPLES_DFT_dict ['U_010K_210_21015_'+'16K_']  = 15400  # 11  #
	SAMPLES_DFT_dict ['U_010K_210_21015_'+'8K_']   = 7000   # 5   #
	SAMPLES_DFT_dict ['U_010K_210_21015_'+'4K_']   = 4200   # 3   #
	SAMPLES_DFT_dict ['U_010K_210_21015_'+'2K_']   = 2800   # 2   #
	SAMPLES_DFT_dict ['U_010K_210_21015_'+'1K_']   = 1400   # 1   #
	######################################
	#
	######################################
	#TEST_CASE__U_010K_210_21014__EN  = 0x0000001  # FS_9K9933  = 210e6/ 21014 =  9993.3 # // 10		210	21014	1500	0.01	0.006662	0.009993	131072	87.381	83	124500.00
	TEST_FREQ_HZ_dict ['U_010K_210_21014_'] = 0.006662e3
	#
	SAMPLES_DFT_dict ['U_010K_210_21014_'+'128K_'] = 28500  # 124500 # 83 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_010K_210_21014_'+'64K_']  = 28500  # 64500  # 43 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_010K_210_21014_'+'32K_']  = 28500  # 19 #
	SAMPLES_DFT_dict ['U_010K_210_21014_'+'16K_']  = 10500  # 7  #
	SAMPLES_DFT_dict ['U_010K_210_21014_'+'8K_']   = 7500   # 5  #
	SAMPLES_DFT_dict ['U_010K_210_21014_'+'4K_']   = 3000   # 2  #
	SAMPLES_DFT_dict ['U_010K_210_21014_'+'2K_']   = 1500   # 1  #
	SAMPLES_DFT_dict ['U_010K_210_21014_'+'1K_']   = 1500   # 1  #
	######################################
	#
	######################################
	#TEST_CASE__U_010K_210_21007__EN  = 0x0000001  # FS_9K9967  = 210e6/ 21007 =  9996.7 # // 10		210	21007	3000	0.01	0.003332	0.009997	131072	43.691	43	129000.00
	TEST_FREQ_HZ_dict ['U_010K_210_21007_'] = 0.003332e3
	#
	SAMPLES_DFT_dict ['U_010K_210_21007_'+'128K_'] = 129000 # 43 #
	SAMPLES_DFT_dict ['U_010K_210_21007_'+'64K_']  = 57000  # 19 #
	SAMPLES_DFT_dict ['U_010K_210_21007_'+'32K_']  = 21000  # 7  #
	SAMPLES_DFT_dict ['U_010K_210_21007_'+'16K_']  = 15000  # 5  #
	SAMPLES_DFT_dict ['U_010K_210_21007_'+'8K_']   = 6000   # 2  #
	SAMPLES_DFT_dict ['U_010K_210_21007_'+'4K_']   = 3000   # 1  #
	SAMPLES_DFT_dict ['U_010K_210_21007_'+'2K_']   = 3000   # 1  #
	SAMPLES_DFT_dict ['U_010K_210_21007_'+'1K_']   = 3000   # 1  #
	######################################
	
	
	
	# test case U_020K_
	######################################
	# //210	1050	0.02	10510	0.01902949572	0.019981	131072	124.8304762	113	118650.00
	TEST_FREQ_HZ_dict ['U_020K_'] = 019.02949572 
	#
	SAMPLES_DFT_dict ['U_020K_'+'128K_'] = 118650 # 113 # poor 2%
	SAMPLES_DFT_dict ['U_020K_'+'64K_']  = 55650  # 53  # poor 0.2%
	SAMPLES_DFT_dict ['U_020K_'+'32K_']  = 24150  # 23  # poor 0.2%
	SAMPLES_DFT_dict ['U_020K_'+'16K_']  = 13650  # 13  # poor 0.2%
	SAMPLES_DFT_dict ['U_020K_'+'8K_']   = 7350   # 7   # poor 0.2%
	SAMPLES_DFT_dict ['U_020K_'+'4K_']   = 3150   # 3   # poor 0.2%
	SAMPLES_DFT_dict ['U_020K_'+'2K_']   = 2100   # 2   # poor 0.2%
	SAMPLES_DFT_dict ['U_020K_'+'1K_']   = 1050   # 1   # poor 0.2%
	######################################
	#
	######################################
	#TEST_CASE__U_020K_210_10505__EN  = 1  # FS_19K99 = 210e6/ 10505 # // 210	2100	0.02	10505	0.009519276535	0.019990	131072	62.4152381	61	128100.00
	TEST_FREQ_HZ_dict ['U_020K_210_10505_'] = 0.009519276535e3
	#
	SAMPLES_DFT_dict ['U_020K_210_10505_'+'128K_'] = 27300  # 128100 # 61 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_020K_210_10505_'+'64K_']  = 27300  # 65100  # 31 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_020K_210_10505_'+'32K_']  = 27300  # 13 #
	SAMPLES_DFT_dict ['U_020K_210_10505_'+'16K_']  = 14700  # 7  # OK 30ppm
	SAMPLES_DFT_dict ['U_020K_210_10505_'+'8K_']   = 6300   # 3  #
	SAMPLES_DFT_dict ['U_020K_210_10505_'+'4K_']   = 2100   # 1  #
	SAMPLES_DFT_dict ['U_020K_210_10505_'+'2K_']   = 2100   # 1  #
	SAMPLES_DFT_dict ['U_020K_210_10505_'+'1K_']   = 2100   # 1  #
	######################################
	#
	######################################
	#TEST_CASE__U_020K_210_10507__EN  = 0x0000001  # FS_19K987  = 210e6/ 10507 = 19986.7 # // 20		210	10507	1500	0.02	0.013324	0.019987	131072	87.381	83	124500.00
	TEST_FREQ_HZ_dict ['U_020K_210_10507_'] = 0.013324e3
	#
	SAMPLES_DFT_dict ['U_020K_210_10507_'+'128K_'] = 28500  # 124500 # 83 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_020K_210_10507_'+'64K_']  = 28500  # 64500  # 43 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_020K_210_10507_'+'32K_']  = 28500  # 19 #
	SAMPLES_DFT_dict ['U_020K_210_10507_'+'16K_']  = 10500  # 7  #
	SAMPLES_DFT_dict ['U_020K_210_10507_'+'8K_']   = 7500   # 5  #
	SAMPLES_DFT_dict ['U_020K_210_10507_'+'4K_']   = 3000   # 2  #
	SAMPLES_DFT_dict ['U_020K_210_10507_'+'2K_']   = 1500   # 1  #
	SAMPLES_DFT_dict ['U_020K_210_10507_'+'1K_']   = 1500   # 1  #
	######################################
	#
	######################################
	#TEST_CASE__U_020K_210_10503__EN  = 0x0000001  # FS_19K994  = 210e6/ 10503 = 19994.3 # // 20		210	10503	3500	0.02	0.005713	0.019994	131072	37.449	37	129500.00
	TEST_FREQ_HZ_dict ['U_020K_210_10503_'] = 0.005713e3
	#
	SAMPLES_DFT_dict ['U_020K_210_10503_'+'128K_'] = 129500 # 37 #
	SAMPLES_DFT_dict ['U_020K_210_10503_'+'64K_']  = 59500  # 17 #
	SAMPLES_DFT_dict ['U_020K_210_10503_'+'32K_']  = 24500  # 7  #
	SAMPLES_DFT_dict ['U_020K_210_10503_'+'16K_']  = 10500  # 3  #
	SAMPLES_DFT_dict ['U_020K_210_10503_'+'8K_']   = 7000   # 2  #
	SAMPLES_DFT_dict ['U_020K_210_10503_'+'4K_']   = 3500   # 1  #
	SAMPLES_DFT_dict ['U_020K_210_10503_'+'2K_']   = 3500   # 1  #
	SAMPLES_DFT_dict ['U_020K_210_10503_'+'1K_']   = 3500   # 1  #
	######################################
	#
	######################################
	#TEST_CASE__U_020K_210_10501__EN  = 0x0000001  # FS_19K998  = 210e6/ 10501 = 19998.1 # // 20		210	10501	10500	0.02	0.001905	0.019998	131072	12.483	11	115500.00
	TEST_FREQ_HZ_dict ['U_020K_210_10501_'] = 0.001905e3
	#
	SAMPLES_DFT_dict ['U_020K_210_10501_'+'128K_'] = 31500  # 115500 # 11 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_020K_210_10501_'+'64K_']  = 31500  # 52500  # 5  # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_020K_210_10501_'+'32K_']  = 31500  # 3  #
	SAMPLES_DFT_dict ['U_020K_210_10501_'+'16K_']  = 10500  # 1  #
	SAMPLES_DFT_dict ['U_020K_210_10501_'+'8K_']   = 10500  # 1  #
	SAMPLES_DFT_dict ['U_020K_210_10501_'+'4K_']   = 10500  # 1  #
	SAMPLES_DFT_dict ['U_020K_210_10501_'+'2K_']   = 10500  # 1  #
	SAMPLES_DFT_dict ['U_020K_210_10501_'+'1K_']   = 10500  # 1  #
	######################################
	
	
	# test case U_050K_
	######################################
	# TEST_CASE__U_050K_210_4204__EN   = 1  # FS_49K95 = 210e6/  4204 # // 210	1050	0.05	4204	0.0475737393		0.049952	131072	124.8304762	113		118650.00
	TEST_FREQ_HZ_dict ['U_050K_210_4204_'] = 0.0475737393e3 
	#
	SAMPLES_DFT_dict ['U_050K_210_4204_'+'128K_'] = 118650 # 113  # 2%
	SAMPLES_DFT_dict ['U_050K_210_4204_'+'64K_']  = 55650  # 53   # 0.1%
	SAMPLES_DFT_dict ['U_050K_210_4204_'+'32K_']  = 24150  # 23   # 0.1% 
	SAMPLES_DFT_dict ['U_050K_210_4204_'+'16K_']  = 13650  # 13   # 0.1%
	SAMPLES_DFT_dict ['U_050K_210_4204_'+'8K_']   = 7350   # 7    # 0.1%
	SAMPLES_DFT_dict ['U_050K_210_4204_'+'4K_']   = 3150   # 3    # 0.1%
	SAMPLES_DFT_dict ['U_050K_210_4204_'+'2K_']   = 2100   # 2    # 0.1%
	SAMPLES_DFT_dict ['U_050K_210_4204_'+'1K_']   = 1050   # 1    # 0.1%
	######################################
	#
	######################################
	#TEST_CASE__U_050K_210_4240__EN   = 1  # FS_49K53 = 210e6/  4240 # // 210	105	0.05	4240	0.4716981132	0.049528	131072	1248.304762	1237	129885.00
	TEST_FREQ_HZ_dict ['U_050K_210_4240_'] = 0.4716981132e3
	#
	SAMPLES_DFT_dict ['U_050K_210_4240_'+'128K_'] = 129885 # 1237 #
	SAMPLES_DFT_dict ['U_050K_210_4240_'+'64K_']  = 64785  # 617  #
	SAMPLES_DFT_dict ['U_050K_210_4240_'+'32K_']  = 32235  # 307  #
	SAMPLES_DFT_dict ['U_050K_210_4240_'+'16K_']  = 15855  # 151  # NG 1.5%
	SAMPLES_DFT_dict ['U_050K_210_4240_'+'8K_']   = 7665   # 73   #
	SAMPLES_DFT_dict ['U_050K_210_4240_'+'4K_']   = 3885   # 37   #
	SAMPLES_DFT_dict ['U_050K_210_4240_'+'2K_']   = 1995   # 19   #
	SAMPLES_DFT_dict ['U_050K_210_4240_'+'1K_']   = 735    # 7    #
	###################################
	#
	######################################
	# TEST_CASE__U_050K_210_4250__EN   = 1  # FS_49K41 = 210e6/  4250  # // 210	84	0.05	4250	0.5882352941	0.049412	131072	1560.380952	1559	130956.00	1559
	TEST_FREQ_HZ_dict ['U_050K_210_4250_'] = 0.5882352941e3
	#
	SAMPLES_DFT_dict ['U_050K_210_4250_'+'128K_'] = 130956 # 1559
	SAMPLES_DFT_dict ['U_050K_210_4250_'+'64K_']  = 64932  # 773
	SAMPLES_DFT_dict ['U_050K_210_4250_'+'32K_']  = 32676  # 389
	SAMPLES_DFT_dict ['U_050K_210_4250_'+'16K_']  = 16212  # 193  # 500ppm  # NG 0.1%
	SAMPLES_DFT_dict ['U_050K_210_4250_'+'8K_']   = 8148   # 97
	SAMPLES_DFT_dict ['U_050K_210_4250_'+'4K_']   = 3948   # 47
	SAMPLES_DFT_dict ['U_050K_210_4250_'+'2K_']   = 1932   # 23
	SAMPLES_DFT_dict ['U_050K_210_4250_'+'1K_']   = 924    # 11
	###################################
	#
	###################################
	#TEST_CASE__U_050K_210_4228__EN   = 1  # FS_49K66 = 210e6/  4228  # // 210	150	0.05	4228	0.3311258278	0.049669	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['U_050K_210_4228_'] = 0.3311258278e3
	#
	SAMPLES_DFT_dict ['U_050K_210_4228_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['U_050K_210_4228_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['U_050K_210_4228_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['U_050K_210_4228_'+'16K_']  = 16350  # 109  # NG 2%
	SAMPLES_DFT_dict ['U_050K_210_4228_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['U_050K_210_4228_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['U_050K_210_4228_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['U_050K_210_4228_'+'1K_']   = 750    # 5    #
	###################################
	#
	###################################
	#TEST_CASE__U_050K_210_4201__EN   = 1  # FS_49K99 = 210e6/  4201 # // 210	4200	0.05	4201	0.01190192811	0.049988	131072	31.20761905	31	130200.00
	TEST_FREQ_HZ_dict ['U_050K_210_4201_'] = 0.01190192811e3
	#
	SAMPLES_DFT_dict ['U_050K_210_4201_'+'128K_'] = 130200 # 31 #
	SAMPLES_DFT_dict ['U_050K_210_4201_'+'64K_']  = 54600  # 13 #
	SAMPLES_DFT_dict ['U_050K_210_4201_'+'32K_']  = 29400  # 7  #
	SAMPLES_DFT_dict ['U_050K_210_4201_'+'16K_']  = 12600  # 3  # OK 40ppm
	SAMPLES_DFT_dict ['U_050K_210_4201_'+'8K_']   = 4200   # 1  #
	SAMPLES_DFT_dict ['U_050K_210_4201_'+'4K_']   = 4200   # 1  #
	SAMPLES_DFT_dict ['U_050K_210_4201_'+'2K_']   = 4200   # 1  #
	SAMPLES_DFT_dict ['U_050K_210_4201_'+'1K_']   = 4200   # 1  #
	###################################
	#
	###################################
	#TEST_CASE__U_050K_210_4202__EN   = 1  # FS_49K98 = 210e6/  4202 # // 210	2100	0.05	4202	0.02379819134	0.049976	131072	62.4152381	61	128100.00
	TEST_FREQ_HZ_dict ['U_050K_210_4202_'] = 0.02379819134e3
	#
	SAMPLES_DFT_dict ['U_050K_210_4202_'+'128K_'] = 27300  # 128100 # 61 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_050K_210_4202_'+'64K_']  = 27300  # 65100  # 31 # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_050K_210_4202_'+'32K_']  = 27300  # 13 #
	SAMPLES_DFT_dict ['U_050K_210_4202_'+'16K_']  = 14700  # 7  # OK 40ppm
	SAMPLES_DFT_dict ['U_050K_210_4202_'+'8K_']   = 6300   # 3  #
	SAMPLES_DFT_dict ['U_050K_210_4202_'+'4K_']   = 2100   # 1  #
	SAMPLES_DFT_dict ['U_050K_210_4202_'+'2K_']   = 2100   # 1  #
	SAMPLES_DFT_dict ['U_050K_210_4202_'+'1K_']   = 2100   # 1  #
	###################################
	#
	###################################
	#TEST_CASE__U_050K_210_4203__EN   = 0x0000001  # FS_49K96   = 210e6/  4203           # // 50		210	4203	1400	0.05	0.035689	0.049964	131072	93.623	89	124600.00
	TEST_FREQ_HZ_dict ['U_050K_210_4203_'] = 0.035689e3
	#
	SAMPLES_DFT_dict ['U_050K_210_4203_'+'128K_'] = 32200  # 124600 # 89  # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_050K_210_4203_'+'64K_']  = 32200  # 60200  # 43  # #__ADC_NUM_SAMPLES_SHORT  = 2**15
	SAMPLES_DFT_dict ['U_050K_210_4203_'+'32K_']  = 32200  # 23  #
	SAMPLES_DFT_dict ['U_050K_210_4203_'+'16K_']  = 15400  # 11  #
	SAMPLES_DFT_dict ['U_050K_210_4203_'+'8K_']   = 7000   # 5   #
	SAMPLES_DFT_dict ['U_050K_210_4203_'+'4K_']   = 4200   # 3   #
	SAMPLES_DFT_dict ['U_050K_210_4203_'+'2K_']   = 2800   # 2   #
	SAMPLES_DFT_dict ['U_050K_210_4203_'+'1K_']   = 1400   # 1   #
	###################################
	
	
	# test case U_100K_
	###################################
	#TEST_CASE__U_100K_210_2102__EN   = 1  # FS_99K91 = 210e6/  2102 # // 210	1050	0.1		2102	0.09514747859		0.099905	131072	124.8304762	113		118650.00
	TEST_FREQ_HZ_dict ['U_100K_210_2102_'] = 0.09514747859e3
	#
	SAMPLES_DFT_dict ['U_100K_210_2102_'+'128K_'] = 118650 # 113 # 50ppm
	SAMPLES_DFT_dict ['U_100K_210_2102_'+'64K_']  = 55650  # 53  # 50ppm
	SAMPLES_DFT_dict ['U_100K_210_2102_'+'32K_']  = 24150  # 23  # 50ppm
	SAMPLES_DFT_dict ['U_100K_210_2102_'+'16K_']  = 13650  # 13  # 50ppm
	SAMPLES_DFT_dict ['U_100K_210_2102_'+'8K_']   = 7350   # 7   # 50ppm
	SAMPLES_DFT_dict ['U_100K_210_2102_'+'4K_']   = 3150   # 3   # 50ppm
	SAMPLES_DFT_dict ['U_100K_210_2102_'+'2K_']   = 2100   # 2   # 50ppm
	SAMPLES_DFT_dict ['U_100K_210_2102_'+'1K_']   = 1050   # 1   # 50ppm
	###################################
	#
	###################################
	#TEST_CASE__U_100K_210_2125__EN   = 1  # FS_98K82 = 210e6/  2125  # // 210	84	0.1		2125	1.176470588		0.098824	131072	1560.380952	1559	130956.00
	TEST_FREQ_HZ_dict ['U_100K_210_2125_'] = 1.176470588e3
	#
	SAMPLES_DFT_dict ['U_100K_210_2125_'+'128K_'] = 130956 # 1559 #
	SAMPLES_DFT_dict ['U_100K_210_2125_'+'64K_']  = 64932  # 773  #
	SAMPLES_DFT_dict ['U_100K_210_2125_'+'32K_']  = 32676  # 389  #
	SAMPLES_DFT_dict ['U_100K_210_2125_'+'16K_']  = 16212  # 193  # OK 0.1%
	SAMPLES_DFT_dict ['U_100K_210_2125_'+'8K_']   = 8148   # 97   #
	SAMPLES_DFT_dict ['U_100K_210_2125_'+'4K_']   = 3948   # 47   #
	SAMPLES_DFT_dict ['U_100K_210_2125_'+'2K_']   = 1932   # 23   #
	SAMPLES_DFT_dict ['U_100K_210_2125_'+'1K_']   = 924    # 11   #
	###################################
	#
	###################################
	#TEST_CASE__U_100K_210_2114__EN   = 1  # FS_99K33 = 210e6/  2114  # // 210	150	0.1		2114	0.6622516556	0.099338	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['U_100K_210_2114_'] = 0.6622516556e3
	#
	SAMPLES_DFT_dict ['U_100K_210_2114_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['U_100K_210_2114_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['U_100K_210_2114_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['U_100K_210_2114_'+'16K_']  = 16350  # 109  # NG 2%
	SAMPLES_DFT_dict ['U_100K_210_2114_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['U_100K_210_2114_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['U_100K_210_2114_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['U_100K_210_2114_'+'1K_']   = 750    # 5    #
	###################################
	#
	###################################
	#TEST_CASE__U_100K_210_2101__EN   = 0x00001  # FS_99K95 = 210e6/  2101 # // 210	2101	2100	0.1	0.047596	0.099952	131072	62.415	61	128100.00
	TEST_FREQ_HZ_dict ['U_100K_210_2101_'] = 0.047596e3
	#
	SAMPLES_DFT_dict ['U_100K_210_2101_'+'128K_'] = 128100 # 61 #
	SAMPLES_DFT_dict ['U_100K_210_2101_'+'64K_']  = 65100  # 31 #
	SAMPLES_DFT_dict ['U_100K_210_2101_'+'32K_']  = 27300  # 13 #
	SAMPLES_DFT_dict ['U_100K_210_2101_'+'16K_']  = 14700  # 7  #
	SAMPLES_DFT_dict ['U_100K_210_2101_'+'8K_']   = 6300   # 3  #
	SAMPLES_DFT_dict ['U_100K_210_2101_'+'4K_']   = 2100   # 1  #
	SAMPLES_DFT_dict ['U_100K_210_2101_'+'2K_']   = 2100   # 1  #
	SAMPLES_DFT_dict ['U_100K_210_2101_'+'1K_']   = 2100   # 1  #
	###################################
	
	
	# test case U_200K_
	###################################
	#TEST_CASE__U_200K_210_1060__EN   = 1  # FS_198K1 = 210e6/  1060 # // 210	105		0.2		1060	1.886792453			0.198113	131072	1248.304762	1237	129885.00
	TEST_FREQ_HZ_dict ['U_200K_210_1060_'] = 1.886792453e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1060_'+'128K_'] = 129885 # 1237 # 0.7%
	SAMPLES_DFT_dict ['U_200K_210_1060_'+'64K_']  = 64785  # 617  # 0.7%
	SAMPLES_DFT_dict ['U_200K_210_1060_'+'32K_']  = 32235  # 307  # 0.7%
	SAMPLES_DFT_dict ['U_200K_210_1060_'+'16K_']  = 15855  # 151  # 0.7%
	SAMPLES_DFT_dict ['U_200K_210_1060_'+'8K_']   = 7665   # 73   # 0.7%
	SAMPLES_DFT_dict ['U_200K_210_1060_'+'4K_']   = 3885   # 37   # 0.7%
	SAMPLES_DFT_dict ['U_200K_210_1060_'+'2K_']   = 1995   # 19   # 0.7%
	SAMPLES_DFT_dict ['U_200K_210_1060_'+'1K_']   = 735    # 7    # 0.7%
	###################################
	#
	###################################
	#TEST_CASE__U_200K_210_1065__EN   = 1  # FS_197K1 = 210e6/  1065  # // 210	70	0.2		1065	2.816901408		0.197183	131072	1872.457143	1747	122290.00
	TEST_FREQ_HZ_dict ['U_200K_210_1065_'] = 2.816901408e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1065_'+'128K_'] = 122290 # 1747 #
	SAMPLES_DFT_dict ['U_200K_210_1065_'+'64K_']  = 65030  # 929  #
	SAMPLES_DFT_dict ['U_200K_210_1065_'+'32K_']  = 32690  # 467  #
	SAMPLES_DFT_dict ['U_200K_210_1065_'+'16K_']  = 16310  # 233  # NG 4%
	SAMPLES_DFT_dict ['U_200K_210_1065_'+'8K_']   = 7910   # 113  #
	SAMPLES_DFT_dict ['U_200K_210_1065_'+'4K_']   = 3710   # 53   #
	SAMPLES_DFT_dict ['U_200K_210_1065_'+'2K_']   = 2030   # 29   #
	SAMPLES_DFT_dict ['U_200K_210_1065_'+'1K_']   = 910    # 13   #
	###################################
	#
	###################################
	#TEST_CASE__U_200K_210_1057__EN   = 1  # FS_198K6 = 210e6/  1057  # // 210	150	0.2		1057	1.324503311		0.198675	131072	873.8133333	863	129450.00
	TEST_FREQ_HZ_dict ['U_200K_210_1057_'] = 1.324503311e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1057_'+'128K_'] = 129450 # 863  #
	SAMPLES_DFT_dict ['U_200K_210_1057_'+'64K_']  = 64950  # 433  #
	SAMPLES_DFT_dict ['U_200K_210_1057_'+'32K_']  = 31650  # 211  #
	SAMPLES_DFT_dict ['U_200K_210_1057_'+'16K_']  = 16350  # 109  # NG 1.5%
	SAMPLES_DFT_dict ['U_200K_210_1057_'+'8K_']   = 7950   # 53   #
	SAMPLES_DFT_dict ['U_200K_210_1057_'+'4K_']   = 3450   # 23   #
	SAMPLES_DFT_dict ['U_200K_210_1057_'+'2K_']   = 1950   # 13   #
	SAMPLES_DFT_dict ['U_200K_210_1057_'+'1K_']   = 750    # 5    #
	###################################
	#
	###################################
	#TEST_CASE__U_200K_210_1056__EN   = 1  # FS_198K9 = 210e6/  1056 # // 210	175	0.2	1056	1.136363636		0.198864	131072	748.9828571	743	130025.00
	TEST_FREQ_HZ_dict ['U_200K_210_1056_'] = 1.136363636e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1056_'+'128K_'] = 130025 # 743 #
	SAMPLES_DFT_dict ['U_200K_210_1056_'+'64K_']  = 65275  # 373 #
	SAMPLES_DFT_dict ['U_200K_210_1056_'+'32K_']  = 31675  # 181 #
	SAMPLES_DFT_dict ['U_200K_210_1056_'+'16K_']  = 15575  # 89  # OK 0.15%
	SAMPLES_DFT_dict ['U_200K_210_1056_'+'8K_']   = 7525   # 43  #
	SAMPLES_DFT_dict ['U_200K_210_1056_'+'4K_']   = 4025   # 23  #
	SAMPLES_DFT_dict ['U_200K_210_1056_'+'2K_']   = 1925   # 11  #
	SAMPLES_DFT_dict ['U_200K_210_1056_'+'1K_']   = 875	   # 5   #
	###################################
	#
	###################################
	#TEST_CASE__U_200K_210_1064__EN   = 1  # FS_197K4 = 210e6/  1064 # // 210	75	0.2	1064	2.631578947		0.197368	131072	1747.626667	1747	131025.00
	TEST_FREQ_HZ_dict ['U_200K_210_1064_'] = 2.631578947e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1064_'+'128K_'] = 131025 # 1747 #
	SAMPLES_DFT_dict ['U_200K_210_1064_'+'64K_']  = 64725  # 863  #
	SAMPLES_DFT_dict ['U_200K_210_1064_'+'32K_']  = 32475  # 433  #
	SAMPLES_DFT_dict ['U_200K_210_1064_'+'16K_']  = 15825  # 211  # NG 1%
	SAMPLES_DFT_dict ['U_200K_210_1064_'+'8K_']   = 8175   # 109  #
	SAMPLES_DFT_dict ['U_200K_210_1064_'+'4K_']   = 3975   # 53   #
	SAMPLES_DFT_dict ['U_200K_210_1064_'+'2K_']   = 1725   # 23   #
	SAMPLES_DFT_dict ['U_200K_210_1064_'+'1K_']   = 975    # 13   #
	###################################
	#
	###################################
	#TEST_CASE__U_200K_210_1075__EN   = 1  # FS_195K3 = 210e6/  1075 # // 210	42	0.2	1075	4.651162791		0.195349	131072	3120.761905	3119	130998.00
	TEST_FREQ_HZ_dict ['U_200K_210_1075_'] = 4.651162791e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1075_'+'128K_'] = 130998 # 3119 #
	SAMPLES_DFT_dict ['U_200K_210_1075_'+'64K_']  = 65226  # 1553 #
	SAMPLES_DFT_dict ['U_200K_210_1075_'+'32K_']  = 32466  # 773  #
	SAMPLES_DFT_dict ['U_200K_210_1075_'+'16K_']  = 16338  # 389  # NG 10%
	SAMPLES_DFT_dict ['U_200K_210_1075_'+'8K_']   = 8106   # 193  #
	SAMPLES_DFT_dict ['U_200K_210_1075_'+'4K_']   = 4074   # 97   #
	SAMPLES_DFT_dict ['U_200K_210_1075_'+'2K_']   = 1974   # 47   #
	SAMPLES_DFT_dict ['U_200K_210_1075_'+'1K_']   = 966    # 23   #
	###################################
	#
	###################################
	#TEST_CASE__U_200K_210_1071__EN   = 1  # FS_196K1 = 210e6/  1071 # // 210	50	0.2	1071	3.921568627		0.196078	131072	2621.44	2621	131050.00
	TEST_FREQ_HZ_dict ['U_200K_210_1071_'] = 3.921568627e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1071_'+'128K_'] = 131050 # 2621 #
	SAMPLES_DFT_dict ['U_200K_210_1071_'+'64K_']  = 65350  # 1307 #
	SAMPLES_DFT_dict ['U_200K_210_1071_'+'32K_']  = 32650  # 653  #
	SAMPLES_DFT_dict ['U_200K_210_1071_'+'16K_']  = 15850  # 317  # NG 8%
	SAMPLES_DFT_dict ['U_200K_210_1071_'+'8K_']   = 8150   # 163  #
	SAMPLES_DFT_dict ['U_200K_210_1071_'+'4K_']   = 3950   # 79   #
	SAMPLES_DFT_dict ['U_200K_210_1071_'+'2K_']   = 1850   # 37   #
	SAMPLES_DFT_dict ['U_200K_210_1071_'+'1K_']   = 950    # 19   #
	###################################
	#
	###################################
	#TEST_CASE__U_200K_210_1055__EN   = 1  # FS_199K1 = 210e6/  1055 # // 210	210	0.2	1055	0.9478672986	0.199052	131072	624.152381	619	129990.00
	TEST_FREQ_HZ_dict ['U_200K_210_1055_'] = 0.9478672986e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1055_'+'128K_'] = 129990 # 619  #
	SAMPLES_DFT_dict ['U_200K_210_1055_'+'64K_']  = 65310  # 311  #
	SAMPLES_DFT_dict ['U_200K_210_1055_'+'32K_']  = 31710  # 151  #
	SAMPLES_DFT_dict ['U_200K_210_1055_'+'16K_']  = 18690  # 89   # NG 0.6%
	SAMPLES_DFT_dict ['U_200K_210_1055_'+'8K_']   = 15330  # 73   #
	SAMPLES_DFT_dict ['U_200K_210_1055_'+'4K_']   = 3990   # 19   #
	SAMPLES_DFT_dict ['U_200K_210_1055_'+'2K_']   = 1470   # 7    #
	SAMPLES_DFT_dict ['U_200K_210_1055_'+'1K_']   = 630    # 3    #
	###################################
	#
	###################################
	#TEST_CASE__U_200K_210_1053__EN   = 1  # FS_199K4 = 210e6/  1053 # // 210	350	0.2	1053	0.5698005698	0.199430	131072	374.4914286	373	130550.00
	TEST_FREQ_HZ_dict ['U_200K_210_1053_'] = 0.5698005698e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1053_'+'128K_'] = 130550 # 373 #
	SAMPLES_DFT_dict ['U_200K_210_1053_'+'64K_']  = 63350  # 181 #
	SAMPLES_DFT_dict ['U_200K_210_1053_'+'32K_']  = 31150  # 89  #
	SAMPLES_DFT_dict ['U_200K_210_1053_'+'16K_']  = 15050  # 43  # OK 0.14%
	SAMPLES_DFT_dict ['U_200K_210_1053_'+'8K_']   = 8050   # 23  #
	SAMPLES_DFT_dict ['U_200K_210_1053_'+'4K_']   = 3850   # 11  #
	SAMPLES_DFT_dict ['U_200K_210_1053_'+'2K_']   = 1750   # 5   #
	SAMPLES_DFT_dict ['U_200K_210_1053_'+'1K_']   = 700    # 2   #
	###################################
	#
	###################################
	#TEST_CASE__U_200K_210_1051__EN   = 1  # FS_199K8 = 210e6/  1051 # // 210	1050	0.2	1051	0.1902949572	0.199810	131072	124.8304762	113	118650.00
	TEST_FREQ_HZ_dict ['U_200K_210_1051_'] = 0.1902949572e3
	#
	SAMPLES_DFT_dict ['U_200K_210_1051_'+'128K_'] = 118650 # 113 #
	SAMPLES_DFT_dict ['U_200K_210_1051_'+'64K_']  = 55650  # 53  #
	SAMPLES_DFT_dict ['U_200K_210_1051_'+'32K_']  = 24150  # 23  #
	SAMPLES_DFT_dict ['U_200K_210_1051_'+'16K_']  = 13650  # 13  # OK 30ppm
	SAMPLES_DFT_dict ['U_200K_210_1051_'+'8K_']   = 7350   # 7   #
	SAMPLES_DFT_dict ['U_200K_210_1051_'+'4K_']   = 3150   # 3   #
	SAMPLES_DFT_dict ['U_200K_210_1051_'+'2K_']   = 2100   # 2   #
	SAMPLES_DFT_dict ['U_200K_210_1051_'+'1K_']   = 1050   # 1   #
	###################################
	
	# test case U_500K_
	###################################
	# //210	105	0.5	424	4.716981132	0.495283	131072	1248.304762	1237	129885.00
	TEST_FREQ_HZ_dict ['U_500K_210_424_'] = 4.716981132e3
	#
	SAMPLES_DFT_dict ['U_500K_210_424_'+'128K_'] = 129885 # 1237 # 0.2%
	SAMPLES_DFT_dict ['U_500K_210_424_'+'64K_']  = 64785  # 617  # 0.2%
	SAMPLES_DFT_dict ['U_500K_210_424_'+'32K_']  = 32235  # 307  # 0.2%
	SAMPLES_DFT_dict ['U_500K_210_424_'+'16K_']  = 15855  # 151  # 0.2%
	SAMPLES_DFT_dict ['U_500K_210_424_'+'8K_']   = 7665   # 73   # 0.2%
	SAMPLES_DFT_dict ['U_500K_210_424_'+'4K_']   = 3885   # 37   # 0.2%
	SAMPLES_DFT_dict ['U_500K_210_424_'+'2K_']   = 1995   # 19   # 0.2%
	SAMPLES_DFT_dict ['U_500K_210_424_'+'1K_']   = 735    # 7    # 0.2%
	###################################
	#
	###################################
	#TEST_CASE__U_500K_210_421__EN    = 1  # FS_498K8 = 210e6/   421 # // 210	420	0.5	421	1.187648456	0.498812	131072	312.0761905	311	130620.00
	TEST_FREQ_HZ_dict ['U_500K_210_421_'] = 1.187648456e3
	#
	SAMPLES_DFT_dict ['U_500K_210_421_'+'128K_'] = 130620 # 311 #
	SAMPLES_DFT_dict ['U_500K_210_421_'+'64K_']  = 63420  # 151 #
	SAMPLES_DFT_dict ['U_500K_210_421_'+'32K_']  = 30660  # 73  #
	SAMPLES_DFT_dict ['U_500K_210_421_'+'16K_']  = 15540  # 37  # OK 50ppm
	SAMPLES_DFT_dict ['U_500K_210_421_'+'8K_']   = 7980   # 19  #
	SAMPLES_DFT_dict ['U_500K_210_421_'+'4K_']   = 2940   # 7   #
	SAMPLES_DFT_dict ['U_500K_210_421_'+'2K_']   = 1260   # 3   #
	SAMPLES_DFT_dict ['U_500K_210_421_'+'1K_']   = 840    # 2   #
	###################################
	
	
	# test case U_001M_
	###################################
	# // 210	105	1	212	9.433962264	0.990566	131072	1248.304762	1237	129885.00
	TEST_FREQ_HZ_dict ['U_001M_210_212_'] = 9.433962264e3
	#
	SAMPLES_DFT_dict ['U_001M_210_212_'+'128K_'] = 129885 # 1237 # 200ppm
	SAMPLES_DFT_dict ['U_001M_210_212_'+'64K_']  = 64785  # 617  # 200ppm
	SAMPLES_DFT_dict ['U_001M_210_212_'+'32K_']  = 32235  # 307  # 200ppm
	SAMPLES_DFT_dict ['U_001M_210_212_'+'16K_']  = 15855  # 151  # 200ppm
	SAMPLES_DFT_dict ['U_001M_210_212_'+'8K_']   = 7665   # 73   # 200ppm
	SAMPLES_DFT_dict ['U_001M_210_212_'+'4K_']   = 3885   # 37   # 200ppm
	SAMPLES_DFT_dict ['U_001M_210_212_'+'2K_']   = 1995   # 19   # 200ppm
	SAMPLES_DFT_dict ['U_001M_210_212_'+'1K_']   = 735    # 7    # 200ppm
	###################################
	#
	###################################
	#TEST_CASE__U_001M_210_211__EN    = 1  # FS_995K3 = 210e6/   211 # // 210	210	1	211	4.739336493	0.995261	131072	624.152381	619	129990.00
	TEST_FREQ_HZ_dict ['U_001M_210_211_'] = 4.739336493e3
	#
	SAMPLES_DFT_dict ['U_001M_210_211_'+'128K_'] = 129990 # 619  #
	SAMPLES_DFT_dict ['U_001M_210_211_'+'64K_']  = 65310  # 311  #
	SAMPLES_DFT_dict ['U_001M_210_211_'+'32K_']  = 31710  # 151  #
	SAMPLES_DFT_dict ['U_001M_210_211_'+'16K_']  = 18690  # 89   # OK 150ppm
	SAMPLES_DFT_dict ['U_001M_210_211_'+'8K_']   = 15330  # 73   #
	SAMPLES_DFT_dict ['U_001M_210_211_'+'4K_']   = 3990   # 19   #
	SAMPLES_DFT_dict ['U_001M_210_211_'+'2K_']   = 1470   # 7    #
	SAMPLES_DFT_dict ['U_001M_210_211_'+'1K_']   = 630    # 3    #
	###################################
	
	
	# test case U_002M_
	###################################
	#TEST_CASE__U_002M_210_106__EN
	# //210	105	2	106	18.86792453	1.981132	131072	1248.304762	1237	129885.00
	TEST_FREQ_HZ_dict ['U_002M_210_106_'] = 18867.92453
	#
	SAMPLES_DFT_dict ['U_002M_210_106_'+'128K_'] = 129885 # 1237 # 100ppm
	SAMPLES_DFT_dict ['U_002M_210_106_'+'64K_']  = 64785  # 617  # 100ppm
	SAMPLES_DFT_dict ['U_002M_210_106_'+'32K_']  = 32235  # 307  # 100ppm
	SAMPLES_DFT_dict ['U_002M_210_106_'+'16K_']  = 15855  # 151  # 100ppm
	SAMPLES_DFT_dict ['U_002M_210_106_'+'8K_']   = 7665   # 73   # 100ppm
	SAMPLES_DFT_dict ['U_002M_210_106_'+'4K_']   = 3885   # 37   # 100ppm
	SAMPLES_DFT_dict ['U_002M_210_106_'+'2K_']   = 1995   # 19   # 100ppm
	SAMPLES_DFT_dict ['U_002M_210_106_'+'1K_']   = 735    # 7    # 100ppm
	###################################
	
	
	# test case U_005M_
	###################################
	# //210	42	5	43	116.2790698	4.883721	131072	3120.761905	3119	130998.00
	TEST_FREQ_HZ_dict ['U_005M_210_43_'] = 116.2790698e3 
	#
	SAMPLES_DFT_dict ['U_005M_210_43_'+'128K_'] = 130998 # 3119 # 100ppm // outlier 400ppm 
	SAMPLES_DFT_dict ['U_005M_210_43_'+'64K_']  = 65226  # 1553 # 100ppm // outlier 400ppm 
	SAMPLES_DFT_dict ['U_005M_210_43_'+'32K_']  = 32466  # 773	 # 100ppm // outlier 400ppm 
	SAMPLES_DFT_dict ['U_005M_210_43_'+'16K_']  = 16338  # 389	 # 100ppm // outlier 400ppm 
	SAMPLES_DFT_dict ['U_005M_210_43_'+'8K_']   = 8106   # 193	 # 100ppm // outlier 400ppm 
	SAMPLES_DFT_dict ['U_005M_210_43_'+'4K_']   = 4074   # 97	 # 100ppm // outlier 400ppm 
	SAMPLES_DFT_dict ['U_005M_210_43_'+'2K_']   = 1974   # 47	 # 100ppm // outlier 400ppm 
	SAMPLES_DFT_dict ['U_005M_210_43_'+'1K_']   = 966    # 23	 # 100ppm // outlier 400ppm
	###################################
	
	
	# test case U_010M_
	###################################
	# //210	21	10	22	454.5454545	9.545455	131072	6241.52381	6229	130809.00
	TEST_FREQ_HZ_dict ['U_010M_210_22_'] = 454.5454545e3
	#
	SAMPLES_DFT_dict ['U_010M_210_22_'+'128K_'] = 130809  # 6229 # 0.3%
	SAMPLES_DFT_dict ['U_010M_210_22_'+'64K_']  = 65499   # 3119 # 0.3%
	SAMPLES_DFT_dict ['U_010M_210_22_'+'32K_']  = 32739   # 1559 # 0.3%
	SAMPLES_DFT_dict ['U_010M_210_22_'+'16K_']  = 16359   # 779  # 0.3%
	SAMPLES_DFT_dict ['U_010M_210_22_'+'8K_']   = 8169    # 389  # 0.3%
	SAMPLES_DFT_dict ['U_010M_210_22_'+'4K_']   = 4053    # 193  # 0.3%
	SAMPLES_DFT_dict ['U_010M_210_22_'+'2K_']   = 2037    # 97   # 0.3%
	SAMPLES_DFT_dict ['U_010M_210_22_'+'1K_']   = 987     # 47   # 0.3%
	###################################
	#
	###################################
	#TEST_CASE__U_010M_210_24__EN     = 0x000000001  # FS_8M750 = 210e6/    24 # // 10000	210	24	7	10	1250.000000	8.750000	131072	18724.571	18719	131033.00
	TEST_FREQ_HZ_dict ['U_010M_210_24_'] = 1250.000000e3
	#
	SAMPLES_DFT_dict ['U_010M_210_24_'+'128K_'] = 131033 # 18719  # 
	SAMPLES_DFT_dict ['U_010M_210_24_'+'64K_']  = 65443  # 9349   # 
	SAMPLES_DFT_dict ['U_010M_210_24_'+'32K_']  = 32753  # 4679   # 
	SAMPLES_DFT_dict ['U_010M_210_24_'+'16K_']  = 16373  # 2339   # 
	SAMPLES_DFT_dict ['U_010M_210_24_'+'8K_']   = 8141   # 1163   # 
	SAMPLES_DFT_dict ['U_010M_210_24_'+'4K_']   = 4039   # 577    # 
	SAMPLES_DFT_dict ['U_010M_210_24_'+'2K_']   = 1981   # 283    # 
	SAMPLES_DFT_dict ['U_010M_210_24_'+'1K_']   = 973    # 139    # 
	###################################
	#
	###################################
	#TEST_CASE__U_010M_210_28__EN     = 0x000000001  # FS_7M500 = 210e6/    28 # // 10000	210	28	3	10	2500.000000	7.500000	131072	43690.667	43669	131007.00
	TEST_FREQ_HZ_dict ['U_010M_210_28_'] = 2500.000000e3
	#
	SAMPLES_DFT_dict ['U_010M_210_28_'+'128K_'] = 131007 # 43669 # 
	SAMPLES_DFT_dict ['U_010M_210_28_'+'64K_']  = 65523  # 21841 # 
	SAMPLES_DFT_dict ['U_010M_210_28_'+'32K_']  = 32727  # 10909 # 
	SAMPLES_DFT_dict ['U_010M_210_28_'+'16K_']  = 16347  # 5449  # 
	SAMPLES_DFT_dict ['U_010M_210_28_'+'8K_']   = 8187   # 2729  # 
	SAMPLES_DFT_dict ['U_010M_210_28_'+'4K_']   = 4083   # 1361  # 
	SAMPLES_DFT_dict ['U_010M_210_28_'+'2K_']   = 2031   # 677   # 
	SAMPLES_DFT_dict ['U_010M_210_28_'+'1K_']   = 1011   # 337   # 
	###################################
	#
	###################################
	#TEST_CASE__U_010M_210_41__EN     = 0x000000001  # FS_5M122 = 210e6/    41 # // 10000	210	41	21	10	243.902439(2*Fs image)	5.121951	131072	6241.524	6229	130809.00
	TEST_FREQ_HZ_dict ['U_010M_210_41_'] = 243.902439e3
	#
	SAMPLES_DFT_dict ['U_010M_210_41_'+'128K_'] = 130809  # 6229 # 
	SAMPLES_DFT_dict ['U_010M_210_41_'+'64K_']  = 65499   # 3119 # 
	SAMPLES_DFT_dict ['U_010M_210_41_'+'32K_']  = 32739   # 1559 # 
	SAMPLES_DFT_dict ['U_010M_210_41_'+'16K_']  = 16359   # 779	 # 
	SAMPLES_DFT_dict ['U_010M_210_41_'+'8K_']   = 8169    # 389	 # 
	SAMPLES_DFT_dict ['U_010M_210_41_'+'4K_']   = 4053    # 193	 # 
	SAMPLES_DFT_dict ['U_010M_210_41_'+'2K_']   = 2037    # 97	 # 
	SAMPLES_DFT_dict ['U_010M_210_41_'+'1K_']   = 987     # 47	 # 
	###################################
	#
	###################################
	#TEST_CASE__U_010M_210_43__EN     = 0x0001  # FS_4M884 = 210e6/    43    # // 210	43		21		10		232.558140			4.883721	131072	6241.524	6229	130809.00
	TEST_FREQ_HZ_dict ['U_010M_210_43_'] = 232.558140e3
	#
	SAMPLES_DFT_dict ['U_010M_210_43_'+'128K_'] = 130809  # 6229 # 
	SAMPLES_DFT_dict ['U_010M_210_43_'+'64K_']  = 65499   # 3119 # 
	SAMPLES_DFT_dict ['U_010M_210_43_'+'32K_']  = 32739   # 1559 # 
	SAMPLES_DFT_dict ['U_010M_210_43_'+'16K_']  = 16359   # 779	 # 
	SAMPLES_DFT_dict ['U_010M_210_43_'+'8K_']   = 8169    # 389	 # 
	SAMPLES_DFT_dict ['U_010M_210_43_'+'4K_']   = 4053    # 193	 # 
	SAMPLES_DFT_dict ['U_010M_210_43_'+'2K_']   = 2037    # 97	 # 
	SAMPLES_DFT_dict ['U_010M_210_43_'+'1K_']   = 987     # 47	 # 
	###################################


## function for writing report file 
def f_write_rpt_file(filename_rpt, csv_files, SAMPLES_DFT, TEST_FREQ_HZ, USE_BACK_END_SAMPLE=0):
	
	# open  report file
	newFile = open(filename_rpt, "wt")
	
	
	## note on SAMPLES_DFT
	# SAMPLES_DFT samples from front-end vs back-end
	#idx_start = 0
	#idx_stop  = data_count
	#
	# for front-end use 
	#idx_start = 0
	#idx_stop  = SAMPLES_DFT
	#
	# for back-end use 
	#idx_start = data_count - SAMPLES_DFT
	#idx_stop  = data_count
	#
	#print(SAMPLES_DFT)
	#print(idx_start)
	#print(idx_stop)
	#
	#USE_BACK_END_SAMPLE = 0 # 1 or 0 for front-end
	
	## read csv files and write report file 
	#
	filecount = 0
	for filename in csv_files:
		filecount += 1
		print('\n>> ({}/{}) Open: {}'.format(filecount, len(csv_files), filename))
		#
		## read data from csv file
		data_header = []
		adc0_list = []
		adc1_list = []
		with open(filename, 'r', newline='') as csvFile:
			read_data = csv.reader(csvFile, delimiter=',')
			line_count = 0
			data_count = 0
			for row in read_data:
				# count lines
				line_count += 1
				## find header
				# find '//'
				if row[0][0:2]=='//':
					# remove '//'
					row[0]=row[0][2:]
					flag_float_all = True
					data_header_tmp = []
					for cc in row:
						if is_int(cc):
							data_header_tmp+=[int(cc)]
						elif is_float(cc):
							data_header_tmp+=[float(cc)]
						else:
							data_header_tmp+=[cc]
							flag_float_all = False
						#
					# found header
					if flag_float_all==True: 
						# copy into header
						data_header = data_header_tmp
						Ns = data_header[1]
						if USE_BACK_END_SAMPLE==1:
							idx_start = Ns - SAMPLES_DFT
							idx_stop  = Ns
						else: # front-end
							idx_start = 0
							idx_stop  = SAMPLES_DFT
						#
						#print(Ns)
						#print(SAMPLES_DFT)
						#print(idx_start)
						#print(idx_stop)
				# assume adc data 
				else:
					#print(data_header)
					#print(data_count)
					#print(idx_start)
					#print(idx_stop)
					#
					if not data_header:
						continue
					if data_count < idx_start:
						data_count += 1
						continue
					#
					data_count += 1
					adc0_list  += [int(row[0])]
					adc1_list  += [int(row[1])]
					#
					if data_count >= idx_stop:
						print(data_count)
						break
				# check data_count : only for front-end use style
				# if data_count == SAMPLES_DFT:
				# 	break
				#
			#
			
			
		## assign: x[n], y[n]
		#print(data_count)
		#print(len(adc0_list))
		#print(len(adc1_list))
		x_n = adc0_list
		y_n = adc1_list
		#x_n = adc0_list[idx_start:idx_stop]
		#y_n = adc1_list[idx_start:idx_stop]
		#
		#print(len(x_n))
		#print(len(y_n))
		
		
		## calculate  sum mean std
		sum_x_n = sum(x_n)
		sum_y_n = sum(y_n)
		mean_x_n = np.mean(x_n)
		mean_y_n = np.mean(y_n)
		std_x_n = np.std(x_n)
		std_y_n = np.std(y_n)
		#
		
		## calculate: X[f] = DFT(x[n]), Y[f] = DFT(y[n]) 
		#X_f = np.fft.fft(x_n)
		X_f = np.fft.fft(x_n, SAMPLES_DFT)
		#Y_f = np.fft.fft(y_n)
		Y_f = np.fft.fft(y_n, SAMPLES_DFT)
		#
		
		## define idx shift 
		#IDX_SHIFT = -1
		IDX_SHIFT = 0
		#IDX_SHIFT = 1
		
		## collect: X[f_t], Y[f_t]
		# para ... IDX_TEST_FREQ
		# adc sampling rate
		Fs = data_header[0] # ADC base ... period count ... replace?
		IDX_TEST_FREQ = int(SAMPLES_DFT / Fs * TEST_FREQ_HZ + 0.5)
		X_f_t = X_f[IDX_TEST_FREQ+IDX_SHIFT]
		Y_f_t = Y_f[IDX_TEST_FREQ+IDX_SHIFT]
		#
		## calculate: R[f_t] = Y[f_t] / X[f_t]
		R_f_t = Y_f_t / X_f_t
		#
		## calculate: Mag(X[F_t]), Mag(Y[F_t]), Mag(R[F_t]), Arg(R[F_t])
		mag_X_f_t = np.absolute(X_f_t)
		mag_Y_f_t = np.absolute(Y_f_t)
		mag_R_f_t = np.absolute(R_f_t)
		ang_R_f_t = np.angle(R_f_t, deg=True)
		#
		## report 
		print('{} = {}'.format('filename',filename))
		print('{} = {}'.format('line_count',line_count))
		print('{} = {}'.format('data_count',data_count))
		print('{} = {}'.format('data_header',data_header))
		print('{} = {}'.format('TEST_FREQ_HZ',TEST_FREQ_HZ))
		print('{} = {}'.format('SAMPLES_DFT',SAMPLES_DFT))
		print('{} = {}'.format('IDX_TEST_FREQ',IDX_TEST_FREQ))
		#
		#
		newFile.write('{}, '.format(filename))                 #[00] 
		newFile.write('{}, '.format(data_header[0]))           #[01] Fs [SPS]
		newFile.write('{}, '.format(data_header[1]))           #[02] Ns [count]
		#newFile.write('{}, '.format(data_count))               #[02] data_count [count]
		newFile.write('{}, '.format(data_header[2]))           #[03] Temp [mC]
		newFile.write('{:6.3f}, '.format(data_header[2]/1000)) #[04] Temp [C] <<<
		newFile.write('{}, '.format(sum_x_n))                  #[05] 
		newFile.write('{}, '.format(sum_y_n))                  #[06] 
		newFile.write('{}, '.format(mean_x_n))                 #[07] 
		newFile.write('{}, '.format(mean_y_n))                 #[08] 
		newFile.write('{}, '.format(std_x_n))                  #[09] 
		newFile.write('{}, '.format(std_y_n))                  #[10] 
		newFile.write('{}, '.format(TEST_FREQ_HZ))             #[11] 
		newFile.write('{}, '.format(SAMPLES_DFT))              #[12] 
		newFile.write('{}, '.format(X_f[0]))                   #[13] 
		newFile.write('{}, '.format(Y_f[0]))                   #[14] 
		#
		newFile.write('{}, '.format(IDX_TEST_FREQ))            #[15] 
		newFile.write('{}, '.format(X_f_t))                    #[16] 
		newFile.write('{}, '.format(Y_f_t))                    #[17] 
		newFile.write('{}, '.format(R_f_t))                    #[18] 
		newFile.write('{:.8f}, '.format(mag_X_f_t))            #[19] <<<
		newFile.write('{:.8f}, '.format(mag_Y_f_t))            #[20] <<<
		newFile.write('{:.8f}, '.format(mag_R_f_t))            #[21] <<<
		newFile.write('{:.8f}, '.format(ang_R_f_t))            #[22] <<<
		#
		newFile.write('{}, '.format(IDX_SHIFT))                #[23] index shift amount 0 or else 
		#
		newFile.write('{}, '.format(data_header[3]))           #[24] WARN_RETRY_CNT [count]
		#
		newFile.write('0\n') #[**] end with 0
		#
	
	
	## close report file
	#
	newFile.close()
	#


## DFT length option
#DFT_LEN_OPT = '128K_' # 17b
#DFT_LEN_OPT = '64K_' # 16b 
#DFT_LEN_OPT = '32K_' # 15b ##
#DFT_LEN_OPT = '16K_' # 14b ##
#DFT_LEN_OPT = '8K_' 
DFT_LEN_OPT = '4K_' # 12b ##
#DFT_LEN_OPT = '2K_'
#DFT_LEN_OPT = '1K_'

## setup coherent sampling condition
# para ... SAMPLES_DFT
# coherent sampling 
# // form : (adc sampling rate) / (test freq) * (prime number) = (number of DFT samples)
# // downsample form : Fs / (Ft - Fs) * (prime number) = (number of DFT samples)
# // downsample form : 1 / (Ft/Fs - 1) * (prime number) = (number of DFT samples)
# // downsample form : Fs / Ft * ((prime number) + (number of DFT samples)) = (number of DFT samples)
# // downsample form : ((prime number)/(number of DFT samples) + 1) = Ft / Fs
#



## DFT data selection option 
USE_BACK_END_SAMPLE = 0 # 1 or 0 for front-end

## assign DFT index shift 
#IDX_SHIFT = -1
#IDX_SHIFT = 0 # no move
#IDX_SHIFT = 1
#IDX_SHIFT = 0

##
# file postfix 
FILE_POST = '*.csv'

#
MAX_FILE_COUNT = 0 # 0 for all
#MAX_FILE_COUNT = 1000 # 0 for all
#MAX_FILE_COUNT = 300 # 0 for all
#MAX_FILE_COUNT = 200 # 0 for all
#MAX_FILE_COUNT = 100 # 0 for all
#MAX_FILE_COUNT = 50 # 0 for all
#
USE_LAST_FILES = 1 # 0 or 1 for last files


##
# batch filenames test control 
BATCH_TEST_ONLY = 0

if not TS_FIXED:
	timestp = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')
else:
	timestp = TS_FIXED

## iterate boards ...
for FILE_PRE  in FILE_PRE_list:
	## iterate cases ...
	print('> {}={}'.format('FILE_PRE',FILE_PRE))
	#
	for cc_dict in case_info__freq_plot :
		# choose case no
		print('> {}={}'.format('cc_dict',cc_dict))
		CASE_NO = cc_dict['case_code']
	
		## setup file location and names
		LOC_CSV = DIR_PRE + FILE_PRE + CASE_NO + FILE_POST
		print(LOC_CSV)
		#
		#
		csv_files = glob.glob(LOC_CSV)
		#print(len(csv_files))
		print('>>> {}={}'.format('len(csv_files)',len(csv_files)))
		if BATCH_TEST_ONLY==1:
			print(csv_files[0])
			print(csv_files[-1])
			print('>>')
			continue
		#
		if len(csv_files) == 0:
			exit()
		
		# resize csv files 
		if MAX_FILE_COUNT>0 and MAX_FILE_COUNT < len(csv_files):
			if USE_LAST_FILES == 0:
				csv_files = csv_files[0:MAX_FILE_COUNT]
			elif USE_LAST_FILES == 1:
				csv_files = csv_files[-MAX_FILE_COUNT:]
			print('>>> {}={}\n'.format('len(csv_files)',len(csv_files)))
		#
		
		## assign TEST_FREQ_HZ and SAMPLES_DFT from dict
		#TEST_FREQ_HZ = TEST_FREQ_HZ_dict[CASE_NO]
		#SAMPLES_DFT  = SAMPLES_DFT_dict[ CASE_NO + DFT_LEN_OPT ]
		TEST_FREQ_HZ = c_DFT_info.TEST_FREQ_HZ_dict[CASE_NO]
		SAMPLES_DFT  = c_DFT_info.SAMPLES_DFT_dict[ CASE_NO + DFT_LEN_OPT ]
		#
		print(TEST_FREQ_HZ)
		print(SAMPLES_DFT)
		
		#input('') ###
		
		
		## name report file
		#
		#if not TS_FIXED:
		#	timestp = datetime.datetime.now().strftime('%Y%m%dT%H%M%S')
		#else:
		#	timestp = TS_FIXED
		#
		#filename_rpt = 'RPT_'+'__{}_{}.txt'.format('TS',timestp)
		#filename_core = 'RPT_' + FILE_PRE + CASE_NO + '{}{}'.format(DFT_LEN_OPT,SAMPLES_DFT) + '__{}_{}'.format('TS',timestp)
		filename_core = RRT_FILE_PRE + FILE_PRE + CASE_NO + '{}_{}'.format(SAMPLES_DFT, DFT_LEN_OPT) + '_{}_{}'.format('TS',timestp)
		filename_rpt = filename_core + '.txt'
		
		
		## call report write function
		f_write_rpt_file(filename_rpt, csv_files, SAMPLES_DFT, TEST_FREQ_HZ, USE_BACK_END_SAMPLE)
	## done : cc_dict
## done : FILE_PRE


