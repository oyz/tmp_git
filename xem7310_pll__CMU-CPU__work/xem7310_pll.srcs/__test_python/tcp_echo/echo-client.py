## echo-client.py
import socket

# https://docs.python.org/3/library/socket.html


## control parameters 

HOST = '192.168.168.123'  # The server's hostname or IP address
PORT = 5025               # The port used by the server

## fucntions

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	s.connect((HOST, PORT))
	#
	msg_str = b'Hello world from PC!\n'
	s.sendall(msg_str)
	data1 = s.recv(1024)
	print('Sent:', repr(msg_str))
	#
	msg_str = b'what is this?\n'
	s.sendall(msg_str)
	data2 = s.recv(524288) # try 131072 524288
	print('Sent:', repr(msg_str))
	#
	msg_str = b'*IDN?\n'
	s.sendall(msg_str)
	data3 = s.recv(1024)
	print('Sent:', repr(msg_str))
	#
print('Received:', repr(data1))
print('Received:', repr(data2))
print('Received:', repr(data3))
