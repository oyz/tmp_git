## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_cmu_cpu__10_self_test_a__adc_mismatch.py : 
#
#	test cases
#		...
#		no_signal
#		100kHz 
#		1MHz 
#		10MHz 
#		...
#		15.0Msps = 210MHz/14  = 15.0        Msps
#		9.55Msps = 210MHz/22  = 9.545454545 Msps
#		3.89Msps = 210MHz/54  = 3.888888889 Msps
#		0.97Msps = 210MHz/216 = 0.972222222 Msps
#  
# power options: ON/OFF 
# DAC options: any ... 0x0000~0x7FFF ... 0V ~ 10V
# dwave freq options: 100kHz ~ 10MHz
# filter bw options: 120kHz, 1.2MHz, 12MHz, 120MHz
# ADC gain options 
#
# note: down-converting = oversampling and decimation...
# note: down-sampling = analog-style decimation...
#  
#  
#  test case 0-0: no signal / noise level check @ 15.0Msps
#  test case 0-1: no signal / noise level check @ 9.55Msps
#  test case 0-2: no signal / noise level check @ 3.89Msps
#  test case 0-3: no signal / noise level check @ 0.97Msps
#  
#  test case 1-0: 100kHz signal, BW 120kHz @ 15.0Msps // 100	15.000	131072	873.8133333	863	129450
#  test case 1-1: 100kHz signal, BW 120kHz @ 9.55Msps
#  test case 1-2: 100kHz signal, BW 120kHz @ 3.89Msps
#  test case 1-3: 100kHz signal, BW 120kHz @ 0.97Msps
#  test case 1-4: 100kHz signal, BW 120kHz @ 0.099Msps << down-sampling // 210	105	0.1	2120	0.9433962264	0.099057	131072	1248.304762	1237	129885
#  
#  test case 2-0: 1MHz signal, BW 1.2MHz @ 15.0Msps
#  test case 2-1: 1MHz signal, BW 1.2MHz @ 9.55Msps
#  test case 2-2: 1MHz signal, BW 1.2MHz @ 3.89Msps
#  test case 2-3: 1MHz signal, BW 1.2MHz @ 0.97Msps << down-sampling
#  test case 2-4: 1MHz signal, BW 1.2MHz @ 0.99Msps << down-sampling // 1	212	9.433962264	0.9905660377	131072	1248.304762	1237	129885
#  
#  test case 3-0: 10MHz signal, BW 12MHz @ 15.0Msps
#  test case 3-1: 10MHz signal, BW 12MHz @ 9.55Msps << down-sampling
#  test case 3-2: 10MHz signal, BW 12MHz @ 3.89Msps
#  test case 3-3: 10MHz signal, BW 12MHz @ 0.97Msps
#  
#  test case 4-0: 100kHz signal, BW 120MHz @ 15.0Msps
#  test case 4-1: 100kHz signal, BW 120MHz @ 9.55Msps
#  test case 4-2: 100kHz signal, BW 120MHz @ 3.89Msps
#  test case 4-3: 100kHz signal, BW 120MHz @ 0.97Msps
#  
#  test case 5-0: 200kHz signal, BW 120kHz @ 15.0Msps
#  
#  test case 6-0: 500kHz signal, BW 1.2MHz @ 15.0Msps
#  
#  test case 7-0: 5MHz signal, BW 12MHz @ 15.0Msps
#  test case 7-1: 5MHz signal, BW 12MHz @ 4.884Msps << down-sampling // 210	42	5	43	116.2790698	4.884	131072	3120.761905	3119	130998
#  
#  test case 8-0: 16.875MHz signal, BW 120MHz @ 15.0Msps << down-sampling
#  
#  test case 9-0: 2MHz signal, BW 1.2MHz @ 14.0Msps
#  test case 9-1: 2MHz signal, BW 1.2MHz @ 1.98Msps << down-sampling // 210/106 = 1.98113208, 1237, 129885, (2000) - ((210000) / 106) = 18.8679245283
#  
#  test case 10-0: 1kHz signal, BW 120kHz @ 15.0Msps
#
#  test case 11-0: 10kHz signal, BW 120kHz @ 15.0Msps


####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
BIT_FILENAME = 'xem7310__cmu_cpu__top.bit'
#BIT_FILENAME = '../_bit/xem7310__cmu_cpu__top__F8_19_0103.bit' # for DWAVE 80MHz, ADC 10.4Msps max.
#BIT_FILENAME = '../_bit/xem7310__cmu_cpu__top__DWAVE135MHz.bit' # for DWAVE 135MHz, ADC 15Msps max.
#
# FPGA serial to access
#FPGA_SERIAL = '' # for any
#FPGA_SERIAL = '1739000J7V' # for module: CMU-CPU-F5500-FPGA1
FPGA_SERIAL = '1908000OVP' # for module: CMU-CPU-F5500-FPGA2
#FPGA_SERIAL = '1739000J8A' # for module: CMU-CPU-F5500-FPGA3
#
# DUMP file prefix
__DUMP_PRE__ = 'DUMP_BD1__'
#__DUMP_PRE__ = 'DUMP_BD2__'
#__DUMP_PRE__ = 'DUMP_BD3__'
#__DUMP_PRE__ = 'DUMP_BD4__'
#__DUMP_PRE__ = 'DUMP_BD5__'
#
__DWAVE_BASE_FREQ = 160e6 # 80e6 # 160e6 # 135e6
#
#DWAVE_PIN_SWAP_DISABLE = 0
DWAVE_PIN_SWAP_DISABLE = 1
DWAVE_IDLE_WARMUP_DISABLE = 0
#
#
__ADC_BASE_FREQ = 210e6 # 125e6
#__ADC_BASE_FREQ = 125e6 
#
FS_0 = 210e6/14  # 15.0Msps
FS_1 = 210e6/22  # 9.55Msps
FS_2 = 210e6/54  # 3.89Msps
FS_3 = 210e6/216 # 0.97Msps
#
FS_14M  = 210e6/15   # 14.0Msps
FS_1M98 = 210e6/106  # 210/106 = 1.98113208Msps
FS_991K = 210e6/212  # 210/212 = 0.9905660377
FS_4M88 = 210e6/43   # 4.884Msps
FS_99K  = 210e6/2120 # 99.057Ksps // 2120	0.099057

#
#FS_0 = 125e6/12  # 10.4Msps
#FS_1 = 125e6/32  # 3.9Msps
#FS_2 = 125e6/66  # 1.89Msps
#FS_3 = 125e6/130 # 0.96Msps
#
##TODO: DUMP setup
DUMP_EN = 1
DUMP_DICT_DIS = 1 # 1 for no dict file.

#
# wirh adc base 210MHz
#PARAM__ADC_INPUT_DELAY_TAP = 31 # NG 1 2
#PARAM__ADC_INPUT_DELAY_TAP = 30 # NG 2
#PARAM__ADC_INPUT_DELAY_TAP = 29 # NG 2
#PARAM__ADC_INPUT_DELAY_TAP = 28 # NG 2
#PARAM__ADC_INPUT_DELAY_TAP = 27 # NG 2
#PARAM__ADC_INPUT_DELAY_TAP = 26 # NG 1
#PARAM__ADC_INPUT_DELAY_TAP = 25 # OK 78ps*25=1.95 nanoseconds
#PARAM__ADC_INPUT_DELAY_TAP = 20 # OK
#PARAM__ADC_INPUT_DELAY_TAP = 19 # 
#PARAM__ADC_INPUT_DELAY_TAP = 15 # OK 78ps*15=1.17 nanoseconds
#PARAM__ADC_INPUT_DELAY_TAP = 11 # OK 
#PARAM__ADC_INPUT_DELAY_TAP = 10 # 
#PARAM__ADC_INPUT_DELAY_TAP = 9 # 
#PARAM__ADC_INPUT_DELAY_TAP = 8 # 
#PARAM__ADC_INPUT_DELAY_TAP = 5 # 
#PARAM__ADC_INPUT_DELAY_TAP = 0 # OK 
#
PARAM__ADC0_INPUT_DELAY_TAP_L =  0 # 15 0 #   // even - DB
PARAM__ADC0_INPUT_DELAY_TAP_H =  0 # 15 0 #   // odd - DA
PARAM__ADC1_INPUT_DELAY_TAP_L =  0 # 15 0
PARAM__ADC1_INPUT_DELAY_TAP_H =  0 # 15 0 # >=28 NG 
#
__ADC_NUM_SAMPLES=131072
__ADC_BITWIDTH=18
#__ADC_BITWIDTH=16
#
# range control option: full range +/-10V
# DAC_volt = 10V /0x7FFF * DAC_code
__DAC_CODE = 0x0000 # 0V for new board ... bias voltages
#__DAC_CODE = 0x400    # 10V /0x7FFF * 0x400  = 0.312509537 volts
#__DAC_CODE = 0x800
#__DAC_CODE = 0xA00
#__DAC_CODE = 0x1000   # 10V /0x7FFF * 0x1000 = 1.25003815 volts
#__DAC_CODE = 0x2000   # 10V /0x7FFF * 0x2000 = 2.5000763 volts
#__DAC_CODE = 0x2A40   # 10V /0x7FFF * 0x2A40 = 3.30088198 volts  # ... "SNR 30dB", 10MHz image signal to 160kHz
#__DAC_CODE = 0x4000   # 10V /0x7FFF * 0x4000 = 5.00015259 volts # ... "SNR 45dB", 10MHz image signal to 160kHz
#__DAC_CODE = 0x5000    # ... SNR 45dB, 10MHz image signal to 160kHz
#__DAC_CODE = 0x6000    # 10V /0x7FFF * 0x6000 = 7.50022889  # ... "SNR 55dB", 10MHz image signal to 160kHz
#__DAC_CODE = 0x7000    # ... SNR 55dB, 10MHz image signal to 160kHz
#__DAC_CODE = 0x7FFF    # ... SNR 55dB, 10MHz image signal to 160kHz
#
# output path selection
TEST_PATH_I_ONLY_EN = 0
TEST_PATH_Q_ONLY_EN = 0

# case: no signal
TEST_CASE_0_0_EN = 0 # <<<
TEST_CASE_0_1_EN = 0
TEST_CASE_0_2_EN = 0
TEST_CASE_0_3_EN = 0

# case: 100kHz
TEST_CASE_1_0_EN = 1 # <<<
TEST_CASE_1_1_EN = 0
TEST_CASE_1_2_EN = 0
TEST_CASE_1_3_EN = 0
TEST_CASE_1_4_EN = 1 # <<<

# case: 1MHz
TEST_CASE_2_0_EN = 1 # <<<
TEST_CASE_2_1_EN = 0
TEST_CASE_2_2_EN = 0
TEST_CASE_2_3_EN = 0
TEST_CASE_2_4_EN = 1 # <<<

# case: 10MHz 
TEST_CASE_3_0_EN = 0
TEST_CASE_3_1_EN = 0
TEST_CASE_3_2_EN = 0
TEST_CASE_3_3_EN = 0

# case: 100kHz
TEST_CASE_4_0_EN = 0
TEST_CASE_4_1_EN = 0
TEST_CASE_4_2_EN = 0
TEST_CASE_4_3_EN = 0

# case: 200kHz
TEST_CASE_5_0_EN = 0 # 

# case: 500kHz
TEST_CASE_6_0_EN = 0

# case: 5MHz 
TEST_CASE_7_0_EN = 1 # <<<
TEST_CASE_7_1_EN = 1 # <<<

# case: max freq 
# *  80MHz DWAVE base freq to support test waveform 10MHz
# * 135MHz DWAVE base freq to support test waveform 16.875MHz
# * 160MHz DWAVE base freq to support test waveform 20MHz

# case: 16.875MHz
TEST_CASE_8_0_EN = 0

# case: 2MHz
TEST_CASE_9_0_EN = 0 #
TEST_CASE_9_1_EN = 0 #

# case: 1kHz
TEST_CASE_10_0_EN = 0 # 

# case: 10kHz
TEST_CASE_11_0_EN = 0 # <<<


#
####

####
## library call
import ok_cmu_cpu__lib as cmu
#
# check configuration : OK_EP_ADRS_CONFIG
#  
EP_ADRS = cmu.conf.OK_EP_ADRS_CONFIG
#  
def form_contents(var,idx):
	return '{} = {}'.format(idx, var[idx])
#
def form_address(var,idx):
	return '{} @ {:#04x}'.format(idx, var[idx])
#  
print(form_contents(EP_ADRS,'board_name'))
print(form_contents(EP_ADRS,'ver'))
print(form_contents(EP_ADRS,'bit_filename'))
#
print(form_address(EP_ADRS,'FPGA_IMAGE_ID'))
#
####

####
## init : dev
dev = cmu.ok_cmu_init()
print(dev)
####

####
ret = cmu.ok_cmu_caller_id()
print(ret)
####

####
## open
ret = cmu.ok_cmu_open(FPGA_SERIAL)
print(ret)
####

####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = cmu.ok_cmu_conf(BIT_FILENAME)
	print(ret)
####  

####
## read fpga_image_id
fpga_image_id = cmu.cmu_read_fpga_image_id()
#
def form_hex_32b(val):
	return '0x{:08X}'.format(val)
#
print(form_hex_32b(fpga_image_id))
####

####
## read FPGA internal temp and volt
ret = cmu.cmu_monitor_fpga()
print(ret)
#
TEMP_FPGA = ret[0]
print(TEMP_FPGA)
####

####
## test counter on 
ret = cmu.cmu_test_counter('ON')
print(ret)
####

###################################################
## subfunctions

##TODO: def setup____test_signal()
def setup____test_signal(TEST_CONF={}): 
	## SPO ##
	cmu.cmu_spo_enable()
	cmu.cmu_spo_init()
	#
	# led on
	cmu.cmu_spo_bit__leds(0xFF) 
	#
	# AMP power control
	#cmu.cmu_spo_bit__amp_pwr('ON')
	#cmu.cmu_spo_bit__amp_pwr('OFF')
	try: 
		cmu.cmu_spo_bit__amp_pwr(TEST_CONF['AMP_PWR_CTRL'])
	except: 
		print('> missing AMP_PWR_CTRL in TEST_CONF!')	
	#
	# ADC gain control 
	#cmu.cmu_spo_bit__adc0_gain('1X')
	#cmu.cmu_spo_bit__adc0_gain('10X')
	#cmu.cmu_spo_bit__adc0_gain('100X')
	#cmu.cmu_spo_bit__adc1_gain('1X')
	#cmu.cmu_spo_bit__adc1_gain('10X')
	#cmu.cmu_spo_bit__adc1_gain('100X')
	try: 
		cmu.cmu_spo_bit__adc0_gain(TEST_CONF['AMP_GAIN_CTRL'])
		cmu.cmu_spo_bit__adc1_gain(TEST_CONF['AMP_GAIN_CTRL'])
	except: 
		print('> missing FILT_PATH_CTRL in AMP_GAIN_CTRL!')	
	#
	# filter path control
	## BW 120MHz 
	#cmu.cmu_spo_bit__vi_bw('120M')
	#cmu.cmu_spo_bit__vq_bw('120M')
	## BW 12MHz 
	#cmu.cmu_spo_bit__vi_bw('12M')
	#cmu.cmu_spo_bit__vq_bw('12M')
	## BW 1.2MHz 
	#cmu.cmu_spo_bit__vi_bw('1M2')
	#cmu.cmu_spo_bit__vq_bw('1M2')
	## BW 120kHz 
	#cmu.cmu_spo_bit__vi_bw('120K')
	#cmu.cmu_spo_bit__vq_bw('120K')
	try: 
		cmu.cmu_spo_bit__vi_bw(TEST_CONF['FILT_PATH_CTRL'])
		cmu.cmu_spo_bit__vq_bw(TEST_CONF['FILT_PATH_CTRL'])
	except: 
		print('> missing FILT_PATH_CTRL in TEST_CONF!')	
	#
	## DAC ##
	cmu.cmu_dac_bias_enable()
	cmu.cmu_dac_bias_init()
	#
	# set DAC code 
	DAC1_CODE = __DAC_CODE
	DAC2_CODE = __DAC_CODE
	## for filter-out monitoring (making small)
	#DAC3_CODE = 0x0800
	#DAC4_CODE = 0x0800
	#DAC3_CODE = 0x1000 # normal 
	#DAC4_CODE = 0x1000
	#DAC3_CODE = 0x2000
	#DAC4_CODE = 0x2000
	#DAC3_CODE = 0x2800
	#DAC4_CODE = 0x2800
	#DAC3_CODE = 0x4000
	#DAC4_CODE = 0x4000
	## for 8f-4level monitoring
	#DAC3_CODE = 0x2000 
	#DAC4_CODE = 0x2000
	#
	DAC3_CODE = 0x0000
	DAC4_CODE = 0x0000
	try: 
		DAC3_CODE = TEST_CONF['DAC_CODE']
		DAC4_CODE = TEST_CONF['DAC_CODE']
	except: 
		print('> missing DAC_CODE in TEST_CONF!')		
	#
	cmu.cmu_dac_bias_set_buffer(
			DAC1=DAC1_CODE,
			DAC2=DAC2_CODE,
			DAC3=DAC3_CODE,
			DAC4=DAC4_CODE)
	#
	cmu.cmu_dac_bias_update()
	#
	ret = cmu.cmu_dac_bias_readback()
	print(ret)
	#
	## DWAVE ##
	#
	# pin swap control 
	if DWAVE_PIN_SWAP_DISABLE == 1:
		cmu.cmu_dwave_pair_ch_pin_swap_disable()
	else:
		cmu.cmu_dwave_pair_ch_pin_swap_enable()
	#
	# R idle warm up 
	if DWAVE_IDLE_WARMUP_DISABLE == 1:
		cmu.cmu_dwave_r_idle_warmup_disable()
	else:
		cmu.cmu_dwave_r_idle_warmup_enable()
	#
	# enable
	cmu.cmu_dwave_enable()
	#
	# read dwave base freq
	DWAVE_BASE_FREQ = cmu.cmu_dwave_read_base_freq()
	print('{}: {:#8.3f} MHz\r'.format('DWAVE_BASE_FREQ',DWAVE_BASE_FREQ/1e6))
	#
	try: 
		if (DWAVE_BASE_FREQ!=TEST_CONF['DWAVE_BASE_FREQ']):
			print('>>> {}: {}'.format('Warning!: DWAVE_BASE_FREQ is not matched',TEST_CONF['DWAVE_BASE_FREQ']))
			input('Press Enter key!')
	except: 
		print('> missing DWAVE_BASE_FREQ in TEST_CONF!')		
	#  
	try: 
		CNT_PERIOD = TEST_CONF['CNT_PERIOD']
	except: 
		CNT_PERIOD = 80 
		print('> missing CNT_PERIOD in TEST_CONF!')
	#
	try: 
		CNT_DIFF = TEST_CONF['CNT_DIFF']
	except: 
		CNT_DIFF   = 60
		print('> missing CNT_DIFF in TEST_CONF!')		
	#  
	# set CNT_PERIOD
	cmu.cmu_dwave_wr_cnt_period(CNT_PERIOD)
	#
	# set CNT_DIFF
	cmu.cmu_dwave_wr_cnt_diff(CNT_DIFF)
	#
	# dwave output control
	if TEST_PATH_I_ONLY_EN:
		cmu.cmu_dwave_wr_output_dis__enable_path_i_only()
	elif TEST_PATH_Q_ONLY_EN:
		cmu.cmu_dwave_wr_output_dis__enable_path_q_only()
	else:
		cmu.cmu_dwave_wr_output_dis__enable_all()
	#
	# set dwave parameters into core logic
	cmu.cmu_dwave_set_para()
	#
	return None

def turn_on__test_signal(TEST_CONF={}):
	# dwave_pulse_on
	cmu.cmu_dwave_pulse_on()
	#
	# wait for pulse stability
	cmu.sleep(1)
	#
	return None

##TODO: def capture__adc_samples()
def capture__adc_samples(TEST_CONF={}):
	## ADC ##
	cmu.cmu_adc_enable()
	#
	# reset adc status
	cmu.cmu_adc_reset()
	#
	# read adc base freq
	ADC_BASE_FREQ = cmu.cmu_adc_read_base_freq()
	print('{}: {:#8.3f} MHz\r'.format('ADC_BASE_FREQ',ADC_BASE_FREQ/1e6))
	#
	# check adc base freq
	try: 
		if (ADC_BASE_FREQ!=TEST_CONF['ADC_BASE_FREQ']):
			print('>>> {}: {}'.format('Warning!: ADC_BASE_FREQ is not matched',TEST_CONF['ADC_BASE_FREQ']))
			input('Press Enter key!')
	except: 
		print('> missing ADC_BASE_FREQ in TEST_CONF!')		
	#
	# set adc sampling freq
	try:
		ADC_CNT_SAMPLE_PERIOD = TEST_CONF['ADC_CNT_SAMPLE_PERIOD']
	except:
		# base 210MHz and base 125MHz ... common integer...
		# 210 = 7 * 3 * 2 * 5 ... 14 min
		# 210MHz/15 = 14 Msps 
		# 125 = 5 * 5 * 5     ... 12 min
		# 125MHz/15 = 8.33333333 Msps
		ADC_CNT_SAMPLE_PERIOD = 15
		print('> missing ADC_CNT_SAMPLE_PERIOD in TEST_CONF!')		
	#
	cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_SMP_PRD',ADC_CNT_SAMPLE_PERIOD)
	#
	# set the number of adc samples
	try:
		ADC_NUM_SAMPLES = TEST_CONF['ADC_NUM_SAMPLES']
	except:
		ADC_NUM_SAMPLES = 100
		print('> missing ADC_NUM_SAMPLES in TEST_CONF!')		
	cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_UPD_SMP',ADC_NUM_SAMPLES)
	#
	# set adc data line input delay tap //$$ TODO: ADC_INPUT_DELAY_TAP rev
	# ADC_INPUT_DELAY_TAP_H
	# ADC_INPUT_DELAY_TAP_L
	try:
		#ADC_INPUT_DELAY_TAP = TEST_CONF['ADC_INPUT_DELAY_TAP'] 
		ADC0_INPUT_DELAY_TAP_L = TEST_CONF['ADC0_INPUT_DELAY_TAP_L'] 
		ADC0_INPUT_DELAY_TAP_H = TEST_CONF['ADC0_INPUT_DELAY_TAP_H'] 
		ADC1_INPUT_DELAY_TAP_L = TEST_CONF['ADC1_INPUT_DELAY_TAP_L'] 
		ADC1_INPUT_DELAY_TAP_H = TEST_CONF['ADC1_INPUT_DELAY_TAP_H'] 
	except:
		#ADC_INPUT_DELAY_TAP = 0
		print('> missing ADC_INPUT_DELAY_TAP in TEST_CONF!')		
	#ADC_INPUT_DELAY_TAP_code = (ADC_INPUT_DELAY_TAP<<27)|(ADC_INPUT_DELAY_TAP<<22)
	#ADC_INPUT_DELAY_TAP_code = (ADC_INPUT_DELAY_TAP<<27)|(ADC_INPUT_DELAY_TAP<<22)|(ADC_INPUT_DELAY_TAP<<17)|(ADC_INPUT_DELAY_TAP<<12)
	ADC_INPUT_DELAY_TAP_code = (ADC1_INPUT_DELAY_TAP_H<<27)|(ADC1_INPUT_DELAY_TAP_L<<22)|(ADC0_INPUT_DELAY_TAP_H<<17)|(ADC0_INPUT_DELAY_TAP_L<<12)
	val = ADC_INPUT_DELAY_TAP_code
	#msk = 0xFFC00000
	msk = 0xFFFFF000
	cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_DLY_TAP_OPT',val,msk)
	#
	# set up  test mode
	PIN_TEST_FRC_HIGH = 0
	PIN_DLLN_FRC_LOW  = 0
	PTTN_CNT_UP_EN    = 0
	ADC_control_code = (PTTN_CNT_UP_EN<<2)|(PIN_DLLN_FRC_LOW<<1)|PIN_TEST_FRC_HIGH
	val = ADC_control_code
	msk = 0x00000007
	cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_DLY_TAP_OPT',val,msk)
	#
	#
	# initialize adc
	cmu.cmu_adc_init()
	#
	# cmu_check_adc_test_pattern
	ret = cmu.cmu_check_adc_test_pattern()
	print(ret)
	if ret == False:
		input('> Please check ADC ... test pattern is bad! ')
	#
	# cmu_adc_is_fifo_empty
	ret = cmu.cmu_adc_is_fifo_empty()
	print(ret)
	#
	# cmu_adc_update
	cmu.cmu_adc_update()
	#
	# cmu_adc_load_from_fifo
	#adc_list = cmu.cmu_adc_load_from_fifo(ADC_NUM_SAMPLES=131072, ADC_BITWIDTH=18)
	adc_list = cmu.cmu_adc_load_from_fifo(__ADC_NUM_SAMPLES, __ADC_BITWIDTH)
	#
	# disable
	cmu.cmu_adc_disable()
	#
	try: 
		TEST_CONF['ADC_LIST_INT'] = adc_list
	except: 
		print('> missing ADC_LIST_INT in TEST_CONF!')		
	#
	return adc_list 

def turn_off_test_signal(TEST_CONF={}):
	## DWAVE ##
	# dwave_pulse_off
	cmu.cmu_dwave_pulse_off()
	cmu.cmu_dwave_disable()
	## DAC ##
	# set default values
	cmu.cmu_dac_bias_set_buffer() 
	cmu.cmu_dac_bias_update() 
	cmu.cmu_dac_bias_disable() 
	## SPO ##
	# AMP power off
	cmu.cmu_spo_bit__amp_pwr('OFF')
	# LED off
	cmu.cmu_spo_bit__leds(0x00)
	cmu.cmu_spo_disable()
	#
	return None

	
def display__adc_samples(TEST_CONF={}):
	try:
		#FS_TARGET     = TEST_CONF['FS_TARGET'    ]
		FS_TRUE       = TEST_CONF['FS_TRUE'      ]
		DUMP_FILE_PRE = TEST_CONF['DUMP_FILE_PRE']
		ADC_LIST_INT  = TEST_CONF['ADC_LIST_INT' ]
		NUM_SMP_ZOOM  = TEST_CONF['NUM_SMP_ZOOM' ]
	except:
		return False
	#
	#cmu.cmu_adc_display_data_list_int (ADC_LIST_INT,FS_TARGET)
	#fig_filename = cmu.cmu_adc_display_data_list_int__zoom (ADC_LIST_INT,DUMP_FILE_PRE,FS_TARGET,NUM_SMP_ZOOM)
	fig_filename = cmu.cmu_adc_display_data_list_int__zoom (ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE,NUM_SMP_ZOOM)
	return fig_filename 

def save__adc_samples_into_csv_file(TEST_CONF={}):
	try:
		#FS_TARGET     = TEST_CONF['FS_TARGET'    ]
		FS_TRUE       = TEST_CONF['FS_TRUE'      ]
		DUMP_FILE_PRE = TEST_CONF['DUMP_FILE_PRE']
		ADC_LIST_INT  = TEST_CONF['ADC_LIST_INT' ]
		TEMP_FPGA     = TEST_CONF['TEMP_FPGA']
	except:
		return False
	# adc_save_data_list_int_to_csv
	#csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(ADC_LIST_INT,DUMP_FILE_PRE,FS_TARGET)
	#csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE)
	csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE,TEMP_FPGA)
	return csv_filename 

def save__conf_result_dict_into_dict_file(TEST_CONF={}):
	ret = None
	#
	if TEST_CONF:
		#
		ret = cmu.cmu_adc_save_conf_result_dict_to_file(TEST_CONF)
		#
	#
	return ret
	
#def report__test_sheet_data_from_csv_file(FILENAME):
#	pass
#	return None 

#def report__test_sheet_data_from_dict_file(FILENAME):
#	pass
#	return None 

##TODO: def conduct__test_conf()
def conduct__test_conf(TEST_CONF):
	## conduct test
	setup____test_signal(TEST_CONF)
	turn_on__test_signal(TEST_CONF)
	capture__adc_samples(TEST_CONF)
	#
	# for TO test 
	#input('')
	#
	turn_off_test_signal(TEST_CONF)
	#
	## show and save figure
	fig_filename = display__adc_samples(TEST_CONF)
	print(fig_filename)
	#
	## save csv
	csv_filename = save__adc_samples_into_csv_file(TEST_CONF)
	print(csv_filename)
	#
	## save dict
	if DUMP_DICT_DIS == 0:
		dict_filename = save__conf_result_dict_into_dict_file(TEST_CONF)
		print(dict_filename)
	#
	## readback dict
	#exec( 'readback_dict = ' + open(dict_filename).read() )
	#print(len(readback_dict))
	#print(readback_dict)
	return None

#######################################################################

####
# common setup
TEST_CONF = {}
#

## read and add temperature to info
TEST_CONF['TEMP_FPGA'] = TEMP_FPGA

##TODO: (1) test case 0-0: no signal / noise level check @ 15.0Msps
if TEST_CASE_0_0_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '0-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'OFF' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 10e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ## 
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (2) test case 0-1: no signal / noise level check @ 9.55Msps
if TEST_CASE_0_1_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '0-1' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'OFF' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 10e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_1  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (3) test case 0-2: no signal / noise level check @ 3.89Msps
if TEST_CASE_0_2_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '0-2' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'OFF' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 10e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_2  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (4) test case 0-3: no signal / noise level check @ 0.97Msps
if TEST_CASE_0_3_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '0-3' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'OFF' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 10e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_3 ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##


##TODO: (5) test case 1-0: 100kHz signal, BW 120kHz @ 15.0Msps
if TEST_CASE_1_0_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '1-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120K' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 100e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (6) test case 1-1: 100kHz signal, BW 120kHz @ 9.55Msps
if TEST_CASE_1_1_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '1-1' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120K' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 100e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_1  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (7) test case 1-2: 100kHz signal, BW 120kHz @ 3.89Msps
if TEST_CASE_1_2_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '1-2' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120K' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 100e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_2  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (8) test case 1-3: 100kHz signal, BW 120kHz @ 0.97Msps
if TEST_CASE_1_3_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '1-3' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120K' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 100e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_3 ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##	
	
##TODO: (*) test case 1-4: 100kHz signal, BW 120kHz @ 0.099Msps << down-sampling // 210	105	0.1	2120	0.9433962264	0.099057	131072	1248.304762	1237	129885
if TEST_CASE_1_4_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '1-4' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120K' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 100e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_99K ## 
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##	
	
##TODO: ( 9) test case 2-0: 1MHz signal, BW 1.2MHz @ 15.0Msps
if TEST_CASE_2_0_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '2-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET =   1e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (10) test case 2-1: 1MHz signal, BW 1.2MHz @ 9.55Msps
if TEST_CASE_2_1_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '2-1' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET =   1e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_1  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (11) test case 2-2: 1MHz signal, BW 1.2MHz @ 3.89Msps
if TEST_CASE_2_2_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '2-2' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET =   1e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_2  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (12) test case 2-3: 1MHz signal, BW 1.2MHz @ 0.97Msps << down-sampling
if TEST_CASE_2_3_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '2-3' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET =   1e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_3 ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##	
	
##TODO: (*) test case 2-4: 1MHz signal, BW 1.2MHz @ 0.97Msps << down-sampling // 1	212	9.433962264	0.9905660377	131072	1248.304762	1237	129885
if TEST_CASE_2_4_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '2-4' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET =   1e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	#FS_TARGET             = FS_3 ##
	FS_TARGET             = FS_991K ##	
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##	
	
##TODO: (13) test case 3-0: 10MHz signal, BW 12MHz @ 15.0Msps
if TEST_CASE_3_0_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '3-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET =  10e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(FS_TARGET-DWAVE_FREQ_TARGET)*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (14) test case 3-1: 10MHz signal, BW 12MHz @ 9.55Msps << down-sampling
if TEST_CASE_3_1_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '3-1' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET =  10e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_1  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (15) test case 3-2: 10MHz signal, BW 12MHz @ 3.89Msps
if TEST_CASE_3_2_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '3-2' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET =  10e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_2  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_BASE_FREQ*10*TEST_CONF['FS_TARGET']) ##
	TEST_CONF['NUM_SMP_ZOOM' ] = 100##
	#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(3*FS_TARGET-DWAVE_BASE_FREQ)*10*TEST_CONF['FS_TARGET']) ##
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (16) test case 3-3: 10MHz signal, BW 12MHz @ 0.97Msps
if TEST_CASE_2_3_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '3-3' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET =  10e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_3 ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
	TEST_CONF['NUM_SMP_ZOOM' ] = 100 ##
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##	
	
	
##TODO: (17) test case 4-0: 100kHz signal, BW 120MHz @ 15.0Msps
if TEST_CASE_4_0_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '4-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 100e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (18) test case 4-1: 100kHz signal, BW 120MHz @ 9.55Msps
if TEST_CASE_4_1_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '4-1' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 100e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_1  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (19) test case 4-2: 100kHz signal, BW 120MHz @ 3.89Msps
if TEST_CASE_4_2_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '4-2' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 100e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_2  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (20) test case 4-3: 100kHz signal, BW 120MHz @ 0.97Msps
if TEST_CASE_4_3_EN:
	## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '4-3' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 100e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_3 ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##	
		
##TODO: (*) test case 5-0: 200kHz signal, BW 120kHz @ 15.0Msps
if TEST_CASE_5_0_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '5-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 1M2
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 200e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (*) test case 6-0: 500kHz signal, BW 1.2MHz @ 15.0Msps
if TEST_CASE_6_0_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '6-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ## 120K vs 1M2 vs 12M 
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 500e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##	
	
##TODO: (*) test case 7-0: 5MHz signal, BW 12MHz @ 15.0Msps
if TEST_CASE_7_0_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '7-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 5e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##		

##TODO: (*) test case 7-1: 5MHz signal, BW 12MHz @ 4.884Msps << down-sampling 
if TEST_CASE_7_1_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '7-1' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '12M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 5e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	#FS_TARGET             = FS_0  ##
	FS_TARGET             = FS_4M88  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) # down-sampling
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##		

##TODO: (*) test case 8-0: 16.875MHz signal, BW 120MHz @ 15.0Msps
if TEST_CASE_8_0_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '8-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120M' ##
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 16.875e6 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) # down-sampling
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##		

##TODO: (*) test case 9-0: 2MHz signal, BW 1.2MHz @ 14.0Msps
if TEST_CASE_9_0_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '9-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ## 120K vs 1M2 vs 12M 
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 2000e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	#FS_TARGET             = FS_0  ##
	FS_TARGET             = FS_14M  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##	

##TODO: (*) test case 9-1: 2MHz signal, BW 1.2MHz @ 1.98Msps
if TEST_CASE_9_1_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '9-1' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '1M2' ## 120K vs 1M2 vs 12M 
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 2000e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	#FS_TARGET             = FS_0  ##
	#FS_TARGET             = FS_14M  ##
	FS_TARGET             = FS_1M98  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) # down-sampling
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##	


	
##TODO: (*) test case 10-0: 1kHz signal, BW 120kHz @ 15.0Msps
if TEST_CASE_10_0_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '10-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '12M' ## 120K vs 12M
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 1e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ## FS_0 vs 1e6
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##

##TODO: (*) test case 11-0: 10kHz signal, BW 120kHz @ 15.0Msps
if TEST_CASE_11_0_EN:
## setup TEST_CONF ##
	TEST_CONF['CASE'           ] = '11-0' ##
	TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
	TEST_CONF['FILT_PATH_CTRL' ] = '120K' ## 120K vs 12M
	TEST_CONF['DAC_CODE'       ] = __DAC_CODE ##
	#
	DWAVE_FREQ_TARGET = 10e3 ##
	DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
	CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
	CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
	DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
	#
	TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
	TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
	TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
	TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
	TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
	#
	FS_TARGET             = FS_0  ##
	ADC_BASE_FREQ         = __ADC_BASE_FREQ
	ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
	FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
	ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
	#ADC_INPUT_DELAY_TAP   = PARAM__ADC_INPUT_DELAY_TAP ##
	ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
	ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
	ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
	ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
	#
	TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
	TEST_CONF['FS_TARGET'            ] = FS_TARGET
	TEST_CONF['FS_TRUE'              ] = FS_TRUE
	TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
	TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
	#TEST_CONF['ADC_INPUT_DELAY_TAP'  ] = ADC_INPUT_DELAY_TAP
	TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
	TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
	TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
	TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
	#
	TEST_CONF['AMP_GAIN_CTRL'] = '1X' ##
	TEST_CONF['ADC_LIST_INT' ] = []
	TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
	####
	#
	if DUMP_EN==1:
		TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
	else:
		TEST_CONF['DUMP_FILE_PRE'  ] = ''
	#
	## conduct TEST_CONF
	conduct__test_conf(TEST_CONF)
##
	
######################################################################



####
## cmu_adc_load_from_csv
#ret = cmu.cmu_adc_load_from_csv(csv_filename)
#adc_list_rb = ret[0]
#fs_rb       = ret[1]
#ns_rb       = ret[2]
#print(adc_list_rb[0][0:5])
#print(adc_list_rb[1][0:5])
#print(fs_rb)
#print(ns_rb)
####

####
## adc_display_data_list_int
#ret = cmu.cmu_adc_display_data_list_int(adc_list_rb,fs_rb)
#print(ret)
####


####
#report__test_sheet_data_from_csv_file(TEST_CONF)


########



###################################################

########
# TEST

####
## test counter off
ret = cmu.cmu_test_counter('OFF')
print(ret)
####

####
## test counter reset
ret = cmu.cmu_test_counter('RESET')
print(ret)
####


####
## close
ret = cmu.ok_cmu_close()
print(ret)
####