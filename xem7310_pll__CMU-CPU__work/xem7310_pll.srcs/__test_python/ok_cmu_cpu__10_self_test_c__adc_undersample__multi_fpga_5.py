## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_cmu_cpu__10_self_test_c__adc_undersample__multi_fpga_1.py : 
#
#   use cmu_spo_bit__adc_pwr_only() to control ADC power
#
#  


####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
BIT_FILENAME = 'xem7310__cmu_cpu__top.bit'
#
# FPGA serial to access
#FPGA_SERIAL = '' # for any
#FPGA_SERIAL = '1739000J7V' # for module: CMU-CPU-F5500-FPGA1
#FPGA_SERIAL = '1908000OVP' # for module: CMU-CPU-F5500-FPGA2
#FPGA_SERIAL = '1739000J8A' # for module: CMU-CPU-F5500-FPGA3
#FPGA_SERIAL = '1739000J63' # for module: CMU-CPU-F5500-FPGA4
FPGA_SERIAL = '1739000J8I' # for module: CMU-CPU-F5500-FPGA5
#
# DUMP file prefix
#__DUMP_PRE__ = 'DUMP_BD1__'
#__DUMP_PRE__ = 'DUMP_BD2__'
#__DUMP_PRE__ = 'DUMP_BD3__'
#__DUMP_PRE__ = 'DUMP_BD4__'
__DUMP_PRE__ = 'DUMP_BD5__'
#
#__DUMP_PRE__ = 'DUMP_BD5_531__'
#
__DWAVE_BASE_FREQ = 160e6 # 80e6 # 160e6 # 135e6
#
#
__ADC_BASE_FREQ = 210e6 # 125e6

## class for sampling freq 
class c_Fs_info:
	""" variables """
	#
	# ADC sampling rates // TODO: Fs
	FS_0 = 210e6/ 14 # 15.0Msps
	FS_1 = 210e6/ 22 # 9.55Msps # for 10MHz test 
	FS_2 = 210e6/ 54 # 3.89Msps
	FS_3 = 210e6/216 # 0.97Msps
	#
	FS_15M00 = 210e6/14   # 15.0Msps
	FS_14M00 = 210e6/15   # 14.0Msps
	#
	#FS_99K  = 210e6/2120 # 99.057Ksps // 2120	0.099057
	#
	FS_100K0 = 210e6/2100
	FS_150K0 = 210e6/1400
	FS_200K0 = 210e6/1050
	FS_210K0 = 210e6/1000
	FS_250K0 = 210e6/ 840
	FS_280K0 = 210e6/ 750
	FS_300K0 = 210e6/ 700
	FS_420K0 = 210e6/ 500
	FS_500K0 = 210e6/ 420
	FS_600K0 = 210e6/ 350
	FS_700K0 = 210e6/ 300
	FS_750K0 = 210e6/ 280
	FS_1M000 = 210e6/ 210
	FS_1M050 = 210e6/ 200
	FS_1M400 = 210e6/ 150 
	FS_1M500 = 210e6/ 140
	FS_2M000 = 210e6/ 105
	FS_2M100 = 210e6/ 100
	FS_2M500 = 210e6/  84
	FS_2M800 = 210e6/  75
	FS_3M000 = 210e6/  70
	FS_4M200 = 210e6/  50
	FS_5M000 = 210e6/  42
	FS_6M000 = 210e6/  35
	FS_7M000 = 210e6/  30
	FS_7M500 = 210e6/  28
	FS_10M00 = 210e6/  21
	FS_10M50 = 210e6/  20
	#
	
	#   case U_001K_   
	#FS_0K999 = 210e6/210010 #
	FS_0K991 = 210e6/212000 #
	FS_0K988 = 210e6/212500
	FS_0K993 = 210e6/211400
	FS_1K000 = 210e6/210100
	#
	FS_0K99941 = 210e6/210125
	FS_0K99950 = 210e6/210105
	FS_0K99964 = 210e6/210075
	FS_0K99983 = 210e6/210035
	FS_0K99988 = 210e6/210025
	FS_0K99993 = 210e6/210015
	
	
	#   case U_002K_   
	#FS_1K999 = 210e6/105008 #
	FS_1K981 = 210e6/106000 #
	FS_1K976 = 210e6/106250
	FS_1K987 = 210e6/105700
	FS_1K999 = 210e6/105050
	#
	FS_1K99857 = 210e6/105075
	FS_1K99867 = 210e6/105070
	FS_1K99933 = 210e6/105035
	
	
	#   case U_005K_   
	#FS_4K999 = 210e6/42008  #
	FS_4K953 = 210e6/ 42400  #
	FS_4K941 = 210e6/ 42500
	FS_4K967 = 210e6/ 42280
	FS_4K998 = 210e6/ 42020
	#
	FS_4K9970  = 210e6/ 42025
	FS_4K9975  = 210e6/ 42021
	FS_4K9982  = 210e6/ 42015
	
	
	#   case U_010K_   
	FS_9K998 = 210e6/ 21004  #
	FS_9K882 = 210e6/ 21250
	FS_9K934 = 210e6/ 21140
	FS_9K995 = 210e6/ 21010
	#
	FS_9K9929  = 210e6/ 21015
	FS_9K9933  = 210e6/ 21014
	FS_9K9967  = 210e6/ 21007
	
	
	#   case U_020K_   
	FS_19K98 = 210e6/ 10510  #
	FS_19K76 = 210e6/ 10625
	FS_19K86 = 210e6/ 10570
	FS_19K99 = 210e6/ 10505
	#
	FS_19K987 = 210e6/ 10507
	FS_19K994 = 210e6/ 10503
	FS_19K998 = 210e6/ 10501
	
	
	#   case U_050K_   
	FS_49K95 = 210e6/  4204
	FS_49K53 = 210e6/  4240  #
	FS_49K41 = 210e6/  4250
	FS_49K66 = 210e6/  4228
	FS_49K99 = 210e6/  4201 
	FS_49K98 = 210e6/  4202 
	#
	FS_49K96 = 210e6/  4203  
	
	#   case U_100K_   
	FS_99K91 = 210e6/  2102  #
	FS_98K82 = 210e6/  2125
	FS_99K33 = 210e6/  2114
	FS_99K95 = 210e6/  2101
	
	#   case U_200K_   
	FS_198K1 = 210e6/  1060  #
	FS_197K1 = 210e6/  1065
	FS_198K6 = 210e6/  1057
	#
	FS_198K9 = 210e6/  1056
	FS_197K4 = 210e6/  1064
	FS_195K3 = 210e6/  1075
	FS_196K1 = 210e6/  1071
	FS_199K1 = 210e6/  1055
	FS_199K4 = 210e6/  1053
	FS_199K8 = 210e6/  1051
	
	#   case U_500K_  
	FS_495K3 = 210e6/   424  #
	FS_494K1 = 210e6/   425
	FS_498K8 = 210e6/   421
	
	#   case U_001M_  
	FS_995K3 = 210e6/   211
	FS_990K6 = 210e6/   212  #
	FS_985K9 = 210e6/   213
	
	#   case U_002M_  
	FS_1M981 = 210e6/106  #
	
	#   case U_005M_   
	FS_4M884 = 210e6/43   #
	
	#   case U_010M_   
	FS_9M545 = 210e6/22   #
	FS_8M750 = 210e6/24
	FS_5M122 = 210e6/41
	####


#
##TODO: DUMP setup
DUMP_EN = 1     #######

#
# with adc base 210MHz
PARAM__ADC0_INPUT_DELAY_TAP_L =  0 # 15 0 #   // even - DB
PARAM__ADC0_INPUT_DELAY_TAP_H =  0 # 15 0 #   // odd - DA
PARAM__ADC1_INPUT_DELAY_TAP_L =  0 # 15 0
PARAM__ADC1_INPUT_DELAY_TAP_H =  0 # 15 0 #
#
#
# reference times
# 
#  undersampling rate  kHz	/ length bits	/ ADC samples	/ ACQ times sec
#  1		12	4096	4.096
#  2		12	4096	2.048
#  5		12	4096	0.8192
#  10		15	32768	3.2768
#  20		15	32768	1.6384
#  50		15	32768	0.65536
#  100		17	131072	1.31072
#  200		17	131072	0.65536
#  500		17	131072	0.262144
#  1000		17	131072	0.131072
#  2000		17	131072	0.065536
#  5000		17	131072	0.0262144
#  10000	17	131072	0.0131072
__ADC_NUM_SAMPLES        = 2**17
__ADC_NUM_SAMPLES_SHORT  = 2**15
__ADC_NUM_SAMPLES_SHORT2 = 2**12
#
__ADC_BITWIDTH=18
#
# range control option: full range +/-10V
# DAC_volt = 10V /0x7FFF * DAC_code
#
# new board conf:
#   DAC_BIAS_A --> PORT_DWAVE_OUT4
#   DAC_BIAS_B --> PORT_DWAVE_OUT3
#   DAC_BIAS_C --> PORT_DWAVE_OUT2
#   DAC_BIAS_D --> PORT_DWAVE_OUT1
#   DAC_BIAS_C --> PORT_VSQ_OUT
#   DAC_BIAS_D --> PORT_VS_OUT
#
__DAC_CODE_A = 0x0000 # 0V for new board ... bias voltages
__DAC_CODE_B = 0x0000 # 0V for new board ... bias voltages
__DAC_CODE_C = 0x0000 # 0V for new board ... bias voltages
__DAC_CODE_D = 0x0000 # 0V for new board ... bias voltages
#
# output path selection
TEST_PATH_I_ONLY_EN = 0
TEST_PATH_Q_ONLY_EN = 0
TEST_PATH_DISABLE   = 0

# test signal wait control 
#__TEST_SIGNAL_WAIT_BEFORE_ADC__SEC = 1
#__TEST_SIGNAL_WAIT_BEFORE_ADC__SEC = 0.01
__TEST_SIGNAL_WAIT_BEFORE_ADC__SEC = 0.0

class  c_case_info:
	""" variables """
	#
	# case: no signal
	TEST_CASE_0_0_EN = 0
	
	## normal sampling cases 
	#
	# selected list:
	#
	TEST_CASE__N_001K_210_15__EN   = 0x001  # FS_14M00 = 210e6/15 # // 210	15	14000	1	NA	14.000	131072	9.362	7	98000.00
	TEST_CASE__N_002K_210_15__EN   = 0x001  # FS_14M00 = 210e6/15 # // 210	15	7000	2	NA	14.000	131072	18.725	17	119000.00
	TEST_CASE__N_005K_210_14__EN   = 0x001  # FS_15M00  = 210e6/14   # //  210	14	3000	5	15.000	131072	43.69066667	43	129000.00
	#
	####
	
	
	
	## undersampling cases 
	#
	# selected list 
	#
	TEST_CASE__U_010K_210_21010__EN  = 0x0001 # FS_9K995   = 210e6/ 21010  # // 210	2100	0.01	21010	0.004759638267	0.009995	131072	62.4152381	61	128100.00
	TEST_CASE__U_020K_210_10505__EN  = 0x0001 # FS_19K99   = 210e6/ 10505  # // 210	2100	0.02	10505	0.009519276535	0.019990	131072	62.4152381	61	128100.00
	TEST_CASE__U_050K_210_4202__EN   = 0x0001 # FS_49K98   = 210e6/  4202  # // 210	2100	0.05	4202	0.02379819134	0.049976	131072	62.4152381	61	128100.00
	TEST_CASE__U_100K_210_2101__EN   = 0x0001 # FS_99K95   = 210e6/  2101  # // 210	2101	2100	0.1		0.047596	0.099952	131072	62.415	61	128100.00
	TEST_CASE__U_200K_210_1051__EN   = 0x0001 # FS_199K8 = 210e6/  1051    # // 210	1050	0.2	1051	0.1902949572	0.199810	131072	124.8304762	113	118650.00
	TEST_CASE__U_500K_210_421__EN    = 0x0001 # FS_498K8 = 210e6/   421    # // 210	420	0.5	421	1.187648456	0.498812	131072	312.0761905	311	130620.00
	TEST_CASE__U_001M_210_211__EN    = 0x0001 # FS_995K3 = 210e6/   211    # // 210	210	1	211	4.739336493	0.995261	131072	624.152381	619	129990.00
	TEST_CASE__U_002M_210_106__EN    = 0x0001 # FS_1M981 = 210e6/   106    # // 210	105		2		106		18.86792453			1.981132	131072	1248.304762	1237	129885.00
	TEST_CASE__U_005M_210_43__EN     = 0x0001 # FS_4M884 = 210e6/    43    # // 210	42		5		43		116.2790698			4.883721	131072	3120.761905	3119	130998.00
	TEST_CASE__U_010M_210_43__EN     = 0x0001 # FS_4M884 = 210e6/    43    # // 210	43		21		10		232.558140			4.883721	131072	6241.524	6229	130809.00
	#
	####
	

####
## library call
import ok_cmu_cpu__lib as cmu
#
# check configuration : OK_EP_ADRS_CONFIG
#  
EP_ADRS = cmu.conf.OK_EP_ADRS_CONFIG
#  
def form_contents(var,idx):
	return '{} = {}'.format(idx, var[idx])
#
def form_address(var,idx):
	return '{} @ {:#04x}'.format(idx, var[idx])
#  
print(form_contents(EP_ADRS,'board_name'))
print(form_contents(EP_ADRS,'ver'))
print(form_contents(EP_ADRS,'bit_filename'))
#
print(form_address(EP_ADRS,'FPGA_IMAGE_ID'))
#
####

####
## init : dev
dev = cmu.ok_cmu_init()
print(dev)
####

####
ret = cmu.ok_cmu_caller_id()
print(ret)
####

####
## open
ret = cmu.ok_cmu_open(FPGA_SERIAL)
print(ret)
####
DevSR =  ret[1]
if not DevSR:  # no device opened 
	# retry
	print('>>> cmu open failed!')
	#
	MAX_RETRY = 10 ###
	INTERVAL_RETRY = 1 # sec 
	cnt_retry_open = 0
	while True:
		cnt_retry_open += 1
		print('>>> retry: {}'.format(cnt_retry_open))
		cmu.sleep(INTERVAL_RETRY)
		ret = cmu.ok_cmu_open(FPGA_SERIAL)
		DevSR =  ret[1]
		if DevSR: # device opened 
			print('>>> dev opend: {}'.format(DevSR))
			break 
		#
		if cnt_retry_open >= MAX_RETRY:
			# max reached 
			print('>>> cmu open retry failed!')
			#exit(0)
			#raise SystemExit(0)
			#exit()
			break
	#

####
## device opened 
if DevSR: 
	
	####
	## FPGA_CONFIGURE
	if FPGA_CONFIGURE==1: 
		ret = cmu.ok_cmu_conf(BIT_FILENAME)
		print(ret)
	####  
	
	####
	## read fpga_image_id
	fpga_image_id = cmu.cmu_read_fpga_image_id()
	#
	def form_hex_32b(val):
		return '0x{:08X}'.format(val)
	#
	print(form_hex_32b(fpga_image_id))
	####
	
	####
	## read FPGA internal temp and volt
	ret = cmu.cmu_monitor_fpga()
	print(ret)
	#
	TEMP_FPGA = ret[0]
	print(TEMP_FPGA)
	####
	
	####
	## test counter on 
	ret = cmu.cmu_test_counter('ON')
	print(ret)
	####
	
	###################################################
	## subfunctions
	
	##TODO: def read_board_condition(TEST_CONF)
	def read_board_condition(TEST_CONF={}):
		## read and add temperature to info
		ret = cmu.cmu_monitor_fpga()
		print(ret)
		#
		TEMP_FPGA = ret[0]
		#
		TEST_CONF['TEMP_FPGA'] = TEMP_FPGA
		return None
	
	##TODO: def setup____test_signal()
	def setup____test_signal(TEST_CONF={}): 
		## SPO ##
		cmu.cmu_spo_enable()
		cmu.cmu_spo_init()
		#
		# led on
		cmu.cmu_spo_bit__leds(0xFF) 
		#
		# AMP power control
		try: 
			cmu.cmu_spo_bit__amp_pwr_only(TEST_CONF['AMP_PWR_CTRL'])
		except: 
			print('> missing AMP_PWR_CTRL in TEST_CONF!')	
		#
		## DAC ##
		cmu.cmu_dac_bias_enable()
		cmu.cmu_dac_bias_init()
		#
		# set DAC code 
		DAC1_CODE = __DAC_CODE_A
		DAC2_CODE = __DAC_CODE_B
		DAC3_CODE = __DAC_CODE_C
		DAC4_CODE = __DAC_CODE_D
		#
		cmu.cmu_dac_bias_set_buffer(
				DAC1=DAC1_CODE,
				DAC2=DAC2_CODE,
				DAC3=DAC3_CODE,
				DAC4=DAC4_CODE)
		#
		cmu.cmu_dac_bias_update()
		#
		ret = cmu.cmu_dac_bias_readback()
		print(ret)
		#
		# enable
		cmu.cmu_dwave_enable()
		#
		# read dwave base freq
		DWAVE_BASE_FREQ = cmu.cmu_dwave_read_base_freq()
		print('{}: {:#8.3f} MHz\r'.format('DWAVE_BASE_FREQ',DWAVE_BASE_FREQ/1e6))
		#
		try: 
			if (DWAVE_BASE_FREQ!=TEST_CONF['DWAVE_BASE_FREQ']):
				print('>>> {}: {}'.format('Warning!: DWAVE_BASE_FREQ is not matched',TEST_CONF['DWAVE_BASE_FREQ']))
				input('Press Enter key!')
		except: 
			print('> missing DWAVE_BASE_FREQ in TEST_CONF!')		
		#  
		try: 
			CNT_PERIOD = TEST_CONF['CNT_PERIOD']
		except: 
			CNT_PERIOD = 80 
			print('> missing CNT_PERIOD in TEST_CONF!')
		#
		try: 
			CNT_DIFF = TEST_CONF['CNT_DIFF']
		except: 
			CNT_DIFF   = 60
			print('> missing CNT_DIFF in TEST_CONF!')		
		#  
		# set CNT_PERIOD
		cmu.cmu_dwave_wr_cnt_period(CNT_PERIOD)
		#
		# set CNT_DIFF
		cmu.cmu_dwave_wr_cnt_diff(CNT_DIFF)
		#
		# dwave output control
		if TEST_PATH_I_ONLY_EN:
			cmu.cmu_dwave_wr_output_dis__enable_path_i_only()
		elif TEST_PATH_Q_ONLY_EN:
			cmu.cmu_dwave_wr_output_dis__enable_path_q_only()
		elif TEST_PATH_DISABLE:
			cmu.cmu_dwave_wr_output_dis__disable_all()
		else:
			cmu.cmu_dwave_wr_output_dis__enable_all()
		#
		# set dwave parameters into core logic
		cmu.cmu_dwave_set_para()
		#
		return None
	
	def turn_on__test_signal(TEST_CONF={}):
		# dwave_pulse_on
		cmu.cmu_dwave_pulse_on()
		#
		# wait for pulse stability
		cmu.sleep(__TEST_SIGNAL_WAIT_BEFORE_ADC__SEC) ### 
		#
		return None
	
	##TODO: def capture__adc_samples()
	def capture__adc_samples(TEST_CONF={}):
		## ADC ##
		#
		# reset adc status
		cmu.cmu_adc_reset()
		#
		# enable
		cmu.cmu_adc_enable()
		#
		# ADC power on ##
		cmu.cmu_spo_bit__adc_pwr_only('ON')
		#
		# read adc base freq
		ADC_BASE_FREQ = cmu.cmu_adc_read_base_freq()
		print('{}: {:#8.3f} MHz\r'.format('ADC_BASE_FREQ',ADC_BASE_FREQ/1e6))
		#
		# check adc base freq
		try: 
			if (ADC_BASE_FREQ!=TEST_CONF['ADC_BASE_FREQ']):
				print('>>> {}: {}'.format('Warning!: ADC_BASE_FREQ is not matched',TEST_CONF['ADC_BASE_FREQ']))
				input('Press Enter key!')
		except: 
			print('> missing ADC_BASE_FREQ in TEST_CONF!')		
		#
		# set adc sampling freq
		try:
			ADC_CNT_SAMPLE_PERIOD = TEST_CONF['ADC_CNT_SAMPLE_PERIOD']
		except:
			# base 210MHz and base 125MHz ... common integer...
			# 210 = 7 * 3 * 2 * 5 ... 14 min
			# 210MHz/15 = 14 Msps 
			# 125 = 5 * 5 * 5     ... 12 min
			# 125MHz/15 = 8.33333333 Msps
			ADC_CNT_SAMPLE_PERIOD = 15
			print('> missing ADC_CNT_SAMPLE_PERIOD in TEST_CONF!')		
		#
		cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_SMP_PRD',ADC_CNT_SAMPLE_PERIOD)
		#
		# set the number of adc samples
		try:
			ADC_NUM_SAMPLES = TEST_CONF['ADC_NUM_SAMPLES']
		except:
			ADC_NUM_SAMPLES = 100
			print('> missing ADC_NUM_SAMPLES in TEST_CONF!')		
		cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_UPD_SMP',ADC_NUM_SAMPLES)
		#
		# set adc data line input delay tap 
		# ADC_INPUT_DELAY_TAP_H
		# ADC_INPUT_DELAY_TAP_L
		try:
			#ADC_INPUT_DELAY_TAP = TEST_CONF['ADC_INPUT_DELAY_TAP'] 
			ADC0_INPUT_DELAY_TAP_L = TEST_CONF['ADC0_INPUT_DELAY_TAP_L'] 
			ADC0_INPUT_DELAY_TAP_H = TEST_CONF['ADC0_INPUT_DELAY_TAP_H'] 
			ADC1_INPUT_DELAY_TAP_L = TEST_CONF['ADC1_INPUT_DELAY_TAP_L'] 
			ADC1_INPUT_DELAY_TAP_H = TEST_CONF['ADC1_INPUT_DELAY_TAP_H'] 
		except:
			#ADC_INPUT_DELAY_TAP = 0
			print('> missing ADC_INPUT_DELAY_TAP in TEST_CONF!')		
		#
		ADC_INPUT_DELAY_TAP_code = (ADC1_INPUT_DELAY_TAP_H<<27)|(ADC1_INPUT_DELAY_TAP_L<<22)|(ADC0_INPUT_DELAY_TAP_H<<17)|(ADC0_INPUT_DELAY_TAP_L<<12)
		val = ADC_INPUT_DELAY_TAP_code
		#
		msk = 0xFFFFF000
		cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_DLY_TAP_OPT',val,msk)
		#
		# set up  test mode
		PIN_TEST_FRC_HIGH = 0
		PIN_DLLN_FRC_LOW  = 0
		PTTN_CNT_UP_EN    = 0
		ADC_control_code = (PTTN_CNT_UP_EN<<2)|(PIN_DLLN_FRC_LOW<<1)|PIN_TEST_FRC_HIGH
		val = ADC_control_code
		msk = 0x00000007
		cmu.cmu_adc_write_wire_endpoint_dict('ADC_HS_DLY_TAP_OPT',val,msk)
		#
		#
		# initialize adc
		ret = cmu.cmu_adc_init()
		if ret == 0:
			return None
		#
		# cmu_check_adc_test_pattern
		ret = cmu.cmu_check_adc_test_pattern()
		print(ret)
		if ret == False:
			# input('> Please check ADC ... test pattern is bad! ')
			return None
		#
		# cmu_adc_is_fifo_empty
		ret = cmu.cmu_adc_is_fifo_empty()
		print(ret)
		#
		# cmu_adc_update
		ret = cmu.cmu_adc_update()
		if ret == 0:
			return None
		#
		# cmu_adc_load_from_fifo
		adc_list = cmu.cmu_adc_load_from_fifo(ADC_NUM_SAMPLES, __ADC_BITWIDTH)
		#
		# ADC power off ##
		cmu.cmu_spo_bit__adc_pwr_only('OFF')
		#
		# disable
		cmu.cmu_adc_disable()
		#
		try: 
			TEST_CONF['ADC_LIST_INT'] = adc_list
		except: 
			print('> missing ADC_LIST_INT in TEST_CONF!')
		#
		return adc_list 
	
	def turn_off_test_signal(TEST_CONF={}):
		## DWAVE ##
		# dwave_pulse_off
		cmu.cmu_dwave_pulse_off()
		cmu.cmu_dwave_disable()
		## DAC ##
		# set default values
		cmu.cmu_dac_bias_set_buffer() 
		cmu.cmu_dac_bias_update() 
		cmu.cmu_dac_bias_disable() 
		## SPO ##
		# AMP power off
		cmu.cmu_spo_bit__amp_pwr_only('OFF')
		# LED off
		cmu.cmu_spo_bit__leds(0x00)
		cmu.cmu_spo_disable()
		#
		return None
	
		
	def display__adc_samples(TEST_CONF={}):
		try:
			#FS_TARGET     = TEST_CONF['FS_TARGET'    ]
			FS_TRUE       = TEST_CONF['FS_TRUE'      ]
			DUMP_FILE_PRE = TEST_CONF['DUMP_FILE_PRE']
			ADC_LIST_INT  = TEST_CONF['ADC_LIST_INT' ]
			NUM_SMP_ZOOM  = TEST_CONF['NUM_SMP_ZOOM' ]
		except:
			return False
		#
		#cmu.cmu_adc_display_data_list_int (ADC_LIST_INT,FS_TARGET)
		fig_filename = cmu.cmu_adc_display_data_list_int__zoom (ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE,NUM_SMP_ZOOM)
		return fig_filename 
	
	def save__adc_samples_into_csv_file(TEST_CONF={}):
		try:
			#FS_TARGET     = TEST_CONF['FS_TARGET'    ]
			FS_TRUE        = TEST_CONF['FS_TRUE'      ]
			DUMP_FILE_PRE  = TEST_CONF['DUMP_FILE_PRE']
			ADC_LIST_INT   = TEST_CONF['ADC_LIST_INT' ]
			TEMP_FPGA      = TEST_CONF['TEMP_FPGA']
			WARN_RETRY_CNT = TEST_CONF['warning__adc_retry_count']
		except:
			return False
		# adc_save_data_list_int_to_csv
		csv_filename = cmu.cmu_adc_save_data_list_int_to_csv(ADC_LIST_INT,DUMP_FILE_PRE,FS_TRUE,TEMP_FPGA,WARN_RETRY_CNT)
		return csv_filename 
	
	def save__conf_result_dict_into_dict_file(TEST_CONF={}):
		ret = None
		#
		if TEST_CONF:
			#
			ret = cmu.cmu_adc_save_conf_result_dict_to_file(TEST_CONF)
			#
		#
		return ret
		
	
	##TODO: def conduct__test_conf() // retry  capture__adc_samples
	def conduct__test_conf(TEST_CONF):
		## conduct test
		#
		read_board_condition(TEST_CONF)
		#
		setup____test_signal(TEST_CONF)
		turn_on__test_signal(TEST_CONF)
		ret = capture__adc_samples(TEST_CONF)
		while ret == None:
			# retry count up
			TEST_CONF['warning__adc_retry_count'] += 1
			print('adc data failed... retry. : {}'.format(TEST_CONF['warning__adc_retry_count']))
			ret = capture__adc_samples(TEST_CONF)
			if TEST_CONF['warning__adc_retry_count'] == 100:
				print('no more retry. : {}'.format(TEST_CONF['warning__adc_retry_count']))
				break
		#
		#
		turn_off_test_signal(TEST_CONF)
		#
		## show and save figure
		fig_filename = display__adc_samples(TEST_CONF)
		print(fig_filename)
		#
		## save csv
		csv_filename = save__adc_samples_into_csv_file(TEST_CONF)
		print(csv_filename)
		#
		## clear data memory
		[xx.clear()  for xx in TEST_CONF['ADC_LIST_INT' ]]
		TEST_CONF['ADC_LIST_INT' ].clear()
		TEST_CONF.clear()
		#
		return None
	
	#######################################################################
	
	####
	# common setup
	TEST_CONF = {}
	#
	
	##TODO: (1) test case 0-0: no signal / noise level check @ 15.0Msps
	if c_case_info.TEST_CASE_0_0_EN:
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = '0-0' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'OFF' ##
		#
		DWAVE_FREQ_TARGET = 100e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET'])
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		# path disable 
		TEST_PATH_DISABLE_prev = TEST_PATH_DISABLE
		TEST_PATH_DISABLE = 1
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
		#
		TEST_PATH_DISABLE = TEST_PATH_DISABLE_prev
		#
	##
	
	##TODO: TEST_CASE N_001K
	if c_case_info.TEST_CASE__N_001K_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	14000	1	NA	14.000	131072	9.362	7	98000.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_001K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 1e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	##TODO: TEST_CASE N_002K
	if c_case_info.TEST_CASE__N_002K_210_15__EN   :  # FS_14M00 = 210e6/15 # // 210	15	7000	2	NA	14.000	131072	18.725	17	119000.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_002K_210_15' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 2e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_14M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	##TODO: TEST_CASE N_005K
	if c_case_info.TEST_CASE__N_005K_210_14__EN   :  # FS_15M00  = 210e6/14   # //  210	14	3000	5	15.000	131072	43.69066667	43	129000.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'N_005K_210_14' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 5e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_15M00  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES ##
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/DWAVE_FREQ_TARGET*10*TEST_CONF['FS_TARGET']) ## normal sample 
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ## undersample
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##
	
	
	####
	
	##TODO: TEST_CASE U_001K
	#
	
	##TODO: TEST_CASE U_002K
	#
	
	##TODO: TEST_CASE__U_005K
	#
	
	##TODO: TEST_CASE__U_010K
	if c_case_info.TEST_CASE__U_010K_210_21010__EN: # FS_9K995 = 210e6/ 21010 # // 210	2100	0.01	21010	0.004759638267	0.009995	131072	62.4152381	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010K_210_21010' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 10e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_9K995  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	##TODO: TEST_CASE__U_020K
	if c_case_info.TEST_CASE__U_020K_210_10505__EN: # FS_19K99 = 210e6/ 10505 # // 210	2100	0.02	10505	0.009519276535	0.019990	131072	62.4152381	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_020K_210_10505' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 20e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_19K99  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	##TODO: TEST_CASE__U_050K
	if c_case_info.TEST_CASE__U_050K_210_4202__EN: # FS_49K98 = 210e6/  4202 # // 210	2100	0.05	4202	0.02379819134	0.049976	131072	62.4152381	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_050K_210_4202' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 50e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_49K98  ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES_SHORT
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*5*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	##TODO: TEST_CASE__U_100K
	if c_case_info.TEST_CASE__U_100K_210_2101__EN: # FS_99K95 = 210e6/  2101 # // 210	2101	2100	0.1	0.047596	0.099952	131072	62.415	61	128100.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_100K_210_2101' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 100e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_99K95 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	##TODO: TEST_CASE__U_200K
	if c_case_info.TEST_CASE__U_200K_210_1051__EN: # FS_199K8 = 210e6/  1051 # // 210	1050	0.2	1051	0.1902949572	0.199810	131072	124.8304762	113	118650.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_200K_210_1051' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 200e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_199K8 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	##TODO: TEST_CASE__U_500K
	if c_case_info.TEST_CASE__U_500K_210_421__EN: # FS_498K8 = 210e6/   421 # // 210	420	0.5	421	1.187648456	0.498812	131072	312.0761905	311	130620.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_500K_210_421' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 500e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_498K8 ## 
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	
	##TODO: TEST_CASE__U_001M
	if c_case_info.TEST_CASE__U_001M_210_211__EN: # FS_995K3 = 210e6/   211 # // 210	210	1	211	4.739336493	0.995261	131072	624.152381	619	129990.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_001M_210_211' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET =   1e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_995K3 ##	
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	#
	
	##TODO: TEST_CASE__U_002M
	if c_case_info.TEST_CASE__U_002M_210_106__EN: # FS_1M981 = 210e6/    106 # // 210	105		2		106		18.86792453			1.981132	131072	1248.304762	1237	129885.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_002M_210_106' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 2000e3 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_1M981  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) # down-sampling
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##	
	
	##TODO: TEST_CASE__U_005M
	if c_case_info.TEST_CASE__U_005M_210_43__EN: # FS_4M884 = 210e6/     43 # // 210	42		5		43		116.2790698			4.883721	131072	3120.761905	3119	130998.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_005M_210_43' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET = 5e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		#FS_TARGET             = c_Fs_info.FS_9  ##
		FS_TARGET             = c_Fs_info.FS_4M884  ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) # down-sampling
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##		
	
	##TODO: TEST_CASE__U_010M
	if c_case_info.TEST_CASE__U_010M_210_43__EN: # FS_4M884 = 210e6/    43 # // 210	43		21		10		232.558140			4.883721	131072	6241.524	6229	130809.00
		## setup TEST_CONF ##
		TEST_CONF['warning__adc_retry_count'] = 0
		#
		TEST_CONF['CASE'           ] = 'U_010M_210_43' ##
		TEST_CONF['AMP_PWR_CTRL'   ] = 'ON' ##
		#
		DWAVE_FREQ_TARGET =  10e6 ##
		DWAVE_BASE_FREQ   = __DWAVE_BASE_FREQ
		CNT_PERIOD        = int(DWAVE_BASE_FREQ/DWAVE_FREQ_TARGET + 0.5)
		CNT_DIFF          = int(CNT_PERIOD*3/4 + 0.5)
		DWAVE_FREQ_TRUE   = DWAVE_BASE_FREQ/CNT_PERIOD
		#
		TEST_CONF['DWAVE_BASE_FREQ'  ] = DWAVE_BASE_FREQ
		TEST_CONF['DWAVE_FREQ_TARGET'] = DWAVE_FREQ_TARGET
		TEST_CONF['DWAVE_FREQ_TRUE'  ] = DWAVE_FREQ_TRUE
		TEST_CONF['CNT_PERIOD'       ] = CNT_PERIOD
		TEST_CONF['CNT_DIFF'         ] = CNT_DIFF
		#
		FS_TARGET             = c_Fs_info.FS_4M884 ##
		ADC_BASE_FREQ         = __ADC_BASE_FREQ
		ADC_CNT_SAMPLE_PERIOD = int(ADC_BASE_FREQ/FS_TARGET + 0.5)
		FS_TRUE               = int(ADC_BASE_FREQ/ADC_CNT_SAMPLE_PERIOD + 0.5)
		ADC_NUM_SAMPLES       = __ADC_NUM_SAMPLES
		#
		ADC0_INPUT_DELAY_TAP_L   = PARAM__ADC0_INPUT_DELAY_TAP_L ##
		ADC0_INPUT_DELAY_TAP_H   = PARAM__ADC0_INPUT_DELAY_TAP_H ##
		ADC1_INPUT_DELAY_TAP_L   = PARAM__ADC1_INPUT_DELAY_TAP_L ##
		ADC1_INPUT_DELAY_TAP_H   = PARAM__ADC1_INPUT_DELAY_TAP_H ##
		#
		TEST_CONF['ADC_BASE_FREQ'        ] = ADC_BASE_FREQ
		TEST_CONF['FS_TARGET'            ] = FS_TARGET
		TEST_CONF['FS_TRUE'              ] = FS_TRUE
		TEST_CONF['ADC_CNT_SAMPLE_PERIOD'] = ADC_CNT_SAMPLE_PERIOD
		TEST_CONF['ADC_NUM_SAMPLES'      ] = ADC_NUM_SAMPLES
		#
		TEST_CONF['ADC0_INPUT_DELAY_TAP_L'  ] = ADC0_INPUT_DELAY_TAP_L
		TEST_CONF['ADC0_INPUT_DELAY_TAP_H'  ] = ADC0_INPUT_DELAY_TAP_H
		TEST_CONF['ADC1_INPUT_DELAY_TAP_L'  ] = ADC1_INPUT_DELAY_TAP_L
		TEST_CONF['ADC1_INPUT_DELAY_TAP_H'  ] = ADC1_INPUT_DELAY_TAP_H
		#
		TEST_CONF['ADC_LIST_INT' ] = []
		#TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-FS_TARGET)*10*TEST_CONF['FS_TARGET']) ##
		TEST_CONF['NUM_SMP_ZOOM' ] = int(1/(DWAVE_FREQ_TARGET-2*FS_TARGET)*10*TEST_CONF['FS_TARGET']) ## for 2*Fs image
		####
		#
		if DUMP_EN==1:
			TEST_CONF['DUMP_FILE_PRE'  ] =__DUMP_PRE__ + TEST_CONF['CASE'] + '_'
		else:
			TEST_CONF['DUMP_FILE_PRE'  ] = ''
		#
		## conduct TEST_CONF
		conduct__test_conf(TEST_CONF)
	##		
	
	########
	
	
	
	###################################################
	
	########
	# TEST
	
	####
	## test counter off
	ret = cmu.cmu_test_counter('OFF')
	print(ret)
	####
	
	####
	## test counter reset
	ret = cmu.cmu_test_counter('RESET')
	print(ret)
	####
	
	
	####
	## close
	ret = cmu.ok_cmu_close()
	print(ret)
	####
	
	
#

