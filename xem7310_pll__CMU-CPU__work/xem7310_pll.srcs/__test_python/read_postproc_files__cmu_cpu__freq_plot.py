# read_postproc_files__cmu_cpu__freq_plot.py
#  
#  read report file and display 
#  read RPT_* files 
#  output LOG_* file
#
#  rev0: draft
#
#  https://www.quora.com/How-does-one-extract-arrays-from-CSV-files-with-columns-in-Python
#
#  https://www.devdungeon.com/content/working-binary-data-python
#  https://pyformat.info/
#  https://matplotlib.org/api/_as_gen/matplotlib.pyplot.psd.html#matplotlib.pyplot.psd
#    https://matplotlib.org/gallery/lines_bars_and_markers/psd_demo.html#sphx-glr-gallery-lines-bars-and-markers-psd-demo-py
#
# for import numpy: https://scipy.org/install.html
#   python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
# for import multiarray:
#   https://stackoverflow.com/questions/21324426/numpy-build-fails-with-cannot-import-multiarray
#   pip install numpy
#   pip install matplotlib
#
# https://matplotlib.org/users/tight_layout_guide.html
#
# https://pythonspot.com/tk-file-dialogs/
#
#



## load library/module
import csv
import numpy as np
import matplotlib.pyplot as plt
plt.ion() # matplotlib interactive mode 
#plt.close('all')
from sys import exit
import tkinter as tk
from tkinter import filedialog
import os
import datetime
#
import glob # for filenames

## controls

## parameters

## functions

def is_int(str):
	try:
		#tmp = int(str)
		int(str)
		return True
	except ValueError:
		return False

def is_float(str):
	try:
		#tmp = float(str)
		float(str)
		return True
	except ValueError:
		return False

def is_complex(str):
	try:
		#tmp = complex(str)
		complex(str)
		return True
	except ValueError:
		return False


## start codes beblow

## read measure from the report file 
def f_read_rpt_file(filename_rpt, MAX_ROW_COUNT=50, LAST_ROW_SIDE_EN=0):
	filename_list  = []
	Fs_list           = []
	TEST_FREQ_HZ_list = []
	SAMPLES_DFT_list  = []
	TEMP_list      = []
	mean_x_list    = []
	mean_y_list    = []
	std_x_list     = []
	std_y_list     = []
	mag_X_f_0_list = []
	mag_Y_f_0_list = []
	mag_X_f_t_list = []
	mag_Y_f_t_list = []
	mag_R_f_t_list = []
	ang_R_f_t_list = []
	with open(filename_rpt, 'r', newline='') as csvFile:
		read_data = csv.reader(csvFile, delimiter=',')
		line_count = 0
		for row in read_data:
			# count lines
			line_count += 1
			#
			# add list 
			#
			filename_list     +=[row[ 0]]
			Fs_list           +=[int(row[1])] ##
			TEMP_list         +=[float(row[ 4])]
			mean_x_list       +=[float(row[ 7])]
			mean_y_list       +=[float(row[ 8])]
			std_x_list        +=[float(row[ 9])]
			std_y_list        +=[float(row[10])]
			TEST_FREQ_HZ_list += [float(row[11])] ##
			#
			SAMPLES_DFT       = int(row[12])
			SAMPLES_DFT_list  += [SAMPLES_DFT] ##
			#
			mag_X_f_0_list    +=[np.real(complex(row[13]))/SAMPLES_DFT] # scale for 1/N*DFT
			mag_Y_f_0_list    +=[np.real(complex(row[14]))/SAMPLES_DFT] # scale for 1/N*DFT
			mag_X_f_t_list    +=[float(row[19])/SAMPLES_DFT] # scale for 1/N*DFT
			mag_Y_f_t_list    +=[float(row[20])/SAMPLES_DFT] # scale for 1/N*DFT
			mag_R_f_t_list    +=[float(row[21])]
			ang_R_f_t_list    +=[float(row[22])]
			#
			if LAST_ROW_SIDE_EN == 0 and line_count >= MAX_ROW_COUNT :
				break
			#
		#
		if LAST_ROW_SIDE_EN == 1 :
			filename_list     =  filename_list    [(-MAX_ROW_COUNT):]
			Fs_list           =  Fs_list          [(-MAX_ROW_COUNT):]
			TEMP_list         =  TEMP_list        [(-MAX_ROW_COUNT):]
			mean_x_list       =  mean_x_list      [(-MAX_ROW_COUNT):]
			mean_y_list       =  mean_y_list      [(-MAX_ROW_COUNT):]
			std_x_list        =  std_x_list       [(-MAX_ROW_COUNT):]
			std_y_list        =  std_y_list       [(-MAX_ROW_COUNT):]
			TEST_FREQ_HZ_list =  TEST_FREQ_HZ_list[(-MAX_ROW_COUNT):]
			SAMPLES_DFT_list  =  SAMPLES_DFT_list [(-MAX_ROW_COUNT):]
			mag_X_f_0_list    =  mag_X_f_0_list   [(-MAX_ROW_COUNT):]
			mag_Y_f_0_list    =  mag_Y_f_0_list   [(-MAX_ROW_COUNT):]
			mag_X_f_t_list    =  mag_X_f_t_list   [(-MAX_ROW_COUNT):]
			mag_Y_f_t_list    =  mag_Y_f_t_list   [(-MAX_ROW_COUNT):]
			mag_R_f_t_list    =  mag_R_f_t_list   [(-MAX_ROW_COUNT):]
			ang_R_f_t_list    =  ang_R_f_t_list   [(-MAX_ROW_COUNT):]
		#
	return  filename_list, Fs_list, TEST_FREQ_HZ_list, SAMPLES_DFT_list, TEMP_list, \
		mean_x_list, mean_y_list, std_x_list, std_y_list, mag_X_f_0_list, mag_Y_f_0_list, \
		mag_X_f_t_list, mag_Y_f_t_list, mag_R_f_t_list, ang_R_f_t_list


####
# ratio vs freq plot : mag-ratio(ch2/ch1) vs freq ... mean / std / cv 
print('> box freq plot')


## filename prefix 
#FILE_PREFIX = ''
#
#FILE_PREFIX = 'RPT_NP_DUMP_BD1_731__'
FILE_PREFIX = 'RPT_NP_DUMP_BD5_531__'
#
#FILE_PREFIX = 'RPT_NP_DUMP_BD1__'
#FILE_PREFIX = 'RPT_NP_DUMP_BD2__'
#FILE_PREFIX = 'RPT_NP_DUMP_BD3__'
#FILE_PREFIX = 'RPT_NP_DUMP_BD4__'
#FILE_PREFIX = 'RPT_NP_DUMP_BD5__'
#
#FILE_PREFIX = 'RPT_XP_DUMP_BD1__'
#FILE_PREFIX = 'RPT_XP_DUMP_BD2__'
#FILE_PREFIX = 'RPT_XP_DUMP_BD3__'
#FILE_PREFIX = 'RPT_XP_DUMP_BD4__'
#FILE_PREFIX = 'RPT_XP_DUMP_BD5__'

# filename postfix 
#FILE_POSTFIX = ''
#
FILE_POSTFIX =  '_*_4K__TS_20190725T151515.txt'
#
## for # DUMP_BD1234__TS_20190703T021532__NP  DUMP_BD1234__TS_20190704T032416__XP
#FILE_POSTFIX = '_*_4K__TS_20190704T161616.txt'
#FILE_POSTFIX = '_*_16K__TS_20190704T161616.txt'
#FILE_POSTFIX = '_*_32K__TS_20190704T161616.txt'
#
## for # DUMP_BD45__TS_20190705T022359__NP
#FILE_POSTFIX =  '_*_4K__TS_20190706T121212.txt'
#FILE_POSTFIX = '_*_16K__TS_20190706T121212.txt'
#FILE_POSTFIX = '_*_32K__TS_20190706T121212.txt'
#
## for # DUMP_BD45__TS_20190705Ts022525__XP
#FILE_POSTFIX =  '_*_4K__TS_20190705T131313.txt'
#FILE_POSTFIX = '_*_16K__TS_20190705T131313.txt'
#FILE_POSTFIX = '_*_32K__TS_20190705T131313.txt'
#
## for # DUMP_BD2345__TS_20190707T063325__NP
#FILE_POSTFIX =  '_*_4K__TS_20190708T090909.txt'
#FILE_POSTFIX = '_*_16K__TS_20190708T090909.txt'
#FILE_POSTFIX = '_*_32K__TS_20190708T090909.txt'
#
## for TS_FIXED = '20190710T191919' ## DUMP_BD12345__TS_20190710T052645__NP
#FILE_POSTFIX =  '_*_4K__TS_20190710T191919.txt'
#FILE_POSTFIX = '_*_16K__TS_20190710T191919.txt'
#FILE_POSTFIX = '_*_32K__TS_20190710T191919.txt'
#
## for TS_FIXED = '20190711T111111' ## DUMP_BD12345__TS_20190711T072400__NP
#FILE_POSTFIX =  '_*_4K__TS_20190711T111111.txt'
#FILE_POSTFIX = '_*_16K__TS_20190711T111111.txt'
#FILE_POSTFIX = '_*_32K__TS_20190711T111111.txt'
#
## for TS_FIXED = '20190712T161616' ## DUMP_BD12345__TS_20190712T071626__NP
#FILE_POSTFIX =  '_*_4K__TS_20190712T161616.txt'
#FILE_POSTFIX = '_*_16K__TS_20190712T161616.txt'
#FILE_POSTFIX = '_*_32K__TS_20190712T161616.txt'
#
## for TS_FIXED = '20190715T101010' ## DUMP_BD12345__TS_20190714T000348__NP
#FILE_POSTFIX =  '_*_4K__TS_20190715T101010.txt'
#FILE_POSTFIX = '_*_16K__TS_20190715T101010.txt'
#FILE_POSTFIX = '_*_32K__TS_20190715T101010.txt'
#
## for TS_FIXED = '20190719T090909' ## DUMP_BD12345__TS_20190719T085350__NP
#FILE_POSTFIX =  '_*_4K__TS_20190719T090909.txt'
#FILE_POSTFIX = '_*_16K__TS_20190719T090909.txt'
#FILE_POSTFIX = '_*_32K__TS_20190719T090909.txt'
#
## for TS_FIXED = '20190720T0121212' ## DUMP_BD12345__TS_20190720T120503__NP
#FILE_POSTFIX =  '_*_4K__TS_20190720T0121212.txt'
#FILE_POSTFIX = '_*_16K__TS_20190720T0121212.txt'
#FILE_POSTFIX = '_*_32K__TS_20190720T0121212.txt'

## file core info for freq plot
file_info__freq_plot = [
	#{'file_name': 'N_001K_210_14'    , 'test_freq':   1e3},
	{'file_name': 'N_001K_210_15'    , 'test_freq':   1e3},
	#{'file_name' : 'U_001K_210_210100' , 'test_freq': 1e3}, #
	#{'file_name': 'N_002K_210_14'    , 'test_freq':   2e3},
	{'file_name': 'N_002K_210_15'    , 'test_freq':   2e3},
	#{'file_name' : 'U_002K_210_105050' , 'test_freq': 2e3}, #
	{'file_name': 'N_005K_210_14'    , 'test_freq':   5e3},
	#{'file_name': 'N_005K_210_15'    , 'test_freq':   5e3},
	#{'file_name' : 'U_005K_210_42020'  , 'test_freq': 5e3}, #
	#{'file_name': 'U_010K_210_21007' , 'test_freq':  10e3},
	{'file_name': 'U_010K_210_21010' , 'test_freq':  10e3},
	#{'file_name': 'U_010K_210_21014' , 'test_freq':  10e3},
	#{'file_name': 'U_020K_210_10501' , 'test_freq':  20e3},
	{'file_name': 'U_020K_210_10505' , 'test_freq':  20e3},
	#{'file_name': 'U_020K_210_10507' , 'test_freq':  20e3},
	{'file_name': 'U_050K_210_4202'  , 'test_freq':  50e3},
	#{'file_name': 'U_050K_210_4203'  , 'test_freq':  50e3},
	{'file_name': 'U_100K_210_2101'  , 'test_freq': 100e3},
	#{'file_name': 'U_100K_210_2102'  , 'test_freq': 100e3},
	{'file_name': 'U_200K_210_1051'  , 'test_freq': 200e3},
	{'file_name': 'U_500K_210_421'   , 'test_freq': 500e3},
	{'file_name': 'U_001M_210_211'   , 'test_freq':   1e6},
	{'file_name': 'U_002M_210_106'   , 'test_freq':   2e6},
	{'file_name': 'U_005M_210_43'    , 'test_freq':   5e6},
	{'file_name': 'U_010M_210_43'    , 'test_freq':  10e6},
	]
#
file_info__freq_plot__old = [
	#{'file_name': 'N_001K_210_14_16K_15000'    , 'test_freq':   1e3},
	{'file_name': 'N_001K_210_15_16K_28000'    , 'test_freq':   1e3},
	#{'file_name': 'N_002K_210_14_16K_15000'    , 'test_freq':   2e3},
	{'file_name': 'N_002K_210_15_16K_21000'    , 'test_freq':   2e3},
	{'file_name': 'N_005K_210_14_16K_15000'    , 'test_freq':   5e3},
	#{'file_name': 'N_005K_210_15_16K_14000'    , 'test_freq':   5e3},
	#{'file_name': 'U_010K_210_21007_16K_15000' , 'test_freq':  10e3},
	{'file_name': 'U_010K_210_21010_16K_14700' , 'test_freq':  10e3},
	#{'file_name': 'U_010K_210_21014_16K_10500' , 'test_freq':  10e3},
	#{'file_name': 'U_020K_210_10501_16K_10500' , 'test_freq':  20e3},
	{'file_name': 'U_020K_210_10505_16K_14700' , 'test_freq':  20e3},
	#{'file_name': 'U_020K_210_10507_16K_10500' , 'test_freq':  20e3},
	{'file_name': 'U_050K_210_4202_16K_14700'  , 'test_freq':  50e3},
	#{'file_name': 'U_050K_210_4203_16K_15400'  , 'test_freq':  50e3},
	{'file_name': 'U_100K_210_2101_16K_14700'  , 'test_freq': 100e3},
	#{'file_name': 'U_100K_210_2102_16K_13650'  , 'test_freq': 100e3},
	{'file_name': 'U_200K_210_1051_16K_13650'  , 'test_freq': 200e3},
	{'file_name': 'U_500K_210_421_16K_15540'   , 'test_freq': 500e3},
	{'file_name': 'U_001M_210_211_16K_18690'   , 'test_freq':   1e6},
	{'file_name': 'U_002M_210_106_16K_15855'   , 'test_freq':   2e6},
	{'file_name': 'U_005M_210_43_16K_16338'    , 'test_freq':   5e6},
	{'file_name': 'U_010M_210_43_16K_16359'    , 'test_freq':  10e6},
	]

# filenames for freq 
file_info__freq_plot__BD1 = [
	#{'file_name': 'RPT_NP_DUMP_BD1__N_001K_210_14_16K_15000__TS_20190703T101010.txt'    , 'test_freq':   1e3},
	{'file_name': 'RPT_NP_DUMP_BD1__N_001K_210_15_16K_28000__TS_20190703T101010.txt'    , 'test_freq':   1e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__N_002K_210_14_16K_15000__TS_20190703T101010.txt'    , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD1__N_002K_210_15_16K_21000__TS_20190703T101010.txt'    , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD1__N_005K_210_14_16K_15000__TS_20190703T101010.txt'    , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__N_005K_210_15_16K_14000__TS_20190703T101010.txt'    , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_010K_210_21007_16K_15000__TS_20190703T101010.txt' , 'test_freq':  10e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_010K_210_21010_16K_14700__TS_20190703T101010.txt' , 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_010K_210_21014_16K_10500__TS_20190703T101010.txt' , 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_020K_210_10501_16K_10500__TS_20190703T101010.txt' , 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_020K_210_10505_16K_14700__TS_20190703T101010.txt' , 'test_freq':  20e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_020K_210_10507_16K_10500__TS_20190703T101010.txt' , 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_050K_210_4202_16K_14700__TS_20190703T101010.txt'  , 'test_freq':  50e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_050K_210_4203_16K_15400__TS_20190703T101010.txt'  , 'test_freq':  50e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_100K_210_2101_16K_14700__TS_20190703T101010.txt'  , 'test_freq': 100e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_100K_210_2102_16K_13650__TS_20190703T101010.txt'  , 'test_freq': 100e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_200K_210_1051_16K_13650__TS_20190703T101010.txt'  , 'test_freq': 200e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_500K_210_421_16K_15540__TS_20190703T101010.txt'   , 'test_freq': 500e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190703T101010.txt'   , 'test_freq':   1e6},
	{'file_name': 'RPT_NP_DUMP_BD1__U_002M_210_106_16K_15855__TS_20190703T101010.txt'   , 'test_freq':   2e6},
	{'file_name': 'RPT_NP_DUMP_BD1__U_005M_210_43_16K_16338__TS_20190703T101010.txt'    , 'test_freq':   5e6},
	{'file_name': 'RPT_NP_DUMP_BD1__U_010M_210_43_16K_16359__TS_20190703T101010.txt'    , 'test_freq':  10e6},
	]

file_info__freq_plot__BD1_old = [
	#{'file_name': 'RPT_NP_DUMP_BD1__N_001K_210_14_16K_15000__TS_20190629T170251.txt'    , 'test_freq':   1e3},
	{'file_name': 'RPT_NP_DUMP_BD1__N_001K_210_15_16K_28000__TS_20190629T170553.txt'    , 'test_freq':   1e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__N_002K_210_14_16K_15000__TS_20190629T170639.txt'    , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD1__N_002K_210_15_16K_21000__TS_20190629T170706.txt'    , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD1__N_005K_210_14_16K_15000__TS_20190629T170731.txt'    , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__N_005K_210_15_16K_14000__TS_20190629T170752.txt'    , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_010K_210_21007_16K_15000__TS_20190629T170902.txt' , 'test_freq':  10e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_010K_210_21010_16K_14700__TS_20190629T170824.txt' , 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_010K_210_21014_16K_10500__TS_20190629T170843.txt' , 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_020K_210_10501_16K_10500__TS_20190629T171003.txt' , 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_020K_210_10505_16K_14700__TS_20190629T170941.txt' , 'test_freq':  20e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_020K_210_10507_16K_10500__TS_20190629T170923.txt' , 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_050K_210_4202_16K_14700__TS_20190629T171048.txt'  , 'test_freq':  50e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_050K_210_4203_16K_15400__TS_20190629T171022.txt'  , 'test_freq':  50e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_100K_210_2101_16K_14700__TS_20190629T171122.txt'  , 'test_freq': 100e3},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_100K_210_2102_16K_13650__TS_20190629T171147.txt'  , 'test_freq': 100e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_200K_210_1051_16K_13650__TS_20190629T171213.txt'  , 'test_freq': 200e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_500K_210_421_16K_15540__TS_20190629T171236.txt'   , 'test_freq': 500e3},
	{'file_name': 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190629T171258.txt'   , 'test_freq':   1e6},
	{'file_name': 'RPT_NP_DUMP_BD1__U_002M_210_106_16K_15855__TS_20190629T171323.txt'   , 'test_freq':   2e6},
	{'file_name': 'RPT_NP_DUMP_BD1__U_005M_210_43_16K_16338__TS_20190629T171352.txt'    , 'test_freq':   5e6},
	#{'file_name': 'RPT_NP_DUMP_BD1__U_010M_210_41_16K_16359__TS_20190629T171426.txt'    , 'test_freq':  10e6},
	]

file_info__freq_plot__BD2 = [
	#{'file_name': 'RPT_NP_DUMP_BD2__N_001K_210_14_16K_15000__TS_20190630T075143.txt'    , 'test_freq':   1e3},
	{'file_name': 'RPT_NP_DUMP_BD2__N_001K_210_15_16K_28000__TS_20190630T075204.txt'    , 'test_freq':   1e3},
	#{'file_name': 'RPT_NP_DUMP_BD2__N_002K_210_14_16K_15000__TS_20190630T075228.txt'    , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD2__N_002K_210_15_16K_21000__TS_20190630T075249.txt'    , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD2__N_005K_210_14_16K_15000__TS_20190630T075314.txt'    , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD2__N_005K_210_15_16K_14000__TS_20190630T075333.txt'    , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD2__U_010K_210_21007_16K_15000__TS_20190630T075432.txt' , 'test_freq':  10e3},
	{'file_name': 'RPT_NP_DUMP_BD2__U_010K_210_21010_16K_14700__TS_20190630T075357.txt' , 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD2__U_010K_210_21014_16K_10500__TS_20190630T075416.txt' , 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD2__U_020K_210_10501_16K_10500__TS_20190630T075524.txt' , 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD2__U_020K_210_10505_16K_14700__TS_20190630T075506.txt' , 'test_freq':  20e3},
	#{'file_name': 'RPT_NP_DUMP_BD2__U_020K_210_10507_16K_10500__TS_20190630T075449.txt' , 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD2__U_050K_210_4202_16K_14700__TS_20190630T075601.txt'  , 'test_freq':  50e3},
	#{'file_name': 'RPT_NP_DUMP_BD2__U_050K_210_4203_16K_15400__TS_20190630T075544.txt'  , 'test_freq':  50e3},
	{'file_name': 'RPT_NP_DUMP_BD2__U_100K_210_2101_16K_14700__TS_20190630T075619.txt'  , 'test_freq': 100e3},
	#{'file_name': 'RPT_NP_DUMP_BD2__U_100K_210_2102_16K_13650__TS_20190630T075639.txt'  , 'test_freq': 100e3},
	{'file_name': 'RPT_NP_DUMP_BD2__U_200K_210_1051_16K_13650__TS_20190630T075658.txt'  , 'test_freq': 200e3},
	{'file_name': 'RPT_NP_DUMP_BD2__U_500K_210_421_16K_15540__TS_20190630T075723.txt'   , 'test_freq': 500e3},
	{'file_name': 'RPT_NP_DUMP_BD2__U_001M_210_211_16K_18690__TS_20190630T075743.txt'   , 'test_freq':   1e6},
	{'file_name': 'RPT_NP_DUMP_BD2__U_002M_210_106_16K_15855__TS_20190630T075806.txt'   , 'test_freq':   2e6},
	{'file_name': 'RPT_NP_DUMP_BD2__U_005M_210_43_16K_16338__TS_20190630T075829.txt'    , 'test_freq':   5e6},
	#{'file_name': 'RPT_NP_DUMP_BD2__U_010M_210_41_16K_16359__TS_20190630T075851.txt'    , 'test_freq':  10e6},
	]

file_info__freq_plot__BD3 = [
	#{'file_name': 'RPT_NP_DUMP_BD3__N_001K_210_14_16K_15000__TS_20190630T080134.txt'    , 'test_freq':   1e3},
	{'file_name': 'RPT_NP_DUMP_BD3__N_001K_210_15_16K_28000__TS_20190630T080201.txt'    , 'test_freq':   1e3},
	#{'file_name': 'RPT_NP_DUMP_BD3__N_002K_210_14_16K_15000__TS_20190630T080239.txt'    , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD3__N_002K_210_15_16K_21000__TS_20190630T080304.txt'    , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD3__N_005K_210_14_16K_15000__TS_20190630T080329.txt'    , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD3__N_005K_210_15_16K_14000__TS_20190630T080350.txt'    , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD3__U_010K_210_21007_16K_15000__TS_20190630T080504.txt' , 'test_freq':  10e3},
	{'file_name': 'RPT_NP_DUMP_BD3__U_010K_210_21010_16K_14700__TS_20190630T080423.txt' , 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD3__U_010K_210_21014_16K_10500__TS_20190630T080443.txt' , 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD3__U_020K_210_10501_16K_10500__TS_20190630T080555.txt' , 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD3__U_020K_210_10505_16K_14700__TS_20190630T080537.txt' , 'test_freq':  20e3},
	#{'file_name': 'RPT_NP_DUMP_BD3__U_020K_210_10507_16K_10500__TS_20190630T080522.txt' , 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD3__U_050K_210_4202_16K_14700__TS_20190630T080629.txt'  , 'test_freq':  50e3},
	#{'file_name': 'RPT_NP_DUMP_BD3__U_050K_210_4203_16K_15400__TS_20190630T080612.txt'  , 'test_freq':  50e3},
	{'file_name': 'RPT_NP_DUMP_BD3__U_100K_210_2101_16K_14700__TS_20190630T080647.txt'  , 'test_freq': 100e3},
	#{'file_name': 'RPT_NP_DUMP_BD3__U_100K_210_2102_16K_13650__TS_20190630T080705.txt'  , 'test_freq': 100e3},
	{'file_name': 'RPT_NP_DUMP_BD3__U_200K_210_1051_16K_13650__TS_20190630T080723.txt'  , 'test_freq': 200e3},
	{'file_name': 'RPT_NP_DUMP_BD3__U_500K_210_421_16K_15540__TS_20190630T080741.txt'   , 'test_freq': 500e3},
	{'file_name': 'RPT_NP_DUMP_BD3__U_001M_210_211_16K_18690__TS_20190630T080801.txt'   , 'test_freq':   1e6},
	{'file_name': 'RPT_NP_DUMP_BD3__U_002M_210_106_16K_15855__TS_20190630T080821.txt'   , 'test_freq':   2e6},
	{'file_name': 'RPT_NP_DUMP_BD3__U_005M_210_43_16K_16338__TS_20190630T080843.txt'    , 'test_freq':   5e6},
	#{'file_name': 'RPT_NP_DUMP_BD3__U_010M_210_41_16K_16359__TS_20190630T080905.txt'    , 'test_freq':  10e6},
	]

file_info__freq_plot__BD4 = [
	#{'file_name': 'RPT_NP_DUMP_BD4__N_001K_210_14_16K_15000__TS_20190701T192743.txt'   , 'test_freq':   1e3},
	{'file_name': 'RPT_NP_DUMP_BD4__N_001K_210_15_16K_28000__TS_20190701T192805.txt'   , 'test_freq':   1e3},
	#{'file_name': 'RPT_NP_DUMP_BD4__N_002K_210_14_16K_15000__TS_20190701T192824.txt'   , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD4__N_002K_210_15_16K_21000__TS_20190701T192838.txt'   , 'test_freq':   2e3},
	{'file_name': 'RPT_NP_DUMP_BD4__N_005K_210_14_16K_15000__TS_20190701T192857.txt'   , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD4__N_005K_210_15_16K_14000__TS_20190701T192911.txt'   , 'test_freq':   5e3},
	#{'file_name': 'RPT_NP_DUMP_BD4__U_010K_210_21007_16K_15000__TS_20190701T192947.txt', 'test_freq':  10e3},
	{'file_name': 'RPT_NP_DUMP_BD4__U_010K_210_21010_16K_14700__TS_20190701T192926.txt', 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD4__U_010K_210_21014_16K_10500__TS_20190701T192937.txt', 'test_freq':  10e3},
	#{'file_name': 'RPT_NP_DUMP_BD4__U_020K_210_10501_16K_10500__TS_20190701T193020.txt', 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD4__U_020K_210_10505_16K_14700__TS_20190701T193009.txt', 'test_freq':  20e3},
	#{'file_name': 'RPT_NP_DUMP_BD4__U_020K_210_10507_16K_10500__TS_20190701T192959.txt', 'test_freq':  20e3},
	{'file_name': 'RPT_NP_DUMP_BD4__U_050K_210_4202_16K_14700__TS_20190701T193128.txt' , 'test_freq':  50e3},
	#{'file_name': 'RPT_NP_DUMP_BD4__U_050K_210_4203_16K_15400__TS_20190701T193118.txt' , 'test_freq':  50e3},
	{'file_name': 'RPT_NP_DUMP_BD4__U_100K_210_2101_16K_14700__TS_20190701T193139.txt' , 'test_freq': 100e3},
	#{'file_name': 'RPT_NP_DUMP_BD4__U_100K_210_2102_16K_13650__TS_20190701T193150.txt' , 'test_freq': 100e3},
	{'file_name': 'RPT_NP_DUMP_BD4__U_200K_210_1051_16K_13650__TS_20190701T193201.txt' , 'test_freq': 200e3},
	{'file_name': 'RPT_NP_DUMP_BD4__U_500K_210_421_16K_15540__TS_20190701T193212.txt'  , 'test_freq': 500e3},
	{'file_name': 'RPT_NP_DUMP_BD4__U_001M_210_211_16K_18690__TS_20190701T193223.txt'  , 'test_freq':   1e6},
	{'file_name': 'RPT_NP_DUMP_BD4__U_002M_210_106_16K_15855__TS_20190701T193234.txt'  , 'test_freq':   2e6},
	{'file_name': 'RPT_NP_DUMP_BD4__U_005M_210_43_16K_16338__TS_20190701T193245.txt'   , 'test_freq':   5e6},
	#{'file_name': 'RPT_NP_DUMP_BD4__U_010M_210_41_16K_16359__TS_20190701T193256.txt'   , 'test_freq':  10e6},
	]

file_info__freq_plot__BD5 = [
	#{'file_name': 'RPT_NP_DUMP_BD5__N_001K_210_14_16K_15000__TS_20190701T193340.txt'   , 'test_freq':   1e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__N_001K_210_15_16K_28000__TS_20190701T193353.txt'   , 'test_freq':   1e3} ,
	#{'file_name': 'RPT_NP_DUMP_BD5__N_002K_210_14_16K_15000__TS_20190701T193408.txt'   , 'test_freq':   2e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__N_002K_210_15_16K_21000__TS_20190701T193419.txt'   , 'test_freq':   2e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__N_005K_210_14_16K_15000__TS_20190701T193431.txt'   , 'test_freq':   5e3} ,
	#{'file_name': 'RPT_NP_DUMP_BD5__N_005K_210_15_16K_14000__TS_20190701T193441.txt'   , 'test_freq':   5e3} ,
	#{'file_name': 'RPT_NP_DUMP_BD5__U_010K_210_21007_16K_15000__TS_20190701T193517.txt', 'test_freq':  10e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__U_010K_210_21010_16K_14700__TS_20190701T193456.txt', 'test_freq':  10e3} ,
	#{'file_name': 'RPT_NP_DUMP_BD5__U_010K_210_21014_16K_10500__TS_20190701T193506.txt', 'test_freq':  10e3} ,
	#{'file_name': 'RPT_NP_DUMP_BD5__U_020K_210_10501_16K_10500__TS_20190701T193550.txt', 'test_freq':  20e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__U_020K_210_10505_16K_14700__TS_20190701T193539.txt', 'test_freq':  20e3} ,
	#{'file_name': 'RPT_NP_DUMP_BD5__U_020K_210_10507_16K_10500__TS_20190701T193530.txt', 'test_freq':  20e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__U_050K_210_4202_16K_14700__TS_20190701T193617.txt' , 'test_freq':  50e3} ,
	#{'file_name': 'RPT_NP_DUMP_BD5__U_050K_210_4203_16K_15400__TS_20190701T193601.txt' , 'test_freq':  50e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__U_100K_210_2101_16K_14700__TS_20190701T193628.txt' , 'test_freq': 100e3} ,
	#{'file_name': 'RPT_NP_DUMP_BD5__U_100K_210_2102_16K_13650__TS_20190701T193638.txt' , 'test_freq': 100e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__U_200K_210_1051_16K_13650__TS_20190701T193649.txt' , 'test_freq': 200e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__U_500K_210_421_16K_15540__TS_20190701T193701.txt'  , 'test_freq': 500e3} ,
	{'file_name': 'RPT_NP_DUMP_BD5__U_001M_210_211_16K_18690__TS_20190701T193710.txt'  , 'test_freq':   1e6} ,
	{'file_name': 'RPT_NP_DUMP_BD5__U_002M_210_106_16K_15855__TS_20190701T193721.txt'  , 'test_freq':   2e6} ,
	{'file_name': 'RPT_NP_DUMP_BD5__U_005M_210_43_16K_16338__TS_20190701T193732.txt'   , 'test_freq':   5e6} ,
	#{'file_name': 'RPT_NP_DUMP_BD5__U_010M_210_41_16K_16359__TS_20190701T193745.txt'   , 'test_freq':  10e6} , 
	]

#
#file_info__freq_plot = file_info__freq_plot__BD1
#
print(len(file_info__freq_plot))
#print(file_info__freq_plot)
print(file_info__freq_plot[0]['file_name'])

# read all lists from files
cnt_files = 0
len_files = len(file_info__freq_plot)
ret_list = []
#
#MAX_ROW_COUNT    =200
MAX_ROW_COUNT    =150 # remove first 50 count
LAST_ROW_SIDE_EN =1   # remove first 50 count
#
file_info__freq_plot__non_empty = []
#
for ff_dict in file_info__freq_plot :
	cnt_files += 1
	#
	filenames_to_read = FILE_PREFIX+ff_dict['file_name']+FILE_POSTFIX ###
	print('>> file {:2}/{:2}: {}'.format(cnt_files,len_files,filenames_to_read))
	filenames_to_read = glob.glob(filenames_to_read)
	print('>>> {} = {}'.format('filenames_to_read',filenames_to_read))
	if not filenames_to_read:
		print('>>> File is missing.')
		continue
	#
	file_info__freq_plot__non_empty += [ff_dict]
	#
	filename_to_read  = filenames_to_read[0] ## choose the first
	print('>>> {} = {}'.format('filename_to_read',filename_to_read))
	#
	ret = f_read_rpt_file(filename_to_read, MAX_ROW_COUNT, LAST_ROW_SIDE_EN)
	#
	#filename_list     = ret[0]
	#Fs_list           = ret[1]
	#TEST_FREQ_HZ_list = ret[2]
	#SAMPLES_DFT_list  = ret[3]
	#TEMP_list         = ret[4]
	#mean_x_list       = ret[5]
	#mean_y_list       = ret[6]
	#std_x_list        = ret[7]
	#std_y_list        = ret[8]
	#mag_X_f_0_list    = ret[9]
	#mag_Y_f_0_list    = ret[10]
	#mag_X_f_t_list    = ret[11]
	#mag_Y_f_t_list    = ret[12]
	#mag_R_f_t_list    = ret[13]
	#ang_R_f_t_list    = ret[14]
	#
	ret_list += [ret]

# test 
print(len(ret_list))
print(len(ret_list[0]))    # ret
print(len(ret_list[0][0])) # ret[0]
print(len(ret_list[0][1])) # ret[1]


## display control 
#
FIG_11_EN = 0
FIG_12_EN = 0
FIG_13_EN = 1
#
plt.ion() # matplotlib interactive mode 

# plot mean elements 
#
num_freq             = len(ret_list)
xlist_trials         = list(range(num_freq))
#
#ylist_last_mag_R_f_t = [ret_list[ii][13][-1]  for ii in xlist_trials]
#ylist_last_ang_R_f_t = [ret_list[ii][14][-1]  for ii in xlist_trials]
#
ylist_mean_mag_R_f_t = [np.mean(ret_list[ii][13])  for ii in xlist_trials]
ylist_mean_ang_R_f_t = [np.mean(ret_list[ii][14])  for ii in xlist_trials]
#
print(ylist_mean_mag_R_f_t)
print(ylist_mean_ang_R_f_t)
#
#xlist_test_freq      = [ff_dict['test_freq'] for ff_dict in file_info__freq_plot]
print(file_info__freq_plot__non_empty)
xlist_test_freq      = [ff_dict['test_freq'] for ff_dict in file_info__freq_plot__non_empty]
#
test_freq_labels     = ['{:3.0f}'.format(tt/1e3) for tt in xlist_test_freq]
#
info_files = FILE_PREFIX +FILE_POSTFIX
#
if FIG_11_EN:
	#FIG_NUM = 11 
	FIG_NUM = None # for new figure windows
	fig = plt.figure(FIG_NUM,figsize=(15,10))
	#
	plt.suptitle('plot of mean data: '+info_files)
	#
	plt.subplot(221) ### 
	#
	plt.plot(xlist_trials,ylist_mean_mag_R_f_t, 'r-')
	plt.xticks(np.arange(num_freq), test_freq_labels)
	plt.ylabel('mag of ratio')
	plt.xlabel('test freq [kHz]')
	#
	plt.subplot(222) ### 
	#
	plt.plot(xlist_test_freq,ylist_mean_mag_R_f_t, 'r-')
	plt.ylabel('mag of ratio')
	plt.xlabel('test freq[Hz]')
	plt.xscale('log')
	#
	plt.subplot(223) ### 
	#
	plt.plot(xlist_trials,ylist_mean_ang_R_f_t, 'r-')
	plt.xticks(np.arange(num_freq), test_freq_labels)
	plt.ylabel('angle of ratio [degree]')
	plt.xlabel('test freq [kHz]')
	#
	plt.subplot(224) ### 
	#
	plt.plot(xlist_test_freq,ylist_mean_ang_R_f_t, 'r-')
	plt.ylabel('angle of ratio [degree]')
	plt.xlabel('test freq[Hz]')
	plt.xscale('log')
	#

# plot boxplot
#
#num_freq             = len(ret_list)
#xlist_trials         = list(range(num_freq))
#
ylist_all_mag_R_f_t = [ret_list[ii][13]  for ii in xlist_trials]
ylist_all_ang_R_f_t = [ret_list[ii][14]  for ii in xlist_trials]
#
if FIG_12_EN:
	#FIG_NUM = 12
	FIG_NUM = None # for new figure windows
	fig = plt.figure(FIG_NUM,figsize=(15,10))
	#
	plt.suptitle('Box plot :'+info_files)
	#
	plt.subplot(221) ### 
	#
	plt.boxplot(ylist_all_mag_R_f_t[0:6], 'r-', labels=test_freq_labels[0:6])
	plt.ylabel('mag of ratio')
	plt.xlabel('test freq [kHz]')
	#
	plt.subplot(222) ### 
	#
	plt.boxplot(ylist_all_mag_R_f_t[6:], 'r-', labels=test_freq_labels[6:])
	plt.ylabel('mag of ratio')
	plt.xlabel('test freq [kHz]')
	#
	plt.subplot(223) ### 
	#
	plt.boxplot(ylist_all_ang_R_f_t[0:6], 'r-', labels=test_freq_labels[0:6])
	plt.ylabel('angle of ratio [degree]')
	plt.xlabel('test freq [kHz]')
	#
	plt.subplot(224) ### 
	#
	plt.boxplot(ylist_all_ang_R_f_t[6:], 'r-', labels=test_freq_labels[6:])
	plt.ylabel('angle of ratio [degree]')
	plt.xlabel('test freq [kHz]')
	#

# plot boxplot + mean plot 
if FIG_13_EN:
	#FIG_NUM = 11 
	FIG_NUM = None # for new figure windows
	fig = plt.figure(FIG_NUM,figsize=(15,10))
	#
	plt.suptitle('boxplot and mean data vs freq: '+info_files)
	#
	plt.subplot(221) ### 
	#
	plt.boxplot(ylist_all_mag_R_f_t, 'r-', labels=test_freq_labels)
	plt.ylabel('mag of ratio')
	plt.xlabel('test freq [kHz]')
	#
	plt.subplot(222) ### 
	#
	plt.plot(xlist_test_freq,ylist_mean_mag_R_f_t, 'r-')
	plt.ylabel('mag of ratio')
	plt.xlabel('test freq[Hz]')
	plt.xscale('log')
	#
	plt.subplot(223) ### 
	#
	plt.boxplot(ylist_all_ang_R_f_t, 'r-', labels=test_freq_labels)
	plt.ylabel('angle of ratio [degree]')
	plt.xlabel('test freq [kHz]')
	#
	plt.subplot(224) ### 
	#
	plt.plot(xlist_test_freq,ylist_mean_ang_R_f_t, 'r-')
	plt.ylabel('angle of ratio [degree]')
	plt.xlabel('test freq[Hz]')
	plt.xscale('log')
	#
	# save figure 
	filename_fig = 'LOG_'+FILE_PREFIX+FILE_POSTFIX[2:-3]+'png' ###
	plt.savefig(filename_fig)

	
## calculate statistics

## print out to log file 
def f_write_log_file(filename_log, file_info__freq_plot, ret_list):
	newFile = open(filename_log, "wt")
	#
	num_freq             = len(ret_list)
	xlist_trials         = list(range(num_freq))
	xlist_test_freq      = [ff_dict['test_freq'] for ff_dict in file_info__freq_plot]
	test_freq_labels     = ['{:3.0f}'.format(tt/1e3) for tt in xlist_test_freq]
	#
	# mean 
	ylist_mean_mag_R_f_t = [np.mean(ret_list[ii][13])  for ii in xlist_trials]
	ylist_mean_ang_R_f_t = [np.mean(ret_list[ii][14])  for ii in xlist_trials]
	# std 
	ylist_std_mag_R_f_t = [np.std(ret_list[ii][13])  for ii in xlist_trials]
	ylist_std_ang_R_f_t = [np.std(ret_list[ii][14])  for ii in xlist_trials]
	# cv 
	ylist_cv_mag_R_f_t = [ylist_std_mag_R_f_t[ii]/abs(ylist_mean_mag_R_f_t[ii])  for ii in xlist_trials]
	ylist_cv_ang_R_f_t = [ylist_std_ang_R_f_t[ii]/abs(ylist_mean_ang_R_f_t[ii])  for ii in xlist_trials]
	#
	print(xlist_test_freq)
	print(test_freq_labels)
	#
	print(ylist_mean_mag_R_f_t)
	print(ylist_mean_ang_R_f_t)
	print(ylist_std_mag_R_f_t )
	print(ylist_std_ang_R_f_t )
	print(ylist_cv_mag_R_f_t  )
	print(ylist_cv_ang_R_f_t  )
	#
	newFile.write('{} = {}\n'.format('filename_log        ', filename_log ))
	#
	newFile.write('{} = {}\n'.format('xlist_test_freq     ', xlist_test_freq ))
	newFile.write('{} = {}\n'.format('test_freq_labels    ', test_freq_labels))
	#
	newFile.write('{} = {}\n'.format('ylist_mean_mag_R_f_t', ylist_mean_mag_R_f_t))
	newFile.write('{} = {}\n'.format('ylist_mean_ang_R_f_t', ylist_mean_ang_R_f_t))
	newFile.write('{} = {}\n'.format('ylist_std_mag_R_f_t ', ylist_std_mag_R_f_t ))
	newFile.write('{} = {}\n'.format('ylist_std_ang_R_f_t ', ylist_std_ang_R_f_t ))
	newFile.write('{} = {}\n'.format('ylist_cv_mag_R_f_t  ', ylist_cv_mag_R_f_t  ))
	newFile.write('{} = {}\n'.format('ylist_cv_ang_R_f_t  ', ylist_cv_ang_R_f_t  ))
	#
	newFile.write('----\n')
	#
	newFile.write('{:22}, {:22}, {:22}, {:22}, {:22}, {:22}, {:22}\n'.format(
			'test freq[kHz]', 
			'mean_mag_R_f_t',
			'std_mag_R_f_t ',
			'cv_mag_R_f_t[ppm]',
			'mean_ang_R_f_t',
			'std_ang_R_f_t ',
			'cv_ang_R_f_t[ppm]'
			))
	for ii in xlist_trials:
		newFile.write('{:22}, {:22}, {:22}, {:22}, {:22}, {:22}, {:22}\n'.format(
			float(test_freq_labels[ii]), 
			ylist_mean_mag_R_f_t  [ii] ,
			ylist_std_mag_R_f_t   [ii] ,
			ylist_cv_mag_R_f_t    [ii]*1e6,
			ylist_mean_ang_R_f_t  [ii] ,
			ylist_std_ang_R_f_t   [ii] ,
			ylist_cv_ang_R_f_t    [ii]*1e6
			))
	#
	newFile.close()

# log filename
filename_log = 'LOG_'+FILE_PREFIX+FILE_POSTFIX[2:] ###
# call 
f_write_log_file(filename_log, file_info__freq_plot, ret_list)
	
## plot stat


# done 

