## ok module for python 3.5
## works both with x64 and win32
## 
##   Windows DLL Usage
##   http://www.opalkelly.com:8090/display/FPSDK/Programming+Languages
##   http://www.microsoft.com/en-us/download/details.aspx?id=40784
##
## https://www.opalkelly.com/examples/home/
## https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel.html
## http://www.opalkelly.com:8090/display/FPSDK/Getting+Started
## wxPython 2.8 http://www.wxpython.org
#
## ok_cmu_cpu__05_dac.py : test code for DAC control
#  

####
## controls
#  
#  
FPGA_CONFIGURE = 0
#
BIT_FILENAME = ''; 
#
####

####
## library call
import ok_cmu_cpu__lib as cmu
#
# check configuration : OK_EP_ADRS_CONFIG
#  
EP_ADRS = cmu.conf.OK_EP_ADRS_CONFIG
#  
def form_contents(var,idx):
	return '{} = {}'.format(idx, var[idx])
#
def form_address(var,idx):
	return '{} @ {:#04x}'.format(idx, var[idx])
#  
print(form_contents(EP_ADRS,'board_name'))
print(form_contents(EP_ADRS,'ver'))
print(form_contents(EP_ADRS,'bit_filename'))
#
print(form_address(EP_ADRS,'FPGA_IMAGE_ID'))
#
####

####
## init : dev
dev = cmu.ok_cmu_init()
print(dev)
####

####
ret = cmu.ok_cmu_caller_id()
print(ret)
####

####
## open
ret = cmu.ok_cmu_open()
print(ret)
####

####
## FPGA_CONFIGURE
if FPGA_CONFIGURE==1: 
	ret = cmu.ok_cmu_conf(BIT_FILENAME)
	print(ret)
####  

####
## read fpga_image_id
fpga_image_id = cmu.cmu_read_fpga_image_id()
#
def form_hex_32b(val):
	return '0x{:08X}'.format(val)
#
print(form_hex_32b(fpga_image_id))
####

####
## read FPGA internal temp and volt
ret = cmu.cmu_monitor_fpga()
print(ret)
####

####
## test counter on 
ret = cmu.cmu_test_counter('ON')
print(ret)
####

########

####
ret = cmu.cmu_spo_enable()
print(ret)
####

####
ret = cmu.cmu_spo_init()
print(ret)
####

####
ret = cmu.cmu_spo_bit__leds(0xFF)
print(ret)
####

####
ret = cmu.cmu_spo_bit__amp_pwr('ON')
print(ret)
####

####
input('> See AMP power on. Enter...')
####

########

####
ret = cmu.cmu_dac_bias_enable()
print(ret)
####

####
ret = cmu.cmu_dac_bias_init()
print(ret)
####


####
DAC1_CODE = 0x1000
DAC2_CODE = 0x1000
DAC3_CODE = 0x4000
DAC4_CODE = 0x4000
ret = cmu.cmu_dac_bias_set_buffer(
		DAC1=DAC1_CODE,
		DAC2=DAC2_CODE,
		DAC3=DAC3_CODE,
		DAC4=DAC4_CODE)
print(ret)
####


####
ret = cmu.cmu_dac_bias_update()
print(ret)
####

####
ret = cmu.cmu_dac_bias_readback()
print(ret)
####

#######

####
input('> See DAC readback and voltages. Enter...')
####


####
# work above!
####


####
cmu.sleep(3)
####

####
ret = cmu.cmu_dac_bias_set_buffer() # set default values
print(ret)
####

####
ret = cmu.cmu_dac_bias_update() 
print(ret)
####

####
ret = cmu.cmu_dac_bias_disable() 
print(ret)
####


####
ret = cmu.cmu_spo_bit__amp_pwr('OFF')
print(ret)
####


####
ret = cmu.cmu_spo_bit__leds(0x00)
print(ret)
####


####
ret = cmu.cmu_spo_disable()
print(ret)
####

####
## test counter off
ret = cmu.cmu_test_counter('OFF')
print(ret)
####

####
## test counter reset
ret = cmu.cmu_test_counter('RESET')
print(ret)
####


####
## close
ret = cmu.ok_cmu_close()
print(ret)
####