import os

## replace AA with BB 

#AA = '_'
#BB = '-'

AA = 'U_100K_210_2102_'
BB = 'U_100K_210_2114_'

# list
#[print(f) for f in os.listdir('.') if not f.startswith('.')]
#[print(f) for f in os.listdir('.') if f.startswith('DUMP_BD1__'+AA)]
[print(f) for f in os.listdir('.') if f.startswith('DUMP_BD1__'+BB)]

# replace 
#[os.rename(f, f.replace(AA, BB)) for f in os.listdir('.') if not f.startswith('.')]
#[os.rename(f, f.replace(AA, BB)) for f in os.listdir('.') if f.startswith('DUMP_BD1__'+AA)]