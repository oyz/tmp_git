#include <stdio.h>
#include <string.h>
//#include "xil_printf.h"

//$$ support CMU-CPU
#include "../CMU_CPU_TOP/platform.h" // for init_platform()
#include "../CMU_CPU_TOP/cmu_cpu_config.h" // CMU-CPU board config
#include "../CMU_CPU_TOP/cmu_cpu.h" // for hw_reset

//$$ support W5500
#include "../CMU_CPU_TOP/ioLibrary/Ethernet/W5500/w5500.h" // for w5500 io functions
#include "../CMU_CPU_TOP/ioLibrary/Ethernet/socket.h"	// Just include one header for WIZCHIP

//$$ loopback socket
#include "../CMU_CPU_TOP/ioLibrary/Appmod/Loopback/loopback.h"


/////////////////////////////////////////
// SOCKET NUMBER DEFINION for Examples //
/////////////////////////////////////////
#define SOCK_TCPS        0
//#define SOCK_UDPS        1

////////////////////////////////////////////////
// Shared Buffer Definition for LOOPBACK TEST //
////////////////////////////////////////////////
#define DATA_BUF_SIZE   2048
uint8_t gDATABUF[DATA_BUF_SIZE];


///////////////////////////////////
// Default Network Configuration //
///////////////////////////////////
wiz_NetInfo gWIZNETINFO = { .mac = {0x00, 0x08, 0xdc,0x00, 0xab, 0xcd},
							.ip = {192, 168, 168, 123},
							.sn = {255,255,255,0},
							.gw = {192, 168, 168, 1},
							.dns = {0,0,0,0},
							.dhcp = NETINFO_STATIC };





//////////////////////////////////
// For example of ioLibrary_BSD //
//////////////////////////////////
void network_init(void);								// Initialize Network information and display it
//int32_t loopback_tcps(uint8_t, uint8_t*, uint16_t);		// Loopback TCP server
//int32_t loopback_udps(uint8_t, uint8_t*, uint16_t);		// Loopback UDP server
//////////////////////////////////


int main(void)
{
   uint8_t tmp;
   int32_t ret = 0;
   uint8_t memsize[2][8] = {{2,2,2,2,2,2,2,2},{2,2,2,2,2,2,2,2}};
   //
   //$$ for CMU-CPU
   u32 adrs;
   u32 value;
   //

   ///////////////////////////////////////////
   // Host dependent peripheral initialized //
   ///////////////////////////////////////////
   init_platform();


   //// FPGA setup
   // test for print on jtag-terminal
   print("> Hello World!! \r\n");
   print(">>> build_info: "__TIME__", " __DATE__ ".\r\n");

   // test master_spi_wz850.v
   xil_printf(">> test master_spi_wz850.v \r\n");
   //
   // ADRS_FPGA_IMAGE_ID
   xil_printf(">>> FPGA_IMAGE_ID: \r\n");
   adrs = ADRS_FPGA_IMAGE;
   value = XIomodule_In32 (adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%08X \r\n", value, adrs);
   //
   // ADRS_TEST_REG
   xil_printf(">>> TEST_REG: \r\n");
   adrs = ADRS_TEST_REG;
   value = 0xABCD1234;
   //
   XIomodule_Out32 (adrs, value);
   xil_printf(">done: wr: 0x%08X @ 0x%08X \r\n", value, adrs);
   //
   value = XIomodule_In32 (adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%08X \r\n", value, adrs);
   //

   //// hw reset wz850
   xil_printf(">>> hw reset wz850 \r\n");
   hw_reset__wz850();


   //// socket setup

	/* WIZCHIP SOCKET Buffer initialize */
	if(ctlwizchip(CW_INIT_WIZCHIP,(void*)memsize) == -1)
	{
	   printf("WIZCHIP Initialized fail. \n");
	   while(1);
	}

	/* PHY link status check */
	do
	{
	   if(ctlwizchip(CW_GET_PHYLINK, (void*)&tmp) == -1)
		  printf("Unknown PHY Link stauts. \n");
	}while(tmp == PHY_LINK_OFF);


	/* Network initialization */
	network_init();


	/*******************************/
	/* WIZnet W5500 Code Examples  */
	/* TCPS/UDPS Loopback test     */
	/*******************************/
	/* Main loop */
	while(1)
	{
		/* Loopback Test */
		// TCP server loopback test
		if( (ret = loopback_tcps(SOCK_TCPS, gDATABUF, 5025)) < 0) {
			printf("SOCKET ERROR : %ld \n", ret);
		}

		// UDP server loopback test
		//if( (ret = loopback_udps(SOCK_UDPS, gDATABUF, 3000)) < 0) {
		//	printf("SOCKET ERROR : %ld \n", ret);
		//}

	} // end of Main loop

    /////
    cleanup_platform();

} // end of main()

/////////////////////////////////////////////////////////////
// Intialize the network information to be used in WIZCHIP //
/////////////////////////////////////////////////////////////
void network_init(void)
{
   uint8_t tmpstr[6];
	ctlnetwork(CN_SET_NETINFO, (void*)&gWIZNETINFO);
	ctlnetwork(CN_GET_NETINFO, (void*)&gWIZNETINFO);

	// Display Network Information
	ctlwizchip(CW_GET_ID,(void*)tmpstr);
	printf(" \n=== %s NET CONF === \n",(char*)tmpstr);
	printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X \n",gWIZNETINFO.mac[0],gWIZNETINFO.mac[1],gWIZNETINFO.mac[2],
		  gWIZNETINFO.mac[3],gWIZNETINFO.mac[4],gWIZNETINFO.mac[5]);
	printf("SIP: %d.%d.%d.%d \n", gWIZNETINFO.ip[0],gWIZNETINFO.ip[1],gWIZNETINFO.ip[2],gWIZNETINFO.ip[3]);
	printf("GAR: %d.%d.%d.%d \n", gWIZNETINFO.gw[0],gWIZNETINFO.gw[1],gWIZNETINFO.gw[2],gWIZNETINFO.gw[3]);
	printf("SUB: %d.%d.%d.%d \n", gWIZNETINFO.sn[0],gWIZNETINFO.sn[1],gWIZNETINFO.sn[2],gWIZNETINFO.sn[3]);
	printf("DNS: %d.%d.%d.%d \n", gWIZNETINFO.dns[0],gWIZNETINFO.dns[1],gWIZNETINFO.dns[2],gWIZNETINFO.dns[3]);
	printf("====================== \n");
}
/////////////////////////////////////////////////////////////
