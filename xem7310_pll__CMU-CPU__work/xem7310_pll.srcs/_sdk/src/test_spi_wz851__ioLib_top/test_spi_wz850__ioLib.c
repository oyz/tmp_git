/*
 * test_spi_wz850__ioLib.c : test wiz850io module over MB MCS IO bus based on ioLibrary
 * note: stdio --> MDM ... see system.mss in bsp
 */

// note:
//   low level IO access driver : xiomodule_l.h
//   logic-design under test : master_spi_wz850.v


#include <stdio.h>
//#include <stdlib.h>
#include "xil_printf.h"
#include "xil_types.h" // u8 s8 ...

//#include "xiomodule_l.h" // low-level driver
//#include "xstatus.h"
//#include "mb_interface.h"

#include "microblaze_sleep.h" // for usleep()

#include "../CMU_CPU_TOP/ioLibrary/Ethernet/W5500/w5500.h" // for w5500 io functions

#include "../CMU_CPU_TOP/platform.h" // for init_platform()
#include "../CMU_CPU_TOP/cmu_cpu_config.h" // CMU-CPU board config
#include "../CMU_CPU_TOP/cmu_cpu.h" // for hw_reset



int main()
{
    //
    u32 adrs;
    u32 value;
	u8 cc;
	u32 ii;
	u32 len;

    init_platform();

    // test for print on jtag-terminal
    print("Hello World!!\n\r");

    // test master_spi_wz850.v
    xil_printf(">> test master_spi_wz850.v \n\r");
    //
    // ADRS_FPGA_IMAGE_ID
    xil_printf(">>> FPGA_IMAGE_ID: \n\r");
    adrs = ADRS_FPGA_IMAGE;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    // ADRS_TEST_REG
    xil_printf(">>> TEST_REG: \n\r");
    adrs = ADRS_TEST_REG;
    value = 0xABCD1234;
    //
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //

    // hw reset for wz850
    xil_printf(">>> trig LAN RESET @  master_spi_wz850: \n\r");
    hw_reset__wz850();

    while (1) {

    //// test WIZCHIP_WRITE
    xil_printf(">>> test WIZCHIP_WRITE: \n\r");
    //
    adrs = ( (0x0040<<8)|(0x06<<3) );
    cc   = 0xCC;
    WIZCHIP_WRITE(adrs, cc);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //
    adrs = ( (0x0041<<8)|(0x06<<3) );
    cc   = 0xAA;
    WIZCHIP_WRITE(adrs, cc);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //
    adrs = ( (0x0042<<8)|(0x06<<3) );
    cc   = 0x55;
    WIZCHIP_WRITE(adrs, cc);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //
    adrs = ( (0x0043<<8)|(0x06<<3) );
    cc   = 0x33;
    WIZCHIP_WRITE(adrs, cc);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //
    adrs = ( (0x0044<<8)|(0x06<<3) );
    cc   = 0xED;
    WIZCHIP_WRITE(adrs, cc);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //

    //// test WIZCHIP_READ
    xil_printf(">>> test WIZCHIP_READ: \n\r");
    //
    adrs = ( (0x0040<<8)|(0x06<<3) );
    cc = WIZCHIP_READ(adrs);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //
    adrs = ( (0x0041<<8)|(0x06<<3) );
    cc = WIZCHIP_READ(adrs);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //
    adrs = ( (0x0042<<8)|(0x06<<3) );
    cc = WIZCHIP_READ(adrs);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //
    adrs = ( (0x0043<<8)|(0x06<<3) );
    cc = WIZCHIP_READ(adrs);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //
    adrs = ( (0x0044<<8)|(0x06<<3) );
    cc = WIZCHIP_READ(adrs);
    xil_printf ("0x%02x @ 0x%06X \n\r",cc, adrs);
    //

    //// test WIZCHIP_WRITE_BUF
    xil_printf(">>> test WIZCHIP_WRITE_BUF: \n\r");
    //
    u8 pbuf[5];
    //
    pbuf[0] = 0x20;
    pbuf[1] = 0x19;
    pbuf[2] = 0x02;
    pbuf[3] = 0x23;
    pbuf[4] = 0xCD;
    //
    adrs = ( (0x0040<<8)|(0x06<<3) );
    len = 5;
    WIZCHIP_WRITE_BUF(adrs, pbuf, len);
    for (ii=0;ii<len;ii++) {
    	xil_printf ("0x%02x @ 0x%06X \n\r",pbuf[ii], (adrs+(ii<<8)));
    }

    //// test WIZCHIP_READ_BUF
    xil_printf(">>> test WIZCHIP_READ_BUF: \n\r");
    //
    WIZCHIP_READ_BUF(adrs, pbuf, len);
    for (ii=0;ii<len;ii++) {
    	xil_printf ("0x%02x @ 0x%06X \n\r",pbuf[ii], (adrs+(ii<<8)));
    }


    //// repeat
    xil_printf (">> Enter X to finish: ");
    //
    //cc = getchar(); // waiting for enter char...
    cc = inbyte(); // input a char // without return char...
    //
    xil_printf ("%c\n\r", cc); // echo a char
    //
    if (cc=='X' || cc=='x') break;
    //
    xil_printf ("\n\r");
    }

    // test sscanf -- pass with stack 2KB and heap 32KB.
    int dd;
    int xx;
    sscanf("123, ABC","%d,%x",&dd,&xx);
    xil_printf("%d,%x \n\r",dd,xx);

    // test scanf -- pass with stack 2KB and heap 28KB.
    xil_printf (">> Enter any to finish: ");
    scanf ("%c",&cc);
    xil_printf ("%c\n\r", cc); // echo a char



    // process for nothing
    print(">> No more code is left! But, logics outside processor are still alive!!\n\r");

    //
    cleanup_platform();
    return 0;
}
