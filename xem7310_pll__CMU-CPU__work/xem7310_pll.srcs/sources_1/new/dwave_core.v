//------------------------------------------------------------------------
// dwave_core.v
//  for dwave logic
//  support 32 bits 
//
//  8f, 4-level design
//	    base clock 80MHz or 12.5ns
//
//		targets freq  --> count 
//		10kHz 100us   --> 8000
//		20kHz 50us    --> 4000
//		40kHz 25us    --> 2000
//		100kHz 10us   --> 800
//		200kHz 5us    --> 400
//		400kHz 2.5us  --> 200
//		1MHz  1us     --> 80
//		2MHz  500ns   --> 40
//		4MHz  250ns   --> 20
//		10MHz 100ns   --> 8
//		note that 16bit counter is enough.
//		
//  - pulse generation info
//  	>> 4-level scheme .
//  	pulse_en:____-----------------------------------------------------------------_____
//  	8f:      _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
//  	index:        1 2 3 4 5 6 7 8
//		...
//		in-phase control 
//  	f_1:     ---------------____------------____------------____------------____------- [pulse_out_f1]
//  	f_2:     -------------________--------________--------________--------________----- [pulse_out_f1_ref]
//  	f_3:     -----__----____________----____________----____________----__________----- [pulse_out_f2]
//  	L_0:     -------____------------____------------____------------____--------------- 
//  	L_1:     -----__----__--------__----__--------__----__--------__----__------------- 
//  	L_2:     -------------__----__--------__----__--------__----__--------__----__----- 
//  	L_3:     ---------------____------------____------------____------------____------- 
//  	...
//		quadrature-phase control
//  	f_4:     -----__------------____------------____------------____------------__----- [pulse_out_f3]
//  	f_5:     -----____--------________--------________--------________--------____----- [pulse_out_f3_ref]
//  	f_6:     -----______----____________----____________----____________----______----- [pulse_out_f4]
//  	L_0:     -----------____------------____------------____------------____----------- 
//  	L_1:     ---------__----__--------__----__--------__----__--------__----__--------- 
//  	L_2:     -------__--------__----__--------__----__--------__----__--------__------- 
//  	L_3:     -----__------------____------------____------------____------------__----- 
//
//	10MHz ex
//		>>>f1_cnt_period = 8
//		>>>f1_pulse_index = 1 // 1 2 ~ 8 // start polocation
//		f1_on =   2 3
//		>>>f1_on_trig = 1
//		>>>f1_off_trig = 3
//		f1_ref
//		f1_bar
//		>>>f2_cnt_period = 8
//		>>>f2_pulse_index = 3 // 1 2 ~ 8 // start polocation
//		f2_on = 1 2 3 4
//		>>>f2_on_trig = 1
//		>>>f2_off_trig = 3
//		others: pulse_en
//
//  - IO
//		clk // 10MHz for reg control 
//		reset_n
//		en
//		clk_dwave // 80MHz for generating waveform. 
//		init 
//		update 
//		test 
//		pulse_period // for future
//		pulse_start_loc // for future
//		pulse_on_trig_loc // for future
//		pulse_off_trig_loc // for future
//		test_datain
//		test_datain_type
//		test_datain_port_sel
//		pulse_out_f#_ref (duty 50%)
//		pulse_out_f#_ref_n (duty 50%)
//		pulse_out_f# (duty adjustable)
//		pulse_out_f#_n
//					#:1~4
//
//
//  - reg
//		r_cnt_pulse#[15:0]
//		r_loc_start#[15:0]
//		r_loc_trig_on#[15:0]
//		r_loc_trig_off#[15:0]
//		r_subpulse_idx#[15:0]
//		r_pulse_en# // enable for r_subpulse_idx# // trig by update
//
//  - timing 
//  	base clock 80MHz or 12.5ns 
//  	...
//		r_pulse_en#			____-----------------------------------------------------------------_____
//		r_cnt_pulse#		88888888888888888888888888888888888888888888888888888888888888888888888888
//		r_loc_start#		33333333333333333333333333333333333333333333333333333333333333333333333333
//		r_loc_trig_on#		11111111111111111111111111111111111111111111111111111111111111111111111111
//		r_loc_trig_off#		33333333333333333333333333333333333333333333333333333333333333333333333333
//  	r_subpulse_idx#		00003456781234567812345678123456781234567812345678123456781234567812300000
//		pulse_out_f#_ref	_______----____----____----____----____----____----____----____----_______
//		pulse_out_f#_ref_n	___________----____----____----____----____----____----____----____--_____
//		pulse_out_f#		___________--______--______--______--______--______--______--______--_____
//		pulse_out_f#_n		_____------__------__------__------__------__------__------__------__-____
//		
//

	
`timescale 1ns / 1ps
module dwave_core (  
	//
	input wire reset_n,
    input wire en,
	//
	input wire clk_dwave, // 80MHz for generating waveform
    // control
	input wire i_pulse_en,
	//
	input wire [31:0] i_cnt_pulse   ,//$$
	input wire [31:0] i_loc_start   ,//$$
	input wire [31:0] i_loc_trig_on ,//$$
	input wire [31:0] i_loc_trig_off,//$$
    // IO
	output wire o_pulse_out_f_ref,
	output wire o_pulse_out_f_ref_n,
	output wire o_pulse_out_f,
	output wire o_pulse_out_f_n,
	// flag
    output wire [7:0] debug_out
);
//
(* keep = "true" *) wire w_pulse_en     = i_pulse_en    ; 
//
wire [31:0] w_cnt_pulse    = i_cnt_pulse   ;//$$
wire [31:0] w_loc_start    = i_loc_start   ;//$$
wire [31:0] w_loc_trig_on  = i_loc_trig_on ;//$$
wire [31:0] w_loc_trig_off = i_loc_trig_off;//$$
//
(* keep = "true" *) reg [31:0] r_subpulse_idx;//$$

//
assign debug_out = 8'b0;

// subpulse index generation 
always @(posedge clk_dwave, negedge reset_n)
	if (!reset_n) begin
        r_subpulse_idx <= 32'd0;//$$
	end
    else if (en) begin
		if (w_pulse_en) begin
			r_subpulse_idx <= (r_subpulse_idx >= w_cnt_pulse)? 32'd01 : r_subpulse_idx + 32'd01;//$$
			end
		else begin
			r_subpulse_idx <= w_loc_start;
			end
		end 
    else begin
        r_subpulse_idx <= 32'd0;//$$ 0 vs w_loc_start
	end

//
(* keep = "true" *) reg r_pulse_out_f_ref;
(* keep = "true" *) reg r_pulse_out_f_ref_n;
(* keep = "true" *) reg r_pulse_out_f;
(* keep = "true" *) reg r_pulse_out_f_n;
//
assign o_pulse_out_f_ref	= r_pulse_out_f_ref;
assign o_pulse_out_f_ref_n	= r_pulse_out_f_ref_n;
assign o_pulse_out_f		= r_pulse_out_f;
assign o_pulse_out_f_n		= r_pulse_out_f_n;

// pulse generation 
wire w_pulse_out_f_trig_on 		= (r_subpulse_idx ==    w_loc_trig_on) ? 1'b1 : 1'b0;
wire w_pulse_out_f_trig_off 	= (r_subpulse_idx ==   w_loc_trig_off) ? 1'b1 : 1'b0;
wire w_pulse_out_f_ref_trig_on 	= (r_subpulse_idx ==      w_cnt_pulse) ? 1'b1 : 1'b0;
wire w_pulse_out_f_ref_trig_off = (r_subpulse_idx =={1'b0,w_cnt_pulse[31:1]}) ? 1'b1 : 1'b0; //$$
//
always @(posedge clk_dwave, negedge reset_n)
	if (!reset_n) begin
		r_pulse_out_f 			<= 1'b0;
		r_pulse_out_f_ref 		<= 1'b0;
		r_pulse_out_f_n 		<= 1'b0;
		r_pulse_out_f_ref_n		<= 1'b0;
		end
	else if (!en) begin 
		r_pulse_out_f 			<= 1'b0;
		r_pulse_out_f_ref 		<= 1'b0;
		r_pulse_out_f_n 		<= 1'b0;
		r_pulse_out_f_ref_n		<= 1'b0;
		end
	else if (w_pulse_en) begin
		r_pulse_out_f		<= (w_pulse_out_f_trig_on)? 1'b1 
							 : (w_pulse_out_f_trig_off)? 1'b0 
							 :  r_pulse_out_f;
		r_pulse_out_f_ref	<= (w_pulse_out_f_ref_trig_on)? 1'b1
							 : (w_pulse_out_f_ref_trig_off)? 1'b0 
							 :  r_pulse_out_f_ref;
		r_pulse_out_f_n		<= (w_pulse_out_f_trig_on)? 1'b0 
							 : (w_pulse_out_f_trig_off)? 1'b1
							 :  r_pulse_out_f_n;
		r_pulse_out_f_ref_n	<= (w_pulse_out_f_ref_trig_on)? 1'b0
							 : (w_pulse_out_f_ref_trig_off)? 1'b1
							 :  r_pulse_out_f_ref_n;
		end
	else begin 
		r_pulse_out_f 			<= 1'b0;
		r_pulse_out_f_ref 		<= 1'b0;
		r_pulse_out_f_n 		<= 1'b0;
		r_pulse_out_f_ref_n		<= 1'b0;
		end
//	
endmodule