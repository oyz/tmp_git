// # this        : xem7310__cmu_cpu__top.v
// # top xdc     : xem7310__cmu_cpu__top.xdc
// # board       : CMU-CPU-TEST-F5500
// # sch         : CMU_CPU_TEST_F5500-181109.pdf

// unused
//`default_nettype none

/* top module integration */
module xem7310__cmu_cpu__top ( 
	// OK interface
	input  wire [4:0]   okUH,
	output wire [2:0]   okHU,
	inout  wire [31:0]  okUHU,
	inout  wire         okAA,
	// external clock ports 
	input  wire         sys_clkp, 
	input  wire         sys_clkn,
	// external ADC ports 
	input  wire			i_XADC_VP,
	input  wire			i_XADC_VN,
	// BANK 13 34 35 signals in connectors
	// MC1 - odd
	output wire			o_B34_L24P,        //$$ ADC_XX_TEST_B 
	output wire			o_B34_L24N,        //$$ ADC_XX_DUAL_LANE_B
	input  wire			i_B34D_L17P,       //$$ ADC_01_DB_P  // ADC_01 PN swap in logic
	input  wire			i_B34D_L17N,       //$$ ADC_01_DB_N  // ADC_01 PN swap in logic
	input  wire			i_B34D_L16P,       //$$ ADC_01_DA_P  // ADC_01 PN swap in logic
	input  wire			i_B34D_L16N,       //$$ ADC_01_DA_N  // ADC_01 PN swap in logic
	input  wire			c_B34D_L14P_SRCC,  //$$ ADC_01_DCO_P // ADC_01 PN swap in logic
	input  wire			c_B34D_L14N_SRCC,  //$$ ADC_01_DCO_N // ADC_01 PN swap in logic
	output wire			o_B34_L10P,        //$$ BIAS_LOAD_B // DAC_BIAS
	output wire			o_B34_L10N,        //$$ BIAS_CLR_B  // DAC_BIAS
	output wire			o_B34_L20P,        //$$ DWAVE_F01 // inverted in logic
	output wire			o_B34_L20N,        //$$ DWAVE_F02 // inverted in logic
	output wire			o_B34_L3P,         //$$ DWAVE_F03 // inverted in logic
	output wire			o_B34_L3N,         //$$ DWAVE_F11 // inverted in logic
	output wire			o_B34_L9P,         //$$ DWAVE_F12 // inverted in logic
	output wire			o_B34_L9N,         //$$ DWAVE_F13 // inverted in logic
	output wire			o_B34_L2P,         //$$ BIAS_SYNCn  // DAC_BIAS
	output wire			o_B34_L2N,         //$$ BIAS_SCLK   // DAC_BIAS
	output wire			o_B34_L4P,         //$$ BIAS_SDI    // DAC_BIAS
	input  wire			i_B34_L4N,         //$$ BIAS_SDO    // DAC_BIAS
	input  wire			i_B34D_L1P,        //$$ ADC_10_DB_P  
	input  wire			i_B34D_L1N,        //$$ ADC_10_DB_N  
	input  wire			i_B34D_L7P,        //$$ ADC_10_DA_P  
	input  wire			i_B34D_L7N,        //$$ ADC_10_DA_N  
	input  wire			c_B34D_L12P_MRCC,  //$$ ADC_10_DCO_P 
	input  wire			c_B34D_L12N_MRCC,  //$$ ADC_10_DCO_N 
    //                                      
	//output wire			o_B13_L2P,         //$$ CC8  // MOSI     // DAC_A2A3_MOSI
	//output wire			o_B13_L2N,         //$$ CC9  // SCLK     // DAC_A2A3_SCLK
	//output wire			o_B13_L4P,         //$$ CC10 // SCSB     // DAC_A2A3_SYNB
	//inout  wire			o_B13_L4N,         //$$ CC11 // TEST-OUT // reserved output
	//input  wire			i_B13_L1P,         //$$ CC12 // MISO     // DAC_A2A3_MISO
	//
	input  wire			i_B13_L2P,         //$$ CC12 // MISO     // DAC_A2A3_MISO
	inout  wire			o_B13_L2N,         //$$ CC11 // TEST-OUT // reserved output
	output wire			o_B13_L4P,         //$$ CC10 // SCSB     // DAC_A2A3_SYNB
	output wire			o_B13_L4N,         //$$ CC9  // SCLK     // DAC_A2A3_SCLK
	output wire			o_B13_L1P,         //$$ CC8  // MOSI     // DAC_A2A3_MOSI
	//
	// MC1 - even
	output wire			o_B34D_L21P,       //$$ ADC_XX_CNV_P
	output wire			o_B34D_L21N,       //$$ ADC_XX_CNV_N
	output wire			o_B34D_L19P,       //$$ ADC_XX_CLK_P
	output wire			o_B34D_L19N,       //$$ ADC_XX_CLK_N   
	input  wire			i_B34D_L23P,       //$$ ADC_11_DB_P  // reserved
	input  wire			i_B34D_L23N,       //$$ ADC_11_DB_N  // reserved
	input  wire			i_B34D_L15P,       //$$ ADC_11_DA_P  // reserved
	input  wire			i_B34D_L15N,       //$$ ADC_11_DA_N  // reserved
	input  wire			c_B34D_L13P_MRCC,  //$$ ADC_11_DCO_P // reserved
	input  wire			c_B34D_L13N_MRCC,  //$$ ADC_11_DCO_N // reserved
	input  wire			c_B34D_L11P_SRCC,  //$$ ADC_00_DCO_P
	input  wire			c_B34D_L11N_SRCC,  //$$ ADC_00_DCO_N
	input  wire			i_B34D_L18P,       //$$ ADC_00_DA_P
	input  wire			i_B34D_L18N,       //$$ ADC_00_DA_N
	input  wire			i_B34D_L22P,       //$$ ADC_00_DB_P
	input  wire			i_B34D_L22N,       //$$ ADC_00_DB_N
	input  wire			i_B34_L6P,         //$$ CC20 // D_D
	input  wire			i_B34_L6N,         //$$ CC21 // C_D
	output wire			o_B34_L5P,         //$$ CC6  // SPO_CD3
	output wire			o_B34_L5N,         //$$ CC7  // SPO_EN3
	input  wire			i_B34_L8P,         //$$ CC22 // B_D
	input  wire			i_B34_L8N,         //$$ CC23 // A_D
	//
	inout  wire			io_B13_SYS_CLK_MC1,//$$ FPGA_IO_C
	output wire			o_B13_L5P,         //$$ CC0 // SPO_CD0
	output wire			o_B13_L5N,         //$$ CC1 // SPO_EN0
	output wire			o_B13_L3P,         //$$ CC2 // SPO_CD1
	output wire			o_B13_L3N,         //$$ CC3 // SPO_EN1
	output wire			o_B13_L16P,        //$$ CC4 // SPO_CD2
	output wire			o_B13_L16N,        //$$ CC5 // SPO_EN2
	inout  wire			io_B13_L1N,        //$$ FPGA_IO_A
	//
	// MC2 - odd
	input  wire			i_B35_L21P,        //$$ LAN_MISO
	output wire			o_B35_L21N,        //$$ LAN_RSTn
	input  wire			i_B35_L19P,        //$$ CC14 // RBCK1
	input  wire			i_B35_L19N,        //$$ CC15 // UNBAL
	inout  wire			o_B35_L18P,        //$$ DAC_D4P   // reserved output
	inout  wire			o_B35_L18N,        //$$ DAC_D4N   // reserved output
	inout  wire			o_B35_L23P,        //$$ DAC_D3P   // reserved output
	inout  wire			o_B35_L23N,        //$$ DAC_D3N   // reserved output
	inout  wire			o_B35_L15P,        //$$ DAC_D2P   // reserved output
	inout  wire			o_B35_L15N,        //$$ DAC_D2N   // reserved output
	inout  wire			o_B35_L9P,         //$$ DAC_D1P   // reserved output
	inout  wire			o_B35_L9N,         //$$ DAC_D1N   // reserved output
	inout  wire			o_B35_L7P,         //$$ DAC_D0P   // reserved output
	inout  wire			o_B35_L7N,         //$$ DAC_D0N   // reserved output
	inout  wire			o_B35_L11P_SRCC,   //$$ DAC_DCI_P // reserved output
	inout  wire			o_B35_L11N_SRCC,   //$$ DAC_DCI_N // reserved output
	inout  wire			o_B35_L4P,         //$$ DAC_D5P   // reserved output
	inout  wire			o_B35_L4N,         //$$ DAC_D5N   // reserved output
	input  wire			i_B35_L6P,         //$$ FPGA_IO_B // sensor signal in from MAX6576ZUT+T
	input  wire			i_B35_L6N,         //$$ CC13 // RBCK2
	inout  wire			o_B35_L1P,         //$$ DAC_D6P   // reserved output
	inout  wire			o_B35_L1N,         //$$ DAC_D6N   // reserved output
	inout  wire			o_B35_L13P_MRCC,   //$$ DAC_D7P   // reserved output
	inout  wire			o_B35_L13N_MRCC,   //$$ DAC_D7N   // reserved output
	input  wire			i_B35D_L12P_MRCC,   //$$ OSC_IN_P // DAC_DCO_P
	input  wire			i_B35D_L12N_MRCC,   //$$ OSC_IN_N // DAC_DCO_N
	//
	//output wire			o_B13_SYS_CLK_MC2, //$$ LAN_PWDN
	input  wire			i_B13_SYS_CLK_MC2, //$$ LAN_PWDN
	inout  wire			io_B13_L17P,       //$$ CD0+      // reserved
	inout  wire			io_B13_L17N,       //$$ CD0-      // reserved
	inout  wire			io_B13_L13P_MRCC,  //$$ CD2+      // reserved
	inout  wire			io_B13_L13N_MRCC,  //$$ CD2-      // reserved
	input  wire			i_B13_L11P_SRCC,   //$$ CC19 // A_R
	// MC2 - even
	output wire			o_B35_IO0,         //$$ LAN_SCSn
	output wire			o_B35_IO25,        //$$ LAN_SCLK
	//                                     
	output wire			o_B35_L24P,        //$$ LAN_MOSI
	//input  wire			i_B35_L24N,        //$$ LAN_INTn
	output wire			o_B35_L24N,        //$$ LAN_INTn
	inout  wire			o_B35_L22P,        //$$ DAC_D8P    // reserved output
	inout  wire			o_B35_L22N,        //$$ DAC_D8N    // reserved output
	inout  wire			o_B35_L20P,        //$$ DAC_D9P    // reserved output
	inout  wire			o_B35_L20N,        //$$ DAC_D9N    // reserved output
	inout  wire			o_B35_L16P,        //$$ DAC_D10P   // reserved output
	inout  wire			o_B35_L16N,        //$$ DAC_D10N   // reserved output
	inout  wire			o_B35_L17P,        //$$ DAC_D11P   // reserved output
	inout  wire			o_B35_L17N,        //$$ DAC_D11N   // reserved output
	inout  wire			o_B35_L14P_SRCC,   //$$ DAC_D12P   // reserved output
	inout  wire			o_B35_L14N_SRCC,   //$$ DAC_D12N   // reserved output
	inout  wire			o_B35_L10P,        //$$ DAC_D13P   // reserved output
	inout  wire			o_B35_L10N,        //$$ DAC_D13N   // reserved output
	inout  wire			o_B35_L8P,         //$$ DAC_D14P   // reserved output
	inout  wire			o_B35_L8N,         //$$ DAC_D14N   // reserved output
	inout  wire			o_B35_L5P,         //$$ DAC_D15P   // reserved output
	inout  wire			o_B35_L5N,         //$$ DAC_D15N   // reserved output
	input  wire			i_B35_L3P,         //$$ DAC_SDO    // reserved
	inout  wire			io_B35_L3N,        //$$ DAC_SDIO   // reserved
	inout  wire			o_B35_L2P,         //$$ DAC_SCLK   // reserved output
	inout  wire			o_B35_L2N,         //$$ DAC_CSB    // reserved output
	//                                     
	inout  wire			io_B13_L14P_SRCC,  //$$ CD3+       // reserved
	inout  wire			io_B13_L14N_SRCC,  //$$ CD3-       // reserved
	inout  wire			io_B13_L15P,       //$$ CD1+       // reserved
	inout  wire			io_B13_L15N,       //$$ CD1-       // reserved
	input  wire			i_B13_L6P,         //$$ CC16 // D_R
	input  wire			i_B13_L6N,         //$$ CC17 // C_R
	input  wire			i_B13_L11N_SRCC,   //$$ CC18 // B_R

	// BANK 15 16 for DDR3
	inout  wire [31:0]  ddr3_dq,      // reserved  
	inout  wire [14:0]  ddr3_addr,    // reserved output
	inout  wire [2 :0]  ddr3_ba,      // reserved output
	inout  wire [0 :0]  ddr3_ck_p,    // reserved output
	inout  wire [0 :0]  ddr3_ck_n,    // reserved output
	inout  wire [0 :0]  ddr3_cke,     // reserved output
	inout  wire         ddr3_cas_n,   // reserved output
	inout  wire         ddr3_ras_n,   // reserved output
	inout  wire         ddr3_we_n,    // reserved output
	inout  wire [0 :0]  ddr3_odt,     // reserved output
	inout  wire [3 :0]  ddr3_dm,      // reserved output
	inout  wire [3 :0]  ddr3_dqs_p,   // reserved 
	inout  wire [3 :0]  ddr3_dqs_n,   // reserved 
	inout  wire         ddr3_reset_n, // reserved output

	// LED on XEM7310
	output wire [7:0]   led
	);


/* Clock PLL */
// clock pll0
wire clk_out1_200M ; // REFCLK 200MHz for IDELAYCTRL // for pll1
wire clk_out2_140M ; // for pll2 ... 140M
wire clk_out3_10M  ; // for slow logic / I2C //
wire clk_out4_125M ; // unused
wire clk_out5_62p5M; // unused
//
wire clk_locked_pre;
clk_wiz_0  clk_wiz_0_inst (
	// Clock out ports  
	.clk_out1_200M (clk_out1_200M ), 
	.clk_out2_140M (clk_out2_140M ),
	.clk_out3_10M  (clk_out3_10M  ), // not exact due to 140MHz
	.clk_out4_125M (clk_out4_125M ), 
	.clk_out5_62p5M(clk_out5_62p5M), 
	// Status and control signals               
	//.locked(clk_locked),
	.locked(clk_locked_pre),
	// Clock in ports
	.clk_in1_p(sys_clkp),
	.clk_in1_n(sys_clkn)
);
// clock pll1
wire clk1_out1_160M; // for DWAVE 
wire clk1_out2_120M; // unused
wire clk1_out3_80M ; // unused
wire clk1_out4_60M ; // unused // 10M exact...
wire clk1_locked;
// clk_wiz_0_1_135M  clk_wiz_0_1_inst ( // 135MHz test
clk_wiz_0_1  clk_wiz_0_1_inst (
	// Clock out ports  
	.clk_out1_160M(clk1_out1_160M),  
	.clk_out2_120M(clk1_out2_120M), 
	.clk_out3_80M (clk1_out3_80M ), 
	.clk_out4_60M (clk1_out4_60M ), 
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk1_locked),
	// Clock in ports
	.clk_in1(clk_out1_200M)
);
// clock pll2
wire clk2_out1_210M; // for HR-ADC and test_clk
wire clk2_out2_105M; // unused
wire clk2_out3_60M ; // for ADC fifo/serdes
wire clk2_out4_30M ; // unused             
wire clk2_locked;
clk_wiz_0_2  clk_wiz_0_2_inst (
	// Clock out ports  
	.clk_out1_210M(clk2_out1_210M),  
	.clk_out2_105M(clk2_out2_105M), 
	.clk_out3_60M (clk2_out3_60M ), 
	.clk_out4_30M (clk2_out4_30M ), 
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk2_locked),
	// Clock in ports
	.clk_in1(clk_out2_140M) // 200M --> 140M for better jitter.
);
// clock pll3 
wire clk3_out1_72M ; // MCS core & UART ref 
wire clk3_out2_144M; // LAN-SPI control 144MHz 
wire clk3_out3_12M ; // IO bridge 12MHz 
// ... DDR3-CON 400MHz (pending)
wire clk3_locked;
clk_wiz_0_3  clk_wiz_0_3_inst (
	// Clock out ports  
	.clk_out1_72M (clk3_out1_72M ),  
	.clk_out2_144M(clk3_out2_144M), 
	.clk_out3_12M (clk3_out3_12M ), 
	// Status and control signals     
	.resetn(clk_locked_pre),          
	.locked(clk3_locked),
	// Clock in ports
	.clk_in1(clk_out1_200M)
);
// clock locked 
wire clk_locked = clk1_locked & clk2_locked & clk3_locked;

// system clock
wire sys_clk	= clk_out3_10M;

// system reset 
wire reset_n	= clk_locked;
wire reset		= ~reset_n;
////
	
// check SW_BUILD_ID
parameter REQ_SW_BUILD_ID = 32'h_ACAC_C8C8; // 0 for bypass 
////
	
// FPGA_IMAGE_ID //TODO:update_revision_code
//parameter FPGA_IMAGE_ID = 32'h2018_0611; // CMU-PLAN-A
//parameter FPGA_IMAGE_ID = 32'h_CA_18_0716; // CMU-1MHz-SUB
//parameter FPGA_IMAGE_ID = 32'h_FF_18_0801; // CMU-SIO-TEST
//parameter FPGA_IMAGE_ID = 32'h_FD_18_0820; // CMU-HSADC-TEST
//parameter FPGA_IMAGE_ID = 32'h_FD_18_0904; // CMU-HSADC-TEST // dwave rev. added dis control
//parameter FPGA_IMAGE_ID = 32'h_FB_18_1105; // CMU-CPU-TEST // dual ADC, 8f-4level DAC.
//parameter FPGA_IMAGE_ID = 32'h_FB_18_11DD; //// serial bus control only 
//parameter FPGA_IMAGE_ID = 32'h_FA_18_1127; //// DAC control 
//parameter FPGA_IMAGE_ID = 32'h_F9_18_1128; //// DWAVE control 
//parameter FPGA_IMAGE_ID = 32'h_F8_18_1207; //// ADC control ... clock loopback test 
//parameter FPGA_IMAGE_ID = 32'h_F8_19_0103; //// DAC_A2A3 pinmap revision
//parameter FPGA_IMAGE_ID = 32'h_F7_18_1222; //// DWAVE clock up to 200MHz : 160MHz also OK
//parameter FPGA_IMAGE_ID = 32'h_F6_19_0107; //// ADC base clock up to 210MHz ... from 125MHz // DWAVE 160MHz
//parameter FPGA_IMAGE_ID = 32'h_F6_19_0108; //// ADC base 210MHz // ADC fifo 70MHz // DWAVE 160MHz // PerfOption in vivado
//parameter FPGA_IMAGE_ID = 32'h_F5_19_0115; //// MCS 72MHz + XSDK(hello)
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0217; //// MCS 72MHz + XSDK(debug net) + LAN-SPIO 144MHz 
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0219; //// MCS 72MHz + XSDK(debug net) + LAN-SPIO 144MHz (end-point rev) 
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0222; //// MCS 72MHz + XSDK(debug net) + LAN-SPIO 144MHz (end-point rev) + ADC input timing xdc rev
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0223; //// MCS 72MHz + XSDK + LAN-SPIO 144MHz + ADC input-tap rev
//parameter FPGA_IMAGE_ID = 32'h_F4_19_0225; //// MCS 72MHz + XSDK + LAN-SPIO 144MHz + ADC input serdes rev
//parameter FPGA_IMAGE_ID = 32'h_F3_19_0306; //// MCS 72MHz + XSDK + LAN-SPIO 144MHz + LAN-Endpoint
//parameter FPGA_IMAGE_ID = 32'h_C8_19_0320; // CMU-CPU-TEST-F5500 
//parameter FPGA_IMAGE_ID = 32'h_C8_19_0329; // CMU-CPU-TEST-F5500  + DWAVE 135MHz 
//parameter FPGA_IMAGE_ID = 32'h_EF_19_0516; // CMU-CPU-TEST-F5500 // DWAVE 135MHz + temp sensor
//parameter FPGA_IMAGE_ID = 32'h_C8_19_0430; // CMU-CPU-TEST-F5500 + rev_a // SPO high 32b bug fix 
//parameter FPGA_IMAGE_ID = 32'h_C8_19_0513; // CMU-CPU-TEST-F5500 + rev_b // DWAVE pulse en sync bug fix 
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0517; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0607; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin swap for out2 (DWAVE_F1*)
//parameter FPGA_IMAGE_ID = 32'h_EE_19_06C7; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin swap for out2 (DWAVE_F1*) + sw warm-up control
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0618; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin swap for out2 (DWAVE_F1*) + sw warm-up control + adc period count 22bit
//parameter FPGA_IMAGE_ID = 32'h_EE_19_0717; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin NON-swap for out2 (DWAVE_F1*) + sw warm-up control + adc period count 22bit
parameter FPGA_IMAGE_ID = 32'h_EE_19_0718; // CMU-CPU-TEST-F5500 // DWAVE 160MHz + temp sensor + dwave pin NON-swap for out2 (DWAVE_F1*) + sw warm-up control + adc period count 22bit + dwaveout timing
//parameter FPGA_IMAGE_ID = 32'h_C8_19_05DD; //// full function release : temp sensor, eeprom, TCP setting. 
////

// ADC_BASE_FREQ
// 250MHz : 32'd250_000_000; //            // 4ns 
// 210MHz : 32'd210_000_000; //            // 4.76190476ns 
// 200MHz : 32'd200_000_000; //            // 5ns 
// 125MHz : 32'd125_000_000; // 0x07735940 // 8ns 
// 120MHz : 32'd120_000_000; // 0x07270E00 // 8.333ns
//parameter ADC_BASE_FREQ = 32'd125_000_000; 
//parameter ADC_BASE_FREQ = 32'd200_000_000; //
parameter ADC_BASE_FREQ = 32'd210_000_000; //
//parameter ADC_BASE_FREQ = 32'd250_000_000;
////

// DWAVE_BASE_FREQ
//  80MHz :  32'd80_000_000; // 0x4C4B400 // 12.5ns 
// 160MHz : 32'd160_000_000; // 0x9896800 // 6.25ns
//parameter DWAVE_BASE_FREQ = 32'd80_000_000; 
//parameter DWAVE_BASE_FREQ = 32'd135_000_000;
parameter DWAVE_BASE_FREQ = 32'd160_000_000;
//parameter DWAVE_BASE_FREQ = 32'd200_000_000;
////

/* USB-FPGA OK interface */
// OK Target interface clk:
(* keep = "true" *) 
wire okClk;
// USB Endpoint connections: // TODO: USB endpoint
// Wire In 		0x00 - 0x1F
wire [31:0] ep00wire; //$$ [TEST] SW_BUILD_ID 
wire [31:0] ep01wire; //$$ [TEST] TEST_CON 
wire [31:0] ep02wire; //$$ [TEST] TEST_CC_DIN 
wire [31:0] ep03wire; // not assigned
wire [31:0] ep04wire; //$$ [DAC] DAC_TEST_IN
wire [31:0] ep05wire; //$$ [DWAVE] DWAVE_DIN_BY_TRIG
wire [31:0] ep06wire; //$$ [DWAVE] DWAVE_CON
wire [31:0] ep07wire; //$$ [SPO] SPO_CON 
wire [31:0] ep08wire; //$$ [SPO] SPO_DIN_B0[31: 0] // SPO_CD0/SPO_EN0, CC0,CC1
wire [31:0] ep09wire; //$$ [SPO] SPO_DIN_B0[63:32] // SPO_CD0/SPO_EN0, CC0,CC1
wire [31:0] ep0Awire; //$$ [SPO] SPO_DIN_B1[31: 0] // SPO_CD1/SPO_EN0, CC2,CC3
wire [31:0] ep0Bwire; //$$ [SPO] SPO_DIN_B1[63:32] // SPO_CD1/SPO_EN0, CC2,CC3
wire [31:0] ep0Cwire; //$$ [SPO] SPO_DIN_B2[31: 0] // SPO_CD2/SPO_EN0, CC4,CC5
wire [31:0] ep0Dwire; //$$ [SPO] SPO_DIN_B2[63:32] // SPO_CD2/SPO_EN0, CC4,CC5
wire [31:0] ep0Ewire; //$$ [SPO] SPO_DIN_B3[31: 0] // SPO_CD3/SPO_EN0, CC6,CC7
wire [31:0] ep0Fwire; //$$ [SPO] SPO_DIN_B3[63:32] // SPO_CD3/SPO_EN0, CC6,CC7
wire [31:0] ep10wire; //$$ [DAC_A2A3] DAC_A2A3_CON
wire [31:0] ep11wire; //$$ [DAC_BIAS] DAC_BIAS_CON
wire [31:0] ep12wire; // not assigned
wire [31:0] ep13wire; // not assigned
wire [31:0] ep14wire; //$$ [DAC_A2A3] DAC_A2A3_DIN21
wire [31:0] ep15wire; //$$ [DAC_A2A3] DAC_A2A3_DIN43
wire [31:0] ep16wire; //$$ [DAC_BIAS] DAC_BIAS_DIN21
wire [31:0] ep17wire; //$$ [DAC_BIAS] DAC_BIAS_DIN43
wire [31:0] ep18wire; //$$ [ADC_HS] ADC_HS_WI 
wire [31:0] ep19wire; // not assigned
wire [31:0] ep1Awire; // not assigned
wire [31:0] ep1Bwire; // not assigned
wire [31:0] ep1Cwire; // not assigned
wire [31:0] ep1Dwire; //$$ [ADC_HS] ADC_HS_UPD_SMP    
wire [31:0] ep1Ewire; //$$ [ADC_HS] ADC_HS_SMP_PRD    
wire [31:0] ep1Fwire; //$$ [ADC_HS] ADC_HS_DLY_TAP_OPT
// Wire Out 	0x20 - 0x3F
wire [31:0] ep20wire; //$$ [TEST] FPGA_IMAGE_ID
wire [31:0] ep21wire; //$$ [TEST] TEST_OUT
wire [31:0] ep22wire; //$$ [TEST] TEST_CC_MON
wire [31:0] ep23wire; //$$ [DWAVE] DWAVE_BASE_FREQ
wire [31:0] ep24wire; //$$ [DAC] DAC_TEST_OUT
wire [31:0] ep25wire; //$$ [DWAVE] DWAVE_DOUT_BY_TRIG
wire [31:0] ep26wire; //$$ [DWAVE] DWAVE_FLAG
wire [31:0] ep27wire; //$$ [SPO] SPO_FLAG
wire [31:0] ep28wire; //$$ [SPO] SPO_MON_B0[31: 0]
wire [31:0] ep29wire; //$$ [SPO] SPO_MON_B0[63:32]
wire [31:0] ep2Awire; //$$ [SPO] SPO_MON_B1[31: 0]
wire [31:0] ep2Bwire; //$$ [SPO] SPO_MON_B1[63:32]
wire [31:0] ep2Cwire; //$$ [SPO] SPO_MON_B2[31: 0]
wire [31:0] ep2Dwire; //$$ [SPO] SPO_MON_B2[63:32]
wire [31:0] ep2Ewire; //$$ [SPO] SPO_MON_B3[31: 0]
wire [31:0] ep2Fwire; //$$ [SPO] SPO_MON_B3[63:32]
wire [31:0] ep30wire; //$$ [DAC_A2A3] DAC_A2A3_FLAG
wire [31:0] ep31wire; //$$ [DAC_BIAS] DAC_BIAS_FLAG
wire [31:0] ep32wire; //$$ [DAC] DAC_TEST_RB1
wire [31:0] ep33wire; //$$ [DAC] DAC_TEST_RB2
wire [31:0] ep34wire; //$$ [DAC_A2A3] DAC_A2A3_RB21 
wire [31:0] ep35wire; //$$ [DAC_A2A3] DAC_A2A3_RB43
wire [31:0] ep36wire; //$$ [DAC_BIAS] DAC_BIAS_RB21
wire [31:0] ep37wire; //$$ [DAC_BIAS] DAC_BIAS_RB43
wire [31:0] ep38wire; //$$ [ADC_HS] ADC_HS_WO
wire [31:0] ep39wire; //$$ [ADC] ADC_BASE_FREQ
wire [31:0] ep3Awire; //$$ [XADC] XADC_TEMP
wire [31:0] ep3Bwire; //$$ [XADC] XADC_VOLT  
wire [31:0] ep3Cwire; //$$ [ADC_HS] ADC_HS_DOUT0
wire [31:0] ep3Dwire; //$$ [ADC_HS] ADC_HS_DOUT1  
wire [31:0] ep3Ewire; //$$ [ADC_HS] ADC_HS_DOUT2
wire [31:0] ep3Fwire; //$$ [ADC_HS] ADC_HS_DOUT3
// Trigger In 	0x40 - 0x5F
wire ep40ck = sys_clk       ; wire [31:0] ep40trig; //$$ [TEST] TEST_TI
wire ep41ck = clk2_out1_210M; wire [31:0] ep41trig; //$$ [TEST] TEST_TI_125M
wire ep42ck = 1'b0; wire [31:0] ep42trig;
wire ep43ck = 1'b0; wire [31:0] ep43trig;
wire ep44ck = 1'b0; wire [31:0] ep44trig;
wire ep45ck = 1'b0; wire [31:0] ep45trig;
wire ep46ck = clk_out3_10M; wire [31:0] ep46trig; //$$ [DWAVE] DWAVE_TI
wire ep47ck = 1'b0; wire [31:0] ep47trig;
wire ep48ck = 1'b0; wire [31:0] ep48trig;
wire ep49ck = 1'b0; wire [31:0] ep49trig;
wire ep4Ack = 1'b0; wire [31:0] ep4Atrig; //$$ TEST_MCS for network command/response
wire ep4Bck = 1'b0; wire [31:0] ep4Btrig;
wire ep4Cck = 1'b0; wire [31:0] ep4Ctrig;
wire ep4Dck = 1'b0; wire [31:0] ep4Dtrig;
wire ep4Eck = 1'b0; wire [31:0] ep4Etrig;
wire ep4Fck = 1'b0; wire [31:0] ep4Ftrig;
wire ep50ck = clk_out3_10M; wire [31:0] ep50trig; //$$ [DAC_BIAS] DAC_BIAS_TI
wire ep51ck = clk_out3_10M; wire [31:0] ep51trig; //$$ [DAC_A2A3] DAC_A2A3_TI
wire ep52ck = 1'b0; wire [31:0] ep52trig;
wire ep53ck = 1'b0; wire [31:0] ep53trig;
wire ep54ck = 1'b0; wire [31:0] ep54trig;
wire ep55ck = 1'b0; wire [31:0] ep55trig;
wire ep56ck = 1'b0; wire [31:0] ep56trig;
wire ep57ck = 1'b0; wire [31:0] ep57trig;
wire ep58ck = clk_out3_10M; wire [31:0] ep58trig; //$$ [ADC_HS] ADC_HS_TI
wire ep59ck = 1'b0; wire [31:0] ep59trig;
wire ep5Ack = 1'b0; wire [31:0] ep5Atrig;
wire ep5Bck = 1'b0; wire [31:0] ep5Btrig;
wire ep5Cck = 1'b0; wire [31:0] ep5Ctrig;
wire ep5Dck = 1'b0; wire [31:0] ep5Dtrig;
wire ep5Eck = 1'b0; wire [31:0] ep5Etrig;
wire ep5Fck = 1'b0; wire [31:0] ep5Ftrig;
// Trigger Out 	0x60 - 0x7F
wire ep60ck = sys_clk     ; wire [31:0] ep60trig; //$$ [TEST] TEST_TO
wire ep61ck = 1'b0; wire [31:0] ep61trig = 32'b0;
wire ep62ck = 1'b0; wire [31:0] ep62trig = 32'b0;
wire ep63ck = 1'b0; wire [31:0] ep63trig = 32'b0;
wire ep64ck = 1'b0; wire [31:0] ep64trig = 32'b0;
wire ep65ck = 1'b0; wire [31:0] ep65trig = 32'b0;
wire ep66ck = 1'b0; wire [31:0] ep66trig = 32'b0;
wire ep67ck = 1'b0; wire [31:0] ep67trig = 32'b0;
wire ep68ck = 1'b0; wire [31:0] ep68trig = 32'b0;
wire ep69ck = 1'b0; wire [31:0] ep69trig = 32'b0;
wire ep6Ack = 1'b0; wire [31:0] ep6Atrig = 32'b0; //$$ TEST_MCS for network command/response
wire ep6Bck = 1'b0; wire [31:0] ep6Btrig = 32'b0;
wire ep6Cck = 1'b0; wire [31:0] ep6Ctrig = 32'b0;
wire ep6Dck = 1'b0; wire [31:0] ep6Dtrig = 32'b0;
wire ep6Eck = 1'b0; wire [31:0] ep6Etrig = 32'b0;
wire ep6Fck = 1'b0; wire [31:0] ep6Ftrig = 32'b0;
wire ep70ck = clk_out3_10M; wire [31:0] ep70trig; //$$ [DAC_BIAS] DAC_BIAS_TO
wire ep71ck = clk_out3_10M; wire [31:0] ep71trig; //$$ [DAC_A2A3] DAC_A2A3_TO
wire ep72ck = 1'b0; wire [31:0] ep72trig = 32'b0;
wire ep73ck = 1'b0; wire [31:0] ep73trig = 32'b0;
wire ep74ck = 1'b0; wire [31:0] ep74trig = 32'b0;
wire ep75ck = 1'b0; wire [31:0] ep75trig = 32'b0;
wire ep76ck = 1'b0; wire [31:0] ep76trig = 32'b0;
wire ep77ck = 1'b0; wire [31:0] ep77trig = 32'b0;
wire ep78ck = clk_out3_10M; wire [31:0] ep78trig; //$$ [ADC_HS] ADC_HS_TO
wire ep79ck = 1'b0; wire [31:0] ep79trig = 32'b0;
wire ep7Ack = 1'b0; wire [31:0] ep7Atrig = 32'b0;
wire ep7Bck = 1'b0; wire [31:0] ep7Btrig = 32'b0;
wire ep7Cck = 1'b0; wire [31:0] ep7Ctrig = 32'b0;
wire ep7Dck = 1'b0; wire [31:0] ep7Dtrig = 32'b0;
wire ep7Eck = 1'b0; wire [31:0] ep7Etrig = 32'b0;
wire ep7Fck = 1'b0; wire [31:0] ep7Ftrig = 32'b0; 
// Pipe In 		0x80 - 0x9F // clock is assumed to use okClk
wire ep80wr; wire [31:0] ep80pipe;
wire ep81wr; wire [31:0] ep81pipe;
wire ep82wr; wire [31:0] ep82pipe;
wire ep83wr; wire [31:0] ep83pipe;
wire ep84wr; wire [31:0] ep84pipe;
wire ep85wr; wire [31:0] ep85pipe;
wire ep86wr; wire [31:0] ep86pipe;
wire ep87wr; wire [31:0] ep87pipe;
wire ep88wr; wire [31:0] ep88pipe;
wire ep89wr; wire [31:0] ep89pipe;
wire ep8Awr; wire [31:0] ep8Apipe; //$$ TEST_MCS for test fifo
wire ep8Bwr; wire [31:0] ep8Bpipe;
wire ep8Cwr; wire [31:0] ep8Cpipe;
wire ep8Dwr; wire [31:0] ep8Dpipe;
wire ep8Ewr; wire [31:0] ep8Epipe;
wire ep8Fwr; wire [31:0] ep8Fpipe;
wire ep90wr; wire [31:0] ep90pipe;
wire ep91wr; wire [31:0] ep91pipe;
wire ep92wr; wire [31:0] ep92pipe;
wire ep93wr; wire [31:0] ep93pipe;
wire ep94wr; wire [31:0] ep94pipe;
wire ep95wr; wire [31:0] ep95pipe;
wire ep96wr; wire [31:0] ep96pipe;
wire ep97wr; wire [31:0] ep97pipe;
wire ep98wr; wire [31:0] ep98pipe;
wire ep99wr; wire [31:0] ep99pipe;
wire ep9Awr; wire [31:0] ep9Apipe;
wire ep9Bwr; wire [31:0] ep9Bpipe;
wire ep9Cwr; wire [31:0] ep9Cpipe;
wire ep9Dwr; wire [31:0] ep9Dpipe;
wire ep9Ewr; wire [31:0] ep9Epipe;
wire ep9Fwr; wire [31:0] ep9Fpipe;
// Pipe Out 	0xA0 - 0xBF
wire epA0rd; wire [31:0] epA0pipe = 32'b0;
wire epA1rd; wire [31:0] epA1pipe = 32'b0;
wire epA2rd; wire [31:0] epA2pipe = 32'b0;
wire epA3rd; wire [31:0] epA3pipe = 32'b0;
wire epA4rd; wire [31:0] epA4pipe = 32'b0;
wire epA5rd; wire [31:0] epA5pipe = 32'b0;
wire epA6rd; wire [31:0] epA6pipe = 32'b0;
wire epA7rd; wire [31:0] epA7pipe = 32'b0;
wire epA8rd; wire [31:0] epA8pipe = 32'b0;
wire epA9rd; wire [31:0] epA9pipe = 32'b0;
wire epAArd; wire [31:0] epAApipe = 32'b0; //$$ TEST_MCS for test fifo
wire epABrd; wire [31:0] epABpipe = 32'b0;
wire epACrd; wire [31:0] epACpipe = 32'b0;
wire epADrd; wire [31:0] epADpipe = 32'b0;
wire epAErd; wire [31:0] epAEpipe = 32'b0;
wire epAFrd; wire [31:0] epAFpipe = 32'b0;
wire epB0rd; wire [31:0] epB0pipe = 32'b0;
wire epB1rd; wire [31:0] epB1pipe = 32'b0;
wire epB2rd; wire [31:0] epB2pipe = 32'b0;
wire epB3rd; wire [31:0] epB3pipe = 32'b0;
wire epB4rd; wire [31:0] epB4pipe = 32'b0;
wire epB5rd; wire [31:0] epB5pipe = 32'b0;
wire epB6rd; wire [31:0] epB6pipe = 32'b0;
wire epB7rd; wire [31:0] epB7pipe = 32'b0;
wire epB8rd; wire [31:0] epB8pipe = 32'b0;
wire epB9rd; wire [31:0] epB9pipe = 32'b0;
wire epBArd; wire [31:0] epBApipe = 32'b0;
wire epBBrd; wire [31:0] epBBpipe = 32'b0;
wire epBCrd; wire [31:0] epBCpipe; //$$ [ADC_HS] ADC_HS_DOUT0_PO
wire epBDrd; wire [31:0] epBDpipe; //$$ [ADC_HS] ADC_HS_DOUT1_PO
wire epBErd; wire [31:0] epBEpipe; //$$ [ADC_HS] ADC_HS_DOUT2_PO
wire epBFrd; wire [31:0] epBFpipe; //$$ [ADC_HS] ADC_HS_DOUT3_PO
////

/* MCS */
//
wire FIT1_Toggle         ;
wire PIT1_Toggle         ;
//wire PIT1_Interrupt      ;
//
wire IO_addr_strobe      ;
wire [31:0] IO_address   ;
wire [3:0] IO_byte_enable;
wire [31:0] IO_read_data ;
wire IO_read_strobe      ;
wire IO_ready            ;
wire [31:0] IO_write_data;
wire IO_write_strobe     ;
//wire UART_rxd            = 1'b0;
//wire UART_txd            ;
wire INTC_IRQ            ;
//wire UART_Interrupt      ;
wire [0:0] INTC_Interrupt= 1'b0;
//
wire [31:0] GPIO1_tri_i  = 32'b0;
wire [31:0] GPIO1_tri_o  ;
//
microblaze_mcs_0 mcs_inst (
	.Clk(clk3_out1_72M),                // input wire Clk
	.Reset(reset),                      // input wire Reset
	//
	.FIT1_Toggle(FIT1_Toggle),          // output wire FIT1_Toggle
	.PIT1_Toggle(PIT1_Toggle),          // output wire PIT1_Toggle	
	//.PIT1_Interrupt(PIT1_Interrupt),    // output wire PIT1_Interrupt
	//
	.IO_addr_strobe(IO_addr_strobe),    // output wire IO_addr_strobe
	.IO_address(IO_address),            // output wire [31 : 0] IO_address
	.IO_byte_enable(IO_byte_enable),    // output wire [3 : 0] IO_byte_enable
	.IO_read_data(IO_read_data),        // input  wire [31 : 0] IO_read_data
	.IO_read_strobe(IO_read_strobe),    // output wire IO_read_strobe
	.IO_ready(IO_ready),                // input  wire IO_ready
	.IO_write_data(IO_write_data),      // output wire [31 : 0] IO_write_data
	.IO_write_strobe(IO_write_strobe),  // output wire IO_write_strobe
	//
	//.UART_rxd(UART_rxd),                // input wire UART_rxd
	//.UART_txd(UART_txd),                // output wire UART_txd
	//
	.INTC_IRQ(INTC_IRQ),                // output wire INTC_IRQ
	//.UART_Interrupt(UART_Interrupt),    // output wire UART_Interrupt
	.INTC_Interrupt(INTC_Interrupt),    // input wire [0 : 0] INTC_Interrupt
	//
	.GPIO1_tri_i(GPIO1_tri_i),          // input  wire [31 : 0] GPIO1_tri_i
	.GPIO1_tri_o(GPIO1_tri_o)           // output wire [31 : 0] GPIO1_tri_o
);
//
wire IO_ready_0;
wire IO_ready_1;
wire IO_ready_ref_0;
wire IO_ready_ref_1;
//
assign IO_ready = IO_ready_ref_0 | IO_ready_ref_1;
//
wire [31:0] IO_read_data_0 ;
wire [31:0] IO_read_data_1 ;
assign IO_read_data = 	(IO_ready_0)? IO_read_data_0: 
						(IO_ready_1)? IO_read_data_1: 
									  32'hC3C3_C3C3;
////


//TODO: mcs_io_bridge debug update
/* MCS IO BRIDGE */
// mcs_io_bridge_inst0:
//   LAN spi control / USB-MCS access switch control 
// mcs_io_bridge_inst1:
//   MCS Endpoint for CMU control (alternative to USB Endpoint) 
//      connect ports to ok_endpoint_wrapper or new wrapper mcs_endpoint_wrapper
//
wire [31:0] w_port_wi_00_0; // control for master_spi_wz850_inst // ={..., FIFO_reset, trig_SPI_frame, trig_LAN_reset}
wire [31:0] w_port_wi_01_0; // frame setup1 for master_spi_wz850_inst // ={8'b0, adrs[15:0], blck[4:0], rdwr, opmd[1:0]}
wire [31:0] w_port_wi_02_0; // frame setup2 for master_spi_wz850_inst // ={16'b0, frame_num_byte_data[15:0]}
wire [31:0] w_port_wi_10_0; // enable CMU control from MCS1 
	//"={11'b0, rst_adc, rst_dwave, rst_bias, rst_spo, rst_mcs_ep, 
	//	 10'b0, po_en, pi_en, to_en, ti_en , wo_en, wi_en}"
wire [31:0] w_port_wo_20_0; // status for master_spi_wz850_inst // ={27'b0, done_frame, INTn, SCSn, RSTn, done_reset};
wire [31:0] w_port_wo_21_0 = 32'b0; // not yet
wire w_wr_80_0; wire [31:0] w_port_pi_80_0; // LAN fifo wr
wire w_wr_81_0; wire [31:0] w_port_pi_81_0; // not yet
wire w_rd_A0_0; wire [31:0] w_port_po_A0_0; // LAN fifo rd
wire w_rd_A1_0; wire [31:0] w_port_po_A1_0 = 32'b0; // not yet
//
mcs_io_bridge #(
	.XPAR_IOMODULE_IO_BASEADDR  (32'h_C000_0000),
	.MCS_IO_INST_OFFSET         (32'h_0000_0000),// instance offset
	.FPGA_IMAGE_ID              (FPGA_IMAGE_ID)  
) mcs_io_bridge_inst0 (
	.clk(clk3_out1_72M), // assume clk3_out1_72M
	.reset_n(reset_n),
	// IO bus
	.i_IO_addr_strobe(IO_addr_strobe),    // input  wire IO_addr_strobe
	.i_IO_address(IO_address),            // input  wire [31 : 0] IO_address
	.i_IO_byte_enable(IO_byte_enable),    // input  wire [3 : 0] IO_byte_enable
	.o_IO_read_data(IO_read_data_0),        // output wire [31 : 0] IO_read_data
	.i_IO_read_strobe(IO_read_strobe),    // input  wire IO_read_strobe
	.o_IO_ready(IO_ready_0),                // output wire IO_ready
	.o_IO_ready_ref(IO_ready_ref_0),                // output wire IO_ready_ref
	.i_IO_write_data(IO_write_data),      // input  wire [31 : 0] IO_write_data
	.i_IO_write_strobe(IO_write_strobe),  // input  wire IO_write_strobe
	// IO port
	.o_port_wi_00(w_port_wi_00_0),          // output wire [31:0]
	.o_port_wi_01(w_port_wi_01_0),          // output wire [31:0]
	.o_port_wi_02(w_port_wi_02_0),          // output wire [31:0]
	.o_port_wi_10(w_port_wi_10_0),          // output wire [31:0]
	.i_port_wo_20(w_port_wo_20_0),          // input  wire [31:0]
	.i_port_wo_21(w_port_wo_21_0),          // input  wire [31:0]
	.o_wr_80(w_wr_80_0), .o_port_pi_80(w_port_pi_80_0), // output wire o_wr_80, output wire [31:0]   o_port_pi_80 ,
	.o_wr_81(w_wr_81_0), .o_port_pi_81(w_port_pi_81_0), // output wire o_wr_81, output wire [31:0]   o_port_pi_81 ,
	.o_rd_A0(w_rd_A0_0), .i_port_po_A0(w_port_po_A0_0), // output wire o_rd_A0, input  wire [31:0]   i_port_po_A0 ,
	.o_rd_A1(w_rd_A1_0), .i_port_po_A1(w_port_po_A1_0), // output wire o_rd_A1, input  wire [31:0]   i_port_po_A1 ,
	//
	.valid()
);
//
wire [31:0] w_port_wi_00_1;
wire [31:0] w_port_wi_01_1;
wire [31:0] w_port_wi_02_1;
wire [31:0] w_port_wi_03_1;
wire [31:0] w_port_wi_04_1;
wire [31:0] w_port_wi_05_1;
wire [31:0] w_port_wi_06_1;
wire [31:0] w_port_wi_07_1;
wire [31:0] w_port_wi_08_1;
wire [31:0] w_port_wi_09_1;
wire [31:0] w_port_wi_0A_1;
wire [31:0] w_port_wi_0B_1;
wire [31:0] w_port_wi_0C_1;
wire [31:0] w_port_wi_0D_1;
wire [31:0] w_port_wi_0E_1;
wire [31:0] w_port_wi_0F_1;
wire [31:0] w_port_wi_10_1;
wire [31:0] w_port_wi_11_1;
wire [31:0] w_port_wi_12_1;
wire [31:0] w_port_wi_13_1;
wire [31:0] w_port_wi_14_1;
wire [31:0] w_port_wi_15_1;
wire [31:0] w_port_wi_16_1;
wire [31:0] w_port_wi_17_1;
wire [31:0] w_port_wi_18_1;
wire [31:0] w_port_wi_19_1;
wire [31:0] w_port_wi_1A_1;
wire [31:0] w_port_wi_1B_1;
wire [31:0] w_port_wi_1C_1;
wire [31:0] w_port_wi_1D_1;
wire [31:0] w_port_wi_1E_1;
wire [31:0] w_port_wi_1F_1;
//
wire [31:0] w_port_wo_20_1;
wire [31:0] w_port_wo_21_1;
wire [31:0] w_port_wo_22_1;
wire [31:0] w_port_wo_23_1;
wire [31:0] w_port_wo_24_1;
wire [31:0] w_port_wo_25_1;
wire [31:0] w_port_wo_26_1;
wire [31:0] w_port_wo_27_1;
wire [31:0] w_port_wo_28_1;
wire [31:0] w_port_wo_29_1;
wire [31:0] w_port_wo_2A_1;
wire [31:0] w_port_wo_2B_1;
wire [31:0] w_port_wo_2C_1;
wire [31:0] w_port_wo_2D_1;
wire [31:0] w_port_wo_2E_1;
wire [31:0] w_port_wo_2F_1;
wire [31:0] w_port_wo_30_1;
wire [31:0] w_port_wo_31_1;
wire [31:0] w_port_wo_32_1;
wire [31:0] w_port_wo_33_1;
wire [31:0] w_port_wo_34_1;
wire [31:0] w_port_wo_35_1;
wire [31:0] w_port_wo_36_1;
wire [31:0] w_port_wo_37_1;
wire [31:0] w_port_wo_38_1;
wire [31:0] w_port_wo_39_1;
wire [31:0] w_port_wo_3A_1;
wire [31:0] w_port_wo_3B_1;
wire [31:0] w_port_wo_3C_1;
wire [31:0] w_port_wo_3D_1;
wire [31:0] w_port_wo_3E_1;
wire [31:0] w_port_wo_3F_1;
//
wire w_ck_40_1 = sys_clk       ; wire [31:0] w_port_ti_40_1; // 
wire w_ck_41_1 = clk2_out1_210M; wire [31:0] w_port_ti_41_1; // 
wire w_ck_46_1 = clk_out3_10M  ; wire [31:0] w_port_ti_46_1; // 
wire w_ck_4A_1 = clk3_out1_72M ; wire [31:0] w_port_ti_4A_1; // 
wire w_ck_50_1 = clk_out3_10M  ; wire [31:0] w_port_ti_50_1; // 
wire w_ck_51_1 = clk_out3_10M  ; wire [31:0] w_port_ti_51_1; // 
wire w_ck_58_1 = clk_out3_10M  ; wire [31:0] w_port_ti_58_1; // 
//
wire w_ck_60_1 = sys_clk       ; wire [31:0] w_port_to_60_1; // 
wire w_ck_6A_1 = clk3_out1_72M ; wire [31:0] w_port_to_6A_1; // 
wire w_ck_70_1 = clk_out3_10M  ; wire [31:0] w_port_to_70_1; // 
wire w_ck_71_1 = clk_out3_10M  ; wire [31:0] w_port_to_71_1; // 
wire w_ck_78_1 = clk_out3_10M  ; wire [31:0] w_port_to_78_1; // 
//
wire w_wr_8A_1; wire [31:0] w_port_pi_8A_1; // 
//
wire w_rd_AA_1; wire [31:0] w_port_po_AA_1; // 
wire w_rd_BC_1; wire [31:0] w_port_po_BC_1; // 
wire w_rd_BD_1; wire [31:0] w_port_po_BD_1; // 
wire w_rd_BE_1; wire [31:0] w_port_po_BE_1; // 
wire w_rd_BF_1; wire [31:0] w_port_po_BF_1; // 
//
wire reset_sw_mcs1_n;
//
mcs_io_bridge #(
	.XPAR_IOMODULE_IO_BASEADDR  (32'h_C000_0000),
	.MCS_IO_INST_OFFSET         (32'h_0001_0000),// instance offset
	.FPGA_IMAGE_ID              (FPGA_IMAGE_ID)  
) mcs_io_bridge_inst1 (
	.clk(clk3_out1_72M), // assume clk3_out1_72M
	.reset_n(reset_n & reset_sw_mcs1_n),
	// IO bus
	.i_IO_addr_strobe(IO_addr_strobe),    // input  wire IO_addr_strobe
	.i_IO_address(IO_address),            // input  wire [31 : 0] IO_address
	.i_IO_byte_enable(IO_byte_enable),    // input  wire [3 : 0] IO_byte_enable
	.o_IO_read_data(IO_read_data_1),        // output wire [31 : 0] IO_read_data
	.i_IO_read_strobe(IO_read_strobe),    // input  wire IO_read_strobe
	.o_IO_ready(IO_ready_1),                // output wire IO_ready
	.o_IO_ready_ref(IO_ready_ref_1),                // output wire IO_ready_ref
	.i_IO_write_data(IO_write_data),      // input  wire [31 : 0] IO_write_data
	.i_IO_write_strobe(IO_write_strobe),  // input  wire IO_write_strobe
	// IO port
	.o_port_wi_00(w_port_wi_00_1),          // output wire [31:0]
	.o_port_wi_01(w_port_wi_01_1),          // output wire [31:0]
	.o_port_wi_02(w_port_wi_02_1),          // output wire [31:0]
	.o_port_wi_03(w_port_wi_03_1),          // output wire [31:0]
	.o_port_wi_04(w_port_wi_04_1),          // output wire [31:0]
	.o_port_wi_05(w_port_wi_05_1),          // output wire [31:0]
	.o_port_wi_06(w_port_wi_06_1),          // output wire [31:0]
	.o_port_wi_07(w_port_wi_07_1),          // output wire [31:0]
	.o_port_wi_08(w_port_wi_08_1),          // output wire [31:0]
	.o_port_wi_09(w_port_wi_09_1),          // output wire [31:0]
	.o_port_wi_0A(w_port_wi_0A_1),          // output wire [31:0]
	.o_port_wi_0B(w_port_wi_0B_1),          // output wire [31:0]
	.o_port_wi_0C(w_port_wi_0C_1),          // output wire [31:0]
	.o_port_wi_0D(w_port_wi_0D_1),          // output wire [31:0]
	.o_port_wi_0E(w_port_wi_0E_1),          // output wire [31:0]
	.o_port_wi_0F(w_port_wi_0F_1),          // output wire [31:0]
	.o_port_wi_10(w_port_wi_10_1),          // output wire [31:0]
	.o_port_wi_11(w_port_wi_11_1),          // output wire [31:0]
	.o_port_wi_12(w_port_wi_12_1),          // output wire [31:0]
	.o_port_wi_13(w_port_wi_13_1),          // output wire [31:0]
	.o_port_wi_14(w_port_wi_14_1),          // output wire [31:0]
	.o_port_wi_15(w_port_wi_15_1),          // output wire [31:0]
	.o_port_wi_16(w_port_wi_16_1),          // output wire [31:0]
	.o_port_wi_17(w_port_wi_17_1),          // output wire [31:0]
	.o_port_wi_18(w_port_wi_18_1),          // output wire [31:0]
	.o_port_wi_19(w_port_wi_19_1),          // output wire [31:0]
	.o_port_wi_1A(w_port_wi_1A_1),          // output wire [31:0]
	.o_port_wi_1B(w_port_wi_1B_1),          // output wire [31:0]
	.o_port_wi_1C(w_port_wi_1C_1),          // output wire [31:0]
	.o_port_wi_1D(w_port_wi_1D_1),          // output wire [31:0]
	.o_port_wi_1E(w_port_wi_1E_1),          // output wire [31:0]
	.o_port_wi_1F(w_port_wi_1F_1),          // output wire [31:0]
	//
	.i_port_wo_20(w_port_wo_20_1),          // input  wire [31:0]
	.i_port_wo_21(w_port_wo_21_1),          // input  wire [31:0]
	.i_port_wo_22(w_port_wo_22_1),          // input  wire [31:0]
	.i_port_wo_23(w_port_wo_23_1),          // input  wire [31:0]
	.i_port_wo_24(w_port_wo_24_1),          // input  wire [31:0]
	.i_port_wo_25(w_port_wo_25_1),          // input  wire [31:0]
	.i_port_wo_26(w_port_wo_26_1),          // input  wire [31:0]
	.i_port_wo_27(w_port_wo_27_1),          // input  wire [31:0]
	.i_port_wo_28(w_port_wo_28_1),          // input  wire [31:0]
	.i_port_wo_29(w_port_wo_29_1),          // input  wire [31:0]
	.i_port_wo_2A(w_port_wo_2A_1),          // input  wire [31:0]
	.i_port_wo_2B(w_port_wo_2B_1),          // input  wire [31:0]
	.i_port_wo_2C(w_port_wo_2C_1),          // input  wire [31:0]
	.i_port_wo_2D(w_port_wo_2D_1),          // input  wire [31:0]
	.i_port_wo_2E(w_port_wo_2E_1),          // input  wire [31:0]
	.i_port_wo_2F(w_port_wo_2F_1),          // input  wire [31:0]
	.i_port_wo_30(w_port_wo_30_1),          // input  wire [31:0]
	.i_port_wo_31(w_port_wo_31_1),          // input  wire [31:0]
	.i_port_wo_32(w_port_wo_32_1),          // input  wire [31:0]
	.i_port_wo_33(w_port_wo_33_1),          // input  wire [31:0]
	.i_port_wo_34(w_port_wo_34_1),          // input  wire [31:0]
	.i_port_wo_35(w_port_wo_35_1),          // input  wire [31:0]
	.i_port_wo_36(w_port_wo_36_1),          // input  wire [31:0]
	.i_port_wo_37(w_port_wo_37_1),          // input  wire [31:0]
	.i_port_wo_38(w_port_wo_38_1),          // input  wire [31:0]
	.i_port_wo_39(w_port_wo_39_1),          // input  wire [31:0]
	.i_port_wo_3A(w_port_wo_3A_1),          // input  wire [31:0]
	.i_port_wo_3B(w_port_wo_3B_1),          // input  wire [31:0]
	.i_port_wo_3C(w_port_wo_3C_1),          // input  wire [31:0]
	.i_port_wo_3D(w_port_wo_3D_1),          // input  wire [31:0]
	.i_port_wo_3E(w_port_wo_3E_1),          // input  wire [31:0]
	.i_port_wo_3F(w_port_wo_3F_1),          // input  wire [31:0]
	//
	.i_ck_40(w_ck_40_1),  .o_port_ti_40(w_port_ti_40_1), // input , output wire [31:0]  ,
	.i_ck_41(w_ck_41_1),  .o_port_ti_41(w_port_ti_41_1), // input , output wire [31:0]  ,
	.i_ck_46(w_ck_46_1),  .o_port_ti_46(w_port_ti_46_1), // input , output wire [31:0]  ,
	.i_ck_4A(w_ck_4A_1),  .o_port_ti_4A(w_port_ti_4A_1), // input , output wire [31:0]  ,
	.i_ck_50(w_ck_50_1),  .o_port_ti_50(w_port_ti_50_1), // input , output wire [31:0]  ,
	.i_ck_51(w_ck_51_1),  .o_port_ti_51(w_port_ti_51_1), // input , output wire [31:0]  ,
	.i_ck_58(w_ck_58_1),  .o_port_ti_58(w_port_ti_58_1), // input , output wire [31:0]  ,
	//
	.i_ck_60(w_ck_60_1),  .i_port_to_60(w_port_to_60_1), // input , input  wire [31:0]  ,	
	.i_ck_6A(w_ck_6A_1),  .i_port_to_6A(w_port_to_6A_1), // input , input  wire [31:0]  ,	
	.i_ck_70(w_ck_70_1),  .i_port_to_70(w_port_to_70_1), // input , input  wire [31:0]  ,	
	.i_ck_71(w_ck_71_1),  .i_port_to_71(w_port_to_71_1), // input , input  wire [31:0]  ,	
	.i_ck_78(w_ck_78_1),  .i_port_to_78(w_port_to_78_1), // input , input  wire [31:0]  ,	
	//
	.o_wr_8A(w_wr_8A_1), .o_port_pi_8A(w_port_pi_8A_1), // output ,output wire [31:0]   , // test 
	//
	.o_rd_AA(w_rd_AA_1), .i_port_po_AA(w_port_po_AA_1), // output ,input  wire [31:0]   , // test
	.o_rd_BC(w_rd_BC_1), .i_port_po_BC(w_port_po_BC_1), // output ,input  wire [31:0]   ,
	.o_rd_BD(w_rd_BD_1), .i_port_po_BD(w_port_po_BD_1), // output ,input  wire [31:0]   ,
	.o_rd_BE(w_rd_BE_1), .i_port_po_BE(w_port_po_BE_1), // output ,input  wire [31:0]   ,
	.o_rd_BF(w_rd_BF_1), .i_port_po_BF(w_port_po_BF_1), // output ,input  wire [31:0]   ,
	// ....
	//
	.valid()
);
//
//
////


//TODO: master_spi_wz850 debug update
/* LAN */
// IO pins
(* keep = "true" *) wire w_LAN_RSTn;
(* keep = "true" *) wire w_LAN_INTn;
(* keep = "true" *) wire w_LAN_SCSn;
(* keep = "true" *) wire w_LAN_SCLK;
(* keep = "true" *) wire w_LAN_MOSI;
(* keep = "true" *) wire w_LAN_MISO;
// IO control 
(* keep = "true" *) wire w_trig_LAN_reset = w_port_wi_00_0[0];
(* keep = "true" *) wire w_done_LAN_reset;
(* keep = "true" *) wire w_trig_SPI_frame = w_port_wi_00_0[1];
(* keep = "true" *) wire w_done_SPI_frame;
(* keep = "true" *) wire w_FIFO_reset     = w_port_wi_00_0[2];
// frame control 
(* keep = "true" *) wire [15:0] w_frame_adrs          = w_port_wi_01_0[23: 8];
(* keep = "true" *) wire [ 4:0] w_frame_ctrl_blck_sel = w_port_wi_01_0[ 7: 3];
(* keep = "true" *) wire        w_frame_ctrl_rdwr_sel = w_port_wi_01_0[ 2: 2];
(* keep = "true" *) wire [ 1:0] w_frame_ctrl_opmd_sel = w_port_wi_01_0[ 1: 0];
(* keep = "true" *) wire [15:0] w_frame_num_byte_data = w_port_wi_02_0[15: 0];
// frame fifo data / control 
(* keep = "true" *) wire [ 7:0] w_frame_data_wr      ;
(* keep = "true" *) wire        w_frame_done_wr      ;
(* keep = "true" *) wire [ 7:0] w_frame_data_rd      ;
(* keep = "true" *) wire        w_frame_done_rd      ;
//
master_spi_wz850 
master_spi_wz850_inst (
	.clk				(clk3_out2_144M), // assume clk3_out2_144M
	.reset_n			(reset_n),
	.clk_reset			(clk3_out3_12M), // clk3_out3_12M
	//
	.i_trig_LAN_reset	(w_trig_LAN_reset), // input  wire i_trig_LAN_reset , // LAN reset trigger
	.o_done_LAN_reset	(w_done_LAN_reset), // output wire o_done_LAN_reset , // LAN reset done 
	.i_trig_SPI_frame	(w_trig_SPI_frame), // input  wire i_trig_SPI_frame , // SPI frame trigger
	.o_done_SPI_frame	(w_done_SPI_frame), // output wire o_done_SPI_frame , // SPI frame done 
	//
	.o_LAN_RSTn			(w_LAN_RSTn),
	.o_LAN_INTn			(w_LAN_INTn),
	.o_LAN_SCSn			(w_LAN_SCSn),
	.o_LAN_SCLK			(w_LAN_SCLK),
	.o_LAN_MOSI			(w_LAN_MOSI),
	.i_LAN_MISO			(w_LAN_MISO),
	//
	.i_frame_adrs         	(w_frame_adrs         ),
	.i_frame_ctrl_blck_sel	(w_frame_ctrl_blck_sel),
	.i_frame_ctrl_rdwr_sel	(w_frame_ctrl_rdwr_sel),
	.i_frame_ctrl_opmd_sel	(w_frame_ctrl_opmd_sel),
	.i_frame_num_byte_data	(w_frame_num_byte_data), // 0 input --> 1 converted by inner logic.
	.i_frame_data_wr      	(w_frame_data_wr      ),
	.o_frame_done_wr		(w_frame_done_wr      ),
	.o_frame_data_rd      	(w_frame_data_rd      ),
	.o_frame_done_rd		(w_frame_done_rd      ),
	//
	.valid				()		
);
//
assign w_port_wo_20_0 = {27'b0, 
	w_done_SPI_frame ,
	w_LAN_INTn ,
	w_LAN_SCSn ,
	w_LAN_RSTn ,
	w_done_LAN_reset};
//
// fifo_generator_3 
//   width "8-bit"
//   depth "16378 = 2^14"
//   standard read mode
// 
fifo_generator_3  LAN_fifo_wr_inst (
	.rst		(~reset_n | ~w_LAN_RSTn | w_FIFO_reset),  // input wire rst 
	.wr_clk		(clk3_out1_72M		),  // input wire wr_clk
	.wr_en		(w_wr_80_0		),  // input wire wr_en
	.din		(w_port_pi_80_0[7:0]	),  // input wire [7 : 0] din
	.wr_ack		(   	),  // output wire wr_ack
	.overflow	(   	),  // output wire overflow
	.prog_full	(   	),  // set at 16378
	.full		(   	),  // output wire full
//	//	
	.rd_clk		(clk3_out1_72M			),  // input wire rd_clk
	.rd_en		(w_frame_done_wr&(w_frame_ctrl_rdwr_sel)	),  // input wire rd_en
	.dout		(w_frame_data_wr	),  // output wire [7 : 0] dout
	.valid		(   	),  // output wire valid
	.underflow	(   	),  // output wire underflow
	.prog_empty	(   	),  // set at 5
	.empty		(   	)   // output wire empty
);
// 
fifo_generator_3  LAN_fifo_rd_inst (
	.rst		(~reset_n | ~w_LAN_RSTn | w_FIFO_reset),  // input wire rst 
	.wr_clk		(clk3_out1_72M			),  // input wire wr_clk
	.wr_en		(w_frame_done_rd&(~w_frame_ctrl_rdwr_sel)	),  // input wire wr_en
	.din		(w_frame_data_rd	),  // input wire [7 : 0] din
	.wr_ack		(   	),  // output wire wr_ack
	.overflow	(   	),  // output wire overflow
	.prog_full	(   	),  // set at 16378
	.full		(   	),  // output wire full
//	//	
	.rd_clk		(clk3_out1_72M		),  // input wire rd_clk
	.rd_en		(w_rd_A0_0		),  // input wire rd_en
	.dout		(w_port_po_A0_0[7:0]	),  // output wire [7 : 0] dout
	.valid		(   	),  // output wire valid
	.underflow	(   	),  // output wire underflow
	.prog_empty	(   	),  // set at 5
	.empty		(   	)   // output wire empty
);
//
assign w_port_po_A0_0[31:8] = 24'b0;
//
////


/* TEST FIFO */ 
// emulate LAN-fifo from/to ADC-fifo
// test-place ADC-fifo with TEST-fifo, which can be read and written.
//
// fifo_generator_4 : test
// 32 bits
// 4096 depth = 2^12
// 2^12 * 4 byte = 16KB
		
fifo_generator_4 TEST_fifo_inst (
  .rst       (~reset_n | ~w_LAN_RSTn | w_FIFO_reset), // input wire rst
  .wr_clk    (clk3_out1_72M),  // input wire wr_clk
  .wr_en     (w_wr_8A_1),      // input wire wr_en
  .din       (w_port_pi_8A_1), // input wire [31 : 0] din
  .rd_clk    (clk3_out1_72M),  // input wire rd_clk
  .rd_en     (w_rd_AA_1),      // input wire rd_en
  .dout      (w_port_po_AA_1), // output wire [31 : 0] dout
  .full      (),  // output wire full
  .wr_ack    (),  // output wire wr_ack
  .empty     (),  // output wire empty
  .valid     ()   // output wire valid
);


/* CMU endpoint switch */
// TODO: enable MCS control over CMU
	//"={11'b0, rst_adc, rst_dwave, rst_bias, rst_spo, rst_mcs_ep, 
	//	 10'b0, po_en, pi_en, to_en, ti_en , wo_en, wi_en}"
//
wire w_rst_adc      = w_port_wi_10_0[20];
wire w_rst_dwave    = w_port_wi_10_0[19];
wire w_rst_bias     = w_port_wi_10_0[18];
wire w_rst_spo      = w_port_wi_10_0[17];
wire w_rst_mcs_ep   = w_port_wi_10_0[16];
//
wire w_mcs_ep_po_en = w_port_wi_10_0[5];
wire w_mcs_ep_pi_en = w_port_wi_10_0[4];
wire w_mcs_ep_to_en = w_port_wi_10_0[3];
wire w_mcs_ep_ti_en = w_port_wi_10_0[2]; 
wire w_mcs_ep_wo_en = w_port_wi_10_0[1];
wire w_mcs_ep_wi_en = w_port_wi_10_0[0];
//

// MCS1 sw reset 
assign reset_sw_mcs1_n = ~w_rst_mcs_ep;

/* check IDs */
//wire [31:0] w_SW_BUILD_ID = ep00wire; // switch with w_port_wi_00_1
wire [31:0] w_SW_BUILD_ID = (w_mcs_ep_wi_en)? w_port_wi_00_1 : ep00wire;
//
wire [31:0] w_FPGA_IMAGE_ID = 
				(w_SW_BUILD_ID==REQ_SW_BUILD_ID)? FPGA_IMAGE_ID : 
				(w_SW_BUILD_ID==32'b0          )? FPGA_IMAGE_ID : 
				32'b0 ;
//
assign ep20wire = (!w_mcs_ep_wo_en)? w_FPGA_IMAGE_ID : 32'hACAC_ACAC;
//
assign w_port_wo_20_1 = (w_mcs_ep_wo_en)? w_FPGA_IMAGE_ID : 32'hACAC_ACAC;
//
////

/* TEST LED */
// test drive
reg  [7:0] r_test;
// Counter 1:
reg  [31:0] div1;
reg         clk1div;
(* keep = "true" *) reg  [7:0]  count1;
reg         count1eq00;
reg         count1eq80;
wire        reset1;
wire        disable1;
// Counter 2:
reg  [23:0] div2;
reg         clk2div;
(* keep = "true" *) reg  [7:0]  count2;
reg         count2eqFF;
wire        reset2;
wire        up2;
wire        down2;
wire        autocount2;
////

/* TEST_CC */
//
//wire [31:0] w_TEST_CC_DIN = ep02wire;
wire [31:0] w_TEST_CC_DIN = (w_mcs_ep_wi_en)? w_port_wi_02_1 : ep02wire;
wire [31:0] w_TEST_CC_MON;
assign ep22wire = w_TEST_CC_MON;
assign w_port_wo_22_1 = (w_mcs_ep_wo_en)? w_TEST_CC_MON : 32'hACAC_ACAC;
// wire names for monitoring
wire CTL_SPO_CDAT0; // CC0
wire CTL_SPO_EN0  ; // CC1
wire CTL_SPO_CDAT1; // CC2
wire CTL_SPO_EN1  ; // CC3
wire CTL_SPO_CDAT2; // CC4
wire CTL_SPO_EN2  ; // CC5
wire CTL_SPO_CDAT3; // CC6
wire CTL_SPO_EN3  ; // CC7
//
wire RB3  ; //$$ i_B13_L1P;       // CC12
wire RB2  ; //$$ i_B35_L6N;       // CC13
wire RB1  ; //$$ i_B35_L19P;      // CC14
wire UNBAL; //$$ i_B35_L19N;      // CC15
wire D_R  ; //$$ i_B13_L6P;       // CC16
wire C_R  ; //$$ i_B13_L6N;       // CC17
wire B_R  ; //$$ i_B13_L11N_SRCC; // CC18
wire A_R  ; //$$ i_B13_L11P_SRCC; // CC19
wire D_D  ; //$$ i_B34_L6P;       // CC20
wire C_D  ; //$$ i_B34_L6N;       // CC21
wire B_D  ; //$$ i_B34_L8P;       // CC22
wire A_D  ; //$$ i_B34_L8N;       // CC23
//
assign w_TEST_CC_MON[0] = CTL_SPO_CDAT0; // CC0
assign w_TEST_CC_MON[1] = CTL_SPO_EN0  ; // CC1
assign w_TEST_CC_MON[2] = CTL_SPO_CDAT1; // CC2
assign w_TEST_CC_MON[3] = CTL_SPO_EN1  ; // CC3
assign w_TEST_CC_MON[4] = CTL_SPO_CDAT2; // CC4
assign w_TEST_CC_MON[5] = CTL_SPO_EN2  ; // CC5
assign w_TEST_CC_MON[6] = CTL_SPO_CDAT3; // CC6
assign w_TEST_CC_MON[7] = CTL_SPO_EN3  ; // CC7
//
assign w_TEST_CC_MON[11:8] = 4'b0;
//
assign w_TEST_CC_MON[12] = RB3  ; //$$ i_B13_L1P;       // CC12
assign w_TEST_CC_MON[13] = RB2  ; //$$ i_B35_L6N;       // CC13
assign w_TEST_CC_MON[14] = RB1  ; //$$ i_B35_L19P;      // CC14
assign w_TEST_CC_MON[15] = UNBAL; //$$ i_B35_L19N;      // CC15
assign w_TEST_CC_MON[16] = D_R  ; //$$ i_B13_L6P;       // CC16
assign w_TEST_CC_MON[17] = C_R  ; //$$ i_B13_L6N;       // CC17
assign w_TEST_CC_MON[18] = B_R  ; //$$ i_B13_L11N_SRCC; // CC18
assign w_TEST_CC_MON[19] = A_R  ; //$$ i_B13_L11P_SRCC; // CC19
assign w_TEST_CC_MON[20] = D_D  ; //$$ i_B34_L6P;       // CC20
assign w_TEST_CC_MON[21] = C_D  ; //$$ i_B34_L6N;       // CC21
assign w_TEST_CC_MON[22] = B_D  ; //$$ i_B34_L8P;       // CC22
assign w_TEST_CC_MON[23] = A_D  ; //$$ i_B34_L8N;       // CC23
//
assign w_TEST_CC_MON[31:24] = 8'b0;
////



/* DWAVE */
//
//wire [31:0] w_DWAVE_DIN_BY_TRIG  = ep05wire;
wire [31:0] w_DWAVE_DIN_BY_TRIG  = (w_mcs_ep_wi_en)? w_port_wi_05_1 : ep05wire;
//wire [31:0] w_DWAVE_CON        = ep06wire;
wire [31:0] w_DWAVE_CON          = (w_mcs_ep_wi_en)? w_port_wi_06_1 : ep06wire;
//
wire [31:0] w_DWAVE_DOUT_BY_TRIG;
assign ep25wire = w_DWAVE_DOUT_BY_TRIG;
assign w_port_wo_25_1 = (w_mcs_ep_wo_en)? w_DWAVE_DOUT_BY_TRIG : 32'hACAC_ACAC;
//
wire [31:0] w_DWAVE_FLAG;
assign ep26wire = w_DWAVE_FLAG;
assign w_port_wo_26_1 = (w_mcs_ep_wo_en)? w_DWAVE_FLAG : 32'hACAC_ACAC;
//
wire [31:0] w_DWAVE_BASE_FREQ = DWAVE_BASE_FREQ;
assign ep23wire = w_DWAVE_BASE_FREQ;
assign w_port_wo_23_1 = (w_mcs_ep_wo_en)? w_DWAVE_BASE_FREQ : 32'hACAC_ACAC;
//
//wire [31:0] w_DWAVE_TI         = ep46trig;
wire [31:0] w_DWAVE_TI = (w_mcs_ep_ti_en)? w_port_ti_46_1 : ep46trig;
//
wire dwave_en 						= w_DWAVE_CON[0];
wire dwave_init 					= w_DWAVE_CON[1];
wire dwave_update 					= w_DWAVE_CON[2];
wire dwave_test 					= w_DWAVE_CON[3];
wire [1:0] dwave_test_datain_type	= w_DWAVE_CON[5:4];
wire [1:0] dwave_test_datain_port	= w_DWAVE_CON[7:6];
wire [31:0] dwave_test_datain		= {16'b0, w_DWAVE_CON[31:16]};
//
wire dwave_init_done;
wire dwave_update_done;
wire dwave_test_done;
//
assign w_DWAVE_FLAG = {28'b0,dwave_test_done,dwave_update_done,dwave_init_done,dwave_en};
// pulse outputs 
(* keep = "true" *) wire dwave_w1f1;
(* keep = "true" *) wire dwave_w1f2;
(* keep = "true" *) wire dwave_w1f3;
(* keep = "true" *) wire dwave_w2f1;
(* keep = "true" *) wire dwave_w2f2;
(* keep = "true" *) wire dwave_w2f3;
// 
dwave_control dwave_control_inst(
	.reset_n(reset_n & ~w_rst_dwave),
	.clk(clk_out3_10M), // assume 10MHz or 100ns
	.en			(dwave_en),
	//
	//.clk_dwave(clk1_out3_80M), // assume 80MHz
	.clk_dwave(clk1_out1_160M), // assume 160MHz 
	//.clk_dwave(clk_out1_200M), // assume 200MHz	
	//
	.init		(dwave_init),
	.update		(dwave_update),
	.test		(dwave_test),
	//
	.test_datain		(dwave_test_datain), 
	.test_datain_type	(dwave_test_datain_type),
	.test_datain_port	(dwave_test_datain_port),
	// trig interface 
	.i_trig_pulse_off      (w_DWAVE_TI[0]),
	.i_trig_pulse_on_cont  (w_DWAVE_TI[1]),
	.i_trig_pulse_on_num   (w_DWAVE_TI[2]),
	.i_trig_set_parameters (w_DWAVE_TI[3]),
	.i_wire_datain_by_trig   (w_DWAVE_DIN_BY_TRIG), // 32b
	.o_wire_dataout_by_trig  (w_DWAVE_DOUT_BY_TRIG), // 32b
	.i_trig_wr_cnt_period  (w_DWAVE_TI[16]),
	.i_trig_rd_cnt_period  (w_DWAVE_TI[24]),
	.i_trig_wr_cnt_diff    (w_DWAVE_TI[17]),
	.i_trig_rd_cnt_diff    (w_DWAVE_TI[25]),
	.i_trig_wr_num_pulses  (w_DWAVE_TI[18]),
	.i_trig_rd_num_pulses  (w_DWAVE_TI[26]),
	// i_phase disable // q_phase disable  
	// {f4,f3,f2,f1}
	//   f1, f2 ... i_phase ... for disable ... 0x3 ..3
	//   f3, f4 ... q_phase ... for disable ... 0xC ..12
	.i_trig_wr_output_dis  (w_DWAVE_TI[19]), 
	.i_trig_rd_output_dis  (w_DWAVE_TI[27]),
	// pulse out
	.pulse_out_f1_ref	(dwave_w1f2), //
	.pulse_out_f1_ref_n	(),
	.pulse_out_f1		(dwave_w1f1), //
	.pulse_out_f1_n		(),
	.pulse_out_f2_ref	(),
	.pulse_out_f2_ref_n	(),
	.pulse_out_f2		(dwave_w1f3), //
	.pulse_out_f2_n		(),
	.pulse_out_f3_ref	(dwave_w2f2), //
	.pulse_out_f3_ref_n	(),
	.pulse_out_f3		(dwave_w2f1), //
	.pulse_out_f3_n		(),
	.pulse_out_f4_ref	(),
	.pulse_out_f4_ref_n	(),
	.pulse_out_f4		(dwave_w2f3), //
	.pulse_out_f4_n		(),
	//
	.init_done			(dwave_init_done),
	.update_done		(dwave_update_done),
	.test_done			(dwave_test_done),
	.error(),
	.debug_out()
);
////


/* XADC */
//
wire [31:0] w_XADC_TEMP;
assign ep3Awire = w_XADC_TEMP;
assign w_port_wo_3A_1 = (w_mcs_ep_wo_en)? w_XADC_TEMP : 32'hACAC_ACAC;
wire [31:0] w_XADC_VOLT;
assign ep3Bwire = w_XADC_VOLT;
assign w_port_wo_3B_1 = (w_mcs_ep_wo_en)? w_XADC_VOLT : 32'hACAC_ACAC;
// XADC_DRP
wire [31:0] MEASURED_TEMP_MC;
wire [31:0] MEASURED_VCCINT_MV;
wire [31:0] MEASURED_VCCAUX_MV;
wire [31:0] MEASURED_VCCBRAM_MV;
//
(* keep = "true" *) wire [7:0] dbg_drp;
//
master_drp_ug480 master_drp_ug480_inst(
	.DCLK				(clk_out3_10M), // input DCLK, // Clock input for DRP
	.RESET				(~reset_n), // input RESET,
	.VP					(i_XADC_VP), // input VP, VN,// Dedicated and Hardwired Analog Input Pair
	.VN					(i_XADC_VN),
	.MEASURED_TEMP		(), // output reg [15:0] MEASURED_TEMP, MEASURED_VCCINT,
	.MEASURED_VCCINT	(),
	.MEASURED_VCCAUX	(), // output reg [15:0] MEASURED_VCCAUX, MEASURED_VCCBRAM,
	.MEASURED_VCCBRAM	(),
	// converted to decimal
	.MEASURED_TEMP_MC		(MEASURED_TEMP_MC), 
	.MEASURED_VCCINT_MV		(MEASURED_VCCINT_MV),
	.MEASURED_VCCAUX_MV		(MEASURED_VCCAUX_MV), 
	.MEASURED_VCCBRAM_MV	(MEASURED_VCCBRAM_MV),
	//
	.ALM_OUT	(), // output wire ALM_OUT,
	.CHANNEL	(), // output wire [4:0] CHANNEL,
	.OT			(), // output wire OT,
	.XADC_EOC	(), // output wire XADC_EOC,
	.XADC_EOS	(), // output wire XADC_EOS
	.debug_out	(dbg_drp)
);
//
assign w_XADC_TEMP	= MEASURED_TEMP_MC;
//
assign w_XADC_VOLT = 
	(count2[7:6]==2'b00)? MEASURED_VCCINT_MV :
	(count2[7:6]==2'b01)? MEASURED_VCCAUX_MV :
	(count2[7:6]==2'b10)? MEASURED_VCCBRAM_MV :					
		32'b0;
////


/* SPO control  */
//   master_spi_SN74LV8153_pio.v   
//   SN74LV8153_pio quad
//
//wire [31:0] w_SPO_CON    = ep07wire;
wire [31:0] w_SPO_CON = (w_mcs_ep_wi_en)? w_port_wi_07_1 : ep07wire;
//
//wire [63:0] w_SPO_DIN_B0 = {ep09wire, ep08wire};
//wire [63:0] w_SPO_DIN_B1 = {ep0Bwire, ep0Awire};
//wire [63:0] w_SPO_DIN_B2 = {ep0Dwire, ep0Cwire};
//wire [63:0] w_SPO_DIN_B3 = {ep0Fwire, ep0Ewire};
wire [63:0] w_SPO_DIN_B0 = (w_mcs_ep_wi_en)? {w_port_wi_09_1, w_port_wi_08_1} : {ep09wire, ep08wire};
wire [63:0] w_SPO_DIN_B1 = (w_mcs_ep_wi_en)? {w_port_wi_0B_1, w_port_wi_0A_1} : {ep0Bwire, ep0Awire};
wire [63:0] w_SPO_DIN_B2 = (w_mcs_ep_wi_en)? {w_port_wi_0D_1, w_port_wi_0C_1} : {ep0Dwire, ep0Cwire};
wire [63:0] w_SPO_DIN_B3 = (w_mcs_ep_wi_en)? {w_port_wi_0F_1, w_port_wi_0E_1} : {ep0Fwire, ep0Ewire};
//
wire [31:0] w_SPO_FLAG;
assign ep27wire = w_SPO_FLAG;
assign w_port_wo_27_1 = (w_mcs_ep_wo_en)? w_SPO_FLAG : 32'hACAC_ACAC;
//
wire [63:0] w_SPO_MON_B0;
wire [63:0] w_SPO_MON_B1;
wire [63:0] w_SPO_MON_B2;
wire [63:0] w_SPO_MON_B3;
//
assign ep28wire = w_SPO_MON_B0[31: 0];
assign ep29wire = w_SPO_MON_B0[63:32];
assign ep2Awire = w_SPO_MON_B1[31: 0];
assign ep2Bwire = w_SPO_MON_B1[63:32];
assign ep2Cwire = w_SPO_MON_B2[31: 0];
assign ep2Dwire = w_SPO_MON_B2[63:32];
assign ep2Ewire = w_SPO_MON_B3[31: 0];
assign ep2Fwire = w_SPO_MON_B3[63:32];
assign w_port_wo_28_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B0[31: 0] : 32'hACAC_ACAC;
assign w_port_wo_29_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B0[63:32] : 32'hACAC_ACAC;
assign w_port_wo_2A_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B1[31: 0] : 32'hACAC_ACAC;
assign w_port_wo_2B_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B1[63:32] : 32'hACAC_ACAC;
assign w_port_wo_2C_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B2[31: 0] : 32'hACAC_ACAC;
assign w_port_wo_2D_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B2[63:32] : 32'hACAC_ACAC;
assign w_port_wo_2E_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B3[31: 0] : 32'hACAC_ACAC;
assign w_port_wo_2F_1 = (w_mcs_ep_wo_en)? w_SPO_MON_B3[63:32] : 32'hACAC_ACAC;
//
wire [63:0] CTL_IO_SPO_0_ctr_in = w_SPO_DIN_B0;
wire [63:0] CTL_IO_SPO_0_mon_out;
	assign w_SPO_MON_B0 = CTL_IO_SPO_0_mon_out;
//
wire [63:0] CTL_IO_SPO_1_ctr_in = w_SPO_DIN_B1; 
wire [63:0] CTL_IO_SPO_1_mon_out;
	assign w_SPO_MON_B1 = CTL_IO_SPO_1_mon_out;
//
wire [63:0] CTL_IO_SPO_2_ctr_in = w_SPO_DIN_B2; 
wire [63:0] CTL_IO_SPO_2_mon_out;
//
	assign w_SPO_MON_B2 = CTL_IO_SPO_2_mon_out;
//
wire [63:0] CTL_IO_SPO_3_ctr_in = w_SPO_DIN_B3;
wire [63:0] CTL_IO_SPO_3_mon_out;
	assign w_SPO_MON_B3 = CTL_IO_SPO_3_mon_out;
//
wire CTL_IO_CON_en					= w_SPO_CON[0];
wire CTL_IO_CON_init 				= w_SPO_CON[1];
wire CTL_IO_CON_update 				= w_SPO_CON[2];
wire CTL_IO_CON_test 				= w_SPO_CON[3];
wire [2:0]  CTL_IO_CON_adrs_start	= w_SPO_CON[6:4];
wire [2:0]  CTL_IO_CON_num_bytes 	= w_SPO_CON[10:8];
wire [13:0] CTL_IO_CON_test_pdata	= w_SPO_CON[29:16];
//
wire CTL_IO_FLAG_SPO_0_init_done  ;
wire CTL_IO_FLAG_SPO_0_update_done;
wire CTL_IO_FLAG_SPO_0_test_done  ;
wire CTL_IO_FLAG_SPO_1_init_done  ;
wire CTL_IO_FLAG_SPO_1_update_done;
wire CTL_IO_FLAG_SPO_1_test_done  ;
wire CTL_IO_FLAG_SPO_2_init_done  ;
wire CTL_IO_FLAG_SPO_2_update_done;
wire CTL_IO_FLAG_SPO_2_test_done  ;
wire CTL_IO_FLAG_SPO_3_init_done  ;
wire CTL_IO_FLAG_SPO_3_update_done;
wire CTL_IO_FLAG_SPO_3_test_done  ;
//
wire CTL_IO_FLAG_SPO_X_init_done;
	assign CTL_IO_FLAG_SPO_X_init_done = 
		CTL_IO_FLAG_SPO_0_init_done  & 
		CTL_IO_FLAG_SPO_1_init_done  & 
		CTL_IO_FLAG_SPO_2_init_done  & 
		CTL_IO_FLAG_SPO_3_init_done  ;
wire CTL_IO_FLAG_SPO_X_update_done;
	assign CTL_IO_FLAG_SPO_X_update_done = 
		CTL_IO_FLAG_SPO_0_update_done & 
		CTL_IO_FLAG_SPO_1_update_done & 
		CTL_IO_FLAG_SPO_2_update_done & 
		CTL_IO_FLAG_SPO_3_update_done ;
wire CTL_IO_FLAG_SPO_X_test_done;
	assign CTL_IO_FLAG_SPO_X_test_done = 
		CTL_IO_FLAG_SPO_0_test_done  & 
		CTL_IO_FLAG_SPO_1_test_done  & 
		CTL_IO_FLAG_SPO_2_test_done  & 
		CTL_IO_FLAG_SPO_3_test_done  ;
//
wire CTL_IO_SPO_0_error;
wire CTL_IO_SPO_1_error;
wire CTL_IO_SPO_2_error;
wire CTL_IO_SPO_3_error;
//
assign w_SPO_FLAG = {
	{CTL_IO_FLAG_SPO_3_test_done,CTL_IO_FLAG_SPO_3_update_done,CTL_IO_FLAG_SPO_3_init_done,CTL_IO_CON_en&(~CTL_IO_SPO_3_error)},
	{CTL_IO_FLAG_SPO_2_test_done,CTL_IO_FLAG_SPO_2_update_done,CTL_IO_FLAG_SPO_2_init_done,CTL_IO_CON_en&(~CTL_IO_SPO_2_error)},
	{CTL_IO_FLAG_SPO_1_test_done,CTL_IO_FLAG_SPO_1_update_done,CTL_IO_FLAG_SPO_1_init_done,CTL_IO_CON_en&(~CTL_IO_SPO_1_error)},
	{CTL_IO_FLAG_SPO_0_test_done,CTL_IO_FLAG_SPO_0_update_done,CTL_IO_FLAG_SPO_0_init_done,CTL_IO_CON_en&(~CTL_IO_SPO_0_error)},
	4'b0,
	4'b0,
	4'b0,
	{CTL_IO_FLAG_SPO_X_test_done,CTL_IO_FLAG_SPO_X_update_done,CTL_IO_FLAG_SPO_X_init_done,CTL_IO_CON_en}
	};
// SPO_0
master_spi_SN74LV8153_pio master_spi_SN74LV8153_pio_SPO_0_inst (  
	.clk			(clk_out3_10M),
	.reset_n		(reset_n & ~w_rst_spo),
	.en				(CTL_IO_CON_en),
	.init			(CTL_IO_CON_init),
	.update			(CTL_IO_CON_update),
	.test			(CTL_IO_CON_test),
	.adrs_start		(CTL_IO_CON_adrs_start),
	.num_bytes		(CTL_IO_CON_num_bytes),
	.test_pdata		(CTL_IO_CON_test_pdata),
	.ctr_in			(CTL_IO_SPO_0_ctr_in), //
	.mon_out		(CTL_IO_SPO_0_mon_out), //
	.spo_cdat		(CTL_SPO_CDAT0), //
	.spo_rst_n		(CTL_SPO_EN0), //
	.init_done		(CTL_IO_FLAG_SPO_0_init_done), //
	.update_done	(CTL_IO_FLAG_SPO_0_update_done), //
	.test_done		(CTL_IO_FLAG_SPO_0_test_done), //
	.error			(CTL_IO_SPO_0_error),
	.debug_out		()
);
// SPO_1
master_spi_SN74LV8153_pio master_spi_SN74LV8153_pio_SPO_1_inst (  
	.clk			(clk_out3_10M),
	.reset_n		(reset_n & ~w_rst_spo),
	.en				(CTL_IO_CON_en),
	.init			(CTL_IO_CON_init),
	.update			(CTL_IO_CON_update),
	.test			(CTL_IO_CON_test),
	.adrs_start		(CTL_IO_CON_adrs_start),
	.num_bytes		(CTL_IO_CON_num_bytes),
	.test_pdata		(CTL_IO_CON_test_pdata),
	.ctr_in			(CTL_IO_SPO_1_ctr_in), //
	.mon_out		(CTL_IO_SPO_1_mon_out), //
	.spo_cdat		(CTL_SPO_CDAT1), //
	.spo_rst_n		(CTL_SPO_EN1), //
	.init_done		(CTL_IO_FLAG_SPO_1_init_done), //
	.update_done	(CTL_IO_FLAG_SPO_1_update_done), //
	.test_done		(CTL_IO_FLAG_SPO_1_test_done), //
	.error			(CTL_IO_SPO_1_error),
	.debug_out		()
);
// SPO_2
master_spi_SN74LV8153_pio master_spi_SN74LV8153_pio_SPO_2_inst (  
	.clk			(clk_out3_10M),
	.reset_n		(reset_n & ~w_rst_spo),
	.en				(CTL_IO_CON_en),
	.init			(CTL_IO_CON_init),
	.update			(CTL_IO_CON_update),
	.test			(CTL_IO_CON_test),
	.adrs_start		(CTL_IO_CON_adrs_start),
	.num_bytes		(CTL_IO_CON_num_bytes),
	.test_pdata		(CTL_IO_CON_test_pdata),
	.ctr_in			(CTL_IO_SPO_2_ctr_in), //
	.mon_out		(CTL_IO_SPO_2_mon_out), //
	.spo_cdat		(CTL_SPO_CDAT2), //
	.spo_rst_n		(CTL_SPO_EN2), //
	.init_done		(CTL_IO_FLAG_SPO_2_init_done), //
	.update_done	(CTL_IO_FLAG_SPO_2_update_done), //
	.test_done		(CTL_IO_FLAG_SPO_2_test_done), //
	.error			(CTL_IO_SPO_2_error),
	.debug_out		()
);
// SPO_3
master_spi_SN74LV8153_pio master_spi_SN74LV8153_pio_SPO_3_inst (  
	.clk			(clk_out3_10M),
	.reset_n		(reset_n & ~w_rst_spo),
	.en				(CTL_IO_CON_en),
	.init			(CTL_IO_CON_init),
	.update			(CTL_IO_CON_update),
	.test			(CTL_IO_CON_test),
	.adrs_start		(CTL_IO_CON_adrs_start),
	.num_bytes		(CTL_IO_CON_num_bytes),
	.test_pdata		(CTL_IO_CON_test_pdata),
	.ctr_in			(CTL_IO_SPO_3_ctr_in), //
	.mon_out		(CTL_IO_SPO_3_mon_out), //
	.spo_cdat		(CTL_SPO_CDAT3), //
	.spo_rst_n		(CTL_SPO_EN3), //
	.init_done		(CTL_IO_FLAG_SPO_3_init_done), //
	.update_done	(CTL_IO_FLAG_SPO_3_update_done), //
	.test_done		(CTL_IO_FLAG_SPO_3_test_done), //
	.error			(CTL_IO_SPO_3_error),//
	.debug_out		()
);
////


/* DAC AD5754 */
// DAC_BIAS, DAC_A2A3
//wire [31:0] w_DAC_BIAS_CON   = ep11wire;
//wire [31:0] w_DAC_BIAS_DIN21 = ep16wire;
//wire [31:0] w_DAC_BIAS_DIN43 = ep17wire;
wire [31:0] w_DAC_BIAS_CON   = (w_mcs_ep_wi_en)? w_port_wi_11_1 : ep11wire;
wire [31:0] w_DAC_BIAS_DIN21 = (w_mcs_ep_wi_en)? w_port_wi_16_1 : ep16wire;
wire [31:0] w_DAC_BIAS_DIN43 = (w_mcs_ep_wi_en)? w_port_wi_17_1 : ep17wire;
//
wire [31:0] w_DAC_BIAS_FLAG;
assign ep31wire = w_DAC_BIAS_FLAG;
assign w_port_wo_31_1 = (w_mcs_ep_wo_en)? w_DAC_BIAS_FLAG : 32'hACAC_ACAC;
wire [31:0] w_DAC_BIAS_RB21;
assign ep36wire = w_DAC_BIAS_RB21;
assign w_port_wo_36_1 = (w_mcs_ep_wo_en)? w_DAC_BIAS_RB21 : 32'hACAC_ACAC;
wire [31:0] w_DAC_BIAS_RB43;
assign ep37wire = w_DAC_BIAS_RB43;
assign w_port_wo_37_1 = (w_mcs_ep_wo_en)? w_DAC_BIAS_RB43 : 32'hACAC_ACAC;
//
//wire [31:0] w_DAC_BIAS_TI    = ep50trig;
wire [31:0] w_DAC_BIAS_TI = (w_mcs_ep_ti_en)? w_port_ti_50_1 : ep50trig;
//
wire [31:0] w_DAC_BIAS_TO;
assign ep70trig = w_DAC_BIAS_TO;
assign w_port_to_70_1 = (w_mcs_ep_to_en)? w_DAC_BIAS_TO : 32'h0000_0000; //$$
// DAC_A2A3
//wire [31:0] w_DAC_A2A3_CON   = ep10wire;
//wire [31:0] w_DAC_A2A3_DIN21 = ep14wire;
//wire [31:0] w_DAC_A2A3_DIN43 = ep15wire;
wire [31:0] w_DAC_A2A3_CON   = (w_mcs_ep_wi_en)? w_port_wi_10_1 : ep10wire;
wire [31:0] w_DAC_A2A3_DIN21 = (w_mcs_ep_wi_en)? w_port_wi_14_1 : ep14wire;
wire [31:0] w_DAC_A2A3_DIN43 = (w_mcs_ep_wi_en)? w_port_wi_15_1 : ep15wire;
//
wire [31:0] w_DAC_A2A3_FLAG;
assign ep30wire = w_DAC_A2A3_FLAG;
assign w_port_wo_30_1 = (w_mcs_ep_wo_en)? w_DAC_A2A3_FLAG : 32'hACAC_ACAC;
wire [31:0] w_DAC_A2A3_RB21;
assign ep34wire = w_DAC_A2A3_RB21;
assign w_port_wo_34_1 = (w_mcs_ep_wo_en)? w_DAC_A2A3_RB21 : 32'hACAC_ACAC;
wire [31:0] w_DAC_A2A3_RB43;
assign ep35wire = w_DAC_A2A3_RB43;
assign w_port_wo_35_1 = (w_mcs_ep_wo_en)? w_DAC_A2A3_RB43 : 32'hACAC_ACAC;
//
//wire [31:0] w_DAC_A2A3_TI    = ep51trig;
wire [31:0] w_DAC_A2A3_TI = (w_mcs_ep_ti_en)? w_port_ti_51_1 : ep51trig;
//
wire [31:0] w_DAC_A2A3_TO;
assign ep71trig = w_DAC_A2A3_TO;
assign w_port_to_71_1 = (w_mcs_ep_to_en)? w_DAC_A2A3_TO : 32'h0000_0000; //$$
// IO pins for DAC_BIAS
wire DAC_BIAS_MISO  ; // DAC_BIAS_MISO
wire DAC_BIAS_MOSI  ; // DAC_BIAS_MOSI
wire DAC_BIAS_SYNB; // DAC_BIAS_SYNB
wire DAC_BIAS_SCLK ; // DAC_BIAS_SCLK
wire DAC_BIAS_CLRB  ; // DAC_BIAS_CLRB
wire DAC_BIAS_LD_B ; // DAC_BIAS_LD_B
// IO pins for DAC_A2A3
wire DAC_A2A3_MOSI; 
wire DAC_A2A3_SCLK; 
wire DAC_A2A3_SYNB; 
wire DAC_A2A3_MISO;
// test 
//wire [31:0] w_DAC_TEST_IN    = ep04wire;
wire [31:0] w_DAC_TEST_IN    = (w_mcs_ep_wi_en)? w_port_wi_04_1 : ep04wire;
//
wire [31:0] w_DAC_TEST_RB1;
assign ep32wire = w_DAC_TEST_RB1;
assign w_port_wo_32_1 = (w_mcs_ep_wo_en)? w_DAC_TEST_RB1 : 32'hACAC_ACAC;
wire [31:0] w_DAC_TEST_RB2;
assign ep33wire = w_DAC_TEST_RB2;
assign w_port_wo_33_1 = (w_mcs_ep_wo_en)? w_DAC_TEST_RB2 : 32'hACAC_ACAC;
wire [31:0] w_DAC_TEST_OUT;
assign ep24wire = w_DAC_TEST_OUT;
assign w_port_wo_24_1 = (w_mcs_ep_wo_en)? w_DAC_TEST_OUT : 32'hACAC_ACAC;
//
wire [31:0] w_DAC_BIAS_TEST_IN  = w_DAC_TEST_IN;
wire [31:0] w_DAC_BIAS_TEST_OUT;
wire [31:0] w_DAC_BIAS_TEST_RB1;
wire [31:0] w_DAC_BIAS_TEST_RB2;
//
wire [31:0] w_DAC_A2A3_TEST_IN  = w_DAC_TEST_IN;
wire [31:0] w_DAC_A2A3_TEST_OUT;
wire [31:0] w_DAC_A2A3_TEST_RB1;
wire [31:0] w_DAC_A2A3_TEST_RB2;
//
assign w_DAC_TEST_OUT = 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b10)? w_DAC_BIAS_TEST_OUT : 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b01)? w_DAC_A2A3_TEST_OUT : 
	32'b0;
assign w_DAC_TEST_RB1 = 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b10)? w_DAC_BIAS_TEST_RB1 : 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b01)? w_DAC_A2A3_TEST_RB1 : 
	32'b0;
assign w_DAC_TEST_RB2 = 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b10)? w_DAC_BIAS_TEST_RB2 : 
	({w_DAC_BIAS_CON[0],w_DAC_A2A3_CON[0]}==2'b01)? w_DAC_A2A3_TEST_RB2 : 
	32'b0;
// DAC AD5754 : DAC_BIAS 
//  master_spi_dac_AD5754__range_con
wire DAC_BIAS_en			= 				w_DAC_BIAS_CON[0];
wire DAC_BIAS_reset		= w_DAC_BIAS_TI[0];
wire DAC_BIAS_init		= w_DAC_BIAS_TI[1] | w_DAC_BIAS_CON[1];
wire DAC_BIAS_update		= w_DAC_BIAS_TI[2] | w_DAC_BIAS_CON[2];
wire DAC_BIAS_test		= w_DAC_BIAS_TI[3] | w_DAC_BIAS_CON[3];
wire [15:0] DAC_BIAS_conf =  				w_DAC_BIAS_CON[31:16];
//
wire DAC_BIAS_init_done	;
wire DAC_BIAS_update_done	;
wire DAC_BIAS_test_done	;
wire DAC_BIAS_error		;
//
wire [31:0] DAC_BIAS_DIN21 = w_DAC_BIAS_DIN21;
wire [31:0] DAC_BIAS_DIN43 = w_DAC_BIAS_DIN43;
//
wire [31:0] DAC_BIAS_RDBK21;
wire [31:0] DAC_BIAS_RDBK43;
//
master_spi_dac_AD5754__range_con master_spi_dac_AD5754_DAC_BIAS_inst ( 
	.clk		(clk_out3_10M), // assume 10MHz or 100ns
	.reset_n	(reset_n & ~DAC_BIAS_reset & ~w_rst_bias), // input
	.en			(DAC_BIAS_en), // input
	//
	.conf		(DAC_BIAS_conf), // 16 bits
	//
	.init		(DAC_BIAS_init), // input
	.update		(DAC_BIAS_update), // input
	.test		(DAC_BIAS_test), // input
    //
	.test_sdi_pdata	(w_DAC_BIAS_TEST_IN ), // input
	.test_sdo_pdata	(w_DAC_BIAS_TEST_OUT), // out 
	.readback_pdata (w_DAC_BIAS_TEST_RB1), // out
	.readback1_pdata(w_DAC_BIAS_TEST_RB2), // out
	// DAC input
	.DAC_VALUE_IN1	(DAC_BIAS_DIN21[15: 0]), // input
	.DAC_VALUE_IN2	(DAC_BIAS_DIN21[31:16]), // input
	.DAC_VALUE_IN3	(DAC_BIAS_DIN43[15: 0]), // input
	.DAC_VALUE_IN4	(DAC_BIAS_DIN43[31:16]), // input
    // DAC readback
	.DAC_READBACK1	(DAC_BIAS_RDBK21[15: 0]),
	.DAC_READBACK2	(DAC_BIAS_RDBK21[31:16]), 
	.DAC_READBACK3	(DAC_BIAS_RDBK43[15: 0]),
	.DAC_READBACK4	(DAC_BIAS_RDBK43[31:16]), 
    // DAC control
    .SCLK			(DAC_BIAS_SCLK	),
    .SYNC_N			(DAC_BIAS_SYNB	),
    .DIN			(DAC_BIAS_MOSI		),
    .SDO			(DAC_BIAS_MISO		), // input
	// GPIO
	.CLR_DAC_N		(DAC_BIAS_CLRB		), // ext pin
	.LOAD_DAC_N		(DAC_BIAS_LD_B	), // ext pin
	.B2C_DAC		(),                // ext pin
    //
    .init_done		(DAC_BIAS_init_done	),
    .update_done	(DAC_BIAS_update_done	),
	.test_done		(DAC_BIAS_test_done	),
	.error			(DAC_BIAS_error		),
	.debug_out()
);
//
assign w_DAC_BIAS_RB21 = DAC_BIAS_RDBK21;
assign w_DAC_BIAS_RB43 = DAC_BIAS_RDBK43;
//
assign w_DAC_BIAS_FLAG = 
	{27'b0,DAC_BIAS_error,
	DAC_BIAS_test_done,DAC_BIAS_update_done,DAC_BIAS_init_done,DAC_BIAS_en}; 
//
assign w_DAC_BIAS_TO[0] = DAC_BIAS_en          ;
assign w_DAC_BIAS_TO[1] = DAC_BIAS_init_done   ;
assign w_DAC_BIAS_TO[2] = DAC_BIAS_update_done ;
assign w_DAC_BIAS_TO[3] = DAC_BIAS_test_done   ;
assign w_DAC_BIAS_TO[4] = DAC_BIAS_error       ;
assign w_DAC_BIAS_TO[31:5] = 27'b0;
// DAC AD5754 : DAC_A2A3 
//  master_spi_dac_AD5754__range_con
wire DAC_A2A3_en			= 				w_DAC_A2A3_CON[0];
wire DAC_A2A3_reset		= w_DAC_A2A3_TI[0];
wire DAC_A2A3_init		= w_DAC_A2A3_TI[1] | w_DAC_A2A3_CON[1];
wire DAC_A2A3_update		= w_DAC_A2A3_TI[2] | w_DAC_A2A3_CON[2];
wire DAC_A2A3_test		= w_DAC_A2A3_TI[3] | w_DAC_A2A3_CON[3];
wire [15:0] DAC_A2A3_conf =  				w_DAC_A2A3_CON[31:16];
//
wire DAC_A2A3_init_done	;
wire DAC_A2A3_update_done	;
wire DAC_A2A3_test_done	;
wire DAC_A2A3_error		;
//
wire [31:0] DAC_A2A3_DIN21 = w_DAC_A2A3_DIN21;
wire [31:0] DAC_A2A3_DIN43 = w_DAC_A2A3_DIN43;
//
wire [31:0] DAC_A2A3_RDBK21;
wire [31:0] DAC_A2A3_RDBK43;
//
master_spi_dac_AD5754__range_con master_spi_dac_AD5754_DAC_A2A3_inst ( 
	.clk		(clk_out3_10M), // assume 10MHz or 100ns
	.reset_n	(reset_n & ~DAC_A2A3_reset & ~w_rst_bias), // input
	.en			(DAC_A2A3_en), // input
	//
	.conf		(DAC_A2A3_conf), // 16 bits
	//
	.init		(DAC_A2A3_init), // input
	.update		(DAC_A2A3_update), // input
	.test		(DAC_A2A3_test), // input
    //
	.test_sdi_pdata	(w_DAC_A2A3_TEST_IN ), // input
	.test_sdo_pdata	(w_DAC_A2A3_TEST_OUT), // out 
	.readback_pdata (w_DAC_A2A3_TEST_RB1), // out
	.readback1_pdata(w_DAC_A2A3_TEST_RB2), // out
	// DAC input
	.DAC_VALUE_IN1	(DAC_A2A3_DIN21[15: 0]), // input
	.DAC_VALUE_IN2	(DAC_A2A3_DIN21[31:16]), // input
	.DAC_VALUE_IN3	(DAC_A2A3_DIN43[15: 0]), // input
	.DAC_VALUE_IN4	(DAC_A2A3_DIN43[31:16]), // input
    // DAC readback
	.DAC_READBACK1	(DAC_A2A3_RDBK21[15: 0]),
	.DAC_READBACK2	(DAC_A2A3_RDBK21[31:16]), 
	.DAC_READBACK3	(DAC_A2A3_RDBK43[15: 0]),
	.DAC_READBACK4	(DAC_A2A3_RDBK43[31:16]), 
    // DAC control
    .SCLK			(DAC_A2A3_SCLK	),
    .SYNC_N			(DAC_A2A3_SYNB	),
    .DIN			(DAC_A2A3_MOSI	),
    .SDO			(DAC_A2A3_MISO	), // input
	// GPIO
	.CLR_DAC_N		(), // ext pin // none
	.LOAD_DAC_N		(), // ext pin // none
	.B2C_DAC		(), // ext pin // none
    //
    .init_done		(DAC_A2A3_init_done	),
    .update_done	(DAC_A2A3_update_done	),
	.test_done		(DAC_A2A3_test_done	),
	.error			(DAC_A2A3_error		),
	.debug_out()
);
//
assign w_DAC_A2A3_RB21 = DAC_A2A3_RDBK21;
assign w_DAC_A2A3_RB43 = DAC_A2A3_RDBK43;
//
assign w_DAC_A2A3_FLAG = 
	{27'b0,DAC_A2A3_error,
	DAC_A2A3_test_done,DAC_A2A3_update_done,DAC_A2A3_init_done,DAC_A2A3_en}; 
//
assign w_DAC_A2A3_TO[0] = DAC_A2A3_en          ;
assign w_DAC_A2A3_TO[1] = DAC_A2A3_init_done   ;
assign w_DAC_A2A3_TO[2] = DAC_A2A3_update_done ;
assign w_DAC_A2A3_TO[3] = DAC_A2A3_test_done   ;
assign w_DAC_A2A3_TO[4] = DAC_A2A3_error       ;
assign w_DAC_A2A3_TO[31:5] = 27'b0;
////

/* ADC_HS */
//wire [31:0] w_ADC_HS_WI             = ep18wire;
wire [31:0] w_ADC_HS_WI = (w_mcs_ep_wi_en)? w_port_wi_18_1 : ep18wire;
//
(* keep = "true" *)
wire [31:0] w_ADC_HS_WO; 
assign ep38wire = w_ADC_HS_WO;
assign w_port_wo_38_1 = (w_mcs_ep_wo_en)? w_ADC_HS_WO : 32'hACAC_ACAC;
//
wire [31:0] w_ADC_HS_DOUT0;
wire [31:0] w_ADC_HS_DOUT1;
wire [31:0] w_ADC_HS_DOUT2;
wire [31:0] w_ADC_HS_DOUT3;
assign ep3Cwire = w_ADC_HS_DOUT0; 
assign ep3Dwire = w_ADC_HS_DOUT1;
assign ep3Ewire = w_ADC_HS_DOUT2;
assign ep3Fwire = w_ADC_HS_DOUT3;
assign w_port_wo_3C_1 = (w_mcs_ep_wo_en)? w_ADC_HS_DOUT0 : 32'hACAC_ACAC;
assign w_port_wo_3D_1 = (w_mcs_ep_wo_en)? w_ADC_HS_DOUT1 : 32'hACAC_ACAC;
assign w_port_wo_3E_1 = (w_mcs_ep_wo_en)? w_ADC_HS_DOUT2 : 32'hACAC_ACAC;
assign w_port_wo_3F_1 = (w_mcs_ep_wo_en)? w_ADC_HS_DOUT3 : 32'hACAC_ACAC;
//
wire [31:0] w_ADC_BASE_FREQ = ADC_BASE_FREQ;
assign ep39wire = w_ADC_BASE_FREQ;
assign w_port_wo_39_1 = (w_mcs_ep_wo_en)? w_ADC_BASE_FREQ : 32'hACAC_ACAC;
//
//wire [31:0] w_ADC_HS_TI             = ep58trig;
wire [31:0] w_ADC_HS_TI = (w_mcs_ep_ti_en)? w_port_ti_58_1 : ep58trig;
//
wire [31:0] w_ADC_HS_TO;
assign ep78trig = w_ADC_HS_TO;
assign w_port_to_78_1 = (w_mcs_ep_to_en)? w_ADC_HS_TO : 32'h0000_0000; //$$
//
wire [31:0] w_ADC_HS_DOUT0_PO;
wire [31:0] w_ADC_HS_DOUT1_PO;
wire [31:0] w_ADC_HS_DOUT2_PO;
wire [31:0] w_ADC_HS_DOUT3_PO;
//
assign epBCpipe = w_ADC_HS_DOUT0_PO; 
assign epBDpipe = w_ADC_HS_DOUT1_PO;
assign epBEpipe = w_ADC_HS_DOUT2_PO;
assign epBFpipe = w_ADC_HS_DOUT3_PO;
//
assign w_port_po_BC_1 = w_ADC_HS_DOUT0_PO; //$$
assign w_port_po_BD_1 = w_ADC_HS_DOUT1_PO; //$$
assign w_port_po_BE_1 = w_ADC_HS_DOUT2_PO; //$$
assign w_port_po_BF_1 = w_ADC_HS_DOUT3_PO; //$$
//
//$$wire w_ADC_HS_DOUT0_PO_RD = epBCrd;
//$$wire w_ADC_HS_DOUT1_PO_RD = epBDrd;
//$$wire w_ADC_HS_DOUT2_PO_RD = epBErd;
//$$wire w_ADC_HS_DOUT3_PO_RD = epBFrd;
wire w_ADC_HS_DOUT0_PO_RD = (w_mcs_ep_po_en)? w_rd_BC_1 : epBCrd;
wire w_ADC_HS_DOUT1_PO_RD = (w_mcs_ep_po_en)? w_rd_BD_1 : epBDrd;
wire w_ADC_HS_DOUT2_PO_RD = (w_mcs_ep_po_en)? w_rd_BE_1 : epBErd;
wire w_ADC_HS_DOUT3_PO_RD = (w_mcs_ep_po_en)? w_rd_BF_1 : epBFrd;

// fifo read clock switch 
//wire c_fifo_read = (w_mcs_ep_po_en)? clk3_out1_72M : okClk;
wire c_fifo_read;
// note clock mux
// BUFGMUX in https://www.xilinx.com/support/documentation/user_guides/ug472_7Series_Clocking.pdf
BUFGMUX bufgmux_c_fifo_read_inst (
	.O(c_fifo_read), 
	.I0(okClk), 
	.I1(clk3_out1_72M), 
	.S(w_mcs_ep_po_en) 
); 


// adc control 
(* keep = "true" *) wire      w_cnv_adc;
(* keep = "true" *) wire      w_clk_adc;
(* keep = "true" *) wire w_pin_test_adc;
(* keep = "true" *) wire  w_pin_dln_adc;
//
wire  w_dco_adc_0; // from pin ADC_00_DCO_N_ISO ANAL_CTRL35
wire w_dat2_adc_0; // from pin ADC_00_DA_P_ISO  ANAL_CTRL34
wire w_dat1_adc_0; // from pin ADC_00_DB_N_ISO  ANAL_CTRL33
//
wire  w_dco_adc_1;
wire w_dat2_adc_1;
wire w_dat1_adc_1;
//
wire  w_dco_adc_2;
wire w_dat2_adc_2;
wire w_dat1_adc_2;
//
wire  w_dco_adc_3;
wire w_dat2_adc_3;
wire w_dat1_adc_3;
//
(* keep = "true" *) wire w_hsadc_reset		= w_ADC_HS_TI[0]; // reset ADC control/serdes/fifo 
(* keep = "true" *) wire w_hsadc_init		= w_ADC_HS_TI[1] | w_ADC_HS_WI[1]; 
(* keep = "true" *) wire w_hsadc_update		= w_ADC_HS_TI[2] | w_ADC_HS_WI[2];
(* keep = "true" *) wire w_hsadc_test		= w_ADC_HS_TI[3] | w_ADC_HS_WI[3];
//
(* keep = "true" *) wire [17:0] fifo_adc0_din;
(* keep = "true" *) wire [17:0] fifo_adc1_din;
(* keep = "true" *) wire [17:0] fifo_adc2_din;
(* keep = "true" *) wire [17:0] fifo_adc3_din;
//
(* keep = "true" *) wire fifo_adc0_wr; // not used
(* keep = "true" *) wire fifo_adc1_wr; // not used
(* keep = "true" *) wire fifo_adc2_wr; // not used
(* keep = "true" *) wire fifo_adc3_wr; // not used
//
wire [17:0] fifo_adc0_dout	;
wire 		fifo_adc0_rd_en	;
wire 		fifo_adc0_valid	;
wire 		fifo_adc0_uflow	; 
wire 		fifo_adc0_pempty;
wire 		fifo_adc0_empty	;
wire 		fifo_adc0_wr_ack;
wire 		fifo_adc0_oflow	;
wire 		fifo_adc0_pfull	;
wire 		fifo_adc0_full	;
//
assign w_ADC_HS_WO[ 8] = fifo_adc0_pempty	;
assign w_ADC_HS_WO[ 9] = fifo_adc0_empty	;
assign w_ADC_HS_WO[10] = fifo_adc0_wr_ack	;
assign w_ADC_HS_WO[11] = fifo_adc0_oflow	;
assign w_ADC_HS_WO[12] = fifo_adc0_pfull	;
assign w_ADC_HS_WO[13] = fifo_adc0_full	;
//
wire [17:0] fifo_adc1_dout	;
wire 		fifo_adc1_rd_en	;
wire 		fifo_adc1_valid	;
wire 		fifo_adc1_uflow	;
wire 		fifo_adc1_pempty;
wire 		fifo_adc1_empty	;
wire 		fifo_adc1_wr_ack;
wire 		fifo_adc1_oflow	;
wire 		fifo_adc1_pfull	;
wire 		fifo_adc1_full	;
//
assign w_ADC_HS_WO[14] = fifo_adc1_pempty	;
assign w_ADC_HS_WO[15] = fifo_adc1_empty	;
assign w_ADC_HS_WO[16] = fifo_adc1_wr_ack	;
assign w_ADC_HS_WO[17] = fifo_adc1_oflow	;
assign w_ADC_HS_WO[18] = fifo_adc1_pfull	;
assign w_ADC_HS_WO[19] = fifo_adc1_full	;
//
wire [17:0] fifo_adc2_dout	;
wire 		fifo_adc2_rd_en	;
wire 		fifo_adc2_valid	;
wire 		fifo_adc2_uflow	;
wire 		fifo_adc2_pempty;
wire 		fifo_adc2_empty	;
wire 		fifo_adc2_wr_ack;
wire 		fifo_adc2_oflow	;
wire 		fifo_adc2_pfull	;
wire 		fifo_adc2_full	;
//
assign w_ADC_HS_WO[20] = fifo_adc2_pempty	;
assign w_ADC_HS_WO[21] = fifo_adc2_empty	;
assign w_ADC_HS_WO[22] = fifo_adc2_wr_ack	;
assign w_ADC_HS_WO[23] = fifo_adc2_oflow	;
assign w_ADC_HS_WO[24] = fifo_adc2_pfull	;
assign w_ADC_HS_WO[25] = fifo_adc2_full	;
//
wire [17:0] fifo_adc3_dout	;
wire 		fifo_adc3_rd_en	;
wire 		fifo_adc3_valid	;
wire 		fifo_adc3_uflow	;
wire 		fifo_adc3_pempty;
wire 		fifo_adc3_empty	;
wire 		fifo_adc3_wr_ack;
wire 		fifo_adc3_oflow	;
wire 		fifo_adc3_pfull	;
wire 		fifo_adc3_full	;
//
assign w_ADC_HS_WO[26] = fifo_adc3_pempty	;
assign w_ADC_HS_WO[27] = fifo_adc3_empty	;
assign w_ADC_HS_WO[28] = fifo_adc3_wr_ack	;
assign w_ADC_HS_WO[29] = fifo_adc3_oflow	;
assign w_ADC_HS_WO[30] = fifo_adc3_pfull	;
assign w_ADC_HS_WO[31] = fifo_adc3_full	;
//
wire w_hsadc_en		= w_ADC_HS_WI[0]; 
//
assign w_ADC_HS_DOUT0 = (w_hsadc_en)? {fifo_adc0_din,14'b0} : 32'b0;
assign w_ADC_HS_DOUT1 = (w_hsadc_en)? {fifo_adc1_din,14'b0} : 32'b0;
assign w_ADC_HS_DOUT2 = (w_hsadc_en)? {fifo_adc2_din,14'b0} : 32'b0;
assign w_ADC_HS_DOUT3 = (w_hsadc_en)? {fifo_adc3_din,14'b0} : 32'b0;
//
(* keep = "true" *) wire w_hsadc_error		; 
(* keep = "true" *) wire w_hsadc_init_done	; 
(* keep = "true" *) wire w_hsadc_update_done; 
(* keep = "true" *) wire w_hsadc_test_done	; 
//
assign w_ADC_HS_TO[ 0] = w_hsadc_en;
assign w_ADC_HS_TO[ 1] = w_hsadc_init_done	;
assign w_ADC_HS_TO[ 2] = w_hsadc_update_done;
assign w_ADC_HS_TO[ 3] = w_hsadc_test_done	;
assign w_ADC_HS_TO[ 4] = 1'b0;
assign w_ADC_HS_TO[ 5] = 1'b0;
assign w_ADC_HS_TO[ 6] = 1'b0;
assign w_ADC_HS_TO[ 7] = 1'b0;
assign w_ADC_HS_TO[ 8] = 1'b0;
assign w_ADC_HS_TO[ 9] = 1'b0;
assign w_ADC_HS_TO[10] = 1'b0;
assign w_ADC_HS_TO[11] = 1'b0;
assign w_ADC_HS_TO[12] = 1'b0;
assign w_ADC_HS_TO[13] = 1'b0;
assign w_ADC_HS_TO[14] = 1'b0;
assign w_ADC_HS_TO[15] = 1'b0;
assign w_ADC_HS_TO[16] = 1'b0;
assign w_ADC_HS_TO[17] = 1'b0;
assign w_ADC_HS_TO[18] = 1'b0;
assign w_ADC_HS_TO[19] = 1'b0;
//
assign w_ADC_HS_TO[20] = fifo_adc0_pempty	;
assign w_ADC_HS_TO[21] = fifo_adc0_empty	;
assign w_ADC_HS_TO[22] = fifo_adc0_pfull	;
assign w_ADC_HS_TO[23] = fifo_adc1_pempty	;
assign w_ADC_HS_TO[24] = fifo_adc1_empty	;
assign w_ADC_HS_TO[25] = fifo_adc1_pfull	;
assign w_ADC_HS_TO[26] = fifo_adc2_pempty	;
assign w_ADC_HS_TO[27] = fifo_adc2_empty	;
assign w_ADC_HS_TO[28] = fifo_adc2_pfull	;
assign w_ADC_HS_TO[29] = fifo_adc3_pempty	;
assign w_ADC_HS_TO[30] = fifo_adc3_empty	;
assign w_ADC_HS_TO[31] = fifo_adc3_pfull	;
//
assign  w_ADC_HS_WO[0] = w_hsadc_en & ~w_hsadc_error;
assign  w_ADC_HS_WO[1] = w_hsadc_init_done	;
assign  w_ADC_HS_WO[2] = w_hsadc_update_done;
assign  w_ADC_HS_WO[3] = w_hsadc_test_done	;
//
assign  w_ADC_HS_WO[7:4] = 4'b0; // not used
//
//wire [31:0] w_ADC_HS_UPD_SMP      = ep1Dwire; // loaded by init // 100
//wire [31:0] w_ADC_HS_SMP_PRD      = ep1Ewire; // loaded by init // 96ns/8ns = 12 ... 1/(96ns)=10.4167Msps
//wire [31:0] w_ADC_HS_DLY_TAP_OPT  = ep1Fwire; // loaded by init 
wire [31:0] w_ADC_HS_UPD_SMP     = (w_mcs_ep_wi_en)? w_port_wi_1D_1 : ep1Dwire;
wire [31:0] w_ADC_HS_SMP_PRD     = (w_mcs_ep_wi_en)? w_port_wi_1E_1 : ep1Ewire;
wire [31:0] w_ADC_HS_DLY_TAP_OPT = (w_mcs_ep_wi_en)? w_port_wi_1F_1 : ep1Fwire;
//
wire [9:0] w_in_delay_tap_serdes0 = w_ADC_HS_DLY_TAP_OPT[21:12]; // {ep1Fwire[31:27],ep1Fwire[26:22]} = {5'd15,5'd15}
wire [9:0] w_in_delay_tap_serdes1 = w_ADC_HS_DLY_TAP_OPT[31:22];
wire [9:0] w_in_delay_tap_serdes2 = w_ADC_HS_DLY_TAP_OPT[21:12]; // share
wire [9:0] w_in_delay_tap_serdes3 = w_ADC_HS_DLY_TAP_OPT[31:22]; // share
//
wire w_pin_test_frc_high = w_ADC_HS_DLY_TAP_OPT[0]; // loaded by init
wire w_pin_dlln_frc_low  = w_ADC_HS_DLY_TAP_OPT[1]; // loaded by init
wire w_pttn_cnt_up_en    = w_ADC_HS_DLY_TAP_OPT[2]; // loaded by init
//
control_adc_ddr_two_lane_LTC2387_reg_serdes_quad #( //$$ TODO: adc rev
	//.PERIOD_CLK_LOGIC_NS (8 ), // ns // for 125MHz @ clk_logic
	//.PERIOD_CLK_LOGIC_NS (5 ), // ns // for 200MHz @ clk_logic
	.PERIOD_CLK_LOGIC_NS (4.76190476), // ns // for 210MHz @ clk_logic
	//.PERIOD_CLK_LOGIC_NS (4 ), // ns // for 250MHz @ clk_logic
	//
	//.DELAY_CLK (9), // 65ns min < 8ns*9=72ns @125MHz
	//.DELAY_CLK (14), // 65ns min < 5ns*14=70ns @200MHz
	.DELAY_CLK (14), // 65ns min < 4.76ns*14=66.7ns @210MHz
	//.DELAY_CLK (17), // 65ns min < 4ns*17=68ns @250MHz
	//
	.DAT1_OUTPUT_POLARITY(4'b0010), // ADC_01 inversion / PN swap 
	.DAT2_OUTPUT_POLARITY(4'b0010), // ADC_01 inversion / PN swap 
	.DCLK_OUTPUT_POLARITY(4'b0010), // ADC_01 inversion / PN swap 
	.MODE_ADC_CONTROL    (4'b0011) // enable adc1 adc0
	)  control_hsadc_quad_inst(
	.clk			(clk_out3_10M), // assume 10MHz or 100ns
	.reset_n		(reset_n & ~w_hsadc_reset & ~w_rst_adc),
	.en				(w_hsadc_en), //
	//
	//.clk_logic		(clk_out4_125M), 
	.clk_logic		(clk2_out1_210M), 
	//.clk_logic		(clk_out1_200M), 
	//
	.clk_fifo		(clk2_out3_60M), // for fifo write
	//$$.clk_bus		(okClk),         // for fifo read //$$ should be shared with MCS1
	.clk_bus		(c_fifo_read  ), // for fifo read //$$ should be shared with MCS1 
	.clk_ref_200M	(clk_out1_200M), // for serdes reference
	//
	.init			(w_hsadc_init	), //
	.update			(w_hsadc_update	), //
	.test			(w_hsadc_test	), //
	//
	.i_num_update_samples		(w_ADC_HS_UPD_SMP), 
	.i_sampling_period_count	(w_ADC_HS_SMP_PRD), 
	//
	.i_in_delay_tap_serdes0		(w_in_delay_tap_serdes0), // ({5'd15,5'd15}),
	.i_in_delay_tap_serdes1		(w_in_delay_tap_serdes1), // ({5'd15,5'd15}),
	.i_in_delay_tap_serdes2		(w_in_delay_tap_serdes2), // ({5'd15,5'd15}),
	.i_in_delay_tap_serdes3		(w_in_delay_tap_serdes3), // ({5'd15,5'd15}),
	//
	.i_pin_test_frc_high		(w_pin_test_frc_high ),
	.i_pin_dlln_frc_low 		(w_pin_dlln_frc_low  ),
	.i_pttn_cnt_up_en   		(w_pttn_cnt_up_en    ),
	//
	.o_cnv_adc				(     w_cnv_adc), //
	.o_clk_adc				(     w_clk_adc), //
	.o_pin_test_adc			(w_pin_test_adc), //
	.o_pin_duallane_adc		( w_pin_dln_adc), //
	//
	 .i_clk_in_adc0		(  w_dco_adc_0),
	.i_data_in_adc0		({w_dat2_adc_0,w_dat1_adc_0}),
	 .i_clk_in_adc1		(  w_dco_adc_1),
	.i_data_in_adc1		({w_dat2_adc_1,w_dat1_adc_1}),
	 .i_clk_in_adc2		(  w_dco_adc_2),
	.i_data_in_adc2		({w_dat2_adc_2,w_dat1_adc_2}),
	 .i_clk_in_adc3		(  w_dco_adc_3),
	.i_data_in_adc3		({w_dat2_adc_3,w_dat1_adc_3}),
	//
	 .o_data_in_fifo_0	(fifo_adc0_din		), // monitor
	 .o_data_in_fifo_1	(fifo_adc1_din		), // monitor
	 .o_data_in_fifo_2	(fifo_adc2_din		), // monitor
	 .o_data_in_fifo_3	(fifo_adc3_din		), // monitor
	 //
	      .o_wr_fifo_0	(fifo_adc0_wr		), // monitor
	      .o_wr_fifo_1	(fifo_adc1_wr		), // monitor
	      .o_wr_fifo_2	(fifo_adc2_wr		), // monitor
	      .o_wr_fifo_3	(fifo_adc3_wr		), // monitor
	 //
	.o_data_out_fifo_0	(fifo_adc0_dout		), // to usb endpoint
	      .i_rd_fifo_0	(fifo_adc0_rd_en	), // to usb endpoint
	   .o_valid_fifo_0	(fifo_adc0_valid	),
	   .o_uflow_fifo_0	(fifo_adc0_uflow	),
	  .o_pempty_fifo_0	(fifo_adc0_pempty	),
	   .o_empty_fifo_0	(fifo_adc0_empty	),
	  .o_wr_ack_fifo_0	(fifo_adc0_wr_ack	),
	   .o_oflow_fifo_0	(fifo_adc0_oflow	),
	   .o_pfull_fifo_0	(fifo_adc0_pfull	),
	    .o_full_fifo_0	(fifo_adc0_full		),
	//
	.o_data_out_fifo_1	(fifo_adc1_dout		), // to usb endpoint
	      .i_rd_fifo_1	(fifo_adc1_rd_en	), // to usb endpoint
	   .o_valid_fifo_1	(fifo_adc1_valid	),
	   .o_uflow_fifo_1	(fifo_adc1_uflow	),
	  .o_pempty_fifo_1	(fifo_adc1_pempty	),
	   .o_empty_fifo_1	(fifo_adc1_empty	),
	  .o_wr_ack_fifo_1	(fifo_adc1_wr_ack	),
	   .o_oflow_fifo_1	(fifo_adc1_oflow	),
	   .o_pfull_fifo_1	(fifo_adc1_pfull	),
	    .o_full_fifo_1	(fifo_adc1_full		),
	//
	.o_data_out_fifo_2	(fifo_adc2_dout		), // to usb endpoint
	      .i_rd_fifo_2	(fifo_adc2_rd_en	), // to usb endpoint
	   .o_valid_fifo_2	(fifo_adc2_valid	),
	   .o_uflow_fifo_2	(fifo_adc2_uflow	),
	  .o_pempty_fifo_2	(fifo_adc2_pempty	),
	   .o_empty_fifo_2	(fifo_adc2_empty	),
	  .o_wr_ack_fifo_2	(fifo_adc2_wr_ack	),
	   .o_oflow_fifo_2	(fifo_adc2_oflow	),
	   .o_pfull_fifo_2	(fifo_adc2_pfull	),
	    .o_full_fifo_2	(fifo_adc2_full		),
	//
	.o_data_out_fifo_3	(fifo_adc3_dout		), // to usb endpoint
	      .i_rd_fifo_3	(fifo_adc3_rd_en	), // to usb endpoint
	   .o_valid_fifo_3	(fifo_adc3_valid	),
	   .o_uflow_fifo_3	(fifo_adc3_uflow	),
	  .o_pempty_fifo_3	(fifo_adc3_pempty	),
	   .o_empty_fifo_3	(fifo_adc3_empty	),
	  .o_wr_ack_fifo_3	(fifo_adc3_wr_ack	),
	   .o_oflow_fifo_3	(fifo_adc3_oflow	),
	   .o_pfull_fifo_3	(fifo_adc3_pfull	),
	    .o_full_fifo_3	(fifo_adc3_full		),
	//
	.init_done		(w_hsadc_init_done	),  //
	.update_done	(w_hsadc_update_done),  //
	.test_done		(w_hsadc_test_done	),  //
	.error			(w_hsadc_error),        //
	.debug_out		()
);
//
assign w_ADC_HS_DOUT0_PO = {fifo_adc0_dout,14'b0}; // 18-bit fifo - 32-bit end-point
assign fifo_adc0_rd_en = w_ADC_HS_DOUT0_PO_RD;
//
assign w_ADC_HS_DOUT1_PO = {fifo_adc1_dout,14'b0}; // 18-bit fifo - 32-bit end-point
assign fifo_adc1_rd_en = w_ADC_HS_DOUT1_PO_RD;
//
assign w_ADC_HS_DOUT2_PO = {fifo_adc2_dout,14'b0}; // 18-bit fifo - 32-bit end-point
assign fifo_adc2_rd_en = w_ADC_HS_DOUT2_PO_RD;
//
assign w_ADC_HS_DOUT3_PO = {fifo_adc3_dout,14'b0}; // 18-bit fifo - 32-bit end-point
assign fifo_adc3_rd_en = w_ADC_HS_DOUT3_PO_RD;
////

// TEST end-point
//wire [31:0] w_TEST_CON = ep01wire;
wire [31:0] w_TEST_CON = (w_mcs_ep_wi_en)? w_port_wi_01_1 : ep01wire;
//
wire [31:0] w_TEST_OUT;
assign ep21wire = w_TEST_OUT; // TEST_OUT 
assign w_port_wo_21_1 = (w_mcs_ep_wo_en)? w_TEST_OUT : 32'hACAC_ACAC;
//
//wire [31:0] w_TEST_TI = ep40trig; //$$ 
wire [31:0] w_TEST_TI = (w_mcs_ep_ti_en)? w_port_ti_40_1 : ep40trig;
//
wire [31:0] w_TEST_TO;
assign ep60trig = w_TEST_TO; 
assign w_port_to_60_1 = (w_mcs_ep_to_en)? w_TEST_TO : 32'h0000_0000; //$$
// Counter 1:
assign reset1     = w_TEST_CON[0]; 
assign disable1   = w_TEST_CON[1]; 
assign autocount2 = w_TEST_CON[2]; 
//
assign w_TEST_OUT[15:0] = {count2[7:0], count1[7:0]}; 
// Counter 2:
assign reset2     = w_TEST_TI[0];
assign up2        = w_TEST_TI[1];
assign down2      = w_TEST_TI[2];
//
assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
////

// LED drive
function [7:0] xem7310_led;
input [7:0] a;
integer i;
begin
	for(i=0; i<8; i=i+1) begin: u
		xem7310_led[i] = (a[i]==1'b1) ? (1'b0) : (1'bz);
	end
end
endfunction
// 
assign led = xem7310_led(r_test ^ count1);
////

// Counter #1
// + Counting using a divided sysclk.
// + Reset sets the counter to 0.
// + Disable turns off the counter.
always @(posedge sys_clk, negedge reset_n) begin
	if (!reset_n) begin
		div1 <= 32'd0; 
		clk1div <= 1'b0;
		count1 <= 8'h00;
		count1eq00 <= 1'b0;
		count1eq80 <= 1'b0;
	end
	else begin
		//
		if (div1 == 32'h000000) begin
			// div1 <= 24'h400000;
			//div1 <= 32'h0BEBC200-1; // 0x0BEBC200 for 1sec @ 200MHz
			div1 <= 32'd010_000_000-1; // 1sec @ 10MHz
			clk1div <= 1'b1;
		end else begin
			div1 <= div1 - 1;
			clk1div <= 1'b0;
		end
		//
		if (clk1div == 1'b1) begin
			if (reset1 == 1'b1) begin
				count1 <= 8'h00;
			end else if (disable1 == 1'b0) begin
				count1 <= count1 + 8'd1;
			end
		end
		//
		if (count1 == 8'h00)
			count1eq00 <= 1'b1;
		else
			count1eq00 <= 1'b0;
		//
		if (count1 == 8'h80)
			count1eq80 <= 1'b1;
		else
			count1eq80 <= 1'b0;
		//
		end
end
////

// Counter #2
// + Reset, up, and down control counter.
// + If autocount is enabled, a divided sys_clk can also
//   upcount.
always @(posedge sys_clk, negedge reset_n) begin
	if (!reset_n) begin
		div2 <= 24'd0; 
		clk2div <= 1'b0;
		count2 <= 8'h00;
		r_test <= 8'h80;
		count2eqFF <= 1'b0;
		end
	else begin
		//
		if (div2 == 24'h000000) begin
			div2 <= 24'h100000;
			clk2div <= 1'b1;
		end else begin
			div2 <= div2 - 1;
			clk2div <= 1'b0;
		end
		//
		if (reset2 == 1'b1) begin
			count2 <= 8'h00;
			r_test <= 8'h80;
			end
		else if (up2 == 1'b1)
			count2 <= count2 + 8'd1;
		else if (down2 == 1'b1)
			count2 <= count2 - 1;
		else if ((autocount2 == 1'b1) && (clk2div == 1'b1)) begin 
			count2 <= count2 + 8'd1;
			//r_test <= {r_test[6:0], r_test[7]}; // circular left
			r_test <= {r_test[0], r_test[7:1]}; // circular right
			end
		//
		if (count2 == 8'hff)
			count2eqFF <= 1'b1;
		else
			count2eqFF <= 1'b0;
		//
		end
end
////


/* pulse loopback test */
//
//wire [31:0] w_TEST_TI_HS = ep41trig;
wire [31:0] w_TEST_TI_HS = (w_mcs_ep_ti_en)? w_port_ti_41_1 : ep41trig;
//
(* keep = "true" *) wire w_pulse_out;
(* keep = "true" *) wire w_pulse_loopback_in;
//
wire test_clk = clk2_out1_210M;
//
(* keep = "true" *) wire w_enable__pulse_loopback = w_TEST_CON[7];
//
(* keep = "true" *) wire w_trig__pulse_shot = w_TEST_TI_HS[1] | w_TEST_TI[8];
//
(* keep = "true" *) reg [7:0] r_cnt__pulse_period;
(* keep = "true" *) reg [7:0] r_cnt__pulse_width;
(* keep = "true" *) reg [7:0] r_cnt__pulse_num;
//
(* keep = "true" *) wire [7:0] w_set__pulse_period = w_TEST_CON[31:24];
(* keep = "true" *) wire [7:0] w_set__pulse_width  = w_TEST_CON[23:16];
(* keep = "true" *) wire [7:0] w_set__pulse_num    = w_TEST_CON[15:8];
//
reg r_pulse_out;
	assign w_pulse_out = r_pulse_out;
//
always @(posedge test_clk, negedge reset_n) begin
	if (!reset_n) begin
		r_pulse_out         <= 1'b0; 
		r_cnt__pulse_period <= 8'b0; 
		r_cnt__pulse_width  <= 8'b0; 
		r_cnt__pulse_num    <= 8'b0; 
		end
	else begin
		//
		if (!w_enable__pulse_loopback) begin
			r_pulse_out         <= 1'b0; 
			r_cnt__pulse_period <= 8'b0; 
			r_cnt__pulse_width  <= 8'b0; 
			r_cnt__pulse_num    <= 8'b0; 
			end
		else if (w_trig__pulse_shot) begin 
			r_pulse_out         <= 1'b0; 
			r_cnt__pulse_period <= (w_set__pulse_period==0)? r_cnt__pulse_period : w_set__pulse_period; 
			r_cnt__pulse_width  <= (w_set__pulse_width ==0)? r_cnt__pulse_width  : w_set__pulse_width ; 
			r_cnt__pulse_num    <= (w_set__pulse_num   ==0)? r_cnt__pulse_num    : w_set__pulse_num   ; 
			end
		else begin 
			if (r_cnt__pulse_width  >0)
				r_pulse_out         <= 1'b1; 
			else 
				r_pulse_out         <= 1'b0; 
			//
			if (r_cnt__pulse_period >1)
				r_cnt__pulse_period <= r_cnt__pulse_period - 1; 
			else if (r_cnt__pulse_period == 1 && r_cnt__pulse_num >1)
				r_cnt__pulse_period <= w_set__pulse_period; 
			else 
				r_cnt__pulse_period <= 8'b0;
			//
			if (r_cnt__pulse_width  >1)
				r_cnt__pulse_width  <= r_cnt__pulse_width  - 1; 
			else if (r_cnt__pulse_period == 1 && r_cnt__pulse_num >1)
				r_cnt__pulse_width  <= w_set__pulse_width ; 
			else 
				r_cnt__pulse_width <= 8'b0;
			//
			if (r_cnt__pulse_period == 1 && r_cnt__pulse_num >0)
				r_cnt__pulse_num    <= r_cnt__pulse_num    - 1; 
			end
		//
		end
end
//
(* keep = "true" *) reg [15:0] r_cnt__pulse_loopback;
	assign w_TEST_OUT[31:16] = r_cnt__pulse_loopback; 
//
//(* keep = "true" *) wire w_reset__pulse_loopback_cnt = w_TEST_TI_125M[0] | w_TEST_TI[9];
(* keep = "true" *) wire w_reset__pulse_loopback_cnt = w_TEST_TI_HS[0] | w_TEST_TI[9];
//
reg [1:0] r_smp__pulse_loopback_in;
wire w_rise__pulse_loopback_in = ~r_smp__pulse_loopback_in[1]&r_smp__pulse_loopback_in[0];
//
always @(posedge test_clk, negedge reset_n) begin
	if (!reset_n) begin
		r_smp__pulse_loopback_in <= 2'b0;
		r_cnt__pulse_loopback    <= 16'd0; 
		end
	else begin
		//
		r_smp__pulse_loopback_in <= {r_smp__pulse_loopback_in[0], w_pulse_loopback_in};
		//
		if (w_reset__pulse_loopback_cnt) begin
			r_cnt__pulse_loopback <= 16'd0; 
			end
		else if (w_rise__pulse_loopback_in) begin
			r_cnt__pulse_loopback <= r_cnt__pulse_loopback + 16'd1;
			end 
		//
		end
end
////


/* okHost wrapper */
//$$ Endpoints
// Wire In 		0x00 - 0x1F
// Wire Out 	0x20 - 0x3F
// Trigger In 	0x40 - 0x5F
// Trigger Out 	0x60 - 0x7F
// Pipe In 		0x80 - 0x9F
// Pipe Out 	0xA0 - 0xBF
ok_endpoint_wrapper  ok_endpoint_wrapper_inst (
	.okUH (okUH ), //input  wire [4:0]   okUH, // external pins
	.okHU (okHU ), //output wire [2:0]   okHU, // external pins
	.okUHU(okUHU), //inout  wire [31:0]  okUHU, // external pins
	.okAA (okAA ), //inout  wire         okAA, // external pin
	// Wire In 		0x00 - 0x1F
	.ep00wire(ep00wire), // output wire [31:0]
	.ep01wire(ep01wire), // output wire [31:0]
	.ep02wire(ep02wire), // output wire [31:0]
	.ep03wire(ep03wire), // output wire [31:0]
	.ep04wire(ep04wire), // output wire [31:0]
	.ep05wire(ep05wire), // output wire [31:0]
	.ep06wire(ep06wire), // output wire [31:0]
	.ep07wire(ep07wire), // output wire [31:0]
	.ep08wire(ep08wire), // output wire [31:0]
	.ep09wire(ep09wire), // output wire [31:0]
	.ep0Awire(ep0Awire), // output wire [31:0]
	.ep0Bwire(ep0Bwire), // output wire [31:0]
	.ep0Cwire(ep0Cwire), // output wire [31:0]
	.ep0Dwire(ep0Dwire), // output wire [31:0]
	.ep0Ewire(ep0Ewire), // output wire [31:0]
	.ep0Fwire(ep0Fwire), // output wire [31:0]
	.ep10wire(ep10wire), // output wire [31:0]
	.ep11wire(ep11wire), // output wire [31:0]
	.ep12wire(ep12wire), // output wire [31:0]
	.ep13wire(ep13wire), // output wire [31:0]
	.ep14wire(ep14wire), // output wire [31:0]
	.ep15wire(ep15wire), // output wire [31:0]
	.ep16wire(ep16wire), // output wire [31:0]
	.ep17wire(ep17wire), // output wire [31:0]
	.ep18wire(ep18wire), // output wire [31:0]
	.ep19wire(ep19wire), // output wire [31:0]
	.ep1Awire(ep1Awire), // output wire [31:0]
	.ep1Bwire(ep1Bwire), // output wire [31:0]
	.ep1Cwire(ep1Cwire), // output wire [31:0]
	.ep1Dwire(ep1Dwire), // output wire [31:0]
	.ep1Ewire(ep1Ewire), // output wire [31:0]
	.ep1Fwire(ep1Fwire), // output wire [31:0]
	// Wire Out 	0x20 - 0x3F
	.ep20wire(ep20wire), // input wire [31:0]
	.ep21wire(ep21wire), // input wire [31:0]
	.ep22wire(ep22wire), // input wire [31:0]
	.ep23wire(ep23wire), // input wire [31:0]
	.ep24wire(ep24wire), // input wire [31:0]
	.ep25wire(ep25wire), // input wire [31:0]
	.ep26wire(ep26wire), // input wire [31:0]
	.ep27wire(ep27wire), // input wire [31:0]
	.ep28wire(ep28wire), // input wire [31:0]
	.ep29wire(ep29wire), // input wire [31:0]
	.ep2Awire(ep2Awire), // input wire [31:0]
	.ep2Bwire(ep2Bwire), // input wire [31:0]
	.ep2Cwire(ep2Cwire), // input wire [31:0]
	.ep2Dwire(ep2Dwire), // input wire [31:0]
	.ep2Ewire(ep2Ewire), // input wire [31:0]
	.ep2Fwire(ep2Fwire), // input wire [31:0]
	.ep30wire(ep30wire), // input wire [31:0]
	.ep31wire(ep31wire), // input wire [31:0]
	.ep32wire(ep32wire), // input wire [31:0]
	.ep33wire(ep33wire), // input wire [31:0]
	.ep34wire(ep34wire), // input wire [31:0]
	.ep35wire(ep35wire), // input wire [31:0]
	.ep36wire(ep36wire), // input wire [31:0]
	.ep37wire(ep37wire), // input wire [31:0]
	.ep38wire(ep38wire), // input wire [31:0]
	.ep39wire(ep39wire), // input wire [31:0]
	.ep3Awire(ep3Awire), // input wire [31:0]
	.ep3Bwire(ep3Bwire), // input wire [31:0]
	.ep3Cwire(ep3Cwire), // input wire [31:0]
	.ep3Dwire(ep3Dwire), // input wire [31:0]
	.ep3Ewire(ep3Ewire), // input wire [31:0]
	.ep3Fwire(ep3Fwire), // input wire [31:0]
	// Trigger In 	0x40 - 0x5F
	.ep40ck(ep40ck), .ep40trig(ep40trig), // input wire, output wire [31:0],
	.ep41ck(ep41ck), .ep41trig(ep41trig), // input wire, output wire [31:0],
	.ep42ck(ep42ck), .ep42trig(ep42trig), // input wire, output wire [31:0],
	.ep43ck(ep43ck), .ep43trig(ep43trig), // input wire, output wire [31:0],
	.ep44ck(ep44ck), .ep44trig(ep44trig), // input wire, output wire [31:0],
	.ep45ck(ep45ck), .ep45trig(ep45trig), // input wire, output wire [31:0],
	.ep46ck(ep46ck), .ep46trig(ep46trig), // input wire, output wire [31:0],
	.ep47ck(ep47ck), .ep47trig(ep47trig), // input wire, output wire [31:0],
	.ep48ck(ep48ck), .ep48trig(ep48trig), // input wire, output wire [31:0],
	.ep49ck(ep49ck), .ep49trig(ep49trig), // input wire, output wire [31:0],
	.ep4Ack(ep4Ack), .ep4Atrig(ep4Atrig), // input wire, output wire [31:0],
	.ep4Bck(ep4Bck), .ep4Btrig(ep4Btrig), // input wire, output wire [31:0],
	.ep4Cck(ep4Cck), .ep4Ctrig(ep4Ctrig), // input wire, output wire [31:0],
	.ep4Dck(ep4Dck), .ep4Dtrig(ep4Dtrig), // input wire, output wire [31:0],
	.ep4Eck(ep4Eck), .ep4Etrig(ep4Etrig), // input wire, output wire [31:0],
	.ep4Fck(ep4Fck), .ep4Ftrig(ep4Ftrig), // input wire, output wire [31:0],
	.ep50ck(ep50ck), .ep50trig(ep50trig), // input wire, output wire [31:0],
	.ep51ck(ep51ck), .ep51trig(ep51trig), // input wire, output wire [31:0],
	.ep52ck(ep52ck), .ep52trig(ep52trig), // input wire, output wire [31:0],
	.ep53ck(ep53ck), .ep53trig(ep53trig), // input wire, output wire [31:0],
	.ep54ck(ep54ck), .ep54trig(ep54trig), // input wire, output wire [31:0],
	.ep55ck(ep55ck), .ep55trig(ep55trig), // input wire, output wire [31:0],
	.ep56ck(ep56ck), .ep56trig(ep56trig), // input wire, output wire [31:0],
	.ep57ck(ep57ck), .ep57trig(ep57trig), // input wire, output wire [31:0],
	.ep58ck(ep58ck), .ep58trig(ep58trig), // input wire, output wire [31:0],
	.ep59ck(ep59ck), .ep59trig(ep59trig), // input wire, output wire [31:0],
	.ep5Ack(ep5Ack), .ep5Atrig(ep5Atrig), // input wire, output wire [31:0],
	.ep5Bck(ep5Bck), .ep5Btrig(ep5Btrig), // input wire, output wire [31:0],
	.ep5Cck(ep5Cck), .ep5Ctrig(ep5Ctrig), // input wire, output wire [31:0],
	.ep5Dck(ep5Dck), .ep5Dtrig(ep5Dtrig), // input wire, output wire [31:0],
	.ep5Eck(ep5Eck), .ep5Etrig(ep5Etrig), // input wire, output wire [31:0],
	.ep5Fck(ep5Fck), .ep5Ftrig(ep5Ftrig), // input wire, output wire [31:0],
	// Trigger Out 	0x60 - 0x7F
	.ep60ck(ep60ck), .ep60trig(ep60trig), // input wire, input wire [31:0],
	.ep61ck(ep61ck), .ep61trig(ep61trig), // input wire, input wire [31:0],
	.ep62ck(ep62ck), .ep62trig(ep62trig), // input wire, input wire [31:0],
	.ep63ck(ep63ck), .ep63trig(ep63trig), // input wire, input wire [31:0],
	.ep64ck(ep64ck), .ep64trig(ep64trig), // input wire, input wire [31:0],
	.ep65ck(ep65ck), .ep65trig(ep65trig), // input wire, input wire [31:0],
	.ep66ck(ep66ck), .ep66trig(ep66trig), // input wire, input wire [31:0],
	.ep67ck(ep67ck), .ep67trig(ep67trig), // input wire, input wire [31:0],
	.ep68ck(ep68ck), .ep68trig(ep68trig), // input wire, input wire [31:0],
	.ep69ck(ep69ck), .ep69trig(ep69trig), // input wire, input wire [31:0],
	.ep6Ack(ep6Ack), .ep6Atrig(ep6Atrig), // input wire, input wire [31:0],
	.ep6Bck(ep6Bck), .ep6Btrig(ep6Btrig), // input wire, input wire [31:0],
	.ep6Cck(ep6Cck), .ep6Ctrig(ep6Ctrig), // input wire, input wire [31:0],
	.ep6Dck(ep6Dck), .ep6Dtrig(ep6Dtrig), // input wire, input wire [31:0],
	.ep6Eck(ep6Eck), .ep6Etrig(ep6Etrig), // input wire, input wire [31:0],
	.ep6Fck(ep6Fck), .ep6Ftrig(ep6Ftrig), // input wire, input wire [31:0],
	.ep70ck(ep70ck), .ep70trig(ep70trig), // input wire, input wire [31:0],
	.ep71ck(ep71ck), .ep71trig(ep71trig), // input wire, input wire [31:0],
	.ep72ck(ep72ck), .ep72trig(ep72trig), // input wire, input wire [31:0],
	.ep73ck(ep73ck), .ep73trig(ep73trig), // input wire, input wire [31:0],
	.ep74ck(ep74ck), .ep74trig(ep74trig), // input wire, input wire [31:0],
	.ep75ck(ep75ck), .ep75trig(ep75trig), // input wire, input wire [31:0],
	.ep76ck(ep76ck), .ep76trig(ep76trig), // input wire, input wire [31:0],
	.ep77ck(ep77ck), .ep77trig(ep77trig), // input wire, input wire [31:0],
	.ep78ck(ep78ck), .ep78trig(ep78trig), // input wire, input wire [31:0],
	.ep79ck(ep79ck), .ep79trig(ep79trig), // input wire, input wire [31:0],
	.ep7Ack(ep7Ack), .ep7Atrig(ep7Atrig), // input wire, input wire [31:0],
	.ep7Bck(ep7Bck), .ep7Btrig(ep7Btrig), // input wire, input wire [31:0],
	.ep7Cck(ep7Cck), .ep7Ctrig(ep7Ctrig), // input wire, input wire [31:0],
	.ep7Dck(ep7Dck), .ep7Dtrig(ep7Dtrig), // input wire, input wire [31:0],
	.ep7Eck(ep7Eck), .ep7Etrig(ep7Etrig), // input wire, input wire [31:0],
	.ep7Fck(ep7Fck), .ep7Ftrig(ep7Ftrig), // input wire, input wire [31:0],
	// Pipe In 		0x80 - 0x9F
	.ep80wr(ep80wr), .ep80pipe(ep80pipe), // output wire, output wire [31:0],
	.ep81wr(ep81wr), .ep81pipe(ep81pipe), // output wire, output wire [31:0],
	.ep82wr(ep82wr), .ep82pipe(ep82pipe), // output wire, output wire [31:0],
	.ep83wr(ep83wr), .ep83pipe(ep83pipe), // output wire, output wire [31:0],
	.ep84wr(ep84wr), .ep84pipe(ep84pipe), // output wire, output wire [31:0],
	.ep85wr(ep85wr), .ep85pipe(ep85pipe), // output wire, output wire [31:0],
	.ep86wr(ep86wr), .ep86pipe(ep86pipe), // output wire, output wire [31:0],
	.ep87wr(ep87wr), .ep87pipe(ep87pipe), // output wire, output wire [31:0],
	.ep88wr(ep88wr), .ep88pipe(ep88pipe), // output wire, output wire [31:0],
	.ep89wr(ep89wr), .ep89pipe(ep89pipe), // output wire, output wire [31:0],
	.ep8Awr(ep8Awr), .ep8Apipe(ep8Apipe), // output wire, output wire [31:0],
	.ep8Bwr(ep8Bwr), .ep8Bpipe(ep8Bpipe), // output wire, output wire [31:0],
	.ep8Cwr(ep8Cwr), .ep8Cpipe(ep8Cpipe), // output wire, output wire [31:0],
	.ep8Dwr(ep8Dwr), .ep8Dpipe(ep8Dpipe), // output wire, output wire [31:0],
	.ep8Ewr(ep8Ewr), .ep8Epipe(ep8Epipe), // output wire, output wire [31:0],
	.ep8Fwr(ep8Fwr), .ep8Fpipe(ep8Fpipe), // output wire, output wire [31:0],
	.ep90wr(ep90wr), .ep90pipe(ep90pipe), // output wire, output wire [31:0],
	.ep91wr(ep91wr), .ep91pipe(ep91pipe), // output wire, output wire [31:0],
	.ep92wr(ep92wr), .ep92pipe(ep92pipe), // output wire, output wire [31:0],
	.ep93wr(ep93wr), .ep93pipe(ep93pipe), // output wire, output wire [31:0],
	.ep94wr(ep94wr), .ep94pipe(ep94pipe), // output wire, output wire [31:0],
	.ep95wr(ep95wr), .ep95pipe(ep95pipe), // output wire, output wire [31:0],
	.ep96wr(ep96wr), .ep96pipe(ep96pipe), // output wire, output wire [31:0],
	.ep97wr(ep97wr), .ep97pipe(ep97pipe), // output wire, output wire [31:0],
	.ep98wr(ep98wr), .ep98pipe(ep98pipe), // output wire, output wire [31:0],
	.ep99wr(ep99wr), .ep99pipe(ep99pipe), // output wire, output wire [31:0],
	.ep9Awr(ep9Awr), .ep9Apipe(ep9Apipe), // output wire, output wire [31:0],
	.ep9Bwr(ep9Bwr), .ep9Bpipe(ep9Bpipe), // output wire, output wire [31:0],
	.ep9Cwr(ep9Cwr), .ep9Cpipe(ep9Cpipe), // output wire, output wire [31:0],
	.ep9Dwr(ep9Dwr), .ep9Dpipe(ep9Dpipe), // output wire, output wire [31:0],
	.ep9Ewr(ep9Ewr), .ep9Epipe(ep9Epipe), // output wire, output wire [31:0],
	.ep9Fwr(ep9Fwr), .ep9Fpipe(ep9Fpipe), // output wire, output wire [31:0],
	// Pipe Out 	0xA0 - 0xBF
	.epA0rd(epA0rd), .epA0pipe(epA0pipe), // output wire, input wire [31:0],
	.epA1rd(epA1rd), .epA1pipe(epA1pipe), // output wire, input wire [31:0],
	.epA2rd(epA2rd), .epA2pipe(epA2pipe), // output wire, input wire [31:0],
	.epA3rd(epA3rd), .epA3pipe(epA3pipe), // output wire, input wire [31:0],
	.epA4rd(epA4rd), .epA4pipe(epA4pipe), // output wire, input wire [31:0],
	.epA5rd(epA5rd), .epA5pipe(epA5pipe), // output wire, input wire [31:0],
	.epA6rd(epA6rd), .epA6pipe(epA6pipe), // output wire, input wire [31:0],
	.epA7rd(epA7rd), .epA7pipe(epA7pipe), // output wire, input wire [31:0],
	.epA8rd(epA8rd), .epA8pipe(epA8pipe), // output wire, input wire [31:0],
	.epA9rd(epA9rd), .epA9pipe(epA9pipe), // output wire, input wire [31:0],
	.epAArd(epAArd), .epAApipe(epAApipe), // output wire, input wire [31:0],
	.epABrd(epABrd), .epABpipe(epABpipe), // output wire, input wire [31:0],
	.epACrd(epACrd), .epACpipe(epACpipe), // output wire, input wire [31:0],
	.epADrd(epADrd), .epADpipe(epADpipe), // output wire, input wire [31:0],
	.epAErd(epAErd), .epAEpipe(epAEpipe), // output wire, input wire [31:0],
	.epAFrd(epAFrd), .epAFpipe(epAFpipe), // output wire, input wire [31:0],
	.epB0rd(epB0rd), .epB0pipe(epB0pipe), // output wire, input wire [31:0],
	.epB1rd(epB1rd), .epB1pipe(epB1pipe), // output wire, input wire [31:0],
	.epB2rd(epB2rd), .epB2pipe(epB2pipe), // output wire, input wire [31:0],
	.epB3rd(epB3rd), .epB3pipe(epB3pipe), // output wire, input wire [31:0],
	.epB4rd(epB4rd), .epB4pipe(epB4pipe), // output wire, input wire [31:0],
	.epB5rd(epB5rd), .epB5pipe(epB5pipe), // output wire, input wire [31:0],
	.epB6rd(epB6rd), .epB6pipe(epB6pipe), // output wire, input wire [31:0],
	.epB7rd(epB7rd), .epB7pipe(epB7pipe), // output wire, input wire [31:0],
	.epB8rd(epB8rd), .epB8pipe(epB8pipe), // output wire, input wire [31:0],
	.epB9rd(epB9rd), .epB9pipe(epB9pipe), // output wire, input wire [31:0],
	.epBArd(epBArd), .epBApipe(epBApipe), // output wire, input wire [31:0],
	.epBBrd(epBBrd), .epBBpipe(epBBpipe), // output wire, input wire [31:0],
	.epBCrd(epBCrd), .epBCpipe(epBCpipe), // output wire, input wire [31:0],
	.epBDrd(epBDrd), .epBDpipe(epBDpipe), // output wire, input wire [31:0],
	.epBErd(epBErd), .epBEpipe(epBEpipe), // output wire, input wire [31:0],
	.epBFrd(epBFrd), .epBFpipe(epBFpipe), // output wire, input wire [31:0],
	// 
	.okClk(okClk)//output wire okClk // sync with write/read of pipe
	);
////


/* BANK signals */
// DAC_A2A3
OBUF obuf_DAC_A2A3_MOSI_inst (.O(o_B13_L1P), .I(DAC_A2A3_MOSI) ); // DAC_A2A3_MOSI // CC8
OBUF obuf_DAC_A2A3_SCLK_inst (.O(o_B13_L4N), .I(DAC_A2A3_SCLK) ); // DAC_A2A3_SCLK // CC9
OBUF obuf_DAC_A2A3_SYNB_inst (.O(o_B13_L4P), .I(DAC_A2A3_SYNB) ); // DAC_A2A3_SYNB // CC10
IBUF ibuf_DAC_A2A3_MISO_inst (.I(i_B13_L2P), .O(DAC_A2A3_MISO) ); // DAC_A2A3_MISO // CC12
// DAC_BIAS 
IBUF ibuf_DAC_BIAS_MISO_inst (.I(i_B34_L4N), .O(DAC_BIAS_MISO  ) ); // BIAS_SDO
OBUF obuf_DAC_BIAS_MOSI_inst (.O(o_B34_L4P), .I(DAC_BIAS_MOSI  ) ); // BIAS_SDI
OBUF obuf_DAC_BIAS_SYNB_inst (.O(o_B34_L2P), .I(DAC_BIAS_SYNB) ); // BIAS_SYNCn
OBUF obuf_DAC_BIAS_SCLK_inst (.O(o_B34_L2N), .I(DAC_BIAS_SCLK ) ); // BIAS_SCLK
//
OBUF obuf_DAC_BIAS_CLRB_inst (.O(o_B34_L10N ), .I(DAC_BIAS_CLRB  ) ); // BIAS_CLR_B
OBUF obuf_DAC_BIAS_LD_B_inst (.O(o_B34_L10P ), .I(DAC_BIAS_LD_B ) ); // BIAS_LOAD_B
// DWAVE // TODO: DWAVE test 
//
// dwave warm-up test 
(* keep = "true" *) wire w_disable__dwave_warmup = w_TEST_CON[5]; // default for warm-up
// output 1 for dwave disable 
// output 0 for dwave warm-up by DC current
wire w_dwave_idle_output = (w_disable__dwave_warmup)? 1'b1 : 1'b0;
//
OBUF obuf_dwave_w1f1_inst (.O(o_B34_L20P), .I((dwave_en)? ~dwave_w1f1 : w_dwave_idle_output) ); // DWAVE_F01 // inverted
OBUF obuf_dwave_w1f2_inst (.O(o_B34_L20N), .I((dwave_en)? ~dwave_w1f2 : w_dwave_idle_output) ); // DWAVE_F02 // inverted
OBUF obuf_dwave_w1f3_inst (.O(o_B34_L3P ), .I((dwave_en)? ~dwave_w1f3 : w_dwave_idle_output) ); // DWAVE_F03 // inverted
//OBUF obuf_dwave_w1f1_inst (.O(o_B34_L20P), .I((dwave_en)? ~dwave_w1f1 : 1'b1) ); // DWAVE_F01 // inverted
//OBUF obuf_dwave_w1f2_inst (.O(o_B34_L20N), .I((dwave_en)? ~dwave_w1f2 : 1'b1) ); // DWAVE_F02 // inverted
//OBUF obuf_dwave_w1f3_inst (.O(o_B34_L3P ), .I((dwave_en)? ~dwave_w1f3 : 1'b1) ); // DWAVE_F03 // inverted
////$$OBUF obuf_dwave_w2f1_inst (.O(o_B34_L3N ), .I((dwave_en)? ~dwave_w2f1 : 1'b1) ); // DWAVE_F11 // inverted
////$$OBUF obuf_dwave_w2f2_inst (.O(o_B34_L9P ), .I((dwave_en)? ~dwave_w2f2 : 1'b1) ); // DWAVE_F12 // inverted
////$$OBUF obuf_dwave_w2f3_inst (.O(o_B34_L9N ), .I((dwave_en)? ~dwave_w2f3 : 1'b1) ); // DWAVE_F13 // inverted
//
// swap test 
//(* keep = "true" *) wire w_disable__dwave2_pin_swap = w_TEST_CON[6]; // default for swap
(* keep = "true" *) wire w_disable__dwave2_pin_swap = w_TEST_CON[6]|~w_TEST_CON[4]; // default for non-swap
//  w_TEST_CON[6] w_TEST_CON[4] = 0 0 : disable__dwave2_pin_swap
//  w_TEST_CON[6] w_TEST_CON[4] = 0 1 : enable__dwave2_pin_swap
//  w_TEST_CON[6] w_TEST_CON[4] = 1 0 : disable__dwave2_pin_swap
//  w_TEST_CON[6] w_TEST_CON[4] = 1 1 : disable__dwave2_pin_swap
//  
//
wire w_dwave_w2f1 = (w_disable__dwave2_pin_swap)? dwave_w2f1 : dwave_w2f3;
wire w_dwave_w2f2 = (w_disable__dwave2_pin_swap)? dwave_w2f2 : dwave_w2f2;
wire w_dwave_w2f3 = (w_disable__dwave2_pin_swap)? dwave_w2f3 : dwave_w2f1;
//
OBUF obuf_dwave_w2f1_inst (.O(o_B34_L3N ), .I((dwave_en)? ~w_dwave_w2f1 : w_dwave_idle_output) ); // DWAVE_F11 // inverted
OBUF obuf_dwave_w2f2_inst (.O(o_B34_L9P ), .I((dwave_en)? ~w_dwave_w2f2 : w_dwave_idle_output) ); // DWAVE_F12 // inverted
OBUF obuf_dwave_w2f3_inst (.O(o_B34_L9N ), .I((dwave_en)? ~w_dwave_w2f3 : w_dwave_idle_output) ); // DWAVE_F13 // inverted
//OBUF obuf_dwave_w2f1_inst (.O(o_B34_L3N ), .I((dwave_en)? ~w_dwave_w2f1 : 1'b1) ); // DWAVE_F11 // inverted
//OBUF obuf_dwave_w2f2_inst (.O(o_B34_L9P ), .I((dwave_en)? ~w_dwave_w2f2 : 1'b1) ); // DWAVE_F12 // inverted
//OBUF obuf_dwave_w2f3_inst (.O(o_B34_L9N ), .I((dwave_en)? ~w_dwave_w2f3 : 1'b1) ); // DWAVE_F13 // inverted
//
OBUF obuf_SPCD0_inst (.O(o_B13_L5P), .I(CTL_SPO_CDAT0) ); // CC0 // SPO_CD0
OBUF obuf_SPEN0_inst (.O(o_B13_L5N), .I(CTL_SPO_EN0  ) ); // CC1 // SPO_EN0
OBUF obuf_SPCD1_inst (.O(o_B13_L3P), .I(CTL_SPO_CDAT1) ); // CC2 // SPO_CD1
OBUF obuf_SPEN1_inst (.O(o_B13_L3N), .I(CTL_SPO_EN1  ) ); // CC3 // SPO_EN1
//
OBUF obuf_SPCD2_inst (.O(o_B13_L16P), .I(CTL_SPO_CDAT2) ); // CC4 // SPO_CD2
OBUF obuf_SPEN2_inst (.O(o_B13_L16N), .I(CTL_SPO_EN2  ) ); // CC5 // SPO_EN2
OBUF obuf_SPCD3_inst (.O(o_B34_L5P ), .I(CTL_SPO_CDAT3) ); // CC6 // SPO_CD3
OBUF obuf_SPEN3_inst (.O(o_B34_L5N ), .I(CTL_SPO_EN3  ) ); // CC7 // SPO_EN3
// 
assign RB3 = DAC_A2A3_MISO;                       // CC12 // RBCK3 or DAC_A2A3_MISO
IBUF ibuf_RB2_inst (.I(i_B35_L6N  ), .O(RB2  ) ); // CC13 // RBCK2
IBUF ibuf_RB1_inst (.I(i_B35_L19P ), .O(RB1  ) ); // CC14 // RBCK1
IBUF ibuf_UNB_inst (.I(i_B35_L19N ), .O(UNBAL) ); // CC15 // UNBAL
//
IBUF ibuf_D_R_inst (.I(i_B13_L6P      ), .O(D_R  ) ); // CC16 // D_R
IBUF ibuf_C_R_inst (.I(i_B13_L6N      ), .O(C_R  ) ); // CC17 // C_R
IBUF ibuf_B_R_inst (.I(i_B13_L11N_SRCC), .O(B_R  ) ); // CC18 // B_R 
IBUF ibuf_A_R_inst (.I(i_B13_L11P_SRCC), .O(A_R  ) ); // CC19 // A_R
//
IBUF ibuf_D_D_inst (.I(i_B34_L6P), .O(D_D  ) ); // CC20 // D_D
IBUF ibuf_C_D_inst (.I(i_B34_L6N), .O(C_D  ) ); // CC21 // C_D
IBUF ibuf_B_D_inst (.I(i_B34_L8P), .O(B_D  ) ); // CC22 // B_D
IBUF ibuf_A_D_inst (.I(i_B34_L8N), .O(A_D  ) ); // CC23 // A_D
//


/* ADC */
// nets for BUFG clocks
wire c_dco_adc_0;
wire c_dco_adc_1;
wire c_dco_adc_2;
wire c_dco_adc_3;
// adc0
IBUFDS ibufds_ADC_00_DCO_inst (.I(c_B34D_L11P_SRCC), .IB(c_B34D_L11N_SRCC), .O(c_dco_adc_0) );
BUFG     bufg_ADC_00_DCO_inst (.I(c_dco_adc_0), .O(w_dco_adc_0) ); 
IBUFDS ibufds_ADC_00_DA_inst (.I(i_B34D_L18P), .IB(i_B34D_L18N), .O(w_dat2_adc_0) );
IBUFDS ibufds_ADC_00_DB_inst (.I(i_B34D_L22P), .IB(i_B34D_L22N), .O(w_dat1_adc_0) );
// adc1
IBUFDS ibufds_ADC_01_DCO_inst (.I(c_B34D_L14P_SRCC), .IB(c_B34D_L14N_SRCC), .O(c_dco_adc_1) ); // check polarity
BUFG     bufg_ADC_01_DCO_inst (.I(c_dco_adc_1), .O(w_dco_adc_1) ); 
IBUFDS ibufds_ADC_01_DA_inst (.I(i_B34D_L16P), .IB(i_B34D_L16N), .O(w_dat2_adc_1) );
IBUFDS ibufds_ADC_01_DB_inst (.I(i_B34D_L17P), .IB(i_B34D_L17N), .O(w_dat1_adc_1) );
// adc2 ... not used
IBUFDS ibufds_ADC_10_DCO_inst (.I(c_B34D_L12P_MRCC), .IB(c_B34D_L12N_MRCC), .O(c_dco_adc_2) );
BUFG     bufg_ADC_10_DCO_inst (.I(c_dco_adc_2), .O(w_dco_adc_2) ); 
IBUFDS ibufds_ADC_10_DA_inst (.I(i_B34D_L7P), .IB(i_B34D_L7N), .O(w_dat2_adc_2) );
IBUFDS ibufds_ADC_10_DB_inst (.I(i_B34D_L1P), .IB(i_B34D_L1N), .O(w_dat1_adc_2) );
// adc3 ... not used
IBUFDS ibufds_ADC_11_DCO_inst (.I(c_B34D_L13P_MRCC), .IB(c_B34D_L13N_MRCC), .O(c_dco_adc_3) ); 
BUFG     bufg_ADC_11_DCO_inst (.I(c_dco_adc_3), .O(w_dco_adc_3) ); 
IBUFDS ibufds_ADC_11_DA_inst (.I(i_B34D_L15P), .IB(i_B34D_L15N), .O(w_dat2_adc_3) );
IBUFDS ibufds_ADC_11_DB_inst (.I(i_B34D_L23P), .IB(i_B34D_L23N), .O(w_dat1_adc_3) );
//
wire w_ADC_XX_CLK = w_clk_adc;
OBUFDS obufds_ADC_XX_CLK_inst 		(.O(o_B34D_L19P), .OB(o_B34D_L19N), .I(w_ADC_XX_CLK)		); // ADC_XX_CLK
//
wire w_ADC_XX_CNV;
	assign w_ADC_XX_CNV = (w_enable__pulse_loopback)? w_pulse_out : w_cnv_adc;
OBUFDS obufds_ADC_XX_CNV_inst 		(.O(o_B34D_L21P), .OB(o_B34D_L21N), .I(w_ADC_XX_CNV)		); // ADC_XX_CNV
//
OBUF obuf_ADC_XX_TEST_B_inst		(.O(o_B34_L24P), .I(~w_pin_test_adc)); // ADC_XX_TEST_B
OBUF obuf_ADC_XX_DUAL_LANE_B_inst 	(.O(o_B34_L24N), .I(~w_pin_dln_adc)	); // ADC_XX_DUAL_LANE_B
// loopback test
wire w_OSC_IN;
	assign w_pulse_loopback_in = w_OSC_IN;
IBUFDS ibufds_OSC_IN_inst (.I(i_B35D_L12P_MRCC), .IB(i_B35D_L12N_MRCC), .O(w_OSC_IN) ); 
////

/* LAN */
//wire w_LAN_PWDN; // unused 
// input  wire			i_B35_L21P,        //$$ LAN_MISO
// output wire			o_B35_L21N,        //$$ LAN_RSTn
// input  wire			i_B13_SYS_CLK_MC2, //$$ LAN_PWDN // unused in WIZ850io // used in WIZ820io
//   ... LAN_PWDN --> input for no use.
// output wire			o_B35_IO0,         //$$ LAN_SCSn
// output wire			o_B35_IO25,        //$$ LAN_SCLK
// output wire			o_B35_L24P,        //$$ LAN_MOSI
// output wire			o_B35_L24N,        //$$ LAN_INTn
//   ... LAN_INTn --> ouput with 1 for idle
OBUF obuf_o_B35_L21N_inst  (.O(o_B35_L21N ), .I(w_LAN_RSTn) ); // w_LAN_RSTn
OBUF obuf_o_B35_IO0_inst   (.O(o_B35_IO0  ), .I(w_LAN_SCSn) ); // w_LAN_SCSn
OBUF obuf_o_B35_IO25_inst  (.O(o_B35_IO25 ), .I(w_LAN_SCLK) ); // w_LAN_SCLK
OBUF obuf_o_B35_L24P_inst  (.O(o_B35_L24P ), .I(w_LAN_MOSI) ); // w_LAN_MOSI
OBUF obuf_o_B35_L24N_inst  (.O(o_B35_L24N ), .I(w_LAN_INTn) ); // w_LAN_INTn
IBUF ibuf_i_B35_L21P_inst  (.I(i_B35_L21P ), .O(w_LAN_MISO) ); // w_LAN_MISO

/* TEMP SENSOR */
// support MAX6576ZUT+T
// temp signal count by 12MHz
// net in sch: FPGA_IO_B
// pin: i_B35_L6P
// check signal on debugger 
// test
(* keep = "true" *) wire w_temp_sig;
(* keep = "true" *) reg r_temp_sig;
(* keep = "true" *) reg r_toggle_temp_sig;
(* keep = "true" *) wire w_rise_temp_sig = ~r_temp_sig & w_temp_sig;
//reg [15:0] r_subcnt_temp_sig_period;
//reg [15:0] r_period_temp_sig_period;
//
IBUF ibuf_i_B35_L6P_inst  (.I(i_B35_L6P ), .O(w_temp_sig) ); // w_temp_sig
//
wire tmps_clk = clk3_out3_12M;
// 
//
always @(posedge tmps_clk, negedge reset_n) begin
	if (!reset_n) begin
		r_temp_sig     <= 1'b0;
		r_toggle_temp_sig  <= 1'b0;
		end
	else begin
		//
		r_temp_sig     <= w_temp_sig;
		//
		if (w_rise_temp_sig) begin 
			r_toggle_temp_sig <= ~r_toggle_temp_sig;
			end
		end
end



// reserved or tempory output 
//OBUF obuf_TMP_____________inst (.O(o_Bxx_LxxP         ), .I(1'b0 ) ); // TMP


/* ddr3 io */
// ddr3 test assignment 
//IBUFDS IBUFDS_ddr3_dqs0_inst (.I(ddr3_dqs_p[0]), .IB(ddr3_dqs_n[0]), .O() ); // unused
//IBUFDS IBUFDS_ddr3_dqs1_inst (.I(ddr3_dqs_p[1]), .IB(ddr3_dqs_n[1]), .O() ); // unused
//IBUFDS IBUFDS_ddr3_dqs2_inst (.I(ddr3_dqs_p[2]), .IB(ddr3_dqs_n[2]), .O() ); // unused
//IBUFDS IBUFDS_ddr3_dqs3_inst (.I(ddr3_dqs_p[3]), .IB(ddr3_dqs_n[3]), .O() ); // unused
//OBUFDS OBUFDS_ddr3_ck___inst (.O(ddr3_ck_p[0] ), .OB(ddr3_ck_n[0] ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_00_inst (.O(ddr3_addr[ 0]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_01_inst (.O(ddr3_addr[ 1]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_02_inst (.O(ddr3_addr[ 2]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_03_inst (.O(ddr3_addr[ 3]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_04_inst (.O(ddr3_addr[ 4]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_05_inst (.O(ddr3_addr[ 5]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_06_inst (.O(ddr3_addr[ 6]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_07_inst (.O(ddr3_addr[ 7]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_08_inst (.O(ddr3_addr[ 8]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_09_inst (.O(ddr3_addr[ 9]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_10_inst (.O(ddr3_addr[10]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_11_inst (.O(ddr3_addr[11]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_12_inst (.O(ddr3_addr[12]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_13_inst (.O(ddr3_addr[13]), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_addr_14_inst (.O(ddr3_addr[14]), .I(1'b0) ); // unused
//
//OBUF OBUF_ddr3_ba_0____inst (.O(ddr3_ba[0]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_ba_1____inst (.O(ddr3_ba[1]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_ba_2____inst (.O(ddr3_ba[2]   ), .I(1'b0) ); // unused
//
//OBUF OBUF_ddr3_cke_0___inst (.O(ddr3_cke[0]  ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_cas_n___inst (.O(ddr3_cas_n   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_ras_n___inst (.O(ddr3_ras_n   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_we_n____inst (.O(ddr3_we_n    ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_odt_0___inst (.O(ddr3_odt[0]  ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_dm_0____inst (.O(ddr3_dm[0]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_dm_1____inst (.O(ddr3_dm[1]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_dm_2____inst (.O(ddr3_dm[2]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_dm_3____inst (.O(ddr3_dm[3]   ), .I(1'b0) ); // unused
//OBUF OBUF_ddr3_reset_n_inst (.O(ddr3_reset_n ), .I(1'b0) ); // unused
////

endmodule
