== CMU-CPU-F5500 ==

=== info ===
* fpga image id: __EE_19_0718
* purpose: pre-test for CMU-CPU-F5500-REVA PCB_19-05-13 // temp sensor + dwave pin NON-swap + dwave warmup control + ADC period 22 bit + dwave out timing
* git branch: cmu_cpu_work__EE__python_test
* git tag: cmu_cpu__EE_19_0718 (to-be) / cmu_cpu__C8_19_MMDD (to-be)

=== new features ===
* dwave pin swap support (from branch: cmu_cpu_work__C8__deave_pin_swap_test)
	for thermal match of PCB artwork (dwave_out2)
* dwave R net warmup by DC current
* temp sensor support: MAX6576ZUT+T
	end-points
		TEST_CON	wi01	0x01	={..., temp_con[3:0] , 5'b0, autocount2, disable1, reset1}	// test count control / temp sensor monitor control
		MON_TEMP	wo3A	0x3A	={temp_mC_dec[31:0]}	 // XADC_temp / external temp sensor
	model
		test_model_temp_sensor_MAX6576.v 
		temp_sensor_readout.v
		tb_temp_sensor_readout.v
* python test code for adc mismatch test batch and display (from other branch)

=== main features ===
* USB interface
* 210MHz ADC base freq to support sampling rate 15Msps
* 160MHz DWAVE base freq to support test waveform 20MHz
* LAN interface to support SCPI commands

=== design top files ===

* RTL verilog top file 
	\xem7310_pll__CMU-CPU__work\xem7310_pll.xpr
* Xilinx vivado project file 	
	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\sources_1\imports\src_ref\xem7310__cmu_cpu__top.v

* XSDK project top location 
	\xem7310_pll__CMU-CPU__work\xem7310_pll.sdk
* XSDK CMU-CPU-TEST-F5500 configuration
	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\_sdk\src\CMU_CPU_TOP\cmu_cpu_config.h
* SCPI server design top file 
	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\_sdk\src\test_spi_wz850\test_spi_wz850.c
* FPGA image + application mem file 
	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\_bit\download__mcs_ep_SBT20190322_C8190320.bit

* Python test batch file 
	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\ok_cmu_cpu__batch.py
* Python ADC test top file 
	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\LAN_cmu_cpu__10_self_test.py
* Python LAN test top file 
	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\tcp_echo\scpi-client3.py
* Python library for LAN interface 
	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\LAN_cmu_cpu__lib.py
* Python CMU-CPU-TEST-F5500 configuration
	\xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\ok_cmu_cpu__lib_conf.py

* Python test batch file for adc mismatch 
	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\ok_cmu_cpu__batch_a__adc_mismatch.py
* Python ADC test top file for adc mismatch
	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\ok_cmu_cpu__10_self_test_a__adc_mismatch.py
* Python post-process file for adc mismatch
	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\postproc_csv_files__cmu_cpu__rev.py
* Python report file for adc mismatch
	xem7310_pll__CMU-CPU__work\xem7310_pll.srcs\__test_python\read_postproc_files__cmu_cpu.py
	
	

	
=== FPGA image (bit) files ===
...